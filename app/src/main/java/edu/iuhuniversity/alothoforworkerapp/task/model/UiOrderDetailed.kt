package edu.iuhuniversity.alothoforworkerapp.task.model

import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderDetail
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import java.util.*

data class UiOrderDetailed(
    val id: String,
    val service: String,
    val worker: String,
    val workingDate: Date?,
    val total: Double,
    val images: List<String>? = null,
    val orderDetails: List<OrderDetail>,
    val status: OrderStatus,
)
