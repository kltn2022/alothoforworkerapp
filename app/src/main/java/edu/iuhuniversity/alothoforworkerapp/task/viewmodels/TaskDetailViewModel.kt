package edu.iuhuniversity.alothoforworkerapp.task.viewmodels

import android.app.Application
import android.os.Build
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.GetOrderByIdUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.ListenRealtimeOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.model.mappers.UiOrderMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*

class TaskDetailViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val updateOrderUseCase: UpdateOrderUseCase,
    @Assisted val getOrderByIdUseCase: GetOrderByIdUseCase,
    @Assisted val listenRealtimeOrderUseCase: ListenRealtimeOrderUseCase,
    @Assisted val uiOrderMapper: UiOrderMapper,
    @Assisted val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<TaskDetailViewModel.State>(State.Loading) {

    private lateinit var mutableOrder: MutableState<Order>

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            updateOrderUseCase: UpdateOrderUseCase,
            getOrderByIdUseCase: GetOrderByIdUseCase,
            listenRealtimeOrderUseCase: ListenRealtimeOrderUseCase,
            uiOrderMapper: UiOrderMapper,
            dispatchersProvider: DispatchersProvider
        ): TaskDetailViewModel
    }

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        data class UpdatedOrder(val updatedOrder: UiOrder) : State()
    }

    fun handleEvent(event: OrderDetailEvent) {
        when (event) {
            is OrderDetailEvent.InjectOrder -> {
                trackOrder(event.order.id!!)
            }
            is OrderDetailEvent.UpdateNextStatus -> {
                updateOrder(event.orderId, event.currentStatus.nextStatus()!!)
            }
        }
    }

    private fun trackOrder(orderId: String) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    listenRealtimeOrderUseCase.execute(orderId)
                        .flowOn(Dispatchers.IO)
                        .distinctUntilChanged()
                        .flowOn(Dispatchers.Main)
                        .collect { result ->
                            Timber.tag("newOrderContent").d(result.toString())
                            if (!this@TaskDetailViewModel::mutableOrder.isInitialized) {
                                mutableOrder = mutableStateOf(result!!)
                            } else {
                                mutableOrder.value = result!!
                            }
                            uiOrderMapper.mapToView(result) {
                                mutableState.value = State.UpdatedOrder(it)
                            }
                        }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun updateOrder(orderId: String, orderStatus: OrderStatus) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }
        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        var orderToUpdate = mutableOrder.value.copy(status = orderStatus)
                        if (orderStatus == OrderStatus.COMPLETED) {
                            orderToUpdate = orderToUpdate.copy(finishedDate = Date())
                        }

                        when (val updateResult = updateOrderUseCase.execute(orderToUpdate)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(updateResult.message.orEmpty())
                            is Resource.Success -> {
                                uiOrderMapper.mapToView(orderToUpdate) {
                                    mutableState.value = State.UpdatedOrder(it)
                                }

                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }
}

sealed class OrderDetailEvent {
    object Loading : OrderDetailEvent()
    data class InjectOrder(val order: UiOrder) : OrderDetailEvent()
    data class UpdateNextStatus(val orderId: String, val currentStatus: OrderStatus) :
        OrderDetailEvent()
}