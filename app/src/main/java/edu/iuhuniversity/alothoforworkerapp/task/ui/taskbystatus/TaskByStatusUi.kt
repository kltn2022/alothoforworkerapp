package edu.iuhuniversity.alothoforworkerapp.task.ui.taskbystatus

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.ChipDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FilterChip
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Done
import androidx.compose.material.icons.rounded.FormatListBulleted
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLightVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLighterVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.ui.OrderList
import edu.iuhuniversity.alothoforworkerapp.task.ui.TaskHeader
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskViewModel

object TaskByStatusUi : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Rounded.FormatListBulleted)
            return remember {
                TabOptions(
                    index = 1u,
                    title = "Theo status",
                    icon = icon
                )
            }
        }

    @OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
    @Composable
    override fun Content() {
        Column(
            verticalArrangement = Arrangement.spacedBy(12.dp),
            horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .fillMaxSize()
                .padding(15.dp, 15.dp, 15.dp, 100.dp)
        ) {

            val selectedStatus = remember { mutableStateOf(OrderStatus.WAITING_FOR_WORK) }
            val orderListScrollState = rememberLazyListState()
            var visible = remember { mutableStateOf(true) }
            val viewModel = (LocalContext.current.getActivity() as MainActivity).taskViewModel

            val state by viewModel.state.collectAsState()
            visible.value = orderListScrollState.firstVisibleItemScrollOffset <= 400

            val (isLoading, setLoading) = remember { mutableStateOf(false) }
            val (error, setError) = remember { mutableStateOf("") }

            when (val result = state) {
                is TaskViewModel.State.Loading -> {
                    setLoading(true)
                }
                is TaskViewModel.State.Error -> {
                    setLoading(false)
                    setError(result.error)
                }
                is TaskViewModel.State.RequestedOrders -> {
                    setLoading(false)
                }
                is TaskViewModel.State.UpdateOrder -> {
                    setLoading(false)
                }
            }

            if (error.isNotEmpty()) {
                DialogMessage(
                    colorType = BootstrapType.SMOOTH,
                    title = "Thông báo lỗi",
                    text = error,
                    onDismissRequest = {
                        setError("")
                    },
                    positiveButton = ActionButton(
                        label = "Đã hiểu",
                        onClickListener = {
                            setError("")
                        }
                    )
                )
            }

            TaskHeader(visible = visible.value) {
                Column() {
                    Text(
                        text = "Chọn trạng thái đơn hàng",
                        style = AloThoTypography.h6.copy(
                            fontSize = 16.sp,
                            fontWeight = FontWeight.SemiBold
                        )
                    )
                    LazyVerticalGrid(
                        columns = GridCells.Fixed(2),
                        contentPadding = PaddingValues(horizontal = 10.dp, vertical = 5.dp),
                    ) {
                        items(OrderStatus.values().toList()) { orderStatus ->
                            FilterChip(
                                selected = selectedStatus.value == orderStatus,
                                onClick = { selectedStatus.value = orderStatus },
                                colors = ChipDefaults.filterChipColors(
                                    backgroundColor = PrimaryLighterVariantColor,
                                    contentColor = PrimaryColor,
                                    selectedBackgroundColor = PrimaryLightVariantColor,
                                ),
                                selectedIcon = {
                                    Icon(
                                        Icons.Rounded.Done,
                                        contentDescription = null,
                                        tint = PrimaryColor
                                    )
                                },
                                modifier = Modifier.padding(5.dp),
                                border = BorderStroke(1.dp, PrimaryColor)
                            ) {
                                Text(
                                    text = orderStatus.title,
                                    style = AloThoTypography.subtitle2.copy(fontWeight = FontWeight.SemiBold),
                                    color = PrimaryColor
                                )
                            }
                        }
                    }
                    Divider()
                }
            }

            OrderList(
                isLoading = isLoading,
                state = orderListScrollState,
                orders = filterOrdersByStatus(
                    viewModel.getOrders().toMutableList(),
                    selectedStatus.value
                ),
                navigator = LocalNavigator.currentOrThrow
            )
        }
    }

    private fun filterOrdersByStatus(
        orders: MutableList<UiOrder>,
        status: OrderStatus
    ): List<UiOrder> {
        return orders.filter { order -> order.status == status }
    }

}