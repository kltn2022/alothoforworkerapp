package edu.iuhuniversity.alothoforworkerapp.task.viewmodels

import android.app.Application
import android.os.Build
import androidx.compose.runtime.mutableStateOf
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.ListenRealtimeOrdersUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.model.mappers.UiOrderMapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class TaskViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val listenRealtimeOrdersUseCase: ListenRealtimeOrdersUseCase,
    @Assisted val updateOrderUseCase: UpdateOrderUseCase,
    @Assisted val uiOrderMapper: UiOrderMapper,
    @Assisted val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<TaskViewModel.State>(State.Loading) {

    private val orders = mutableStateOf(listOf<UiOrder>())

    fun getOrders() = orders.value

    fun setOrders(newOrders: List<UiOrder>) {
        orders.value = newOrders
        Timber.tag("Loaded-Order").d(orders.value.toString())
    }

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            listenRealtimeOrdersUseCase: ListenRealtimeOrdersUseCase,
            updateOrderUseCase: UpdateOrderUseCase,
            uiOrderMapper: UiOrderMapper,
            dispatchersProvider: DispatchersProvider
        ): TaskViewModel
    }

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        data class RequestedOrders(val orders: List<UiOrder>) : State()
        data class UpdateOrder(val order: Order) : State()
    }

    fun handleEvent(event: GetOrderEvent) {
        when (event) {
            is GetOrderEvent.LoadOrders -> loadOrders()
            is GetOrderEvent.UpdateOrder -> updateOrder(event.order)
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun updateOrder(order: Order) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }
        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = updateOrderUseCase.execute(order)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> mutableState.value =
                                State.UpdateOrder(order)
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun loadOrders() {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    listenRealtimeOrdersUseCase.execute()
                        .flowOn(Dispatchers.IO)
                        .distinctUntilChanged()
                        .flowOn(Dispatchers.Main)
                        .collect { result ->
                            Timber.tag("orderResult").d(result.toString())
                            uiOrderMapper.mapToView(result) {
                                mutableState.value = State.RequestedOrders(it)
                            }
                        }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }

}

sealed class GetOrderEvent {
    object Loading : GetOrderEvent()
    object LoadOrders : GetOrderEvent()
    data class UpdateOrder(val order: Order) : GetOrderEvent()
}
