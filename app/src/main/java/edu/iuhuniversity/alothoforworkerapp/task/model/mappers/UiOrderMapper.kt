package edu.iuhuniversity.alothoforworkerapp.task.model.mappers

import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker.Companion.toWorker
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service.Companion.toService
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.ServiceType.Companion.toServiceType
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.toResponse
import edu.iuhuniversity.alothoforworkerapp.core.presentation.model.mappers.UiMapper
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

class UiOrderMapper @Inject constructor() : UiMapper<Order, UiOrder> {
    override suspend fun mapToView(input: Order, output: (UiOrder) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val getServicesAsync = async {
                input.orderDetails.map {
                    CommonUtils.getDocumentReference(
                        CollectionConst.COLLECTION_SERVICE,
                        it.serviceId
                    )
                        .get()
                        .toResponse { docSnap -> docSnap.toService() }
                        .data!!
                }
            }

            val serviceList = getServicesAsync.await()

            val getServiceTypeAsync = async {
                serviceList.first().serviceType.get().toResponse { it.toServiceType() }
            }
            val serviceType = getServiceTypeAsync.await().data

            val workerAsync = async {
                input.worker?.get()?.toResponse { it.toWorker() }
            }
            val worker = workerAsync.await()?.data

            val uiOrder = UiOrder(
                id = input.id,
                status = input.status!!,
                total = input.total ?: input.estimatedFee ?: 0.0,
                worker = worker,
                services = serviceList,
                serviceType = serviceType?.name ?: "Dịch vụ mở khoá",
                estimatedTotal = input.estimatedFee ?: 0.0,
                workingDate = input.workingDate,
                orderDetails = input.orderDetails,
                images = input.images
            )

            output(uiOrder)
        }
    }

    override suspend fun mapToView(input: Order): UiOrder {
        TODO("Not yet implemented")
    }

    override suspend fun mapToView(input: List<Order?>, output: (List<UiOrder>) -> Unit) {
        CoroutineScope(Dispatchers.IO).launch {
            val outputList = mutableListOf<UiOrder>()

            input.mapTo(outputList) { order ->
                val getServicesAsync = async {
                    order?.orderDetails?.map {
                        Timber.tag("orderDetail").d(it.toString())
                        CommonUtils.getDocumentReference(
                            CollectionConst.COLLECTION_SERVICE,
                            it.serviceId
                        )
                            .get()
                            .toResponse { docSnap -> docSnap.toService() }
                            .data!!
                    }
                }

                val serviceList = getServicesAsync.await()

                val getServiceTypeAsync = async {
                    serviceList?.first()?.serviceType?.get()?.toResponse { it.toServiceType() }
                }
                val serviceType = getServiceTypeAsync.await()?.data

                val workerAsync = async {
                    order?.worker?.get()?.toResponse { it.toWorker() }
                }
                val worker = workerAsync.await()?.data

                UiOrder(
                    id = order?.id,
                    status = order?.status!!,
                    total = order.total ?: order.estimatedFee ?: 0.0,
                    worker = worker,
                    services = serviceList.orEmpty(),
                    serviceType = serviceType?.name ?: "Dịch vụ mở khoá",
                    estimatedTotal = order.estimatedFee ?: 0.0,
                    workingDate = order.workingDate,
                    orderDetails = order.orderDetails,
                    images = order.images
                )
            }

            output(outputList)
        }
    }
}
