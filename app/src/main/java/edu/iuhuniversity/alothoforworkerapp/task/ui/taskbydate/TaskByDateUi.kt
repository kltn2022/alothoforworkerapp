package edu.iuhuniversity.alothoforworkerapp.task.ui.taskbydate

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Event
import androidx.compose.material3.Divider
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import com.himanshoe.kalendar.common.KalendarSelector
import com.himanshoe.kalendar.common.KalendarStyle
import com.himanshoe.kalendar.common.data.KalendarEvent
import com.himanshoe.kalendar.common.theme.Fire100
import com.himanshoe.kalendar.common.theme.Fire15
import com.himanshoe.kalendar.common.theme.Fire50
import com.himanshoe.kalendar.ui.Kalendar
import com.himanshoe.kalendar.ui.KalendarType
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.utils.DateUtils
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLightVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.LocalDateUtils
import edu.iuhuniversity.alothoforworkerapp.task.ui.KalendarTypes
import edu.iuhuniversity.alothoforworkerapp.task.ui.OrderList
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskViewModel
import timber.log.Timber
import java.time.LocalDate

object TaskByCalendarUi : Tab {
    // lấy list --> gen ra KalendarEvent --> hiện lên calendar
    // nhấn vào ngày nào -> hiện task ngày đó.
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Rounded.Event)
            return remember {
                TabOptions(
                    index = 0u,
                    title = "Theo ngày",
                    icon = icon
                )
            }
        }

    @RequiresApi(Build.VERSION_CODES.O)
    @Composable
    override fun Content() {
        val viewModel = (LocalContext.current.getActivity() as MainActivity).taskViewModel
        val state by viewModel.state.collectAsState()

        val orderListScrollState = rememberLazyListState()
        val kalendarType = remember {
            mutableStateOf(KalendarTypes.FIREY)
        }
        var filteredOrders: MutableList<UiOrder> = remember {
            mutableListOf()
        }
        val selectedDate = remember {
            mutableStateOf(LocalDate.now())
        }


        val (error, setError) = remember { mutableStateOf("") }
        val (isLoading, setLoading) = remember { mutableStateOf(false) }

        when (val result = state) {
            is TaskViewModel.State.Loading -> {
                setLoading(true)
            }
            is TaskViewModel.State.Error -> {
                setLoading(false)
                setError(result.error)
            }
            is TaskViewModel.State.RequestedOrders -> {
                setLoading(false)
            }
            is TaskViewModel.State.UpdateOrder -> {
                setLoading(false)
            }
        }

        if (orderListScrollState.isScrollInProgress) {
            DisposableEffect(Unit) {
                onDispose {
                    if (!orderListScrollState.isScrollInProgress && orderListScrollState.firstVisibleItemScrollOffset < 400) {
                        kalendarType.value = KalendarTypes.FIREY
                    } else {
                        kalendarType.value = KalendarTypes.OCEANIC
                    }
                }
            }
        }

        Column(
            verticalArrangement = Arrangement.spacedBy(12.dp),
            horizontalAlignment = Alignment.Start,
            modifier = Modifier
                .fillMaxSize()
                .padding(15.dp)
        ) {
            // khi scroll xuống thì tự động thu nhỏ
            Kalendar(
                kalendarType = if (kalendarType.value == KalendarTypes.FIREY) KalendarType.Firey() else KalendarType.Oceanic(),
                onCurrentDayClick = { day, event ->
                    Timber.d(day.toString())
                    // filteredOrders =
                    selectedDate.value = day
                },
                errorMessage = {
                    // Handle the error if any
                    setError(it)
                },
                kalendarEvents = generateEvents(viewModel.getOrders()),
                kalendarStyle = KalendarStyle(
                    kalendarBackgroundColor = PrimaryLightVariantColor,
                    kalendarSelector = KalendarSelector.Circle(
                        eventTextColor = PrimaryColor,
                        selectedColor = PrimaryColor.copy(alpha = 0.5f),
                        todayColor = PrimaryColor.copy(alpha = 0.15f),
                    )
                )
            )
            Divider()

            OrderList(
                isLoading = isLoading,
                state = orderListScrollState,
                orders = filterTasksByDate(viewModel.getOrders(), selectedDate.value),
                navigator = LocalNavigator.currentOrThrow
            )

        }

        if (error.isNotEmpty()) {
            DialogMessage(
                colorType = BootstrapType.SMOOTH,
                title = "Thông báo lỗi",
                text = error,
                onDismissRequest = {
                    setError("")
                },
                positiveButton = ActionButton(
                    label = "Đã hiểu",
                    onClickListener = {
                        setError("")
                    }
                )
            )
        }

    }

    private fun generateEvents(orders: List<UiOrder>): List<KalendarEvent> {
        return orders
            .map { order ->
                KalendarEvent(
                    date = LocalDateUtils.convertToLocalDateViaMillisecond(order.workingDate!!)!!,
                    eventName = order.id.orEmpty(),
                    eventDescription = ""
                )
            }.toMutableList()
    }

    private fun filterTasksByDate(
        orders: List<UiOrder>,
        selectedDate: LocalDate
    ): MutableList<UiOrder> {
        return orders
            .filter { order ->
                val workingLocalDate =
                    LocalDateUtils.convertToLocalDateViaMillisecond(order.workingDate!!)!!
                workingLocalDate.year == selectedDate.year && workingLocalDate.month == selectedDate.month && workingLocalDate.dayOfMonth == selectedDate
                    .dayOfMonth
            }.toMutableList()
    }
}

