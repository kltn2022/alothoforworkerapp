package edu.iuhuniversity.alothoforworkerapp.task.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskDetailViewModel
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class TaskManagementModule {
    @Binds
    @IntoMap
    @ScreenModelFactoryKey(TaskViewModel.Factory::class)
    abstract fun bindTaskViewModelFactory(taskManagementFactory: TaskViewModel.Factory): ScreenModelFactory

    @Binds
    @IntoMap
    @ScreenModelFactoryKey(TaskDetailViewModel.Factory::class)
    abstract fun bindTaskDetailViewModelFactory(taskDetailManagementFactory: TaskDetailViewModel.Factory): ScreenModelFactory
}