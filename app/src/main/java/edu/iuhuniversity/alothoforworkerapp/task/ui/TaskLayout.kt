package edu.iuhuniversity.alothoforworkerapp.task.ui

import android.app.Application
import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cafe.adriel.voyager.navigator.tab.*
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.EmptyDataSection
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.LoadingDataSection
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import edu.iuhuniversity.alothoforworkerapp.core.utils.toNavRailOptions
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.task.TaskItem
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.navigate
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.ui.taskbydate.TaskByCalendarUi
import edu.iuhuniversity.alothoforworkerapp.task.ui.taskbystatus.TaskByStatusUi
import edu.iuhuniversity.alothoforworkerapp.task.ui.taskdetail.TaskDetailActivity
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.GetOrderEvent
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskViewModel

class TaskManagementScreen : AndroidScreen() {

    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow
        val currentContext = LocalContext.current
        val mainActivity = currentContext.getComponentActivity() as MainActivity

        val updateOrdersUseCase = mainActivity.updateOrderUseCase
        val dispatchersProvider = mainActivity.dispatchersProvider
        val orderMapper = mainActivity.orderMapper
        val listenRealtimeOrdersUseCase = mainActivity.listenRealtimeOrdersUseCase

        mainActivity.taskViewModel = getScreenModel<TaskViewModel, TaskViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                listenRealtimeOrdersUseCase,
                updateOrdersUseCase,
                orderMapper,
                dispatchersProvider
            )
        }

        LifecycleEffect(
            onStarted = {
                mainActivity.taskViewModel.handleEvent(GetOrderEvent.LoadOrders)
            },
            onDisposed = {
            }
        )

        TaskUi(navigator, mainActivity.taskViewModel)
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TaskUi(navigator: Navigator, viewModel: TaskViewModel) {
    val state by viewModel.state.collectAsState()

    viewModel.handleEvent(GetOrderEvent.LoadOrders)

    when (val result = state) {
        is TaskViewModel.State.Loading -> {

        }
        is TaskViewModel.State.RequestedOrders -> {
            viewModel.setOrders(result.orders.toMutableList())
        }
    }

    TabNavigator(TaskByCalendarUi) {
        Scaffold() {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    //.padding(top = 50.dp)
                    .zIndex(5f)
            ) {
                NavigationRail(
                    contentColor = PrimaryColor,
                    header = {
                        Image(
                            painter = painterResource(id = R.drawable.ic_logo_ab),
                            contentDescription = null,
                            modifier = Modifier
                                .size(24.dp)
                                .clip(CircleShape)
                                .border(1.dp, PrimaryColor, CircleShape)
                        )
                    },
                    containerColor = Lian_Color_LightGray,

                    ) {

                    NavRailItem(TaskByCalendarUi)
                    NavRailItem(TaskByStatusUi)
                }
                Column(
                    modifier = Modifier
                        .background(Color.White)
                        .fillMaxSize()
                ) {
                    CurrentTab()
                }
            }
        }
    }
}

enum class KalendarTypes {
    FIREY,
    OCEANIC
}

@Composable
private fun NavRailItem(tab: Tab) {
    val tabNavigator = LocalTabNavigator.current
    val navRailItem = tab.options.toNavRailOptions()

    NavigationRailItem(
        selected = tabNavigator.current == tab,
        onClick = {
            tabNavigator.current = tab
        },
        icon = { Icon(painter = navRailItem.icon!!, contentDescription = null) },
        label = {
            Text(
                text = navRailItem.title, textAlign = TextAlign.Center,
                modifier = Modifier.padding(top = 10.dp)
            )
        },
        colors = NavigationRailItemDefaults.colors(
            indicatorColor = PrimaryLightVariantColor
        )
    )
}

@Composable
fun OrderList(
    modifier: Modifier = Modifier,
    isLoading: Boolean,
    navigator: Navigator,
    state: LazyListState,
    orders: List<UiOrder>
) {
    // DemoDataProvider.defaultOrders
    val context = LocalContext.current
    if (isLoading) {
        LoadingDataSection()
    } else {
        if (orders.isEmpty()) {
            EmptyDataSection(title = "đơn hàng")
        } else {
            LazyList(
                items = orders,
                modifier = Modifier.then(modifier),
                lazyListState = state
            ) {
                TaskItem(it) {
                    // navigator.parent?.push(TaskDetailScreen(it))
                    context.navigate(TaskDetailActivity::class.java, "uiOrder" to it)
                }
            }
        }
    }

}

@Composable
fun TaskHeader(
    visible: Boolean,
    content: @Composable() AnimatedVisibilityScope.() -> Unit
) {
    AnimatedVisibility(
        visible = visible,
        enter = fadeIn(initialAlpha = 0.4f),
        exit = fadeOut(animationSpec = tween(200))
    ) {
        content()
    }
}