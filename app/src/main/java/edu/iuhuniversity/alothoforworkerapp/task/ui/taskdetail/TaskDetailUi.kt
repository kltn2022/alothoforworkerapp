package edu.iuhuniversity.alothoforworkerapp.task.ui.taskdetail

import android.app.Application
import androidx.compose.animation.rememberSplineBasedDecay
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.*
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.workerStatusActions
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.AloThoTopAppBar
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.OrientationList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.Lian_Color_Order_Detail
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.CurrencyUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.core.utils.parse
import edu.iuhuniversity.alothoforworkerapp.core.utils.toDateFormat
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.OrderDetailEvent
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskDetailViewModel
import timber.log.Timber
import javax.inject.Inject

class TaskDetailScreen @Inject constructor(
    private val order: UiOrder
) : AndroidScreen() {
    @Composable
    override fun Content() {
        val currentContext = LocalContext.current

        val updateOrdersUseCase =
            (currentContext.getComponentActivity() as TaskDetailActivity).updateOrderUseCase
        val dispatchersProvider =
            (currentContext.getComponentActivity() as TaskDetailActivity).dispatchersProvider
        val getOrderByIdUseCase =
            (currentContext.getComponentActivity() as TaskDetailActivity).getOrderByIdUseCase
        val listenRealtimeOrderUseCase =
            (currentContext.getComponentActivity() as TaskDetailActivity).listenRealtimeOrderUseCase
        val uiOrderMapper = (currentContext.getComponentActivity() as TaskDetailActivity).orderMapper

        val viewModel: TaskDetailViewModel =
            getScreenModel<TaskDetailViewModel, TaskDetailViewModel.Factory> {
                it.create(
                    currentContext.applicationContext as Application,
                    updateOrdersUseCase,
                    getOrderByIdUseCase,
                    listenRealtimeOrderUseCase,
                    uiOrderMapper,
                    dispatchersProvider
                )
            }

        viewModel.handleEvent(OrderDetailEvent.InjectOrder(order))

        val imageLoadingUtil =
            (LocalContext.current.getComponentActivity() as TaskDetailActivity).imageLoadingUtil

        TaskDetailUi(viewModel, imageLoadingUtil)
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalMaterialApi::class)
@Composable
fun TaskDetailUi(
    viewModel: TaskDetailViewModel,
    imageLoadingUtil: ImageLoadingUtil,
) {
    val order = remember { mutableStateOf(UiOrder()) }
    val state by viewModel.state.collectAsState()
    val activity = LocalContext.current.getComponentActivity()
    val navigator = LocalNavigator.currentOrThrow
    val (isOrderInitialized, setOrderInitialized) = remember { mutableStateOf(false) }
    val (error, setError) = remember { mutableStateOf("") }

    when (val result = state) {
        is TaskDetailViewModel.State.Loading -> {

        }
        is TaskDetailViewModel.State.Error -> {
            setError(result.error)
        }
        is TaskDetailViewModel.State.UpdatedOrder -> {
            order.value = result.updatedOrder
            if (!isOrderInitialized) {
                setOrderInitialized(true)
            }
        }
    }

    val decayAnimationSpec = rememberSplineBasedDecay<Float>()
    val scrollBehavior = remember(decayAnimationSpec) {
        TopAppBarDefaults.exitUntilCollapsedScrollBehavior(decayAnimationSpec)
    }

    if (isOrderInitialized) {
        Scaffold(
            topBar = {
                AloThoTopAppBar(
                    title = "Chi tiết đơn hàng",
                    scrollBehavior = scrollBehavior,
                    onBackPressed = {
                        (activity as TaskDetailActivity).finishAfterTransition()
                    }
                )
            },
            modifier = Modifier
                .nestedScroll(scrollBehavior.nestedScrollConnection)
                .statusBarsPadding()
                .navigationBarsPadding()
        ) { innerPadding ->
            Column(
                horizontalAlignment = Alignment.Start,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        10.dp,
                        innerPadding.calculateTopPadding(),
                        10.dp,
                        innerPadding.calculateBottomPadding()
                    )
                    .background(Color.White),
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 10.dp)
                ) {
                    Column() {
                        Text(
                            text = "Mã đơn hàng",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.SemiBold
                        )
                        Text(text = "#${order.value.id}")
                    }

                    Chip(
                        onClick = {},
                        colors = ChipDefaults.chipColors(
                            backgroundColor = Color.parse(order.value.status?.color ?: "")
                        )
                    ) {
                        Text(text = order.value.status?.title.orEmpty())
                    }
                }

                InformationLine(
                    title = "Dịch vụ: ",
                    value = order.value.services?.first()?.name.orEmpty()
                )
                InformationLine(title = "Thợ sửa: ", value = order.value.worker?.displayName.orEmpty())
                InformationLine(
                    title = "Ngày làm việc: ",
                    value = order.value.workingDate?.toDateFormat().orEmpty()
                )
                InformationLine(
                    title = "Tổng tiền:",
                    value = CurrencyUtils.formatVnCurrency(order.value.estimatedTotal.toString())
                )

                Column(
                    modifier = Modifier.padding(10.dp)
                ) {
                    Text(
                        text = "Hình ảnh",
                        style = AloThoTypography.subtitle1,
                        fontWeight = FontWeight.SemiBold
                    )

                    if (!order.value.images.isNullOrEmpty()) {
                        LazyList(
                            items = order.value.images!!,
                            orientation = OrientationList.HORIZONTAL
                        ) {
                            imageLoadingUtil.ComposeImage(data = it)
                        }
                    }
                }
                Column(
                    modifier = Modifier.padding(10.dp),
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        text = "Chi tiết đơn hàng",
                        style = AloThoTypography.subtitle1,
                        fontWeight = FontWeight.SemiBold
                    )

                    LazyList(
                        items = order.value.services.orEmpty(),
                        orientation = OrientationList.HORIZONTAL
                    ) {
                        Card(
                            shape = RoundedCornerShape(10.dp),
                            backgroundColor = Lian_Color_Order_Detail,
                            modifier = Modifier.padding(10.dp)
                        ) {
                            Column(
                                horizontalAlignment = Alignment.Start,
                                verticalArrangement = Arrangement.spacedBy(5.dp),
                                modifier = Modifier.padding(10.dp)
                            ) {
                                Text(text = it?.name.orEmpty())
                                Text(text = CurrencyUtils.formatVnCurrency(it?.price.toString()))
                            }
                        }
                    }

                    val label = remember { mutableStateOf("") }
                    LaunchedEffect(order.value.status) {
                        label.value =
                            workerStatusActions.find { it.first == order.value.status!! }?.second.orEmpty()
                    }

                    if (label.value.isNotEmpty()) {
                        FilledTonalButton(
                            onClick = {
                                viewModel.handleEvent(
                                    OrderDetailEvent.UpdateNextStatus(
                                        order.value.id!!,
                                        order.value.status!!
                                    )
                                )
                            },
                            colors = ButtonDefaults.filledTonalButtonColors(
                                containerColor = order.value.status?.nextStatus()?.color.let {
                                    if (it != null) Color.parse(
                                        it
                                    ) else PrimaryVariantColor
                                },
                            ),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 15.dp)
                        ) {
                            androidx.compose.material.Text(
                                label.value,
                                style = AloThoTypography.subtitle1,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 18.sp
                            )
                        }
                    }

                    if (order.value.status == OrderStatus.WAITING_FOR_WORK) {
                        FilledTonalButton(
                            onClick = {
                                viewModel.handleEvent(
                                    OrderDetailEvent.UpdateNextStatus(
                                        order.value.id!!,
                                        order.value.status!!
                                    )
                                )
                            },
                            colors = ButtonDefaults.filledTonalButtonColors(
                                containerColor = Color.Red,
                                contentColor = Color.White
                            ),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 5.dp)
                        ) {
                            androidx.compose.material.Text(
                                "Huỷ đơn hàng",
                                style = AloThoTypography.subtitle1,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 18.sp
                            )
                        }
                    }
                }
            }
        }
    }

    if (error.isNotEmpty()) {
        DialogMessage(
            title = "Thông báo lỗi",
            text = error,
            onDismissRequest = { setError("") },
            messageType = MessageType.ERROR,
            positiveButton = ActionButton("Đã hiểu") { setError("") })
    }
}

@Composable
private fun InformationLine(
    modifier: Modifier = Modifier,
    title: String,
    value: String
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 10.dp, vertical = 5.dp)
            .then(modifier)
    ) {
        Text(text = title, style = AloThoTypography.subtitle1, fontWeight = FontWeight.SemiBold)
        Text(text = value, style = AloThoTypography.subtitle1)
    }
}