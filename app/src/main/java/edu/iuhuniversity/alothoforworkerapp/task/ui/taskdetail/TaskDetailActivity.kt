package edu.iuhuniversity.alothoforworkerapp.task.ui.taskdetail

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.core.view.WindowCompat
import androidx.navigation.ActivityNavigator
import cafe.adriel.voyager.navigator.Navigator
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.GetOrderByIdUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.ListenRealtimeOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderStatusUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction.InsertTransactionUseCase
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.task.model.mappers.UiOrderMapper
import javax.inject.Inject

@AndroidEntryPoint
class TaskDetailActivity : ComponentActivity() {

    @Inject
    lateinit var updateOrderUseCase: UpdateOrderUseCase

    @Inject
    lateinit var dispatchersProvider: DispatchersProvider

    @Inject
    lateinit var getOrderByIdUseCase: GetOrderByIdUseCase

    @Inject
    lateinit var listenRealtimeOrderUseCase: ListenRealtimeOrderUseCase

    @Inject
    lateinit var orderMapper: UiOrderMapper

    @Inject
    lateinit var insertTransactionUseCase: InsertTransactionUseCase

    @Inject
    lateinit var updateOrderStatusUseCase: UpdateOrderStatusUseCase

    @Inject
    lateinit var imageLoadingUtil: ImageLoadingUtil

    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTheme(R.style.Theme_AloThoForWorkerApp)

        setContent {
            CompositionLocalProvider(
                LocalOverScrollConfiguration provides null,
            ) {
                WindowCompat.setDecorFitsSystemWindows(window, false)
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = isSystemInDarkTheme()

                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = PrimaryColor,
                    )
                }

                if (intent?.hasExtra("uiOrder") == true) {
                    val uiOrder = intent.getParcelableExtra<UiOrder>("uiOrder")
                    Navigator(screen = TaskDetailScreen(uiOrder!!))
                } else {
                    DialogMessage(
                        title = "Thông báo lỗi",
                        text = "Có lỗi không mong muốn xảy ra. Vui lòng quay lại!",
                        onDismissRequest = { finish() },
                        positiveButton = ActionButton("Quay lại") {
                            finishAfterTransition()
                        }
                    )
                }
            }
        }
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}