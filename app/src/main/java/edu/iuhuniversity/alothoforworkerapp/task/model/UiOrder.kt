package edu.iuhuniversity.alothoforworkerapp.task.model

import android.os.Parcel
import android.os.Parcelable
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderDetail
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class UiOrder(
    val id: String? = null,
    val total: Double? = 0.0,
    val estimatedTotal: Double? = 0.0,
    val services: List<Service?>? = emptyList(),
    val serviceType: String? = null,
    val status: OrderStatus? = null,
    val worker: Worker? = null,
    val workingDate: Date? = null,
    val images: List<String>? = null,
    val orderDetails: List<OrderDetail>? = emptyList(),
) : Parcelable {
    companion object : Parceler<UiOrder> {
        override fun create(parcel: Parcel): UiOrder {
            val orderDetails = ArrayList<OrderDetail>()
            val services = ArrayList<Service>()

            return UiOrder(
                id = parcel.readString(),
                total = parcel.readDouble(),
                estimatedTotal = parcel.readDouble(),
                services = services.apply {
                    parcel.readList(
                        this,
                        Service::class.java.classLoader
                    )
                },
                serviceType = parcel.readString(),
                status = parcel.readParcelable(OrderStatus::class.java.classLoader),
                worker = parcel.readParcelable(Worker::class.java.classLoader),
                workingDate = parcel.readSerializable() as Date?,
                images = parcel.createStringArrayList(),
                orderDetails = orderDetails.apply {
                    parcel.readList(
                        this,
                        OrderDetail::class.java.classLoader
                    )
                },
            )
        }

        override fun UiOrder.write(parcel: Parcel, flags: Int) {
            parcel.writeString(this.id)
            if (this.total != null) parcel.writeDouble(this.total) else parcel.writeDouble(0.0)
            if (this.estimatedTotal != null) parcel.writeDouble(this.estimatedTotal) else parcel.writeDouble(
                0.0
            )
            parcel.writeTypedList(this.services)
            parcel.writeString(this.serviceType)
            this.worker?.writeToParcel(parcel, 0)
            this.status?.writeToParcel(parcel, 0)
            parcel.writeSerializable(this.workingDate)
            parcel.writeStringList(this.images)
            parcel.writeTypedList(this.orderDetails)
        }
    }
}
