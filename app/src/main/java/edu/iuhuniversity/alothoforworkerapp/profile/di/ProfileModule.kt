package edu.iuhuniversity.alothoforworkerapp.profile.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class ProfileModule {

    @Binds
    @IntoMap
    @ScreenModelFactoryKey(ProfileViewModel.Factory::class)
    abstract fun bindProfileViewModelFactory(profileViewModelFactory: ProfileViewModel.Factory): ScreenModelFactory
}