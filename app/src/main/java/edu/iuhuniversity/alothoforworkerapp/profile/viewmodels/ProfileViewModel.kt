package edu.iuhuniversity.alothoforworkerapp.profile.viewmodels

import android.app.Application
import android.net.Uri
import android.os.Build
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.core.net.toUri
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.constant.FileConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.LogoutUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.file.UploadFilesUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetCurrentUserUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.UpdateUserInfoUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ProfileViewModel @AssistedInject constructor(
    @Assisted private val app: Application,
    @Assisted private val currentUserUseCase: GetCurrentUserUseCase,
    @Assisted private val updateUserInfoUseCase: UpdateUserInfoUseCase,
    @Assisted private val uploadFilesUseCase: UploadFilesUseCase,
    @Assisted private val logoutUseCase: LogoutUseCase,
    @Assisted private val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<ProfileViewModel.State>(State.Loading()) {

    private lateinit var currentWorker: MutableState<Worker>

    fun getCurrentUser(): Worker? {
        return if (!this::currentWorker.isInitialized) null
        else currentWorker.value
    }

    fun setCurrentUser(worker: Worker) {
        if (!this::currentWorker.isInitialized) {
            currentWorker = mutableStateOf(worker)
        } else {
            currentWorker.value = worker
        }
    }

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            currentUserUseCase: GetCurrentUserUseCase,
            updateUserInfoUseCase: UpdateUserInfoUseCase,
            uploadFilesUseCase: UploadFilesUseCase,
            logoutUseCase: LogoutUseCase,
            dispatchersProvider: DispatchersProvider
        ): ProfileViewModel
    }

    sealed class State {
        data class Loading(val percent: Float = 0f) : State()
        data class CurrentUserProfile(val currentUser: Worker) : State()
        data class Error(val error: String) : State()
        object UpdatedProfile : State()
        object Logout : State()
    }

    fun handleEvent(event: ProfileEvent) {
        when (event) {
            is ProfileEvent.Loading -> {
                displayProfile()
            }
            is ProfileEvent.Logout -> {
                logout()
            }
            is ProfileEvent.UpdateProfile -> {
                updateProfile(event.updatedUser, event.newPhotoUri)
            }
        }
    }

    private fun uploadAndDateProfile(updatedUserInfo: Worker, newPhotoUri: String?) {
        val errorMessage = "Có lỗi xảy ra"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        if (newPhotoUri != null) {
            coroutineScope.launch(exceptionHandler) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (isNetworkIsAvailable(app)) {
                        uploadFilesUseCase.execute(
                            FileConst.AVATAR_USER_FOLDER_PATH,
                            listOf(newPhotoUri.toUri()),
                            object : FileUploaderCallback {
                                override fun onError(e: Exception?) {
                                    mutableState.value = State.Error(e?.message.orEmpty())
                                }

                                override fun onFinish(responses: List<String>?) {
                                    updateProfile(updatedUserInfo, responses?.first())
                                }

                                override fun onProgressUpdate(
                                    currentPercent: Long,
                                    totalPercent: Long,
                                    fileNumber: Long
                                ) {
                                    mutableState.value = State.Loading(
                                        (currentPercent / totalPercent).times(100).toFloat()
                                    )
                                    Timber.tag("onProgressUpdate")
                                        .d("$currentPercent / $totalPercent / $fileNumber")
                                }

                            }
                        )
                    }
                }
            }
        } else {
            updateProfile(updatedUserInfo, null)
        }
    }

    private fun updateProfile(updatedUserInfo: Worker, photoUrl: String?) {
        val errorMessage = "Có lỗi xảy ra"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        val dataToUpdate = mutableMapOf<String, Any>()
        dataToUpdate["displayName"] = updatedUserInfo.displayName.orEmpty()
        if (photoUrl != null) {
            dataToUpdate["photoUrl"] = photoUrl
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = updateUserInfoUseCase.execute(
                            updatedUserInfo.id,
                            dataToUpdate
                        )) {
                            is Resource.Loading -> mutableState.value =
                                State.Loading()
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                setCurrentUser(updatedUserInfo)
                                mutableState.value = State.UpdatedProfile
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }


    private fun displayProfile() {
        val errorMessage = "Có lỗi xảy ra"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = currentUserUseCase.execute()) {
                            is Resource.Loading -> mutableState.value = State.Loading()
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> mutableState.value = State.CurrentUserProfile(result?.data!!)
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())

    }

    private fun logout() = coroutineScope.launch {
        logoutUseCase.execute()
        mutableState.value = State.Logout
    }
}


sealed class ProfileEvent {
    object Loading : ProfileEvent()
    object Logout : ProfileEvent()
    data class UpdateProfile(val updatedUser: Worker, val newPhotoUri: String?) : ProfileEvent()
}