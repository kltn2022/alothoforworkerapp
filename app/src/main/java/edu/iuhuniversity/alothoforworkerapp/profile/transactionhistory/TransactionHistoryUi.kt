package edu.iuhuniversity.alothoforworkerapp.profile.transactionhistory

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Payments
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.EmptyDataSection
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.OrientationList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.Lian_Color_LighterGray
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor

@Preview
@Composable
fun TransactionHistoryUi() {
//    TransactionList(transactions = listOf(
//        Transaction(,)
//    ))

}

@Composable
fun TransactionList(transactions: List<Transaction>) {
    if (transactions.isEmpty()) {
        EmptyDataSection(title = "giao dịch")
    } else {
        LazyList(items = transactions, orientation = OrientationList.VERTICAL) {
            TransactionItem(it) {

            }
        }
    }
}

@Composable
private fun TransactionItem(
    item: Transaction,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(vertical = 10.dp, horizontal = 10.dp),
        horizontalArrangement = Arrangement.Start,
    ) {
        Icon(
            imageVector = Icons.Rounded.Payments, contentDescription = null,
            tint = PrimaryColor,
            modifier = Modifier
                .fillMaxHeight()
                .align(Alignment.CenterVertically)
                .clip(CircleShape)
                .background(Lian_Color_LighterGray)
                .padding(10.dp)
        )
        Column(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .padding(start = 12.dp)
        ) {
            Text(
                text = "${item.amount} VNĐ",
                style = AloThoTypography.body1,
                fontWeight = FontWeight.SemiBold
            )
            Text(
                text =  item.order.id,
                color = Color.Gray,
                style = AloThoTypography.body2
            )
        }
    }
}