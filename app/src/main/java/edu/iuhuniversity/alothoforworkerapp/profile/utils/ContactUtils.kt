package edu.iuhuniversity.alothoforworkerapp.profile.utils

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity

const val ALOTHO_EMAIL = "support@alotho.vn"
const val ALOTHO_PHONE = "0968958053"

fun sendSupportEmail(activity: AppCompatActivity, onError: (e: Exception) -> Unit) {
    val emailIntent = Intent(
        Intent.ACTION_SENDTO, Uri.fromParts(
            "mailto", ALOTHO_EMAIL, null
        )
    )
    emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(ALOTHO_EMAIL))
    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "[Hỗ trợ] Tôi cần sự hỗ trợ từ AloThoVN")
    emailIntent.putExtra(Intent.EXTRA_TEXT, "<Nhập nội dung mà bạn muốn hỗ trợ>")

    try {
        activity.startActivity(Intent.createChooser(emailIntent, "Choose Email Client..."))
    } catch (e: Exception) {
        onError(e)
    }
}

fun callSupportPhone(activity: AppCompatActivity, onError: (e: Exception) -> Unit) {
    val mIntent = Intent(Intent.ACTION_DIAL)
    mIntent.data = Uri.parse("tel:$ALOTHO_PHONE")
    mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(ALOTHO_EMAIL))

    try {
        activity.startActivity(mIntent)
    } catch (e: Exception) {
        onError(e)
    }
}
