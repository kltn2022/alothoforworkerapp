package edu.iuhuniversity.alothoforworkerapp.profile

import android.app.Application
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ChevronRight
import androidx.compose.material.icons.rounded.ManageAccounts
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.dropShadow
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.profile.editprofile.EditProfileScreen
import edu.iuhuniversity.alothoforworkerapp.profile.utils.callSupportPhone
import edu.iuhuniversity.alothoforworkerapp.profile.utils.sendSupportEmail
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileEvent
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileViewModel

object PersonalizeScreen : Screen {
    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow
        val currentContext = LocalContext.current

        val currentUserUseCase = (currentContext as MainActivity).currentUserUseCase
        val updateUserInfoUseCase = currentContext.updateUserInfoUseCase
        val dispatchersProvider = currentContext.dispatchersProvider
        val logoutUseCase = currentContext.logoutUseCase
        val imageLoadingUtil = currentContext.imageLoadingUtil
        val uploadFilesUseCase = currentContext.uploadFilesUseCase

        currentContext.profileViewModel = getScreenModel<ProfileViewModel, ProfileViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                currentUserUseCase,
                updateUserInfoUseCase,
                uploadFilesUseCase,
                logoutUseCase,
                dispatchersProvider
            )
        }

        currentContext.profileViewModel.handleEvent(ProfileEvent.Loading)

        PersonalizeUi(navigator, currentContext.profileViewModel, imageLoadingUtil)
    }
}

@Composable
fun PersonalizeUi(
    navigator: Navigator,
    viewModel: ProfileViewModel,
    imageLoadingUtil: ImageLoadingUtil
) {
    val state by viewModel.state.collectAsState()

    when (val result = state) {
        is ProfileViewModel.State.CurrentUserProfile -> {
            viewModel.setCurrentUser(result.currentUser)
        }
        is ProfileViewModel.State.Logout -> {
            navigator.popAll()
        }
    }

    DisplayCurrentProfile(
        navigator = navigator,
        viewModel = viewModel,
        imageLoadingUtil = imageLoadingUtil
    )
}

@Composable
fun DisplayCurrentProfile(
    navigator: Navigator,
    viewModel: ProfileViewModel,
    imageLoadingUtil: ImageLoadingUtil
) {
    Box(
        contentAlignment = Alignment.TopCenter,
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(294.dp)
                .clip(
                    RoundedCornerShape(
                        topStart = 0.dp, topEnd = 0.dp,
                        bottomStart = 20.dp, bottomEnd = 20.dp
                    )
                )
                .background(PrimaryColor)
        )

        Column(
            verticalArrangement = Arrangement.spacedBy(16.dp),
        ) {
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(top = 32.dp, start = 12.dp, end = 12.dp)
            ) {
                Icon(
                    imageVector = Icons.Rounded.ManageAccounts,
                    contentDescription = null,
                    tint = Color.White,
                    modifier = Modifier.size(40.dp)
                )
                Text(
                    text = "Cài đặt",
                    style = AloThoTypography.h5.copy(
                        fontWeight = FontWeight.Bold,
                        color = Color.White
                    )
                )
            }

            Card(
                modifier = Modifier
                    .padding(vertical = 12.dp)
                    .fillMaxWidth()
                    .padding(horizontal = 12.dp),
                shape = RoundedCornerShape(16.dp),
                elevation = 5.dp
            ) {
                val checkedState = remember { mutableStateOf(true) }
                val activity = LocalContext.current.getActivity() as MainActivity

                Column(modifier = Modifier.fillMaxWidth()) {
                    PersonalAccount(viewModel.getCurrentUser(), imageLoadingUtil) {
                        navigator.push(EditProfileScreen)
                    }
                    Divider()
                    HeaderTitle(title = "Cài đặt tài khoản")
                    SettingItem(title = "Lịch sử công việc", trailingIcon = null) {

                    }
                    SettingItem(title = "Lịch sử giao dịch", trailingIcon = null) {

                    }
                    SettingItem(title = "Đăng xuất", trailingIcon = null) {
                        viewModel.handleEvent(ProfileEvent.Logout)
                    }
                    SettingItem(title = "Thông báo", trailingIcon = {
                        Switch(
                            checked = checkedState.value,
                            onCheckedChange = { checkedState.value = it },
                            colors = SwitchDefaults.colors(
                                checkedThumbColor = PrimaryColor,
                                checkedTrackColor = PrimaryVariantColor
                            )
                        )
                    }) {
                    }

                    Divider()
                    HeaderTitle(title = "Lên hệ và hỗ trợ")

                    ContactItem(
                        title = "Gọi hỗ trợ",
                        content = "096 895 8053",
                        trailingIcon = null
                    ) {
                        callSupportPhone(activity) {
                            activity.showSnackBar("Gọi hỗ trợ không thành công. Xin hãy thử lại")
                        }
                    }
                    ContactItem(
                        title = "Email hỗ trợ",
                        content = "support@alotho.vn",
                        trailingIcon = null
                    ) {
                        sendSupportEmail(activity) {
                            activity.showSnackBar("Gửi email không thành công. Xin hãy thử lại")
                        }
                    }

                    Divider()

                    HeaderTitle(title = "Khác")
                    SettingItem(title = "Về chúng tôi", trailingIcon = null) {

                    }
                    SettingItem(title = "Chính sách riêng tư", trailingIcon = null) {

                    }
                }

            }
        }

    }
}


@Composable
private fun PersonalAccount(
    currentUser: Worker?,
    imageLoadingUtil: ImageLoadingUtil,
    onClick: () -> Unit
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(12.dp),
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(12.dp)
            .clickable { onClick() }
    ) {
        val photoUrl = currentUser?.photoUrl
        if (!photoUrl.isNullOrEmpty()) {
            imageLoadingUtil.ComposeImage(
                data = currentUser.photoUrl,
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
                    .dropShadow(CircleShape)
            )
        } else {
            Image(
                painter = painterResource(id = R.drawable.ic_user_circle_solid),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(40.dp)
                    .clip(CircleShape)
                    .dropShadow(CircleShape)
            )
        }
        Text(
            text = (currentUser?.displayName ?: currentUser?.phoneNumber).orEmpty(),
            style = AloThoTypography.subtitle1,
            fontWeight = FontWeight.SemiBold
        )
    }
}

@Composable
private fun HeaderTitle(title: String) {
    Text(
        text = title,
        style = AloThoTypography.subtitle1,
        fontWeight = FontWeight.SemiBold,
        color = Lian_Color_Settings_Header,
        modifier = Modifier
            .padding(12.dp)
    )
}

@Composable
private fun SettingItem(
    title: String,
    trailingIcon: (@Composable() RowScope.() -> Unit)?,
    onClick: () -> Unit
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(horizontal = 12.dp, vertical = 8.dp)
    ) {
        Text(text = title, style = AloThoTypography.subtitle1, fontWeight = FontWeight.SemiBold)
        if (trailingIcon == null) {
            Icon(imageVector = Icons.Rounded.ChevronRight, contentDescription = null)
        } else {
            trailingIcon()
        }
    }
}

@Composable
private fun ContactItem(
    title: String = "dassda",
    content: String = "dsadasdas333",
    trailingIcon: (@Composable() RowScope.() -> Unit)? = null,
    onClick: () -> Unit = {}
) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(horizontal = 12.dp, vertical = 8.dp)
    ) {
        Text(text = title, style = AloThoTypography.subtitle1, fontWeight = FontWeight.SemiBold)
        if (trailingIcon == null) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(5.dp)
            ) {
                Text(
                    text = content,
                    style = AloThoTypography.subtitle1,
                    fontWeight = FontWeight.Normal,
                    color = SecondaryColor,
                )
                Icon(imageVector = Icons.Rounded.ChevronRight, contentDescription = null)
            }

        } else {
            trailingIcon()
        }
    }
}