package edu.iuhuniversity.alothoforworkerapp.profile.editprofile

import android.Manifest
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.androidx.AndroidScreen
import com.himanshoe.pluck.PluckConfiguration
import com.himanshoe.pluck.ui.Pluck
import com.himanshoe.pluck.ui.permission.Permission
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.constant.PermissionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.*
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.core.utils.PermissionChecker
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileEvent
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileViewModel
import timber.log.Timber

object EditProfileScreen : AndroidScreen() {
    @Composable
    override fun Content() {
        val viewModel = (LocalContext.current.getActivity() as MainActivity).profileViewModel
        val imageLoadingUtil = (LocalContext.current.getActivity() as MainActivity).imageLoadingUtil

        EditProfileUi(viewModel = viewModel, imageLoadingUtil = imageLoadingUtil)
    }
}


@Composable
fun EditProfileUi(
    viewModel: ProfileViewModel,
    imageLoadingUtil: ImageLoadingUtil,
) {
    ConstraintLayout() {
        val state by viewModel.state.collectAsState()

        val (topAppBar, header, avatar, btnChangeAvatar, profileSection, btnUpdateProfile, snackBar, loadingBar) = createRefs()
        val isChangeAvatarClicked = remember {
            mutableStateOf(false)
        }
        val avatarImg = remember { mutableStateOf(Uri.EMPTY) }

        val currentUser = remember { mutableStateOf(viewModel.getCurrentUser()) }
        val (isSnackbarShow, setSnackbarShow) = remember {
            mutableStateOf(false)
        }

        when (val result = state) {
            is ProfileViewModel.State.UpdatedProfile -> {
                setSnackbarShow(true)
                if (isSnackbarShow) {
                    SnackbarMessage(
                        message = "Đã cập nhật profile!",
                        snackbarType = MessageType.SUCCESS,
                        onActionClickListener = { setSnackbarShow(false) },
                        actionLabel = "Đã hiểu",
                        onDismiss = {
                            setSnackbarShow(it)
                        },
                        modifier = Modifier.constrainAs(snackBar) {
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                    )
                }
            }
            is ProfileViewModel.State.Error -> {
                setSnackbarShow(true)
                if (isSnackbarShow && result.error.isNotEmpty()) {
                    SnackbarMessage(
                        message = result.error,
                        snackbarType = MessageType.ERROR,
                        actionLabel = "Đã hiểu",
                        onActionClickListener = { setSnackbarShow(false) },
                        onDismiss = {
                            setSnackbarShow(it)
                        },
                        modifier = Modifier.constrainAs(snackBar) {
                            bottom.linkTo(parent.bottom)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                    )
                }
            }
            is ProfileViewModel.State.Loading -> {
                Timber.tag("Loading").d("Loadingggg....")
                MyLinearProgressIndicator(
                    progress = result.percent,
                    modifier = Modifier.constrainAs(loadingBar) {
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    })
            }
        }

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(150.dp)
                .background(PrimaryColor)
                .constrainAs(header) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        ) {

        }

        AloThoTopAppBar(
            title = "Thông tin hồ sơ",
            modifier = Modifier.constrainAs(topAppBar) {
                top.linkTo(parent.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        )

        val photoUrl = currentUser.value?.photoUrl
        if (!photoUrl.isNullOrEmpty()) {
            imageLoadingUtil.ComposeImage(
                data = if (avatarImg.value == Uri.EMPTY) photoUrl else avatarImg.value,
                modifier = Modifier
                    .size(100.dp)
                    .clip(CircleShape)
                    .border(5.dp, Color.White, shape = CircleShape)
                    .clickable { }
                    .constrainAs(avatar) {
                        top.linkTo(header.bottom)
                        bottom.linkTo(header.bottom)
                        start.linkTo(header.start)
                        end.linkTo(header.end)
                    }
            )
        } else {
            Image(
                painter = painterResource(id = R.drawable.ic_user_circle_solid),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(100.dp)
                    .clip(CircleShape)
                    .border(5.dp, Color.White, shape = CircleShape)
                    .clickable { isChangeAvatarClicked.value = true }
                    .constrainAs(avatar) {
                        top.linkTo(header.bottom)
                        bottom.linkTo(header.bottom)
                        start.linkTo(header.start)
                        end.linkTo(header.end)
                    }
            )
        }


        Image(
            painter = painterResource(id = R.drawable.avatar_icon_edit),
            contentDescription = null,
            modifier = Modifier
                .size(26.dp)
                .clip(CircleShape)
                .constrainAs(btnChangeAvatar) {
                    bottom.linkTo(avatar.bottom)
                    end.linkTo(avatar.end)
                }
                .clickable {
                    isChangeAvatarClicked.value = true
                }
        )

        val galleryLauncher = rememberLauncherForActivityResult(
            ActivityResultContracts.GetContent()
        ) {
            Timber.tag("photoUri").d(it.toString())
            isChangeAvatarClicked.value = false
            if (it != null) {
                avatarImg.value = it
            }
        }

        if (isChangeAvatarClicked.value) {
            PermissionChecker(permissions = PermissionConst.EDIT_PROFILE_PERMISSION) {
                galleryLauncher.launch("image/*")
            }
        }

        val displayName =
            rememberSaveable { mutableStateOf(currentUser.value?.displayName.orEmpty()) }
        val phoneNumber =
            rememberSaveable { mutableStateOf(currentUser.value?.phoneNumber.orEmpty()) }
        val email = rememberSaveable { mutableStateOf(currentUser.value?.email.orEmpty()) }

        Column(
            modifier = Modifier
                .constrainAs(profileSection) {
                    top.linkTo(avatar.bottom, 30.dp)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(horizontal = 36.dp),
            verticalArrangement = Arrangement.spacedBy(19.dp),
            horizontalAlignment = Alignment.Start
        ) {
            OutlinedTextfield(
                label = "Họ và tên",
                value = displayName.value,
                error = "",
                onValueChange = {
                    displayName.value = it
                },
                keyboardActions = KeyboardActions {
                    // validate(text)
                },
                keyboardOptions = KeyboardOptions(
                    imeAction = ImeAction.Next
                )
            )
            OutlinedTextfield(
                label = "Số điện thoại",
                value = phoneNumber.value,
                error = "",
                readOnly = true,
                onValueChange = {
                    phoneNumber.value = it
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Number,
                    imeAction = ImeAction.Next
                )
            )
            OutlinedTextfield(
                label = "Địa chỉ email",
                value = phoneNumber.value,
                error = "",
                readOnly = true,
                onValueChange = {
                    email.value = it
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email,
                    imeAction = ImeAction.Done
                )
            )
        }

        FilledTonalButton(
            onClick = {
                val ptUrl: String = avatarImg.value.toString().ifEmpty {
                    currentUser.value?.photoUrl.orEmpty()
                }

                viewModel.handleEvent(
                    ProfileEvent.UpdateProfile(
                        currentUser.value?.copy(displayName = displayName.value)!!,
                        ptUrl
                    )
                )
            },
            colors = ButtonDefaults.filledTonalButtonColors(
                containerColor = PrimaryVariantColor,
            ),
            modifier = Modifier
                .constrainAs(btnUpdateProfile) {
                    top.linkTo(profileSection.bottom, 20.dp)
                    start.linkTo(profileSection.start)
                    end.linkTo(profileSection.end)
                }
        ) {
            Text("Cập nhật thông tin")
        }
    }
}