package edu.iuhuniversity.alothoforworkerapp.makeapayment.viewmodels

import android.app.Application
import android.net.UrlQuerySanitizer
import android.os.Build
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.vnpay.CreatePaymentRequest
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderStatusUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.payment.CreateMomoPaymentUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction.InsertTransactionUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.EPaymentUtil
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import kotlinx.coroutines.*

class MakeAPaymentViewModel @AssistedInject constructor(
    @Assisted private val app: Application,
    @Assisted private val createMomoPaymentUseCase: CreateMomoPaymentUseCase,
    @Assisted private val insertTransactionUseCase: InsertTransactionUseCase,
    @Assisted private val updateOrderStatusUseCase: UpdateOrderStatusUseCase,
    @Assisted private val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<MakeAPaymentViewModel.State>(State.Loading) {

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            createMomoPaymentUseCase: CreateMomoPaymentUseCase,
            insertTransactionUseCase: InsertTransactionUseCase,
            updateOrderStatusUseCase: UpdateOrderStatusUseCase,
            dispatchersProvider: DispatchersProvider
        ): MakeAPaymentViewModel
    }

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        data class CreatedMomoPaymentRequest(val resp: ProcessingPayment.Response) : State()
        object ProcessedPaymentResult : State()
        data class GeneratedVnpayPaymentUrl(val paymentUrl: String) : State()
    }

    fun handleEvent(event: MakeAPaymentEvent) {
        when (event) {
            is MakeAPaymentEvent.MakeAPaymentByMomo -> createMoMoPayment(event.order)
            is MakeAPaymentEvent.ProcessingPaymentResult -> {
                processingPaymentResult(event.result.orderId, event.result.amount)
            }
            is MakeAPaymentEvent.MakeAPaymentByVnPay -> generateVnpayPaymentUrl(event.order)
            is MakeAPaymentEvent.ProcessingVnpayPaymentResult -> {
                val sanitizer = UrlQuerySanitizer()
                sanitizer.allowUnregisteredParamaters = true
                sanitizer.parseUrl(event.returnedUrl)

                 val responseCode = sanitizer.getValue("vnp_ResponseCode")
                 if (responseCode == "00") { // Success
                     val orderId = sanitizer.getValue("vnp_OrderInfo")
                     val amount = sanitizer.getValue("vnp_Amount").toLong()
                     processingPaymentResult(orderId, amount)
                 } else {
                     mutableState.value = State.Error("Giao dịch bằng ví VNPay thất bại!")
                 }
            }
        }
    }

    private fun processingPaymentResult(orderId: String, amount: Long) {
        // update Order Status
        // insert Transaction History
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {

                        try {
                            listOf(
                                async {
                                    updateOrderStatusUseCase.execute(
                                        orderId,
                                        OrderStatus.COMPLETED
                                    )
                                },
                                async {
                                    insertTransactionUseCase.execute(
                                        Transaction.createNew(
                                            amount.toDouble(),
                                            order = CommonUtils.getDocumentReference(
                                                CollectionConst.COLLECTION_ORDER,
                                                orderId
                                            )
                                        )
                                    )
                                }
                            ).awaitAll()
                            mutableState.value = State.ProcessedPaymentResult
                        } catch (e: Throwable) {
                            onFailure(e)
                        }
                    }
                }
            }
        }
    }

    /**
     * STEP #1: TẠO MOMO PAYMENT -> TRẢ VỀ DEEP LINK
     * STEP #2: MỞ APP = DEEP LINK -> THANH TOÁN XONG --> RETURN VỀ RETURN_URL KÈM CÁC PARAMS
     * STEP #3: DỰA TRÊN FIELD [RESULT_CODE] ĐỂ BIẾT CÓ THANH TOÁN THÀNH CÔNG KHÔNG -> CHUYỂN MÀN HÌNH THÔNG BÁO
     */
    private fun createMoMoPayment(
        order: Order,
    ) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        val request = ProcessingPayment.Request.createFrom(order)

                        when (val result = createMomoPaymentUseCase.execute(request)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                mutableState.value = State.CreatedMomoPaymentRequest(result.data!!)
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun generateVnpayPaymentUrl(order: Order) {
        val vnPaymentRequest =
            CreatePaymentRequest.createFrom(order.id, order.total?.toLong() ?: 1000, null)
        val paymentUrl = EPaymentUtil.getPaymentUrl(vnPaymentRequest)
        mutableState.value = State.GeneratedVnpayPaymentUrl(paymentUrl)
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }
}

sealed class MakeAPaymentEvent {
    data class MakeAPaymentByMomo(val order: Order) : MakeAPaymentEvent()
    data class MakeAPaymentByVnPay(val order: Order) : MakeAPaymentEvent()
    data class ProcessingPaymentResult(val result: ProcessingPayment.Result) : MakeAPaymentEvent()
    data class ProcessingVnpayPaymentResult(val returnedUrl: String) : MakeAPaymentEvent()
}