package edu.iuhuniversity.alothoforworkerapp.makeapayment.ui

import android.app.Application
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.makeapayment.viewmodels.MakeAPaymentViewModel

data class MakeAPaymentScreen(
    val orderId: String
) : AndroidScreen() {

    @Composable
    override fun Content() {
        val currentContext = LocalContext.current
        val navigator = LocalNavigator.currentOrThrow

        val createMomoPaymentUseCase = (currentContext as MainActivity).createMomoPaymentUseCase
        val insertTransactionUseCase = currentContext.insertTransactionUseCase
        val updateOrderStatusUseCase = currentContext.updateOrderStatusUseCase
        val dispatchersProvider = currentContext.dispatchersProvider

        val viewModel = getScreenModel<MakeAPaymentViewModel, MakeAPaymentViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                createMomoPaymentUseCase,
                insertTransactionUseCase,
                updateOrderStatusUseCase,
                dispatchersProvider
            )
        }

        val state by viewModel.state.collectAsState()
        when (val result = state) {
            is MakeAPaymentViewModel.State.Loading -> {

            }
            is MakeAPaymentViewModel.State.Error -> {

            }
            is MakeAPaymentViewModel.State.CreatedMomoPaymentRequest -> {

            }
            is MakeAPaymentViewModel.State.GeneratedVnpayPaymentUrl -> {

            }
            is MakeAPaymentViewModel.State.ProcessedPaymentResult -> {

            }
        }
    }

}
