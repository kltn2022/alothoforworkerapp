package edu.iuhuniversity.alothoforworkerapp.notification

import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.navigator.tab.CurrentTab
import cafe.adriel.voyager.navigator.tab.TabNavigator
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.AloThoTab
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.TabItem
import edu.iuhuniversity.alothoforworkerapp.notification.ui.ConservationTabUi
import edu.iuhuniversity.alothoforworkerapp.notification.ui.NotificationTabUi

@OptIn(ExperimentalMaterial3Api::class)
object NotificationUi : AndroidScreen() {
    @Composable
    override fun Content() {
        TabNavigator(NotificationTabUi) {
            Scaffold(
                topBar = {
                    AloThoTab {
                        TabItem(tab = NotificationTabUi)
                        TabItem(tab = ConservationTabUi)
                    }
                },
                modifier = Modifier.statusBarsPadding()
            ) {
                CurrentTab()
            }
        }
    }

}
