package edu.iuhuniversity.alothoforworkerapp.notification.viewmodels

import android.app.Application
import android.os.Build
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.GetNotificationUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

// show Saved Notifications From Room
class NotificationViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val getSaveNotificationUseCase: GetNotificationUseCase,
    @Assisted val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<NotificationViewModel.State>(State.Loading) {

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            getSaveNotificationUseCase: GetNotificationUseCase,
            dispatchersProvider: DispatchersProvider
        ): NotificationViewModel
    }

    sealed class State {
        object Loading : State()
        data class LoadSavedNotifications(val notifications: List<FcmNotification>) : State()
        data class Error(val error: String) : State()
    }

    fun handleEvent(event: NotificationEvent) {
        when (event) {
            is NotificationEvent.Loading -> loadSavedNotifications()
        }
    }

    private fun loadSavedNotifications() {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = getSaveNotificationUseCase.execute()) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            is Resource.Success -> mutableState.value =
                                State.LoadSavedNotifications(result.data.orEmpty())
                        }
                    }
                } else {
                    mutableState.value = State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }
}

sealed class NotificationEvent {
    object Loading : NotificationEvent()
}

