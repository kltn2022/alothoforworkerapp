package edu.iuhuniversity.alothoforworkerapp.notification.ui

import android.app.Application
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.NotificationsActive
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.EmptyDataSection
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.OrientationList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.Lian_Color_LighterGray
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.NotificationEvent
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.NotificationViewModel

object NotificationTabUi : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Rounded.NotificationsActive)

            return remember {
                TabOptions(
                    0u,
                    "Thông báo",
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        val currentContext = LocalContext.current
        val currentActivity = currentContext.getComponentActivity() as MainActivity
        val getSavedNotificationsUseCase =
            currentActivity.getSaveNotificationUseCase
        val dispatchersProvider =
            currentActivity.dispatchersProvider

        val viewModel = getScreenModel<NotificationViewModel, NotificationViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                getSavedNotificationsUseCase,
                dispatchersProvider
            )
        }

        viewModel.handleEvent(NotificationEvent.Loading)

        val state by viewModel.state.collectAsState()
        var notifications = remember {
            listOf<FcmNotification>()
        }

        when (val result = state) {
            is NotificationViewModel.State.Loading -> {
                // Loading Effect
            }
            is NotificationViewModel.State.LoadSavedNotifications -> {
                notifications = result.notifications
            }
            is NotificationViewModel.State.Error -> {
                currentActivity.showSnackBar(result.error)
            }
        }

        if (notifications.isEmpty()) {
            EmptyDataSection(title = "thông báo", modifier = Modifier.padding(top = 50.dp))
        } else {
            NotificationList(
                notifications = notifications
            )
        }
    }
}

@Composable
fun NotificationList(notifications: List<FcmNotification>) {
    if (notifications.isEmpty()) {
        EmptyDataSection(title = "thông báo")
    } else {
        LazyList(items = notifications, orientation = OrientationList.VERTICAL) {
            NotificationItem(it) {

            }
        }
    }
}

@Composable
fun NotificationItem(
    item: FcmNotification,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(vertical = 10.dp, horizontal = 10.dp),
        horizontalArrangement = Arrangement.Start,
    ) {
        Icon(
            imageVector = Icons.Rounded.NotificationsActive, contentDescription = null,
            tint = PrimaryColor,
            modifier = Modifier
                .fillMaxHeight()
                .align(Alignment.CenterVertically)
                .clip(CircleShape)
                .background(Lian_Color_LighterGray)
                .padding(10.dp)
        )
        Column(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .padding(start = 12.dp)
        ) {
            Text(
                text = item.title,
                style = AloThoTypography.body1,
                fontWeight = FontWeight.SemiBold
            )
            Text(
                text = item.body,
                color = Color.Gray,
                style = AloThoTypography.body2
            )
        }
    }
}

