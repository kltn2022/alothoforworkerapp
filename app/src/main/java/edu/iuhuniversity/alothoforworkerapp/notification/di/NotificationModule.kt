package edu.iuhuniversity.alothoforworkerapp.notification.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.ConservationViewModel
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.NotificationViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class NotificationModule {
    @Binds
    @IntoMap
    @ScreenModelFactoryKey(NotificationViewModel.Factory::class)
    abstract fun bindNotificationViewModelFactory(notificationViewModelFactory: NotificationViewModel.Factory): ScreenModelFactory

    @Binds
    @IntoMap
    @ScreenModelFactoryKey(ConservationViewModel.Factory::class)
    abstract fun bindConservationViewModelFactory(conservationViewModelFactory: ConservationViewModel.Factory): ScreenModelFactory
}