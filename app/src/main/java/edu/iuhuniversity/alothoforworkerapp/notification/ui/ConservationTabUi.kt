package edu.iuhuniversity.alothoforworkerapp.notification.ui

import android.app.Application
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Message
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.tab.Tab
import cafe.adriel.voyager.navigator.tab.TabOptions
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.ConservationInfo
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.EmptyDataSection
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.Lian_Color_LighterGray
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.ConservationEvent
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.ConservationViewModel
import edu.iuhuniversity.alothoforworkerapp.notification.viewmodels.NotificationViewModel

object ConservationTabUi : Tab {

    override val options: TabOptions
        @Composable
        get() {
            val icon = rememberVectorPainter(Icons.Rounded.Message)

            return remember {
                TabOptions(
                    1u,
                    "Hội thoại",
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        val currentContext = LocalContext.current
        val currentActivity = currentContext.getComponentActivity() as MainActivity
        val dispatchersProvider =
            currentActivity.dispatchersProvider
        val listenForConservationsUseCase = currentActivity.listenForConservationsUseCase
        val imageLoadingUtil = currentActivity.imageLoadingUtil

        val viewModel = getScreenModel<ConservationViewModel, ConservationViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                listenForConservationsUseCase,
                dispatchersProvider
            )
        }

        viewModel.handleEvent(ConservationEvent.LoadConservation)

        val state by viewModel.state.collectAsState()
        var conservations = remember {
            listOf<ConservationInfo?>()
        }
        when (val result = state) {
            is ConservationViewModel.State.Loading -> {

            }
            is ConservationViewModel.State.Error -> {
                currentActivity.showSnackBar(result.error)
            }
            is ConservationViewModel.State.LoadedConservations -> {
                conservations = result.conservations
            }
        }

        ConservationList(imageLoadingUtil, conservations.requireNoNulls())
    }
}

@Composable
fun ConservationList(
    imageLoadingUtil: ImageLoadingUtil,
    conservations: List<ConservationInfo>
) {
    if (conservations.isEmpty()) {
        EmptyDataSection(title = "hội thoại", modifier = Modifier.padding(top = 50.dp))
    } else {
        LazyList(
            items = conservations
        ) {
            ConservationItem(
                imageLoadingUtil,
                item = it
            ) {

            }
        }
    }

}

@Composable
fun ConservationItem(
    imageLoadingUtil: ImageLoadingUtil,
    item: ConservationInfo,
    onClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(vertical = 10.dp, horizontal = 10.dp),
        horizontalArrangement = Arrangement.spacedBy(12.dp)
    ) {
        imageLoadingUtil.ComposeImage(
            data = item.consultant.photoUrl,
            modifier = Modifier
                .clip(CircleShape)
                .border(1.dp, Color.White, CircleShape)
                .padding(10.dp)
                .align(Alignment.CenterVertically)
                .weight(2f, false)
        )
        Column(
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .weight(7f, true)
        ) {
            Text(
                text = item.consultant.displayName,
                style = AloThoTypography.body1,
                fontWeight = FontWeight.SemiBold
            )
            Text(
                text = item.lastMessage.orEmpty(),
                color = Color.Gray,
                style = AloThoTypography.body2,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
        Column(
            modifier = Modifier
                .align(Alignment.Bottom)
                .weight(1f, true),
            horizontalAlignment = Alignment.End
        ) {
            Text(
                text = "19 min",
                color = Lian_Color_LighterGray,
                style = AloThoTypography.body2,
                maxLines = 1,
                textAlign = TextAlign.Right,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}
