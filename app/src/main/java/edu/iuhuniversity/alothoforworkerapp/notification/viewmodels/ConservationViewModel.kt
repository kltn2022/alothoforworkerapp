package edu.iuhuniversity.alothoforworkerapp.notification.viewmodels

import android.app.Application
import android.os.Build
import androidx.compose.runtime.mutableStateOf
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.ConservationInfo
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message.ListenForConservationsUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ConservationViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val listenForConservationsUseCase: ListenForConservationsUseCase,
    @Assisted val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<ConservationViewModel.State>(State.Loading) {

    private var currentUser = mutableStateOf(User("", "", "", "", ""))

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            listenForConservationsUseCase: ListenForConservationsUseCase,
            dispatchersProvider: DispatchersProvider
        ): ConservationViewModel
    }

    sealed class State {
        object Loading : State()
        data class LoadedConservations(val conservations: List<ConservationInfo?>) : State()
        data class Error(val error: String) : State()
    }

    fun handleEvent(event: ConservationEvent) {
        when (event) {
            is ConservationEvent.LoadConservation -> loadConservations()
        }
    }

    private fun loadConservations() {
        var errorMessage = ""
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        listenForConservationsUseCase.execute()
                            .catch { exception ->
                                errorMessage = exception.message.orEmpty()
                            }
                            .collect { conservations ->
                                mutableState.value = State.LoadedConservations(conservations)
                                // get Current User
                                // get Worker by ID
                            }
                    }
                } else {
                    mutableState.value = State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }
}

sealed class ConservationEvent {
    object LoadConservation : ConservationEvent()
}
