package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.service

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service.ServiceRepository

class GetServiceByIdUseCase(private val serviceRepository: ServiceRepository) {
    suspend fun execute(id: String) = serviceRepository.getService(id)
}