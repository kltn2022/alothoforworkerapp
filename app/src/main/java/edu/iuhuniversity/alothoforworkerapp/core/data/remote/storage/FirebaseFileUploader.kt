package edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage

import android.content.Context
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.net.toFile
import androidx.core.net.toUri
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import edu.iuhuniversity.alothoforworkerapp.core.utils.FileUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.toStorageLocation
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.File
import java.util.*

class FirebaseFileUploader(
    private val context: Context,
    private val firebaseStorage: FirebaseStorage
) : StorageDataSource.Upload {
    private lateinit var fileUploaderCallback: FileUploaderCallback
    private var folderPath: String? = ""
    private lateinit var requestFiles: List<File>
    private lateinit var downloadedFiles: MutableList<String>
    private var uploadIndex = 0
    private var totalFileLength: Long = 0
    private var totalFileUploaded: Long = 0

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun uploadFiles(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    ) {
        this.fileUploaderCallback = fileUploaderCallback
        this.folderPath = folderPath
        this.uploadIndex = 0
        this.totalFileLength = 0
        this.totalFileUploaded = 0

        this.requestFiles = requestFiles.map { uri ->
            File(FileUtils.getPath(context, uri)!!)
        }

        this.requestFiles.let {
            try {
                this.totalFileLength += it.sumOf { file -> file.length() }
            } catch (e: Exception) {
                Timber.d(e)
            }
        }

        doUploadFiles()
    }

    @ExperimentalCoroutinesApi
    private suspend fun doUploadFiles() {
        if (requestFiles.isNotEmpty()) {
            requestFiles.forEach {
                uploadSingleFile(it)
            }
        } else {
            fileUploaderCallback.onFinish(downloadedFiles)
        }
    }

    @ExperimentalCoroutinesApi
    private suspend fun uploadSingleFile(file: File) {
        // generate storage ref with custom filename
        val uploadReference =
            folderPath?.createFileReference(firebaseStorage, file.name)
        return uploadReference
            ?.putFile(file.toUri())!!
            .getUploadFlow()
            .flowOn(Dispatchers.IO)
            .distinctUntilChanged()
            .flowOn(Dispatchers.Main)
            .collect { taskSnapshot ->
                val task = taskSnapshot.task
                task.addOnProgressListener {
                    if (requestFiles.size == 1) {
                        fileUploaderCallback.onProgressUpdate(
                            currentPercent = it.bytesTransferred,
                            totalPercent = it.totalByteCount,
                            fileNumber = 1
                        )
                    } else {
                        val currentPercent =
                            (it.bytesTransferred.div(it.totalByteCount)).times(100)
                        val totalPercent =
                            ((totalFileUploaded.plus(it.bytesTransferred)).div(totalFileLength)).times(
                                100
                            )
                        fileUploaderCallback.onProgressUpdate(
                            currentPercent, totalPercent, requestFiles.indexOf(file).plus(1).toLong()
                        )
                    }

                }
                if (task.isSuccessful) {
                    Timber.tag("downloadUrl").d(uploadReference.path.toStorageLocation(context))
                    uploadIndex++
                    totalFileUploaded.plus(file.length())

                    if (!this::downloadedFiles.isInitialized) {
                        downloadedFiles = mutableListOf()
                    }

                    val uploadPath = uploadReference.path.toStorageLocation(context)
                    downloadedFiles.add(uploadPath)

                    if (uploadIndex == requestFiles.size) {
                        fileUploaderCallback.onFinish(downloadedFiles)
                    }
                } else {
                    fileUploaderCallback.onError(task.exception)
                }
            }
    }

}

fun String.createFileReference(
    firebaseStorage: FirebaseStorage,
    fileName: String? = "noname",
): StorageReference {
    val customFileName = fileName.plus("_").plus(Date().time.toString())
    return firebaseStorage.reference.child(this.plus(customFileName))
}

interface FileUploaderCallback {
    fun onError(e: Exception?)
    fun onFinish(responses: List<String>?)
    fun onProgressUpdate(currentPercent: Long, totalPercent: Long, fileNumber: Long)
}

interface UploadTaskCallback {
    fun onError(e: Exception?)
    fun onFinish(taskSnapshot: UploadTask.TaskSnapshot)
    fun onProgress(taskSnapshot: UploadTask.TaskSnapshot)
}


@ExperimentalCoroutinesApi
fun UploadTask.getUploadFlow(): Flow<UploadTask.TaskSnapshot> {
    return callbackFlow {
        addOnProgressListener { taskSnapshot ->
            trySend(taskSnapshot)
        }
        addOnCompleteListener { it ->
            it.addOnSuccessListener {
                trySend(it)
            }
            it.addOnCanceledListener {
                Timber.d("Upload::Canceled")
                cancel(
                    message = "Upload canceled",
                    cause = exception
                )
                return@addOnCanceledListener
            }

            it.addOnFailureListener { ex ->
                Timber.d("Upload::Failure")
                cancel(
                    message = "Upload failed",
                    cause = ex
                )
                return@addOnFailureListener
            }
        }
        awaitClose {
            Timber.d("Cancelling downloading task")
            cancel()
        }
    }
}
