package edu.iuhuniversity.alothoforworkerapp.core.data.model.account

import android.os.Parcelable
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import edu.iuhuniversity.alothoforworkerapp.core.utils.CustomTypeParcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.io.Serializable

@Parcelize
data class User(
    val uid: String?,
    val displayName: String?,
    val phoneNumber: String?,
    val email: String?,
    val photoUrl: String?,
    val favoriteWorkerList: @WriteWith<CustomTypeParcelable.DocumentReferenceNullableListParceler> List<DocumentReference>? = null
) : Parcelable, Serializable {
    companion object {
        fun convertFrom(firebaseUser: FirebaseUser?): User? {
            if (firebaseUser != null) {
                return User(
                    uid = firebaseUser.uid,
                    displayName = firebaseUser.displayName.orEmpty(),
                    email = firebaseUser.email.orEmpty(),
                    phoneNumber = firebaseUser.phoneNumber.orEmpty(),
                    photoUrl = firebaseUser.photoUrl.takeIf { it != null }.toString()
                )
            }
            return null
        }

        fun DocumentSnapshot.toUser(): User {
            return User(
                uid = getString("uid").orEmpty(),
                displayName = getString("displayName").orEmpty(),
                email = getString("email").orEmpty(),
                phoneNumber = getString("phoneNumber").orEmpty(),
                photoUrl = getString("photoUrl").orEmpty()
            )
        }
    }
}
