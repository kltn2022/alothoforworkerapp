package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import edu.iuhuniversity.alothoforworkerapp.R

private val fontSfUiDisplay = FontFamily(
    fonts = listOf(
        Font(R.font.sf_ui_display_light, FontWeight.Light),
        Font(R.font.sf_ui_display_medium, FontWeight.Medium),
        Font(R.font.sf_ui_display_semibold, FontWeight.SemiBold),
        Font(R.font.sf_ui_display_regular, FontWeight.Normal),
    )
)

// Set of Material typography styles to start with
val Typography = Typography(
    body1 = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    button = TextStyle(
        fontFamily = fontSfUiDisplay,
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp,
        color = Color.White,
    ),
    caption = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    )
)

private val defaultTypography = Typography()
val AloThoTypography = Typography(
    h1 = defaultTypography.h1.copy(fontFamily = fontSfUiDisplay),
    h2 = defaultTypography.h2.copy(fontFamily = fontSfUiDisplay),
    h3 = defaultTypography.h3.copy(fontFamily = fontSfUiDisplay),
    h4 = defaultTypography.h4.copy(fontFamily = fontSfUiDisplay),
    h5 = defaultTypography.h5.copy(fontFamily = fontSfUiDisplay),
    h6 = defaultTypography.h6.copy(fontFamily = fontSfUiDisplay),
    subtitle1 = defaultTypography.subtitle1.copy(fontFamily = fontSfUiDisplay),
    subtitle2 = defaultTypography.subtitle2.copy(fontFamily = fontSfUiDisplay),
    body1 = defaultTypography.body1.copy(fontFamily = fontSfUiDisplay),
    body2 = defaultTypography.body2.copy(fontFamily = fontSfUiDisplay),
    button = defaultTypography.button.copy(fontFamily = fontSfUiDisplay),
    caption = defaultTypography.caption.copy(fontFamily = fontSfUiDisplay),
    overline = defaultTypography.overline.copy(fontFamily = fontSfUiDisplay)
)

