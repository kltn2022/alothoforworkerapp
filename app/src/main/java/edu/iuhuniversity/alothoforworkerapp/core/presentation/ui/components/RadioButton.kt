package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.foundation.selection.selectableGroup
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyGridFor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor

@Composable
fun LianRadioButton(
    modifier: Modifier = Modifier.selectableGroup(),
    options: List<String>,
    rowSize: Int = 1,
    onOptionSelectedListener: (String) -> Unit,
    customColors: RadioButtonColors? = null
) {
    val selectedOption = remember { mutableStateOf(options[0]) }

    LazyGridFor(
        items = options,
        rowSize = rowSize,
        modifier = Modifier.selectableGroup()
    ) {
        options.forEach { text ->
            Row(
                Modifier
                    .then(modifier)
                    .fillMaxWidth()
                    .wrapContentSize(align = Alignment.CenterStart)
                    .selectable(
                        selected = (selectedOption.value == text),
                        onClick = {
                            onOptionSelectedListener(text)
                            selectedOption.value = text
                        },
                        role = Role.RadioButton
                    )
                    .padding(vertical = 5.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                RadioButton(
                    selected = (selectedOption.value == text),
                    onClick = null,
                    colors = customColors
                        ?: RadioButtonDefaults.colors(
                            selectedColor = PrimaryColor,
                            unselectedColor = PrimaryVariantColor
                        )
                )
                Text(
                    text = text,
                    style = AloThoTypography.body1,
                    modifier = Modifier
                        .padding(start = 16.dp)
                )
            }
        }
    }
}