package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import android.os.Handler
import android.os.Looper

internal class AndroidEventRaiser : EventRaiser {
    private val mainThreadHandler: Handler = Handler(Looper.getMainLooper())
    override fun raiseEvent(r: Runnable?) {
        if (r != null) {
            mainThreadHandler.post(r)
        }
    }

}