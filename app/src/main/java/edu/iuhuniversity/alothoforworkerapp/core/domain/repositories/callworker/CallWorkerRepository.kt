package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.callworker

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker


interface CallWorkerRepository {
    // update current address
    // search worker within [x] km
    // listen current user [lat, long]

    suspend fun updateCurrentUserAddress(address: Address): Resource<Boolean>
    suspend fun searchWorkersWithin(radius: Long = 5): Resource<List<Worker>>
}