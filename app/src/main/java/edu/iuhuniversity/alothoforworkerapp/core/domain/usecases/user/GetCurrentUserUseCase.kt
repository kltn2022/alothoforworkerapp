package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository

class GetCurrentUserUseCase(private val workerRepository: WorkerRepository) {
    suspend fun execute() = workerRepository.getCurrentUser()
}