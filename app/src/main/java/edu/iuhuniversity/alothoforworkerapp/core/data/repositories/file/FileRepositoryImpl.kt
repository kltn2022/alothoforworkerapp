package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file

import android.net.Uri
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasource.FileRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file.FileRepository

class FileRepositoryImpl(
    private val fileRemoteDataSource: FileRemoteDataSource
) : FileRepository {
    override suspend fun getDownloadUrlFromStorage(storageUri: String): Resource<String> {
        fileRemoteDataSource.getDownloadUrl(storageUri)
            .let { resource ->
                if (resource.data != null) {
                    return Resource.Success(resource.data.toString())
                }
                return Resource.Error(message = resource.message!!)
            }
    }

    override suspend fun uploadFiles(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    ) {
        return fileRemoteDataSource.uploadFiles(folderPath, requestFiles, fileUploaderCallback)
    }


}