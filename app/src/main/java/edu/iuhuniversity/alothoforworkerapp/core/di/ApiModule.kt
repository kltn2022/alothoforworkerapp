package edu.iuhuniversity.alothoforworkerapp.core.di

import com.google.firebase.auth.FirebaseAuth
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.BuildConfig
import edu.iuhuniversity.alothoforworkerapp.core.data.preferences.MyPreferenceDataStore
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.api.RetrofitClientInstance
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.*
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.payment.PaymentService
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApiModule {
    private val BASE_FCM_URL = "https://fcm.googleapis.com/"
    private val DOMAIN_MOMO_SANDBOX = "https://test-payment.momo.vn/"

    @Singleton
    @Provides
    fun provideIFcmService(): IFcmService {
        return RetrofitClientInstance
            .getInstance(BASE_FCM_URL, BuildConfig.FCM_SERVER_KEY)
            .create(IFcmService::class.java)
    }

    @Singleton
    @Provides
    fun provideFirebaseSendMessageService(iFcmService: IFcmService): FirebaseSendMessageService =
        FirebaseSendMessageServiceImpl(iFcmService)

    @Singleton
    @Provides
    fun provideMoMoPaymentService(): PaymentService.Momo {
        return RetrofitClientInstance
            .getInstance(DOMAIN_MOMO_SANDBOX, "")
            .create(PaymentService.Momo::class.java)
    }
}