package edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction

enum class TransactionMethod(title: String) {
    ZALOPAY ("Ví ZaloPay"),
    VNPAY ("Ví VNPay"),
    MOMO ("Ví MoMo"),
}