package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.rounded.ArrowBackIosNew
import androidx.compose.material3.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.zIndex
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor

@Preview
@Composable
fun BasicTopAppBar() {
    Column {
        TopAppBar(
            title = { Text("Simple TopAppBar") },
            navigationIcon = {
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(Icons.Filled.Menu, contentDescription = null)
                }
            },
            actions = {
                // RowScope here, so these icons will be placed horizontally
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(Icons.Filled.Favorite, contentDescription = "Localized description")
                }
                IconButton(onClick = { /* doSomething() */ }) {
                    Icon(Icons.Filled.Favorite, contentDescription = "Localized description")
                }
            }
        )
        LinearProgressIndicator(
            modifier = Modifier.fillMaxWidth(),
            progress = 0.5f
        )
    }
}

@Composable
fun AloThoHomeTopAppBar(title: String? = null,) {
    val navigator = LocalNavigator.currentOrThrow

    SmallTopAppBar(
        title = {
            if (!title.isNullOrEmpty()) {
                Text(
                    title,
                    color = Color.White,
                    style = AloThoTypography.subtitle2,
                    fontWeight = FontWeight.SemiBold
                )
            }
        },
        navigationIcon = {
            if (navigator.canPop) {
                androidx.compose.material3.IconButton(onClick = {
                    navigator.pop()
                }) {
                    androidx.compose.material3.Icon(
                        painter = rememberVectorPainter(image = Icons.Rounded.ArrowBackIosNew),
                        contentDescription = null,
                        tint = Color.White,
                    )
                }
            }
        },
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = PrimaryColor,
        ),
        modifier = Modifier
            .zIndex(4f)
    )
}

@Composable
fun AloThoTopAppBar(
    modifier: Modifier = Modifier,
    title: String? = null,
    scrollBehavior: TopAppBarScrollBehavior? = null,
    onBackPressed: (() -> Unit)? = null,
    actions: @Composable RowScope.() -> Unit = {},
) {
    val navigator = LocalNavigator.currentOrThrow
    SmallTopAppBar(
        title = {
            if (!title.isNullOrEmpty()) {
                Text(
                    title,
                    color = Color.White,
                    style = AloThoTypography.subtitle2,
                    fontWeight = FontWeight.SemiBold
                )
            }
        },
        navigationIcon = {
            if (onBackPressed != null || navigator.canPop) {
                androidx.compose.material3.IconButton(onClick = {
                    if (navigator.canPop) navigator.pop()
                    if (onBackPressed != null) onBackPressed()
                }) {
                    androidx.compose.material3.Icon(
                        painter = rememberVectorPainter(image = Icons.Rounded.ArrowBackIosNew),
                        contentDescription = null,
                        tint = Color.White,
                    )
                }
            }
        },
        actions = actions,
        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
            containerColor = PrimaryColor,
        ),
        scrollBehavior = scrollBehavior,
        modifier = Modifier
            .zIndex(4f)
            .then(modifier)
    )
}
