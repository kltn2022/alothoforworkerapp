package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.callworker

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.callworker.CallWorkerRepository

class CallWorkerRepositoryImpl : CallWorkerRepository {
    override suspend fun updateCurrentUserAddress(address: Address): Resource<Boolean> {
        TODO("Not yet implemented")
    }

    override suspend fun searchWorkersWithin(radius: Long): Resource<List<Worker>> {
        TODO("Not yet implemented")
    }
}