package edu.iuhuniversity.alothoforworkerapp.core.presentation.model.mappers

import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.presentation.model.UiService
import javax.inject.Inject

class UiServiceMapper @Inject constructor(): UiMapper<Service, UiService> {
    override suspend fun mapToView(input: Service): UiService {
        return UiService(
            id = input.id,
            name = input.name
        )
    }

    override suspend fun mapToView(input: List<Service?>, output: (List<UiService>) -> Unit) {
        TODO("Not yet implemented")
    }

    override suspend fun mapToView(input: Service, output: (UiService) -> Unit) {
        TODO("Not yet implemented")
    }
}