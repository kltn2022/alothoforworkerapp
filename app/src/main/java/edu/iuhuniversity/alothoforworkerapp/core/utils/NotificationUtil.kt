package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.app.RemoteInput
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.R

////////////////////////
const val CALL_WORKER_CHANNEL_ID = "7005"

fun displayNotificationA(
    context: Context,
    notificationId: Int,
    title: String,
    content: String,
    channelId: String,
    @DrawableRes icon: Int? = null,
    autoCancel: Boolean? = true,
    priority: Int? = NotificationCompat.PRIORITY_HIGH,
    contentIntent: PendingIntent? = null,
    vararg actions: NotificationCompat.Action
) {
    val notification = NotificationCompat.Builder(context, channelId)
        .apply {
            setContentTitle(title)
            setContentText(content)
            icon?.let { setSmallIcon(it) }
            autoCancel?.let { setAutoCancel(it) }
            priority?.let { setPriority(priority) }
            contentIntent?.let { setContentIntent(contentIntent) }
            if (actions.isNotEmpty()) {
                actions.forEach { addAction(it) }
            }
        }
        .build()
    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
        notificationId,
        notification
    )
}

fun displayNotification(
    context: Context,
    notificationId: Int,
    fcmNotification: FcmNotification,
    autoCancel: Boolean? = true,
    priority: Int? = NotificationCompat.PRIORITY_HIGH,
    contentIntent: PendingIntent? = null,
    vararg actions: NotificationCompat.Action
) {
    val notification = NotificationCompat.Builder(context, CALL_WORKER_CHANNEL_ID)
        .apply {
            setContentTitle(fcmNotification.title)
            setContentText(fcmNotification.body)
            if (fcmNotification.icon.isNullOrEmpty().not()) {
                setSmallIcon(getDrawableIdByName(fcmNotification.icon, context))
            } else {
                setSmallIcon(R.mipmap.ic_launcher_round)
            }
            autoCancel?.let { setAutoCancel(it) }
            priority?.let { setPriority(priority) }
            contentIntent?.let { setContentIntent(contentIntent) }
            if (actions.isNotEmpty()) {
                actions.forEach { addAction(it) }
            }
        }
        .build()
    (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).notify(
        notificationId,
        notification
    )
}

/**
 * You should call this method in order to create notification channel in Application or
 * MainActivity Class
 */
fun createNotificationChannel(
    context: Context,
    channelId: String,
    channelName: String,
    channelDescription: String
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val importance = NotificationManager.IMPORTANCE_HIGH
        val channel = NotificationChannel(channelId, channelName, importance).apply {
            description = channelDescription
        }
        (context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
            channel
        )
    }
}

fun buildDirectReplyAction(
    replyResultKey: String,
    inputPlaceholder: String,
    replyAction: NotificationCompat.Action,
): NotificationCompat.Action {
    val remoteInput: RemoteInput = RemoteInput.Builder(replyResultKey).run {
        setLabel(inputPlaceholder)
        build()
    }
    return NotificationCompat.Action.Builder(replyAction)
        .addRemoteInput(remoteInput)
        .build()
}

fun receiveRemoteInput(
    intent: Intent,
    onReceivedRemoteInput: (Bundle?) -> Unit
) {
    val remoteInput = RemoteInput.getResultsFromIntent(intent)
    remoteInput?.let { onReceivedRemoteInput(remoteInput) }
}