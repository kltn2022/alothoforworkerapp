package edu.iuhuniversity.alothoforworkerapp.core.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable
import java.util.*

@Parcelize
data class Offer(
    var id: String,
    var title: String,
    var subtitle: String?,
    var content: String,
    var thumbnail: String,
    var createdAt: Date = Date(),
    var code: String? = null
) : Parcelable, Serializable


