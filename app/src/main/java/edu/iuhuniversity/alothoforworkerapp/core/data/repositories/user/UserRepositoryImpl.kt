package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user

import com.google.firebase.auth.FirebaseUser
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasource.UserRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository

class UserRepositoryImpl(
    private val userRemoteDataSource: UserRemoteDataSource
) : UserRepository {
    override suspend fun getUser(uid: String): Resource<User?> = userRemoteDataSource.getUser(uid)

    override suspend fun getUserByPhoneNumber(phoneNumber: String): Resource<User?> =
        userRemoteDataSource.getUserByPhoneNumber(phoneNumber)

    override suspend fun insertUser(firebaseUser: FirebaseUser): Resource<User?> =
        userRemoteDataSource.insertUser(firebaseUser)

    override suspend fun updateUserInfo(
        uid: String,
        data: MutableMap<String, Any>
    ): Resource<Boolean> = userRemoteDataSource.updateUserInfo(uid, data)


}