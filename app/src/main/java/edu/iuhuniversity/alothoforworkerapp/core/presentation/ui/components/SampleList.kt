package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
// import org.mingroup.completekotlinjetpackcompose.data.model.MallCategory
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography

@Composable
fun InsuranceListItem(
    title: String,
    subtitle: String,
    resId: Int,
    onClick: (() -> Unit) = {}
) {
    Card(
        shape = RoundedCornerShape(10.dp),
        modifier = Modifier
            .fillMaxWidth()
            .height(280.dp)
            .clip(RoundedCornerShape(10.dp))
            .clickable(onClick = onClick)
    ) {
        Image(
            painter = painterResource(id = resId),
            contentDescription = "",
            colorFilter = ColorFilter.colorMatrix(
                ColorMatrix().apply {
                    this.setToSaturation(0f)
                }),
            contentScale = ContentScale.Crop,
            modifier = Modifier
        )
        Column(
            verticalArrangement = Arrangement.Bottom,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    brush = Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Black.copy(alpha = 0.8f),
                            Color.Black,
                        )
                    )
                )
                .padding(
                    horizontal = 15.dp,
                    vertical = 10.dp
                )
        ) {
            Text(
                text = title,
                color = Color.White,
                style = AloThoTypography.h5.copy(fontWeight = FontWeight.SemiBold),
                modifier = Modifier
                    .fillMaxWidth()
            )
            Text(
                text = subtitle,
                color = Color.White,
                style = AloThoTypography.subtitle2.copy(fontWeight = FontWeight.Light),
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .fillMaxWidth()
            )
        }
    }
}

//@Composable
//fun BrandListItem(
//    title: String,
//    subtitle: String,
//    width: Dp = 140.dp,
//    height: Dp = 210.dp,
//    cornerSize: Dp = 20.dp,
//    onClick: () -> Unit
//) {
//    Box(
//        modifier = Modifier
//            .size(
//                width = width,
//                height = height,
//            )
//            .clip(
//                shape = RoundedCornerShape(
//                    corner = CornerSize(cornerSize)
//                )
//            )
//            .clickable(onClick = onClick)
//    ) {
//        Image(
//            painter = painterResource(id = R.drawable.kem_duong),
//            contentDescription = "thumbnail",
//            modifier = Modifier
//                .fillMaxSize()
//                .clip(RoundedCornerShape(20.dp)),
//            contentScale = ContentScale.Crop
//        )
//        Image(
//            painter = painterResource(id = R.drawable.ic_logo_ab),
//            contentDescription = "logo",
//            alignment = Alignment.TopEnd,
//            modifier = Modifier
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 10.dp
//                )
//                .fillMaxWidth()
//                .size(20.dp)
//        )
//        Column(
//            verticalArrangement = Arrangement.Bottom,
//            modifier = Modifier
//                .fillMaxHeight()
//                .fillMaxWidth()
//                .background(
//                    brush = Brush.verticalGradient(
//                        colors = listOf(
//                            Color.Transparent,
//                            Color.Transparent,
//                            Color.Black
//                        )
//                    )
//                )
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 15.dp
//                )
//        ) {
//            Text(
//                text = title,
//                color = Color.White,
//                style = LianTypography.h6.copy(
//                    fontWeight = FontWeight.SemiBold,
//                    fontSize = 16.sp
//                )
//            )
//            Text(
//                text = subtitle,
//                color = Color.White,
//                style = MaterialTheme.typography.caption
//            )
//        }
//    }
//}
//
//@Composable
//fun PromotionListItem(
//    width: Dp = 350.dp,
//    height: Dp = 130.dp,
//    cornerSize: Dp = 20.dp,
//    selectedItem: (String) -> (Unit)
//) {
//    val roundedCornerShape = RoundedCornerShape(cornerSize)
//    Box(
//        modifier = Modifier
//            .size(
//                width = width,
//                height = height,
//            )
//            .clip(
//                shape = roundedCornerShape
//            )
//            .background(
//                color = PrimaryVariantColor
//            )
//            .clickable { selectedItem("Item selected") }
//    ) {
//        Image(
//            painter = painterResource(id = R.drawable.bao_hiem_tru_cot_gia_dinh),
//            contentDescription = "thumbnail",
//            modifier = Modifier
//                .fillMaxSize()
//                .clip(roundedCornerShape),
//            contentScale = ContentScale.Crop
//        )
//        Image(
//            painter = painterResource(id = R.drawable.ic_logo_ab),
//            contentDescription = "logo",
//            alignment = Alignment.TopStart,
//            modifier = Modifier
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 10.dp
//                )
//                .fillMaxWidth()
//                .size(20.dp)
//        )
//    }
//}
//
//@Composable
//fun ProductListItem(
//    selectedItem: () -> Unit
//) {
//    Box(
//        modifier = Modifier
//            .fillMaxWidth()
//            .height(160.dp)
//            .clip(
//                shape = RoundedCornerShape(
//                    corner = CornerSize(20.dp)
//                )
//            )
//            .background(
//                color = PrimaryVariantColor
//            )
//            .clickable(onClick = selectedItem)
//    ) {
//        Image(
//            painter = painterResource(id = R.drawable.kem_duong),
//            contentDescription = "thumbnail",
//            contentScale = ContentScale.Crop,
//            modifier = Modifier
//                .fillMaxSize()
//                .clip(RoundedCornerShape(20.dp))
//        )
//        Image(
//            painter = painterResource(id = R.drawable.ic_logo_ab),
//            contentDescription = "logo",
//            alignment = Alignment.TopEnd,
//            modifier = Modifier
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 10.dp
//                )
//                .fillMaxWidth()
//                .size(30.dp)
//        )
//        Column(
//            verticalArrangement = Arrangement.Bottom,
//            modifier = Modifier
//                .fillMaxHeight()
//                .fillMaxWidth()
//                .gradientBackground(
//                    colors = listOf(
//                        Color.Transparent,
//                        Color.Transparent,
//                        Color.Black.copy(alpha = 0.5f)
//                    ),
//                    angle = 270f
//                )
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 15.dp
//                )
//        ) {
//            Text(
//                text = "Bộ chăm sóc da mặt Dior",
//                color = Color.White,
//                fontSize = 14.sp,
//                fontWeight = FontWeight.Normal
//            )
//            Text(
//                text = "1.240.000 đ",
//                color = Color.White,
//                style = MaterialTheme.typography.h6,
//                fontWeight = FontWeight.SemiBold
//            )
//        }
//    }
//}
//
//@Composable
//fun CategoryListItem(
//    mallCategory: MallCategory,
//    selectedItem: (MallCategory) -> Unit
//) {
//    Box(
//        modifier = Modifier
//            .size(size = 120.dp)
//            .clip(
//                shape = RoundedCornerShape(
//                    corner = CornerSize(20.dp)
//                )
//            )
//            .background(
//                color = Color.White
//            )
//            .clickable { selectedItem(mallCategory) }
//    ) {
//        Image(
//            painter = painterResource(id = R.drawable.bao_hiem_mai_am),
//            contentDescription = "thumbnail",
//            contentScale = ContentScale.Crop,
//            modifier = Modifier
//                .fillMaxSize()
//                .clip(RoundedCornerShape(20.dp))
//        )
//        Column(
//            verticalArrangement = Arrangement.SpaceBetween,
//            modifier = Modifier
//                .fillMaxHeight()
//                .fillMaxWidth()
//                .background(
//                    color = Color.Black.copy(alpha = 0.6f),
//                    shape = RoundedCornerShape(
//                        corner = CornerSize(20.dp),
//                    )
//                )
//                .padding(
//                    horizontal = 10.dp,
//                    vertical = 15.dp
//                )
//        ) {
//            Text(
//                text = mallCategory.name!!,
//                color = Color.White,
//                fontSize = 16.sp,
//                style = MaterialTheme.typography.h6,
//                fontWeight = FontWeight.Bold
//            )
//            Text(
//                text = "${mallCategory.totalProduct} sản phẩm",
//                color = Color.White,
//                fontSize = 11.sp,
//                style = MaterialTheme.typography.body1
//            )
//        }
//    }
//}
//
//enum class ListType {
//    HORIZONTAL,
//    VERTICAL
//}
//
//@Composable
//fun <T> ListWithTitle(
//    title: String,
//    type: ListType = ListType.VERTICAL,
//    dataList: List<T>,
//    spaceBetweenItem: Dp = 10.dp,
//    modifier: Modifier,
//    itemContent:  @Composable() (LazyItemScope.(T) -> Unit)
//) {
//    Column(
//        modifier = modifier
//            .padding(
//                vertical = 10.dp
//            ),
//        verticalArrangement = Arrangement.spacedBy(spaceBetweenItem)
//    ) {
//        Text(
//            text = title,
//            fontWeight = FontWeight.Bold,
//            style = MaterialTheme.typography.subtitle1,
//            modifier = Modifier
//                .padding(start = 5.dp)
//        )
//        if (type == ListType.HORIZONTAL) {
//            LazyRow(
//                modifier = Modifier
//                    .fillMaxWidth(),
//                horizontalArrangement = Arrangement.spacedBy(spaceBetweenItem)
//            ) {
//                items(
//                    items = dataList,
//                    itemContent = itemContent
//                )
//            }
//        } else {
//            LazyColumn(
//                verticalArrangement = Arrangement.spacedBy(spaceBetweenItem)
//            ) {
//                items(
//                    items = dataList,
//                    itemContent = itemContent
//                )
//            }
//        }
//
//    }
//}
//
//
//
//@Preview(showBackground = true)
//@Composable
//fun Preview() {
////    BrandListItem(
////        title = "Dior",
////        subtitle = "Lian Corporation",
////    ) {
////
////    }
//
////    ProductListItem {
////
////    }
//
//    val category = MallCategory("1", "Kem dưỡng da", 2, "")
//    CategoryListItem(mallCategory = category) {
//
//    }
//}