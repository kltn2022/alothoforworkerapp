package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.annotations.SerializedName
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.RoomTypeConverters
import java.util.*

/**
 * Refer: https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support
 */
@Entity(tableName = "TBL_NOTIFICATION")
@TypeConverters(RoomTypeConverters.DateConverter::class)
data class FcmNotification(
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @ColumnInfo
    @SerializedName("title")
    val title: String, // The notification's title.
    @ColumnInfo
    @SerializedName("body")
    val body: String, // The notification's body text.
    @ColumnInfo
    @SerializedName("icon")
    val icon: String? = null, // The URL to use for the notification's icon.
    @ColumnInfo
    @SerializedName("android_channel_id")
    val channelId: String? = null, // The notification's channel id (new in Android O).
    @ColumnInfo
    @SerializedName("sound") // The sound to play when the device receives the notification. Sound files must reside in /res/raw/.
    val sound: String? = null,
    @ColumnInfo
    @SerializedName("color")
    val color: String? = null, // The notification's icon color, expressed in #rrggbb format.
    @ColumnInfo
    @SerializedName("click_action")
    val clickAction: String? = null, // For all URL values, HTTPS is required.
    @ColumnInfo
    val timestamp: Date? = Date()
) {
    companion object {
        fun RemoteMessage.Notification.toFcmNotification(
            notificationId: Long? = null
        ): FcmNotification {
            return FcmNotification(
                id = notificationId,
                title = this.title!!,
                body = this.body!!,
                icon = this.icon,
                clickAction = this.clickAction
            )
        }
    }
}