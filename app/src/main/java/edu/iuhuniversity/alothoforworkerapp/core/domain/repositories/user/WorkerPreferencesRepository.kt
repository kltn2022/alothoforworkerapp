package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import kotlinx.coroutines.flow.Flow

interface WorkerPreferencesRepository {
    val currentUserUid: Flow<String?>
    val workerPreference: Flow<Worker?>
    val authToken: Flow<String?>
    suspend fun saveAuthToken(token: String)
    suspend fun saveCurrentWorkerInfo(firebaseUser: FirebaseUser)
    suspend fun saveRememberedLoginSession(phone: String, password: String)
    suspend fun updateCurrentWorkerInfo(request: UserProfileChangeRequest)
    suspend fun logout()
}