package edu.iuhuniversity.alothoforworkerapp.core.data.model.order

import android.os.Parcelable
import android.text.TextUtils
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
enum class OrderStatus(
    val title: String,
    val color: String
) : Parcelable, Serializable {
    // KHÁCH ĐẶT -> ALLOCATING
    // THỢ NHẬN -> ĐANG ĐỢI LÀM VIỆC X
    // ON_THE_MOVE -> THỢ ĐẾN LÀM X
    // ARRIVED -> THỢ X
    // WORKING -> ĐANG LÀM VIỆC (THỢ) X
    // COMPLETED -> HOÀN THÀNH (THỢ) X
    // ĐÃ THANH TOÁN -> KHÁCH Y
    ALLOCATING("Đang tìm thợ", "#3298dc") {
        override fun nextStatus(): OrderStatus? {
            return WAITING_FOR_WORK
        }

    },
    WAITING_FOR_WORK("Đang đợi làm việc", "#3298dc") {
        override fun nextStatus(): OrderStatus? {
            return ON_THE_MOVE
        }

    },
    ON_THE_MOVE("Đang di chuyển", "#17a2b8") {
        override fun nextStatus(): OrderStatus? {
            return ARRIVED
        }
    },
    ARRIVED("Đã đến nơi", "#FFA900") {
        override fun nextStatus(): OrderStatus? {
            return WORKING
        }
    },
    WORKING("Đang làm việc", "#006064") {
        override fun nextStatus(): OrderStatus? {
            return COMPLETED
        }

    },
    COMPLETED("Đã hoàn thành", "#48c774") {
        override fun nextStatus(): OrderStatus? {
            return null
        }
    },
    PURCHASED("Đã thanh toán", "#53c748") {
        override fun nextStatus(): OrderStatus? {
            return null
        }
    },
    CANCELED("Đơn đã từ chối", "#f14668") {
        override fun nextStatus(): OrderStatus? {
            return null
        }
    };

    abstract fun nextStatus(): OrderStatus?

    companion object {
        fun getOrderStatusByTitle(title: String?): OrderStatus? {
            if (!TextUtils.isEmpty(title)) {
                for (orderStatus in values()) {
                    if (orderStatus.title == title) {
                        return orderStatus
                    }
                }
            }
            return null
        }
    }
}

val workerStatusActions: List<Pair<OrderStatus, String>> = listOf(
    OrderStatus.WAITING_FOR_WORK to "Bắt đầu di chuyển",
    OrderStatus.ON_THE_MOVE to "Đã đến nơi",
    OrderStatus.ARRIVED to "Bắt đầu làm việc",
    OrderStatus.WORKING to "Hoàn thành công việc",
    OrderStatus.COMPLETED to "",
    OrderStatus.CANCELED to "",
)