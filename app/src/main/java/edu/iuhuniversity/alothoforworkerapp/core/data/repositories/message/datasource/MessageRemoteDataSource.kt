package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasource

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Conservation
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Message
import kotlinx.coroutines.flow.Flow

interface MessageRemoteDataSource {
    suspend fun sendMessage(message: Message) : Resource<Message?>
    suspend fun listenForChatMessages(conservationId: String): Flow<List<Message?>>
    suspend fun getConservationInfo(conservationId: String): Resource<Conservation?>
    suspend fun getConservations(): Flow<List<Conservation?>>
}