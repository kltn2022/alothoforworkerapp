package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.firebase.firestore.GeoPoint

import com.google.firebase.firestore.QueryDocumentSnapshot

class GeoQueryDocumentChange(
    var documentSnapshot: QueryDocumentSnapshot,
    var geoPoint: GeoPoint
)

