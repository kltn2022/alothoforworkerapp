package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
val VNP_DATE_FORMAT = SimpleDateFormat("yyyyMMddHHmmss")

@SuppressLint("SimpleDateFormat")
val TIME_FORMAT_12 = SimpleDateFormat("KK:mm aa")

@SuppressLint("SimpleDateFormat")
val TIME_FORMAT_24 = SimpleDateFormat("HH:mm")

@SuppressLint("SimpleDateFormat")
val DATE_FORMAT = SimpleDateFormat("dd/MM/yyyy")

fun Date.toTime24Format(): String {
    return TIME_FORMAT_24.format(this)
}

fun convertToTime24Format(date: Date): String {
    return TIME_FORMAT_24.format(date)
}

fun Date.toTime12Format(): String {
    return TIME_FORMAT_12.format(this)
}

fun Date.toDateFormat(): String {
    return DATE_FORMAT.format(this)
}

fun convertToDateFormat(date: Date): String {
    return DATE_FORMAT.format(date)
}