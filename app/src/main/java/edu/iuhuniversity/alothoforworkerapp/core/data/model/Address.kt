package edu.iuhuniversity.alothoforworkerapp.core.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Address(
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val fullAddress: String = ""
) : Parcelable
