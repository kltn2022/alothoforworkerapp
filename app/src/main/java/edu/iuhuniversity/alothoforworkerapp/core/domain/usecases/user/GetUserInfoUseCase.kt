package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository

class GetUserInfoUseCase(private val userRepository: UserRepository) {
    suspend fun execute(uid: String) = userRepository.getUser(uid)
}