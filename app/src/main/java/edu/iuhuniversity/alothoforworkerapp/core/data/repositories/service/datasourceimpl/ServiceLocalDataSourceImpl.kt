package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasourceimpl

import edu.iuhuniversity.alothoforworkerapp.core.data.cache.service.ServiceDao
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource

class ServiceLocalDataSourceImpl(
    private val serviceDao: ServiceDao
) : ServiceDataSource.Cache {
    override suspend fun getServicesByTypeFromDb(serviceTypeId: String): MutableList<Service> {
        return serviceDao.getServicesByServiceType(serviceTypeId)
            .map {
                it.toDomain()
            }
            .toMutableList()
    }

    override suspend fun getServiceById(serviceId: String): Service? {
        return serviceDao.getServiceById(serviceId)?.toDomain()
    }


}