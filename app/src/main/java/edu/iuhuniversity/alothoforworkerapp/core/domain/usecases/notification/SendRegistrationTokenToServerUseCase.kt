package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository

class SendRegistrationTokenToServerUseCase(private val tokenRepository: TokenRepository) {
    suspend fun execute(token: RegistrationToken) = tokenRepository.sendRegistrationToServer(token)
}