package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import timber.log.Timber

class OrderRepositoryImpl(
    private val orderCacheDataSource: OrderDataSource.Cache,
    private val orderRemoteDataSource: OrderDataSource.Remote,
    //private val orderMapper: UiOrderMapper
) : OrderRepository {

    override suspend fun getOrdersFromCache(): Resource<List<Order>> {
        lateinit var orderList: List<Order>
        try {
            orderList = orderCacheDataSource.getOrdersFromDb()
        } catch (e: Exception) {
            Timber.i(e.message!!)
            return Resource.Error(e.message!!)
        }

        if (orderList.isNotEmpty()) {
            return Resource.Success(orderList)
        } else {
            orderList = orderRemoteDataSource.getOrders().first() as List<Order>
        }
        return Resource.Success(orderList)
    }

    override suspend fun getOrderFromCache(orderId: String): Resource<Order?> {
        var order: Order?
        try {
            order = orderCacheDataSource.getOrderById(orderId)
        } catch (e: Exception) {
            Timber.i(e.message!!)
            return Resource.Error(e.message!!)
        }

        return Resource.Success(order)
    }

    override suspend fun insertOrder(order: Order): Resource<Order?> {
        return orderRemoteDataSource.insertOrder(order)
    }


    override suspend fun getOrders(): Resource<List<Order>> {
        return getOrdersFromCache()
    }

//    override suspend fun getOrders(): Resource<List<UiOrder>> {
//        when (val result = getOrdersFromCache()) {
//            is Resource.Success -> {
//                return Resource.Success(
//                    result.data?.map {
//                        orderMapper.mapToView(it)
//                    } ?: emptyList()
//                )
//            }
//            is Resource.Error -> Resource.Error(result.message.orEmpty(), null)
//            is Resource.Loading -> Resource.Loading()
//        }
//    }

    override suspend fun updateOrder(
        order: Order
    ): Resource<Boolean> {
        // orderCacheDataSource.updateOrder(order)
        return orderRemoteDataSource.updateOrder(order)
    }

    override suspend fun updateOrderStatus(
        orderId: String,
        status: OrderStatus
    ): Resource<Boolean> {
        // orderCacheDataSource.updateOrder(getOrder(orderId).data!!.copy(status = status))
        return orderRemoteDataSource.updateOrderStatus(orderId, status)
    }

    override suspend fun updateOrders(): List<Order> {
        //orderCacheDataSource.clearAll()
        (orderRemoteDataSource.getOrders().first() as List<Order>).let { list ->
            orderCacheDataSource.saveOrdersToDb(list)
            return list
        }
    }

    override suspend fun getOrder(id: String): Resource<Order?> {
        return orderRemoteDataSource.getOrder(id)
    }

    override suspend fun listenRealtimeOrders(): Flow<List<Order?>> {
        return orderRemoteDataSource.getOrders()
    }

    override suspend fun listenRealtimeOrder(orderId: String): Flow<Order?> {
        return orderRemoteDataSource.listenRealtimeOrder(orderId)
    }


}