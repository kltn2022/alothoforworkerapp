package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.fcm

import edu.iuhuniversity.alothoforworkerapp.core.constant.CallWorkerConst
import edu.iuhuniversity.alothoforworkerapp.core.constant.NotificationType
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.IFcmService
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmData
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.SendNotificationRepository

class SendNotificationRepositoryImpl(
    private val iFcmService: IFcmService,
) : SendNotificationRepository {
    override suspend fun sendNotificationToCustomer(
        token: String?,
        order: Order
    ): Resource<FcmResponse> {
        // lấy token của customer
        // gửi qua token đó
        if (token != null) {
            val data = FcmMessagePayload(
                to = token,
                data = FcmData(
                    type = NotificationType.RECEIVE_CALL_WORKER_NOTIFICATION.toString(),
                    body = order.id
                ),
                notification = FcmNotification(
                    title = "Chúc mừng! Đã có thợ nhận đơn hàng của bạn",
                    body = "Nhấn vào để biết thêm chi tiết",
                ),
            )

            val response = iFcmService.sendMessage(data)
            return if (response.isSuccessful) {
                Resource.Success(response.body()!!)
            } else {
                Resource.Error(response.message())
            }
        } else {
            return Resource.Error("Token không hợp lệ!")
        }
    }
}