package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel

data class CarouselItem(
    val id: Int,
    val title: String,
    val subtitle: String,
    val imageId: Int,
    val source: String = "demo source"
)

data class CarouselSliderItem(
    val id: Int,
    val imageId: Int
)