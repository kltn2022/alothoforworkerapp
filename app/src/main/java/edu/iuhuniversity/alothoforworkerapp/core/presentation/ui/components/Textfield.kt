package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Error
import androidx.compose.material.icons.filled.Password
import androidx.compose.material.icons.twotone.Backpack
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import timber.log.Timber

@Composable
fun CustomTextField(
    modifier: Modifier = Modifier,
    value: String,
    label: String? = null,
    placeholder: String? = "",
    trailingIcon: ImageVector? = null,
    trailingIconOnClick: (() -> Unit) = {},
    error: String,
    primaryColor: Color = Lian_Color_Text,
    textColor: Color = Color.Black,
    singleLine: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    contentAlignment: Alignment = Alignment.CenterStart,
    onValueChange: (String) -> Unit,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default
) {
    val textState = remember {
        mutableStateOf(value)
    }
    val focusState = remember {
        mutableStateOf(false)
    }
    Column(modifier = modifier) {
        if (!label.isNullOrEmpty()) {
            Text(
                text = label,
                color = if (error.isNotEmpty()) Lian_Color_Error else primaryColor,
                style = AloThoTypography.subtitle1,
                fontWeight = FontWeight.Medium,
                fontSize = 18.sp
            )
        }
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Box(
                contentAlignment = contentAlignment,
                modifier = Modifier.weight(9f)
            ) {
                BasicTextField(
                    value = value,
                    onValueChange = {
                        onValueChange(it)
                        textState.value = it
                    },
                    modifier = Modifier
                        .onFocusEvent { state ->
                            focusState.value to state.hasFocus
                            Timber.i(focusState.value.toString())
                        }
                        .padding(vertical = 7.dp),
                    textStyle = AloThoTypography.body1.copy(color = textColor),
                    singleLine = singleLine,
                    maxLines = maxLines,
                    readOnly = readOnly,
                    enabled = enabled,
                    keyboardActions = keyboardActions,
                    keyboardOptions = keyboardOptions
                )
                if (textState.value.isEmpty()) {
                    Text(
                        text = placeholder ?: "",
                        style = AloThoTypography.body1.copy(color = textColor),
                    )
                }
            }
            if (error.isNotEmpty()) {
                TextFieldIcon(
                    Modifier
                        .weight(1f),
                    Icons.Filled.Error,
                    Lian_Color_Error
                )
            } else {
                if (trailingIcon != null) {
                    TextFieldIcon(
                        Modifier
                            .weight(1f),
                        trailingIcon,
                        primaryColor,
                        onClick = trailingIconOnClick
                    )
                }
            }
        }
        Divider(
            color = if (error.isNotEmpty()) Lian_Color_Error else primaryColor,
            thickness = if (focusState.value) {
                3.dp
            } else {
                2.dp
            }
        )
        if (error.isNotEmpty()) {
            Text(
                text = error,
                color = Lian_Color_Error,
                style = AloThoTypography.caption.copy(fontSize = 13.sp),
                modifier = Modifier
                    .padding(
                        top = 5.dp,
                        end = if (trailingIcon != null) 36.dp else 0.dp
                    ),
            )
        }

    }
}

data class Border(
    val width: Dp,
    val color: Color
)

@Composable
fun RoundedTextField(
    modifier: Modifier = Modifier,
    label: String,
    value: String,
    leadingIcon: ImageVector? = null,
    trailingIcon: ImageVector? = null,
    leadingIconOnClick: (() -> Unit) = {},
    trailingIconOnClick: (() -> Unit) = {},
    error: String,
    singleLine: Boolean = true,
    maxLines: Int = Int.MAX_VALUE,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    onValueChange: (String) -> Unit,
    keyboardActions: KeyboardActions,
    cornerSize: Dp = 10.dp,
    backgroundColor: Color = Lian_Color_LightGray,
    textColor: Color = Color.Black,
    labelTextColor: Color = Lian_Color_Text_Ab,
    iconColor: Color = Lian_Color_Text_Ab,
    border: Border = Border(0.dp, Color.Transparent),
) {
    val textState = remember {
        mutableStateOf(value)
    }
    val focusState = remember {
        mutableStateOf(false)
    }
    Column {
        Row(
            modifier = modifier
                .background(backgroundColor, shape = RoundedCornerShape(cornerSize))
                .border(
                    width = if (error.isNotEmpty()) {
                        2.dp
                    } else border.width,
                    color = if (error.isNotEmpty()) {
                        Lian_Color_Error
                    } else {
                        border.color
                    },
                    shape = RoundedCornerShape(cornerSize)
                )
                .padding(horizontal = 0.dp, vertical = 8.dp)
                .clip(RoundedCornerShape(cornerSize)),
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (leadingIcon != null) {
                TextFieldIcon(
                    imageVector = leadingIcon,
                    tint = iconColor,
                    modifier = Modifier
                        .weight(1f),
                    onClick = leadingIconOnClick
                )
            }
            Box(
                contentAlignment = Alignment.CenterStart,
                modifier = Modifier
                    .weight(8f)
            ) {
                BasicTextField(
                    value = value,
                    onValueChange = {
                        onValueChange(it)
                        textState.value = it
                    },
                    modifier = Modifier
                        .onFocusEvent { state ->
                            focusState to state.hasFocus
                        }
                        .padding(vertical = 7.dp, horizontal = 0.dp),
                    textStyle = AloThoTypography.body1.copy(
                        color = textColor,
                        fontWeight = FontWeight.SemiBold
                    ),
                    singleLine = singleLine,
                    maxLines = maxLines,
                    readOnly = readOnly,
                    enabled = enabled,
                    keyboardActions = keyboardActions,
                )
                if (textState.value.isEmpty()) {
                    Text(
                        text = label,
                        style = AloThoTypography.body1,
                        color = labelTextColor,
                        //modifier = Modifier.padding(horizontal = 8.dp)
                    )
                }
            }
            if (error.isNotEmpty()) {
                TextFieldIcon(
                    Modifier
                        .weight(1f),
                    Icons.Filled.Error,
                    Lian_Color_Error
                )
            } else {
                if (trailingIcon != null) {
                    TextFieldIcon(
                        imageVector = trailingIcon,
                        tint = iconColor,
                        modifier = Modifier
                            .weight(1f),
                        onClick = trailingIconOnClick
                    )
                }
            }
        }

        if (error.isNotEmpty()) {
            Text(
                text = error,
                color = Lian_Color_Error,
                style = AloThoTypography.caption.copy(fontSize = 13.sp),
                modifier = Modifier
                    .padding(
                        vertical = 5.dp,
                        horizontal = 16.dp
                    ),
            )
        }
    }
}

@Composable
fun TextFieldIcon(
    modifier: Modifier = Modifier,
    imageVector: ImageVector,
    tint: Color = Lian_Color_Text,
    onClick: (() -> Unit) = { }
) {
    IconButton(
        onClick = onClick
    ) {
        Icon(
            imageVector = imageVector,
            contentDescription = "icon",
            tint = tint,
            modifier = modifier
                .size(24.dp)
        )
    }


}

@Composable
fun OutlinedTextfield(
    label: String? = null,
    value: String,
    primaryColor: Color = Lian_Color_Text,
    error: String,
    placeholder: String? = "",
    trailingIcon: ImageVector? = null,
    trailingIconOnClick: (() -> Unit) = {},
    textColor: Color = Color.Black,
    singleLine: Boolean = true,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    onValueChange: (String) -> Unit,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    keyboardOptions: KeyboardOptions = KeyboardOptions.Default
) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        if (!label.isNullOrEmpty()) {
            Text(
                text = label,
                color = if (error.isNotEmpty()) Lian_Color_Error else primaryColor,
                style = AloThoTypography.subtitle1,
                fontWeight = FontWeight.SemiBold,
                fontSize = 18.sp
            )
        }
        OutlinedTextField(
            value = value,
            onValueChange = onValueChange,
            textStyle = AloThoTypography.body1.copy(color = textColor),
            singleLine = singleLine,
            readOnly = readOnly,
            enabled = enabled,
            keyboardActions = keyboardActions,
            keyboardOptions = keyboardOptions,
            placeholder = {
                if (!placeholder.isNullOrEmpty()) {
                    Text(
                        text = placeholder ?: "",
                        style = AloThoTypography.body1.copy(color = textColor),
                    )
                }
            },
            shape = RoundedCornerShape(50.dp),
            trailingIcon = {
                if (trailingIcon != null) {
                    TextFieldIcon(
                        Modifier
                            .weight(1f),
                        trailingIcon,
                        primaryColor,
                        onClick = trailingIconOnClick
                    )
                }
            },
            colors = TextFieldDefaults.outlinedTextFieldColors(
                backgroundColor = Lian_Color_LightGray,
            ),
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewTextField() {
    var text by rememberSaveable { mutableStateOf("") }
    var error by rememberSaveable { mutableStateOf("") }

    fun validate(text: String) {
        error = if (text.count() < 5)
            "Phải > 5"
        else ""
    }

    CustomTextField(
        value = text,
        label = "Fullname",
        placeholder = "Nhập email của bạn",
        onValueChange = {
            text = it
        },
        error = error,
        keyboardActions = KeyboardActions {
            validate(text)
        },
        trailingIcon = Icons.Filled.Password
    )
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewTextField2() {
    var text by rememberSaveable { mutableStateOf("") }
    var error by rememberSaveable { mutableStateOf("") }

    fun validate(text: String) {
        error = if (text.count() < 5)
            "Phải > 5"
        else ""
    }

    RoundedTextField(
        value = text,
        label = "Fullname",
        onValueChange = {
            text = it
        },
        error = error,
        keyboardActions = KeyboardActions {
            validate(text)
        },
        trailingIcon = Icons.Filled.Password,
        leadingIcon = Icons.TwoTone.Backpack,
        cornerSize = 50.dp
    )
}

