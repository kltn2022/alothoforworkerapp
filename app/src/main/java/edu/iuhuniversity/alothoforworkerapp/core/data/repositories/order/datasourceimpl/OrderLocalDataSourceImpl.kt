package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasourceimpl

import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedorder.CachedOrder
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.order.OrderDao
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class OrderLocalDataSourceImpl(
    private val orderDao: OrderDao
) : OrderDataSource.Cache {

    override suspend fun insertOrderToDb(order: Order) {
        CoroutineScope(Dispatchers.IO).launch {
            orderDao.insertOrder(CachedOrder.fromDomain(order))
        }
    }

    override suspend fun getOrdersFromDb(): MutableList<Order> {
        return orderDao.getOrders()
            .map { it.toDomain() }
            .toMutableList()
    }

    override suspend fun getOrderById(orderId: String): Order? {
        return orderDao.getOrderById(orderId)?.toDomain()
    }

    override suspend fun saveOrdersToDb(orders: List<Order>) {
        CoroutineScope(Dispatchers.IO).launch {
            orderDao.updateOrders(orders.map { CachedOrder.fromDomain(it) })
        }
    }

    override suspend fun updateOrder(order: Order) {
        CoroutineScope(Dispatchers.IO).launch {
            orderDao.updateOrder(CachedOrder.fromDomain(order))
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            orderDao.deleteAllOrders()
        }
    }
}