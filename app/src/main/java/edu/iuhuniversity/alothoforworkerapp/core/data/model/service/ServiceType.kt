package edu.iuhuniversity.alothoforworkerapp.core.data.model.service

import android.os.Parcelable
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class ServiceType(
    val id: String,
    val name: String,
    val icon: String
) : Parcelable, Serializable {
    companion object {
        fun DocumentSnapshot.toServiceType(): ServiceType {
            return ServiceType(
                id = getString("id").orEmpty(),
                name = getString("name").orEmpty(),
                icon = getString("icon").orEmpty()
            )
        }
    }
}
