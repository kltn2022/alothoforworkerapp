package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

internal class ThreadEventRaiser : EventRaiser {
    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()
    override fun raiseEvent(r: Runnable?) {
        executorService.submit(r)
    }

}