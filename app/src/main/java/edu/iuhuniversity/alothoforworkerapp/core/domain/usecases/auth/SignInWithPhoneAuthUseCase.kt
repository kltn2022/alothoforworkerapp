package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository

class SignInWithPhoneAuthUseCase(private val authRepository: AuthRepository) {
    suspend fun execute(verificationId: String, otp: String) =
        authRepository.signInWithPhoneAuthCredential(verificationId, otp)
}