package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.IFcmService
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.fcm.SendNotificationRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.SendNotificationRepository

class SendNotificationToCustomerUseCase(private val sendNotificationRepository: SendNotificationRepository) {
    suspend fun execute(token: String?, order: Order): Resource<FcmResponse> =
        sendNotificationRepository.sendNotificationToCustomer(token, order)
}