package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models

data class FcmData<T> (
    val type: String,
    val body: T
)
