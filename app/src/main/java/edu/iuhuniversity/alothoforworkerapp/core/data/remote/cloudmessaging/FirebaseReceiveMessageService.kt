package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging

import android.app.ActivityManager
import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Parcelable
import android.widget.Toast
import cafe.adriel.voyager.core.screen.Screen
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.constant.NotificationType
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmData
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmReceivedMessage.Companion.checkType
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmReceivedMessage.Companion.toFcmReceivedMessage
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.SendRegistrationTokenToServerUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.displayNotification
import edu.iuhuniversity.alothoforworkerapp.core.utils.getActivityOption
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.ui.IncomingCallWorkerRequestActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.Serializable
import javax.inject.Inject

const val FCM_TOKEN = "fcm_token"

/**
 * [FirebaseReceiveMessageService] A Firebase Cloud Messaging Service
 * [TODO: Please Implement Me]
 * + Add the following to your app's [manifest]: https://firebase.google.com/docs/cloud-messaging/android/client#manifest
 * + Debug:
 *  + (1) https://www.raywenderlich.com/9227276-firebase-cloud-messaging-for-android-sending-push
 *  -notifications#toc-anchor-015
 *  + (2) https://firebase.google.com/docs/cloud-messaging/android/topic-messaging#override
 *  -ondeletedmessages
 *
 */
@AndroidEntryPoint
class FirebaseReceiveMessageService : FirebaseMessagingService() {

    @Inject
    lateinit var firebaseAuth: FirebaseAuth

    @Inject
    lateinit var sendRegistrationTokenUseCase: SendRegistrationTokenToServerUseCase

    @Inject
    lateinit var notificationLocalDataSourceImpl: NotificationRepository

    override fun onCreate() {
        super.onCreate()
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)

        if (firebaseAuth.currentUser != null) {
            CoroutineScope(Dispatchers.IO).launch {
                sendRegistrationTokenUseCase.execute(
                    RegistrationToken(
                        uid = firebaseAuth.uid,
                        token
                    )
                )
                // preferenceDataStore.putData(FCM_TOKEN, token)
            }
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        // Foreground: The notification and the data are both handled in onMessageReceived().
        // Background: The System UI handles the notification, while onMessageReceived() handles the data payload.

        checkIfBackgroundRestricted()

        if (remoteMessage.data.isNotEmpty() || remoteMessage.notification != null) {
            Timber.tag("remoteMessage").d(remoteMessage.toString())

            val remoteMessageType = remoteMessage.checkType()
            if (remoteMessageType != null) {
                when (NotificationType.valueOf(remoteMessageType)) {
                    NotificationType.SEND_CALL_WORKER_REQUEST -> {
                        handleMessage<String>(
                            remoteMessage,
                            onDataMessageAction = { data ->
                                // navigateTo(IncomingCallWorkerRequestScreen(data.body))
                                Timber.tag("remoteData").d(data.toString())
                                navigate(
                                    IncomingCallWorkerRequestActivity::class.java,
                                    Pair("orderId", data.body)
                                )
                            },
                            onNotificationMessageAction = { notification ->
                                saveRemoteMessageToLocal(notification)
                            }
                        )
                    }
                    NotificationType.NEW_AUDIO_CALL_NOTIFICATION -> {

                    }
                    NotificationType.NEW_MESSAGE_NOTIFICATION -> {

                    }
                }
            }
        }
    }

    // https://www.py4u.net/discuss/680262
    private fun checkIfBackgroundRestricted() {
        val activityManager = getSystemService(ACTIVITY_SERVICE) as ActivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (activityManager.isBackgroundRestricted) {
                // Inform user how to disable background restricted settings
                Toast.makeText(applicationContext, "something", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onDeletedMessages() {
        super.onDeletedMessages()
    }

    private fun saveRemoteMessageToLocal(notification: FcmNotification) {
        CoroutineScope(Dispatchers.IO).launch {
            notificationLocalDataSourceImpl.saveNotification(notification)
        }
    }

    private fun <T> handleMessage(
        remoteMessage: RemoteMessage,
        onDataMessageAction: (FcmData<T>) -> Unit,
        onNotificationMessageAction: (FcmNotification) -> Unit
    ) {
        val receivedMessage = remoteMessage.toFcmReceivedMessage<T>()
        if (receivedMessage.data != null) {
            onDataMessageAction(receivedMessage.data)
        }

        val notification = receivedMessage.notification
        if (notification != null) {
            displayNotification(
                this,
                0,
                notification,
            )
            onNotificationMessageAction(notification)
        }
    }

    private fun navigateTo(screen: Screen) {
        (application.getActivity() as MainActivity).addNavigator(screen)
    }

    private fun <T> navigate(activity: Class<T>, extra: Pair<String, Any>) {
        val mainActivity = applicationContext.getActivity()
        val options = getActivityOption(applicationContext).toBundle()

        val intent = if (mainActivity != null) {
            Intent(mainActivity as MainActivity, activity)
        } else {
            Intent(applicationContext, activity)
        }

        if (extra.second is Parcelable) {
            intent.putExtra(extra.first, extra.second as Parcelable)
        } else {
            intent.putExtra(extra.first, extra.second as String)
        }


        if (mainActivity == null) intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        startActivity(intent, options)
    }
}
