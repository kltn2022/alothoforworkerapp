package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository

class ListenRealtimeOrderUseCase(private val orderRepository: OrderRepository) {
    suspend fun execute(orderId: String) = orderRepository.listenRealtimeOrder(orderId)
}