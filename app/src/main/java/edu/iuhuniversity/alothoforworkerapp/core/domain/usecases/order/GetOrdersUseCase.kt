package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository

class GetOrdersUseCase(private val orderRepository: OrderRepository) {
    suspend fun execute() = orderRepository.getOrders()
}