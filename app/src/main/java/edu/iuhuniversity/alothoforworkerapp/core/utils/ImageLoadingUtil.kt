package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import coil.compose.AsyncImage
import coil.request.ImageRequest
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetAuthTokenUseCase
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import java.net.URLEncoder

class ImageLoadingUtil(
    private val getAuthTokenUseCase: GetAuthTokenUseCase
) {
    @Composable
    fun <T> ComposeImage(
        modifier: Modifier = Modifier,
        data: T,
        placeholder: Painter? = null
    ) {
        if (data is String) {
            val imageUrl: String = data

            val token = runBlocking { getAuthTokenUseCase.execute().first() }
            // photoUrl = gs://lian-app-71016.appspot.com/insurances/bao_hiem_tai_nan_xe.jpg
            // https://firebasestorage.googleapis.com/v0/b/lian-app-71016.appspot.com/o/insurances%2Fbao_hiem_cham_soc_phai_dep.jpg?alt=media&token=6fbf43ab-741e-4b48-b03d-bc0009e93099
            val storageBucketUrl = stringResource(id = R.string.cloud_storage_bucket)

            val fileRef =
                URLEncoder.encode(imageUrl.substringAfter("gs://${storageBucketUrl}/"), "UTF-8")
            val httpPhotoUrl =
                "https://firebasestorage.googleapis.com/v0/b/${storageBucketUrl}/o/${fileRef}?alt=media"

            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(httpPhotoUrl)
                    .crossfade(true)
                    .addHeader("Authorization", "Bearer $token")
                    .build(),
                contentDescription = null,
                placeholder = placeholder,
                contentScale = ContentScale.Crop,
                modifier = Modifier.then(modifier)
            )
        } else {
            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(data)
                    .crossfade(true)
                    .build(),
                contentDescription = null,
                placeholder = placeholder,
                contentScale = ContentScale.Crop,
                modifier = Modifier.then(modifier)
            )
        }
    }
}

fun String.toStorageLocation(
    context: Context,
): String {
    val storageBucketUrl = context.getString(R.string.cloud_storage_bucket)
    return "gs://${storageBucketUrl}${this}"
}