package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.firebase.firestore.DocumentSnapshot

internal class EventListenerBridge(private val listener: GeoQueryEventListener) :
    GeoQueryDataEventListener {
    override fun onDataEntered(documentSnapshot: DocumentSnapshot?, location: GeoLocation?) {
        listener.onKeyEntered(documentSnapshot!!.id, location)
    }

    override fun onDataExited(documentSnapshot: DocumentSnapshot?) {
        listener.onKeyExited(documentSnapshot!!.id)
    }

    override fun onDataMoved(documentSnapshot: DocumentSnapshot?, location: GeoLocation?) {
        listener.onKeyMoved(documentSnapshot!!.id, location)
    }

    override fun onDataChanged(documentSnapshot: DocumentSnapshot?, location: GeoLocation?) {
        // No-op.
    }

    override fun onGeoQueryReady() {
        listener.onGeoQueryReady()
    }

    override fun onGeoQueryError(error: Exception?) {
        listener.onGeoQueryError(error)
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) {
            return true
        }
        if (o == null || javaClass != o.javaClass) {
            return false
        }
        val that = o as EventListenerBridge
        return listener == that.listener
    }

    override fun hashCode(): Int {
        return listener.hashCode()
    }
}