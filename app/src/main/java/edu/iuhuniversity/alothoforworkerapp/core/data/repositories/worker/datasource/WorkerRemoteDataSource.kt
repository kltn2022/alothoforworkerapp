package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasource

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker

interface WorkerRemoteDataSource {
    suspend fun getWorker(uid: String): Resource<Worker?>
    suspend fun getWorker(documentReference: DocumentReference): Resource<Worker?>
    suspend fun getWorkerByPhoneNumber(phoneNumber: String): Resource<Worker?>
    suspend fun insertWorker(firebaseUser: FirebaseUser): Resource<Worker?>
    suspend fun updateWorkerInfo(uid: String, data: MutableMap<String, Any>): Resource<Boolean>
}