package edu.iuhuniversity.alothoforworkerapp.core.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification.FcmNotificationDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.order.OrderDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.service.ServiceDao
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasourceimpl.OrderLocalDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasourceimpl.ServiceLocalDataSourceImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class LocalDataModule {

    @Singleton
    @Provides
    fun provideOrderLocalDataSource(orderDao: OrderDao): OrderDataSource.Cache =
        OrderLocalDataSourceImpl(orderDao)

    // fun provideNotificationLocalDataSource(notificationDao: FcmNotificationDao): Notification

    @Singleton
    @Provides
    fun provideServiceLocalDataSource(serviceDao: ServiceDao): ServiceDataSource.Cache =
        ServiceLocalDataSourceImpl(serviceDao)

}