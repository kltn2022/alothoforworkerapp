package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.payment

import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.payment.PaymentRepository

class CreateMomoPaymentUseCase(private val paymentRepository: PaymentRepository) {
    suspend fun execute(request: ProcessingPayment.Request) =
        paymentRepository.createMomoPayment(request)
}