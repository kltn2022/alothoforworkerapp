package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.ButtonDefaults.OutlinedBorderSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessageAttribute.Companion.toDialogMessageAttribute
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapColorSystem
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType

@Composable
fun DialogMessage(
    modifier: Modifier = Modifier,
    colorType: BootstrapType = BootstrapType.FILLED,
    messageType: MessageType = MessageType.INFO,
    title: String,
    text: String,
    negativeButton: ActionButton? = null,
    positiveButton: ActionButton? = null,
    onDismissRequest: () -> Unit,
    properties: DialogProperties = DialogProperties(),
) {
    Dialog(
        onDismissRequest = onDismissRequest,
        properties = properties
    ) {
        DialogMessageContent(
            modifier = modifier,
            title = title,
            text = text,
            colorType = colorType,
            messageType = messageType,
            negativeButton = negativeButton,
            positiveButton = positiveButton,
            onCloseListener = onDismissRequest
        )
    }
}

private val DialogPadding = Modifier.padding(24.dp)
private val TextPadding = Modifier.padding(top = 10.dp, bottom = 20.dp)
private val DialogCornerShape = RoundedCornerShape(24.dp)

@Composable
internal fun DialogMessageContent(
    colorType: BootstrapType,
    messageType: MessageType,
    modifier: Modifier = Modifier,
    title: String,
    text: String,
    negativeButton: ActionButton? = null,
    positiveButton: ActionButton? = null,
    onCloseListener: () -> Unit
) {
    val dialogAttribute = colorType.toDialogMessageAttribute(messageType.color)

    Column(
        modifier = modifier
            .clip(DialogCornerShape)
            .background(dialogAttribute.backgroundColor)
            .then(DialogPadding)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Icon(
                painter = painterResource(id = messageType.resIconId),
                contentDescription = "",
                tint = dialogAttribute.iconTintColor,
                modifier = Modifier
                    .size(24.dp)
                    .weight(1f)
            )
            Text(
                text = title,
                style = AloThoTypography.subtitle1,
                fontWeight = FontWeight.SemiBold,
                color = dialogAttribute.textColor,
                modifier = Modifier
                    .padding(start = 10.dp)
                    .weight(8f)
            )
            IconButton(onClick = onCloseListener) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = "",
                    tint = dialogAttribute.iconTintColor,
                    modifier = Modifier
                        .size(24.dp)
                        .weight(1f)
                )
            }
        }
        Text(
            text = text,
            style = AloThoTypography.body2,
            color = dialogAttribute.textColor,
            modifier = TextPadding
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            if (negativeButton != null) {
                Button(
                    onClick = negativeButton.onClickListener,
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = dialogAttribute.btnNegativeBg,
                    ),
                    border = BorderStroke(
                        width = OutlinedBorderSize,
                        color = dialogAttribute.btnNegativeBorder
                    ),
                    elevation = if (!dialogAttribute.btnNegativeElevation) null else
                        ButtonDefaults.elevation(),
                    modifier = Modifier.sizeIn(maxWidth = 200.dp)
                ) {
                    Text(
                        text = negativeButton.label,
                        style = AloThoTypography.button,
                        fontSize = 12.sp,
                        fontWeight = FontWeight.SemiBold,
                        color = dialogAttribute.btnNegativeTextColor,
                    )
                }
            }
            if (positiveButton != null) {
                Button(
                    onClick = positiveButton.onClickListener,
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = dialogAttribute.btnPositiveBg,
                    ),
                    elevation = if (!dialogAttribute.btnPositiveElevation) null else ButtonDefaults.elevation()
                ) {
                    Text(
                        text = positiveButton.label,
                        style = AloThoTypography.button,
                        fontWeight = FontWeight.SemiBold,
                        color = dialogAttribute.btnPositiveTextColor,
                        fontSize = 12.sp
                    )
                }
            }

        }
    }
}

data class ActionButton(
    val label: String,
    val onClickListener: () -> Unit
)

data class DialogMessageAttribute(
    val backgroundColor: Color,
    val textColor: Color,
    val iconTintColor: Color,
    val btnPositiveTextColor: Color,
    val btnPositiveBg: Color,
    val btnPositiveElevation: Boolean = true,
    val btnNegativeTextColor: Color,
    val btnNegativeBg: Color,
    val btnNegativeBorder: Color = Color.Transparent,
    val btnNegativeElevation: Boolean = true,
) {
    companion object {
        fun BootstrapType.toDialogMessageAttribute(
            colorType: BootstrapColor
        ): DialogMessageAttribute {
            return when (this) {
                BootstrapType.FILLED -> DialogMessageAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_50),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30),
                    btnPositiveBg = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    btnPositiveTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_70),
                    btnNegativeBg = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_60),
                    btnNegativeTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                )
                BootstrapType.SMOOTH -> DialogMessageAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_5),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_100),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30),
                    btnPositiveBg = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_60),
                    btnPositiveTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    btnNegativeBg = Color.Transparent,
                    btnNegativeTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_70),
                    btnNegativeBorder = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30),
                    btnNegativeElevation = false,
                )
                BootstrapType.RAISED -> DialogMessageAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_100),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30),
                    btnPositiveBg = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_5),
                    btnPositiveTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_70),
                    btnNegativeBg = Color.Transparent,
                    btnNegativeTextColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_70),
                    btnPositiveElevation = false,
                    btnNegativeElevation = false,
                )
            }
        }
    }
}

@Preview
@Composable
fun PreviewDialog() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        val (snackbarVisibleState, setSnackBarState) = remember { mutableStateOf(false) }

        Button(onClick = { setSnackBarState(!snackbarVisibleState) }) {
            Text("Show Snackbar")
        }
        if (snackbarVisibleState) {
            DialogMessage(
                colorType = BootstrapType.RAISED,
                title = "This is default dialog",
                text = "First you need import component to your code. To use the component, copy and paste an example from the CODE tab. Default props of component can be omitted, they are applied automatically.",
                onDismissRequest = {
                   setSnackBarState(!snackbarVisibleState)
                },
                negativeButton = ActionButton(
                    label = "Maybe Next Time",
                    onClickListener = {

                    }
                ),
                positiveButton = ActionButton(
                    label = "Sure, Continue",
                    onClickListener = {

                    }
                )
            )
        }
    }
}