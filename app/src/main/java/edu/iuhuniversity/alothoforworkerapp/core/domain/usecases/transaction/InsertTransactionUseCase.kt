package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction

import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction.TransactionRepository

class InsertTransactionUseCase(private val transactionRepository: TransactionRepository) {
    suspend fun execute(transaction: Transaction) =
        transactionRepository.insertTransaction(transaction)
}