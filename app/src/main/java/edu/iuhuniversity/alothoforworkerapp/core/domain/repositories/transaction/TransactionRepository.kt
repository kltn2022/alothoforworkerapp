package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction
import kotlinx.coroutines.flow.Flow

interface TransactionRepository {
    suspend fun getTransactions(): Flow<List<Transaction?>>
    suspend fun insertTransaction(transaction: Transaction): Resource<Transaction?>
}