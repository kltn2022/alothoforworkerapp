package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository

class LogoutUseCase(private val authRepository: AuthRepository) {
    fun execute() = authRepository.logout()
}