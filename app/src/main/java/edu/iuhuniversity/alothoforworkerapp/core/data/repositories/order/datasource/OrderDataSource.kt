package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource

import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedorder.CachedOrder
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import kotlinx.coroutines.flow.Flow

sealed interface OrderDataSource {
    interface Cache {
        suspend fun insertOrderToDb(order: Order)
        suspend fun getOrdersFromDb(): MutableList<Order>
        suspend fun getOrderById(orderId: String): Order?
        suspend fun saveOrdersToDb(orders: List<Order>)
        suspend fun updateOrder(order: Order)
        suspend fun clearAll()
    }

    interface Remote {
        suspend fun insertOrder(order: Order): Resource<Order?>
        suspend fun getOrders(): Flow<List<Order?>>
        suspend fun getOrder(orderId: String): Resource<Order?>
        suspend fun cancelOrder(orderId: String, note: String): Resource<Boolean>
        suspend fun updateOrderStatus(orderId: String, status: OrderStatus): Resource<Boolean>
        suspend fun updateOrder(orderToUpdate: Order): Resource<Boolean>
        suspend fun listenRealtimeOrder(orderId: String): Flow<Order?>
    }
}