package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken.Companion.toToken
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.toResponse

class TokenDataSourceImpl(
    firebaseFirestore: FirebaseFirestore,
    private val firebaseMessaging: FirebaseMessaging,
    private val firebaseAuth: FirebaseAuth,
) : TokenDataSource {
    private var datasource =
        FirestoreDataSourceImpl<RegistrationToken>(firebaseFirestore.collection(CollectionConst.COLLECTION_TOKEN))

    override suspend fun generateRegistrationToken(): Resource<Boolean> {
        if (firebaseAuth.currentUser != null) {
            when (val result = firebaseMessaging.token.toResponse()) {
                is Resource.Success -> {
                    sendRegistrationToServer(RegistrationToken(firebaseAuth.uid, result.data!!))
                }
                else -> {

                }
            }
        }
        return Resource.Loading()
    }

    override suspend fun sendRegistrationToServer(registrationToken: RegistrationToken): Resource<Boolean> {
        return datasource.set(
            registrationToken.uid!!,
            registrationToken
        )
    }

    override suspend fun getRegistrationToken(uid: String): Resource<RegistrationToken?> {
        return datasource.get(uid) {
            it.toToken()
        }
    }
}