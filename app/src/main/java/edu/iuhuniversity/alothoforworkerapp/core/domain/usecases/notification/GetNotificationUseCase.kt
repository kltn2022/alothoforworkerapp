package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository

class GetNotificationUseCase(private val notificationRepository: NotificationRepository) {
    suspend fun execute() = notificationRepository.getSavedNotification()
}