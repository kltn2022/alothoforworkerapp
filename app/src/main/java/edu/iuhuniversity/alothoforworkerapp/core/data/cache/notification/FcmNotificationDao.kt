package edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification

import androidx.room.*
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification

@Dao
interface FcmNotificationDao {
    @Query("SELECT * FROM tbl_notification")
    fun getSavedNotification() : MutableList<FcmNotification>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveNotification(notification: FcmNotification)

    @Delete
    fun deleteNotification(notification: FcmNotification)
}