package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.payment

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment

interface PaymentRepository {
    suspend fun createMomoPayment(payload: ProcessingPayment.Request): Resource<ProcessingPayment.Response>
}