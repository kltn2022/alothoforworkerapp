package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.worker

import com.google.firebase.firestore.DocumentReference
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository

class GetWorkerUseCase(private val workerRepository: WorkerRepository) {
    suspend fun execute(documentReference: DocumentReference) = workerRepository.getWorker(documentReference)
}