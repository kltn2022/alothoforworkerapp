package edu.iuhuniversity.alothoforworkerapp.core.worker

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.work.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mapbox.geojson.Point
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoFire
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation
import edu.iuhuniversity.alothoforworkerapp.core.utils.LocationPermissionChecker
import edu.iuhuniversity.alothoforworkerapp.core.utils.buildMapboxNavigation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import java.util.concurrent.TimeUnit

class UpdateLocationWorker(
    private val context: Context,
    private val workerParameters: WorkerParameters
) : CoroutineWorker(context, workerParameters) {

    private val firebaseAuth = FirebaseAuth.getInstance()

    private val workerLocationRef: CollectionReference =
        FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_WORKER_GEOLOCATION)
    private val workerGeoFire = GeoFire(workerLocationRef)

    override suspend fun doWork(): Result {
        return withContext(Dispatchers.IO) {
            try {
                val mapboxNavigation: MapboxNavigation = buildMapboxNavigation(context)

                mapboxNavigation.apply {
                    if (ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            context,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        startTripSession()
                    } else {
                        Timber.tag("NoPermission").d("Ko được cấp quyền")
                    }

                    registerLocationObserver(object : LocationObserver {
                        override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
                            val enhancedLocation = locationMatcherResult.enhancedLocation

                            val point =
                                Point.fromLngLat(enhancedLocation.longitude, enhancedLocation.latitude)

                            Timber.tag("updatedPoint").d(point.toString())

                            updateCurrentLocationToServer(point)
                        }

                        override fun onNewRawLocation(rawLocation: Location) {}

                    })
                }
                Result.success()
            } catch (e: Exception) {
                Timber.tag(TAG).d(e.localizedMessage.orEmpty())
                Result.failure()
            }
        }
    }

    private fun updateCurrentLocationToServer(point: Point) {
            if (firebaseAuth.currentUser != null) {
                workerGeoFire.setLocation(firebaseAuth.currentUser?.uid,
                    GeoLocation(point.latitude(), point.longitude()),
                    object : GeoFire.CompletionListener {
                        override fun onComplete(key: String?, exception: Exception?) {
                            if (exception != null) {
                                Timber.tag(
                                    "UpdateCurrentLocation"
                                ).d(
                                    "There was an error saving the location to Geofire: $exception"
                                );
                            } else {
                                Timber.tag(
                                    "UpdateCurrentLocation"
                                ).d(
                                    "Location saved on server successfully!"
                                );
                            }
                        }
                    })
            }
    }

    companion object {
        private const val TAG = "UpdateLocationWorker"
        private const val DEFAULT_MIN_INTERVAL = 5L
        private val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .setRequiresStorageNotLow(true)
            .setRequiresBatteryNotLow(true)
            .build()

        @JvmStatic
        fun schedule(workManager: WorkManager, activity: AppCompatActivity) {
            val updateLocationWorker = PeriodicWorkRequestBuilder<UpdateLocationWorker>(
                DEFAULT_MIN_INTERVAL, TimeUnit.MINUTES
            )
                .setConstraints(constraints)
                .addTag(TAG)
                .build()

            workManager.enqueueUniquePeriodicWork(
                TAG,
                ExistingPeriodicWorkPolicy.KEEP,
                updateLocationWorker
            )
            observeWork(workManager, updateLocationWorker.id, activity)
        }

        private fun observeWork(workManager: WorkManager, id: UUID, activity: AppCompatActivity) {
            workManager.getWorkInfoByIdLiveData(id)
                .observe(activity) { info ->
                    if (info != null && info.state.isFinished) {
                        Timber.tag(TAG).d("Updated!!")
                    }
                }
        }
    }
}