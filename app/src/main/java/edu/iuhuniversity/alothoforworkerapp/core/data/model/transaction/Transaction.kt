package edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction

import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.CustomTypeParcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.io.Serializable
import java.util.*

@Parcelize
data class Transaction(
    val from: @WriteWith<CustomTypeParcelable.DocumentReferenceParceler> DocumentReference,
    val order: @WriteWith<CustomTypeParcelable.DocumentReferenceParceler> DocumentReference,
    val amount: Double? = 0.0,
    val createdAt: Date? = Date()
) : Parcelable, Serializable {
    companion object {
        fun createNew(
            amount: Double,
            order: DocumentReference?
        ): Transaction {
            return Transaction(
                CommonUtils.getCurrentUserRef()!!,
                order!!,
                amount,
                Calendar.getInstance().time
            )
        }

        fun DocumentSnapshot.toTransaction(): Transaction {
            return Transaction(
                from = getDocumentReference("from")!!,
                order = getDocumentReference("order")!!,
                amount = getDouble("amount"),
                createdAt = getDate("createdAt")!!
            )
        }
    }
}
