package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order

import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository

class UpdateOrderStatusUseCase(private val orderRepository: OrderRepository) {
    suspend fun execute(orderId: String, status: OrderStatus) =
        orderRepository.updateOrderStatus(orderId, status)
}