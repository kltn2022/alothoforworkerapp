package edu.iuhuniversity.alothoforworkerapp.core.data.model

import java.util.*

data class News (
    val id: String,
    val title: String,
    val createdAt: Date,
    val thumbnail: String,
    val content: String
)