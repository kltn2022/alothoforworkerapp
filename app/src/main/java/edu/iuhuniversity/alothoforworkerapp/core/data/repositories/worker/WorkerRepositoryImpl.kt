package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasource.WorkerRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository
import kotlinx.coroutines.flow.first

class WorkerRepositoryImpl(
    private val workerPreferencesRepository: WorkerPreferencesRepository,
    private val workerRemoteDataSource: WorkerRemoteDataSource,
    private val firebaseAuth: FirebaseAuth,
) : WorkerRepository {
    override suspend fun getWorker(uid: String): Resource<Worker?> =
        workerRemoteDataSource.getWorker(uid)

    override suspend fun getWorker(documentReference: DocumentReference): Resource<Worker?> =
        workerRemoteDataSource.getWorker(documentReference)

    override suspend fun getWorkerByPhoneNumber(phoneNumber: String): Resource<Worker?> =
        workerRemoteDataSource.getWorkerByPhoneNumber(phoneNumber)

    override suspend fun getCurrentUser(): Resource<Worker?>? {
//        val storedUser = workerPreferencesRepository.workerPreference.first()
//
//        return if (storedUser != null) {
//            Resource.Success(storedUser)
//        } else {
            return firebaseAuth.uid?.let { workerRemoteDataSource.getWorker(it) }
        // }

    }

    override suspend fun updateWorkerInfo(
        uid: String,
        data: MutableMap<String, Any>
    ): Resource<Boolean>  = workerRemoteDataSource.updateWorkerInfo(uid, data)

    override suspend fun insertWorker(firebaseUser: FirebaseUser): Resource<Worker?> {
        return workerRemoteDataSource.insertWorker(firebaseUser)
    }

}