package edu.iuhuniversity.alothoforworkerapp.core.data.cache.model

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Feedback
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderDetail
import java.util.*

sealed class RoomTypeConverters {
    val gson = Gson()

    abstract class ListConverter<T> {
        @TypeConverter
        fun mapListToString(value: List<T>): String {
            val gson = Gson()
            val type = object : TypeToken<List<T>>() {}.type
            return gson.toJson(value, type)
        }

        @TypeConverter
        fun mapStringToList(value: String): List<T> {
            val gson = Gson()
            val type = object : TypeToken<List<T>>() {}.type
            return gson.fromJson(value, type)
        }
    }

    class OrderDetailsConverter : ListConverter<OrderDetail>()
    class StringListConverter : ListConverter<String>()

    abstract class ObjectConverter<T> {
        @TypeConverter
        fun mapObjectToString(value: T): String {
            val gson = Gson()
            val type = object : TypeToken<T>() {}.type
            return gson.toJson(value, type)
        }

        @TypeConverter
        fun mapStringToObject(value: String): T {
            val gson = Gson()
            val type = object : TypeToken<T>() {}.type
            return gson.fromJson(value, type)
        }
    }

    class AddressConverter(): ObjectConverter<Address>()

    class FeedbackConverter() : ObjectConverter<Feedback>()

    class DateConverter(): RoomTypeConverters() {
        @TypeConverter
        fun fromTimestamp(value: Long?): Date? {
            return if (value == null) null else Date(value)
        }

        @TypeConverter
        fun dateToTimestamp(date: Date?): Long? {
            return date?.time
        }
    }

}