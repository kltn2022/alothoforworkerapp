package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasourceimpl

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Conservation
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Conservation.Companion.toConservation
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Message
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Message.Companion.toMessage
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreRealtimeDataSourceImpl
import kotlinx.coroutines.flow.Flow
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasource.MessageRemoteDataSource

class MessageRemoteDataSourceImpl(
    private val db: FirebaseFirestore
) : MessageRemoteDataSource {

    private val messageCollection = db.collection(CollectionConst.COLLECTION_MESSAGE);
    private var messageDataSource: FirestoreDataSource<Message> =
        FirestoreDataSourceImpl(messageCollection)

    private val messageRealtimeDataSource: FirestoreDataSource.RealtimeOperations<Message> =
        FirestoreRealtimeDataSourceImpl()

    private val conservationCollection = db.collection(CollectionConst.COLLECTION_CONSERVATION);
    private var conservationDataSource: FirestoreDataSource<Conservation> =
        FirestoreDataSourceImpl(conservationCollection)
    private val conservationRealtimeDataSource: FirestoreDataSource.RealtimeOperations<Conservation> =
        FirestoreRealtimeDataSourceImpl()

    override suspend fun sendMessage(message: Message): Resource<Message?> {
        return messageDataSource.insert(message) { it.toMessage() }
    }


    override suspend fun listenForChatMessages(
        conservationId: String,
    ): Flow<List<Message?>> {
        return messageRealtimeDataSource.registerListenFor(
            messageCollection
                .whereEqualTo("conservationId", conservationId)
                .orderBy("timestamp", Query.Direction.DESCENDING)
        ) {
            it.toMessage()!!
        }
    }

    override suspend fun getConservationInfo(conservationId: String): Resource<Conservation?> {
        return conservationDataSource
            .get(conservationId) {
                it.toConservation()
            }
    }

    override suspend fun getConservations(): Flow<List<Conservation?>> {
        val customerDocRef = db.collection(CollectionConst.COLLECTION_USER)
            .document(FirebaseAuth.getInstance().currentUser?.uid!!)

        return conservationRealtimeDataSource.registerListenFor(
            conservationCollection.whereEqualTo("customerId", customerDocRef.id)
        ) {
            it.toConservation()
        }
    }

}