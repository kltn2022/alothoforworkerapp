package edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import kotlinx.coroutines.flow.Flow

interface FirestoreDataSource<T> {
    var totalItems: Long?

    suspend fun get(documentId: String, transformObject: (DocumentSnapshot) -> T): Resource<T?>
    suspend fun getAll(transformObject: (DocumentSnapshot) -> T): Resource<MutableList<T>>

    suspend fun insert(data: T, transformObject: (DocumentSnapshot) -> T?): Resource<T?>
    suspend fun update(documentId: String, data: MutableMap<String, Any>): Resource<Boolean>
    suspend fun set(documentId: String, data: T): Resource<Boolean>
    suspend fun delete(documentId: String): Resource<Boolean>
    suspend fun filterWith(
        query: Query,
        transformObject: (DocumentSnapshot) -> T
    ): Resource<MutableList<T>>

    interface RealtimeOperations<T> {
        suspend fun registerListenFor(
            query: Query,
            transformObject: (DocumentSnapshot) -> T?
        ): Flow<List<T?>>

        suspend fun registerSingleListenFor(
            documentReference: DocumentReference,
            transformObject: (DocumentSnapshot) -> T?
        ): Flow<T?>
    }

    suspend fun update(
        keyValue: Pair<String, String>,
        data: MutableMap<String, Any>
    ): Resource<Boolean>
}