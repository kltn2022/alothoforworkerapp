package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.file

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file.FileRepository

class GetDownloadUrlUseCase(private val fileRepository: FileRepository) {
    suspend fun execute(storageUri: String) : Resource<String> {
        return fileRepository.getDownloadUrlFromStorage(storageUri)
    }
}