package edu.iuhuniversity.alothoforworkerapp.core.data.model.message

import com.google.firebase.firestore.DocumentSnapshot
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker

data class Conservation(
    val id: String? = "",
    val customerId: String? = "",
    val consultantId: String? = ""
) {
    companion object {
        fun DocumentSnapshot.toConservation(): Conservation {
            val customerId = getString("customerId")
            val consultantId = getString("consultantId")
            return Conservation(
                id,
                customerId,
                consultantId
            )
        }
    }
}

data class ConservationInfo(
    val id: String? = "",
    val customer: User,
    val consultant: Worker,
    val lastMessage: String? = "",
)