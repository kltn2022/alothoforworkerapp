package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

interface GeoQueryDataValueEventListener {
    fun onDocumentChange(documentChanges: List<GeoQueryDocumentChange?>?)
}