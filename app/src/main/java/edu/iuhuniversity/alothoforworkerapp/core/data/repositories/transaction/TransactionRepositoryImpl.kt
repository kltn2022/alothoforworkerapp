package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.transaction

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction
import edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction.Transaction.Companion.toTransaction
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreRealtimeDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction.TransactionRepository
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import kotlinx.coroutines.flow.Flow

class TransactionRepositoryImpl(
    private val db: FirebaseFirestore
) : TransactionRepository {
    private val transactionCollection =
        db.collection(CollectionConst.COLLECTION_CUSTOMER_TRANSACTION_HISTORY);
    private val realtimeDataSource: FirestoreDataSource.RealtimeOperations<Transaction> =
        FirestoreRealtimeDataSourceImpl()
    private val dataSource: FirestoreDataSource<Transaction> =
        FirestoreDataSourceImpl(transactionCollection)

    override suspend fun getTransactions(): Flow<List<Transaction?>> {
        return realtimeDataSource
            .registerListenFor(
                transactionCollection
                    .whereEqualTo("from", CommonUtils.getCurrentUserRef())
                    .orderBy("timestamp", Query.Direction.DESCENDING)
            ) {
                it.toTransaction()
            }
    }

    override suspend fun insertTransaction(transaction: Transaction): Resource<Transaction?> {
        return dataSource
            .insert(transaction) {
                it.toTransaction()
            }
    }
}