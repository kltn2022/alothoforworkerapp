package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.text.TextUtils
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

object CurrencyUtils {
    /**
     * Get price with VietNam currency
     *
     * @param context
     * @param price
     */
    fun formatVnCurrency(price: String): String {
        var formatedPrice = price
        val format: NumberFormat = DecimalFormat("#,##0.00") // #,##0.00 ¤ (¤:// Currency symbol)
        format.currency = Currency.getInstance(Locale.US) //Or default locale
        formatedPrice = if (!TextUtils.isEmpty(formatedPrice)) formatedPrice else "0"
        formatedPrice = formatedPrice.trim { it <= ' ' }
        formatedPrice = format.format(formatedPrice.toDouble())
        formatedPrice = formatedPrice.replace(",".toRegex(), "\\.")
        if (formatedPrice.endsWith(".00")) {
            val centsIndex = formatedPrice.lastIndexOf(".00")
            if (centsIndex != -1) {
                formatedPrice = formatedPrice.substring(0, centsIndex)
            }
        }
        formatedPrice = String.format("%s đ", formatedPrice)
        return formatedPrice
    }
}