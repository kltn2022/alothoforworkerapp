package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasourceimpl

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order.Companion.toOrder
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreRealtimeDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import kotlinx.coroutines.flow.Flow
import timber.log.Timber

class OrderRemoteDataSourceImpl(
    private val db: FirebaseFirestore
) : OrderDataSource.Remote {
    private val orderCollection = db.collection(CollectionConst.COLLECTION_ORDER);
    private val dataSource: FirestoreDataSource<Order> = FirestoreDataSourceImpl(orderCollection)
    private val realtimeDataSource: FirestoreDataSource.RealtimeOperations<Order> =
        FirestoreRealtimeDataSourceImpl()

    override suspend fun insertOrder(order: Order): Resource<Order?> {
        return dataSource.insert(order) { it.toOrder() }
    }

    override suspend fun getOrders(): Flow<List<Order?>> {
        val workerDocRef = db.collection(CollectionConst.COLLECTION_WORKER)
            .document(FirebaseAuth.getInstance().currentUser?.uid!!)

        return realtimeDataSource
            .registerListenFor(
                orderCollection
                    .whereEqualTo("worker", workerDocRef)
            ) {
                it.toOrder()
            }
    }

    override suspend fun getOrder(orderId: String): Resource<Order?> {
        return dataSource.get(orderId) {
            it.toOrder()!!
        }
    }

    override suspend fun cancelOrder(orderId: String, note: String): Resource<Boolean> {
        return dataSource.update(
            orderId,
            mutableMapOf(Pair("status", OrderStatus.CANCELED), Pair("note", note))
        )
    }

    override suspend fun updateOrderStatus(
        orderId: String,
        status: OrderStatus
    ): Resource<Boolean> {
        return dataSource.update(
            orderId,
            mutableMapOf(Pair("status", status))
        )
    }

    override suspend fun updateOrder(orderToUpdate: Order): Resource<Boolean> {
        return dataSource.set(orderToUpdate.id, orderToUpdate)
    }

    override suspend fun listenRealtimeOrder(orderId: String): Flow<Order?> {
        return realtimeDataSource
            .registerSingleListenFor(
                orderCollection.document(orderId)
            ) {
                it.toOrder()
            }
    }
}