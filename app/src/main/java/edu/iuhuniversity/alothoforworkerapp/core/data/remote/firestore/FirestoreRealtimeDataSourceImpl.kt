package edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

class FirestoreRealtimeDataSourceImpl<T>() : FirestoreDataSource.RealtimeOperations<T> {

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun registerListenFor(
        query: Query,
        transformObject: (DocumentSnapshot) -> T?
    ): Flow<List<T?>> {
        return query.getDataFlow { querySnapshots ->
            querySnapshots?.documents?.map { transformObject(it) } ?: listOf()
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    override suspend fun registerSingleListenFor(
        documentReference: DocumentReference,
        transformObject: (DocumentSnapshot) -> T?
    ): Flow<T?> {
        return documentReference.getDataFlow { documentSnapshot ->
            documentSnapshot?.let { transformObject(it) }
        }
    }

}