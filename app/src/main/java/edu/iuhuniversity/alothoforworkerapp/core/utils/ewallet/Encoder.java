/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet;

import static android.content.Context.WIFI_SERVICE;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Formatter;

import javax.crypto.Cipher;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import timber.log.Timber;

@SuppressWarnings("restriction")
public class Encoder {

    private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();
//    private static final String ENCODING = "UTF-8";

    private static final String HMAC_SHA256 = "HmacSHA256";
    private static final String TAG = Encoder.class.getSimpleName();


    @SuppressWarnings("resource")
    private static String toHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length * 2);
        Formatter formatter = new Formatter(sb);
        for (byte b : bytes) {
            formatter.format("%02x", b);
        }
        return sb.toString();
    }

    public static String signHmacSHA256(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey.getBytes(), HMAC_SHA256);
        Mac mac = Mac.getInstance(HMAC_SHA256);
        mac.init(secretKeySpec);
        byte[] rawHmac = mac.doFinal(data.getBytes(StandardCharsets.UTF_8));
        return toHexString(rawHmac);
    }

    public static String getSHA(String data) throws Exception {
        MessageDigest md = MessageDigest.getInstance("SHA");
        md.update(data.getBytes(StandardCharsets.UTF_8));
        byte[] ba = md.digest();
        StringBuilder sb = new StringBuilder(ba.length * 2);

        for (byte b : ba) {
            sb.append(HEX_CHARS[(((int) b & 0xFF) / 16) & 0x0F]);
            sb.append(HEX_CHARS[((int) b & 0xFF) % 16]);
        }
        return sb.toString();
    }

    public static String decode64(String s) {
        try {
            byte[] valueDecoded = Base64.decode(s.getBytes(), Base64.DEFAULT);
            return new String(valueDecoded);
        } catch (Exception e) {
            return "";
        }
    }

    public static String encode64(String s) {
        // encode data on your side using BASE64
        byte[] bytesEncoded = Base64.encode(s.getBytes(), Base64.DEFAULT);
        return new String(bytesEncoded);
    }

    public static String hashSHA256(String input) throws Exception {
        try {
            MessageDigest sha = MessageDigest.getInstance("SHA-256");
            sha.update(input.getBytes());
            BigInteger dis = new BigInteger(1, sha.digest());
            String result = dis.toString(16);
            if (!result.startsWith("0") && result.length() < 64) {
                result = "0" + result;
            }
            return result.toUpperCase();
        } catch (NoSuchAlgorithmException ex) {
            throw new Exception(ex);
        }
    }

    public static String Sha256(String message) {
        String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hash = md.digest(message.getBytes(StandardCharsets.UTF_8));

            // converting byte array to Hexadecimal String
            StringBuilder sb = new StringBuilder(2 * hash.length);
            for (byte b : hash) {
                sb.append(String.format("%02x", b & 0xff));
            }

            digest = sb.toString();

        } catch (NoSuchAlgorithmException ex) {
            digest = "";
            // Logger.getLogger(StringReplace.class.getName()).log(Level.SEVERE,
            // null, ex);
        }
        return digest;
    }

    public static String hmacSha1(String value, String key) throws Exception {
        // Get an hmac_sha1 key from the raw key bytes
        byte[] keyBytes = key.getBytes();
        SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

        // Get an hmac_sha1 Mac instance and initialize with the signing key
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);

        // Compute the hmac on input data bytes
        byte[] rawHmac = mac.doFinal(value.getBytes());
        return Base64.encodeToString(rawHmac, Base64.DEFAULT);
    }

    public static String encryptRSA(byte[] dataBytes, String publicKey) throws Exception {
        // Note: You can use java.util.Base64 instead of sun.misc.*
        PublicKey pubk;
//        BASE64Decoder decoder = new BASE64Decoder();
//        BASE64Encoder encoder = new BASE64Encoder();
//        java.util.Base64.Decoder decoder = java.util.Base64.getDecoder();
//        java.util.Base64.Encoder encoder = java.util.Base64.getEncoder();

        byte[] publicKeyBytes = Base64.decode(publicKey, Base64.DEFAULT);
//        byte[] publicKeyBytes = decoder.decodeBuffer(publicKey);
        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        pubk = keyFactory.generatePublic(publicKeySpec);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(Cipher.ENCRYPT_MODE, pubk);
        //return encoder.encodeToString(cipher.doFinal(dataBytes)).replace("\r", "");

        return new String(Base64.encode(cipher.doFinal(dataBytes), Base64.DEFAULT)).replace(System.lineSeparator(),
                "");
    }

    public static String decryptRSA(String encryptData, String privateKey) {
        try {
            byte[] privateKeyBytes = Base64.decode(privateKey, Base64.DEFAULT);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            PrivateKey prvk = keyFactory.generatePrivate(privateKeySpec);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, prvk);
            return new String(cipher.doFinal(Base64.decode(encryptData, Base64.DEFAULT)));
        } catch (Exception ex) {
            Timber.d("[DecryptRSA] Error: " + ex);
            return "";
        }
    }

    public static String getIpAddress(Context context) throws UnknownHostException {
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        int ipInt = wifiManager.getConnectionInfo().getIpAddress();
        return InetAddress.getByAddress(
                ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(ipInt).array())
                .getHostAddress();
    }
}