package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response

import com.google.gson.annotations.SerializedName

/**
 *  Topic message HTTP response body (JSON).
 * + Refer: https://firebase.google.com/docs/cloud-messaging/http-server-ref#interpret-downstream
 * (Table 6)
 */
data class FcmResult(
    @SerializedName("message_id")
    val message_id: String, // The topic message ID when FCM has successfully received the request and will attempt to deliver to all subscribed devices.
    @SerializedName("registration_id")
    val registration_id: String, // This parameter specifies the registration token for the client app that the message was processed and sent to.
    @SerializedName("error")
    val error: String  // Error that occurred when processing the message.
)