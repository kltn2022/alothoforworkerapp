package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response

import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmData
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification.Companion.toFcmNotification

data class FcmReceivedMessage<T>(
    @SerializedName("data")
    val data: FcmData<T>? = null,
    @SerializedName("notification")
    val notification: FcmNotification? = null
) {
    companion object {
        @Suppress("UNCHECKED_CAST")
        fun <T> RemoteMessage.toFcmReceivedMessage(): FcmReceivedMessage<T> {
            val data: FcmData<T> = Gson().fromJson(this.data.toString(), FcmData::class.java) as FcmData<T>
            val notification = this.notification?.toFcmNotification()

            return FcmReceivedMessage(
                data = data,
                notification = notification
            )
        }

        fun RemoteMessage.checkType(): String? {
            return this.data["type"]
        }
    }


}