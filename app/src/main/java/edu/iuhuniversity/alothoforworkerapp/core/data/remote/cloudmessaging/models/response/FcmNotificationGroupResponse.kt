package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response

import com.google.gson.annotations.SerializedName

data class FcmNotificationGroupResponse(
    @SerializedName("notification_key")
    val notificationKey: String // APA91bGHXQBB...9QgnYOEURwm0I3lmyqzk2TXQ
)