package edu.iuhuniversity.alothoforworkerapp.core.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.MyDatabase
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification.FcmNotificationDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.order.OrderDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.service.ServiceDao
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun provideAloThoDatabase(app: Application): MyDatabase {
        return Room.databaseBuilder(
            app,
            MyDatabase::class.java,
            "alotho_db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideNotificationDao(myDatabase: MyDatabase): FcmNotificationDao {
        return myDatabase.notificationDao()
    }

    @Singleton
    @Provides
    fun provideOrderDao(myDatabase: MyDatabase): OrderDao {
        return myDatabase.orderDao()
    }

    @Singleton
    @Provides
    fun provideServiceDao(myDatabase: MyDatabase): ServiceDao {
        return myDatabase.serviceDao()
    }
}
