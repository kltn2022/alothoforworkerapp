package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user

import com.google.firebase.auth.FirebaseUser
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User

interface UserRepository {
    suspend fun getUser(uid: String): Resource<User?>
    suspend fun getUserByPhoneNumber(phoneNumber: String): Resource<User?>
    suspend fun insertUser(firebaseUser: FirebaseUser): Resource<User?>
    suspend fun updateUserInfo(uid: String, data: MutableMap<String, Any>): Resource<Boolean>
}