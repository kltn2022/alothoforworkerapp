package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request


import com.google.gson.annotations.SerializedName
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmData
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification

data class FcmMessagePayload<T>(
    @SerializedName("to")
    val to: String, // aUniqueKey
    @SerializedName("data")
    val data: FcmData<T>,
    @SerializedName("notification")
    val notification: FcmNotification? = null
)