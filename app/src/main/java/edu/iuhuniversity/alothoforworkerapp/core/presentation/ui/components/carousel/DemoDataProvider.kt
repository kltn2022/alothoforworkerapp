package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel

import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Offer
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.ServiceType


object DemoDataProvider {

//    val itemList = listOf(
//        CarouselItem(
//            1,
//            "Bảo hiểm Trụ cột gia đình",
//            "Trụ cột Gia đình là gói sản phẩm bảo hiểm của công ty bảo hiểm Viễn Đông. Đây là gói bảo hiểm được định phí thấp với mục tiêu hỗ trợ và phòng chống các rủi ro tài chính có thể xảy ra khi khách hàng không may mắc các bệnh Ung thư, Đột quỵ,... hay các bệnh khác.",
//            R.drawable.bao_hiem_tru_cot_gia_dinh
//        ),
//        CarouselItem(
//            2,
//            "Chăm sóc toàn diện phái đẹp",
//            "Hãy để BẢO HIỂM CHĂM SÓC TOÀN DIỆN PHÁI ĐẸP của Bảo hiểm Viễn Đông (VASS) đồng hành với những người phụ nữ yêu thương bên bạn như một cách nâng niu sắc đẹp Việt luôn tỏa sáng theo thời gian.",
//            R.drawable.bao_hiem_cham_soc_phai_dep
//        ),
//        CarouselItem(
//            3,
//            "Bảo hiểm nhà tư nhân",
//            "VASS sẽ bồi thường cho NĐBH các tổn thất, thiệt hại vật chất của tài sản là ngôi nhà được bảo hiểm tại địa điểm được bảo hiểm của Người được bảo hiểm trong thời hạn bảo hiểm bị thiệt hại hay tổn thất gây ra do: Cháy; Nổ; Sét đánh, được nêu trong Giấy chứng nhận bảo với điều kiện các tổn thất này không thuộc điểm loại trừ được qui định bởi quy tắc Bảo hiểm Nhà tư nhân số 160/2015/QĐ-VASS/TGĐ ngày 09/06/2015 của Tổng Giám Đốc Công ty CP Bảo Hiểm Viễn Đông (đính kèm).",
//            R.drawable.bao_hiem_mai_am
//        ),
//        CarouselItem(
//            4,
//            "Bảo hiểm tai nạn xe máy",
//            "Mang lại sự yên tâm cho bạn và những người thân yêu trong gia đình chỉ với 50000đ/ngày/người",
//            R.drawable.bao_hiem_tai_nan_xe
//        ),
//    )

    val slider = listOf<CarouselSliderItem>(
        CarouselSliderItem(
            1,
            R.drawable.ic_slider1
        ),
        CarouselSliderItem(
            2,
            R.drawable.ic_slider2
        ),
        CarouselSliderItem(
            3,
            R.drawable.ic_slider3
        ),
        CarouselSliderItem(
            4,
            R.drawable.ic_slider4
        )
    )

//    val item = CarouselItem(
//        4,
//        "Bảo hiểm tai nạn xe máy",
//        "Mang lại sự yên tâm cho bạn và những người thân yêu trong gia đình chỉ với 50000đ/ngày/người",
//        R.drawable.bao_hiem_tai_nan_xe
//    )

    val defaultServiceTypeList = listOf(
        ServiceType(
            "1",
            "Thợ sửa khoá",
            R.drawable.ic_tho_sua_chua_nuoc.toString(),
        ),
        ServiceType(
            "2",
            "Thợ sửa điện",
            R.drawable.ic_tho_sua_dien_gia_dung.toString(),
        ),
        ServiceType(
            "3",
            "Thợ sửa máy tính",
            R.drawable.ic_tho_sua_may_tinh.toString(),
        ),
        ServiceType(
            "4",
            "Thợ thông hút cống",
            R.drawable.ic_tho_thong_hut_cong.toString(),
        )
    )

    val defaultOfferList = listOf<Offer>(
        Offer(
            "1",
            "Tiêu đề 1",
        "Subtitle 1",
            content = "22fsd",
            ""
        ),
        Offer(
            "2",
            "Tiêu đề 2",
            "Subtitle 2",
            content = "22fsd3423",
            ""
        ),
        Offer(
            "3",
            "Tiêu đề 3",
            "Subtitle 3",
            content = "22fsd",
            ""
        )
    )

    val defaultOrders = listOf<SampleOrder>(
        SampleOrder(
            id = "DH001",
            total = 56000.0,
            service = "Hút ống cống",
            status = OrderStatus.WAITING_FOR_WORK,
            worker = "Nguyễn Văn A"
        ),
        SampleOrder(
            id = "DH001",
            total = 63000.0,
            service = "Thay bóng đèn",
            status = OrderStatus.CANCELED,
            worker = "Nguyễn Văn B"
        ),
        SampleOrder(
            id = "DH002",
            total = 33000.0,
            service = "Thay bóng đèn",
            status = OrderStatus.CANCELED,
            worker = "Nguyễn Văn B"
        ),
        SampleOrder(
            id = "DH0044",
            total = 35000.0,
            service = "Thay bóng đèn",
            status = OrderStatus.CANCELED,
            worker = "Nguyễn Văn B"
        )

    )
}

data class SampleOrder(
    val id: String,
    val total: Double,
    val service: String,
    val status: OrderStatus,
    val worker: String
)