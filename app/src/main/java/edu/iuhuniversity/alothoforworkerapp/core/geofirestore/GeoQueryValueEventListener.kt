package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.firebase.firestore.DocumentChange

interface GeoQueryValueEventListener {
    fun onDocumentChange(documentChanges: List<DocumentChange?>?)
}