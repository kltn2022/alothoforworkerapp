package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken

interface TokenRepository {
    suspend fun generateRegistrationToken(): Resource<Boolean>
    suspend fun sendRegistrationToServer(registrationToken: RegistrationToken): Resource<Boolean>
    suspend fun getRegistrationToken(uid: String): Resource<RegistrationToken?>
}