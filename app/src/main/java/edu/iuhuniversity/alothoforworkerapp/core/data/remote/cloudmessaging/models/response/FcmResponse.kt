package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response

import com.google.gson.annotations.SerializedName

/**
 * Downstream HTTP message response body (JSON)
 * + Refer: https://firebase.google.com/docs/cloud-messaging/http-server-ref#interpret-downstream
 * (Table 5. Downstream HTTP message response body (JSON))
 */
data class FcmResponse(
    @SerializedName("multicast_id")
    val multicast_id: Long? = null, // Unique ID (number) identifying the multicast message.
    @SerializedName("success")
    val success: Int? = null, // Number of messages that were processed without an error.
    @SerializedName("failure")
    val failure: Int? = null, // Number of messages that could not be processed.
    @SerializedName("canonical_ids")
    val canonical_ids: Int? = null,
    @SerializedName("failed_registration_ids")
    val failed_registration_ids: List<String>,
    @SerializedName("results")
    val results: List<FcmResult>? = null // Array of objects representing the status of the messages processed.
)
