package edu.iuhuniversity.alothoforworkerapp.core.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file.FileRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.SendNotificationRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.payment.PaymentRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service.ServiceRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction.TransactionRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.CheckIfUserSignedInUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.LogoutUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SendOtpUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SignInWithPhoneAuthUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.file.UploadFilesUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message.ListenForConservationsUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.*
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.*
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.payment.CreateMomoPaymentUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.service.GetServiceByIdUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.service.GetServicesByTypeUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction.InsertTransactionUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.*
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.worker.UpdateWorkerInfoUseCase
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {
    @Singleton
    @Provides
    fun provideLogoutUseCase(authRepository: AuthRepository) = LogoutUseCase(authRepository)

    @Singleton
    @Provides
    fun provideSendOtpUseCase(authRepository: AuthRepository) = SendOtpUseCase(authRepository)

    @Singleton
    @Provides
    fun provideSignInWithPhoneAuthUseCase(authRepository: AuthRepository) =
        SignInWithPhoneAuthUseCase(authRepository)

    @Singleton
    @Provides
    fun provideCheckIfUserSignedIn(authRepository: AuthRepository) =
        CheckIfUserSignedInUseCase(authRepository)

    @Singleton
    @Provides
    fun provideGetAuthTokenUseCase(workerPreferencesRepository: WorkerPreferencesRepository) =
        GetAuthTokenUseCase(workerPreferencesRepository)

    @Singleton
    @Provides
    fun provideGetUserInfoByPhoneUseCase(userRepository: UserRepository) =
        GetUserInfoByPhoneUseCase(userRepository)

    @Singleton
    @Provides
    fun provideGetUserInfoUseCase(userRepository: UserRepository) =
        GetUserInfoUseCase(userRepository)

    @Singleton
    @Provides
    fun provideInsertUserInfoUseCase(userRepository: UserRepository) =
        InsertUserInfoUseCase(userRepository)

    @Singleton
    @Provides
    fun provideUpdateUserInfoUseCase(workerRepository: WorkerRepository) =
        UpdateUserInfoUseCase(workerRepository)

    @Singleton
    @Provides
    fun provideGetOrdersUseCase(orderRepository: OrderRepository) =
        GetOrdersUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideUpdateOrderUseCase(orderRepository: OrderRepository) =
        UpdateOrderUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideUpdateOrdersUseCase(orderRepository: OrderRepository) =
        UpdateOrdersUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideGetOrderByIdUseCase(orderRepository: OrderRepository) =
        GetOrderByIdUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideGetNotificationsUseCase(notificationRepository: NotificationRepository) =
        GetNotificationUseCase(notificationRepository)

    @Singleton
    @Provides
    fun provideDeleteNotificationUseCase(notificationRepository: NotificationRepository) =
        DeleteNotificationUseCase(notificationRepository)

    @Singleton
    @Provides
    fun provideSaveNotificationUseCase(notificationRepository: NotificationRepository) =
        SaveNotificationUseCase(notificationRepository)

    @Singleton
    @Provides
    fun provideListenForConservationsUseCase(messageRepository: MessageRepository) =
        ListenForConservationsUseCase(messageRepository)

    @Singleton
    @Provides
    fun provideGetServiceByIdUseCase(serviceRepository: ServiceRepository) =
        GetServiceByIdUseCase(serviceRepository)

    @Singleton
    @Provides
    fun provideGetServicesByTypeUseCase(serviceRepository: ServiceRepository) =
        GetServicesByTypeUseCase(serviceRepository)

    @Singleton
    @Provides
    fun provideGetRegistrationTokenUseCase(tokenRepository: TokenRepository) =
        GetRegistrationTokenUseCase(tokenRepository)

    @Singleton
    @Provides
    fun provideSendNotificationToCustomer(sendNotificationRepository: SendNotificationRepository) =
        SendNotificationToCustomerUseCase(sendNotificationRepository)

    @Singleton
    @Provides
    fun provideUploadFilesUseCase(fileRepository: FileRepository) =
        UploadFilesUseCase(fileRepository)

    @Singleton
    @Provides
    fun provideGetCurrentUserUseCase(workerRepository: WorkerRepository) =
        GetCurrentUserUseCase(workerRepository)

    @Singleton
    @Provides
    fun provideSendRegistrationTokenUseCase(tokenRepository: TokenRepository) =
        SendRegistrationTokenToServerUseCase(tokenRepository)

    @Singleton
    @Provides
    fun provideInsertTransactionUseCase(transactionRepository: TransactionRepository) =
        InsertTransactionUseCase(transactionRepository)

    @Singleton
    @Provides
    fun provideUpdateOrderStatusUseCase(orderRepository: OrderRepository) =
        UpdateOrderStatusUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideCreateMomoPaymentUseCase(paymentRepository: PaymentRepository) =
        CreateMomoPaymentUseCase(paymentRepository)

    @Singleton
    @Provides
    fun provideUpdateWorkerInfoUseCase(workerRepository: WorkerRepository) =
        UpdateWorkerInfoUseCase(workerRepository)

    @Singleton
    @Provides
    fun provideGenerateFcmTokenUseCase(tokenRepository: TokenRepository) =
        GenerateFcmTokenUseCase(tokenRepository)

    @Singleton
    @Provides
    fun provideListenRealtimeOrders(orderRepository: OrderRepository) =
        ListenRealtimeOrdersUseCase(orderRepository)

    @Singleton
    @Provides
    fun provideListenRealtimeOrder(orderRepository: OrderRepository) =
        ListenRealtimeOrderUseCase(orderRepository)
}