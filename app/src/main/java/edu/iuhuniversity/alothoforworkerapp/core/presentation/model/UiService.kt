package edu.iuhuniversity.alothoforworkerapp.core.presentation.model

data class UiService(
    val id: String,
    val name: String,
)
