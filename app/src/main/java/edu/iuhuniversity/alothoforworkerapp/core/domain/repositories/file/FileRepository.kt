package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file

import android.net.Uri
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback

interface FileRepository {
    suspend fun getDownloadUrlFromStorage(storageUri: String): Resource<String>
    suspend fun uploadFiles(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    )
}