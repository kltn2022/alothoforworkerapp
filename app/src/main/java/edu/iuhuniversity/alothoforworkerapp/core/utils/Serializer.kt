package edu.iuhuniversity.alothoforworkerapp.core.utils

import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.*
import javax.inject.Inject
import javax.inject.Singleton
import java.lang.reflect.Type;


@Singleton
class Serializer {
    private var gson: Gson

    init {
        // Custom serializer that accepts a DocumentReference data type and returns the
        // reference path as a string.
        val referenceSerializer: JsonSerializer<DocumentReference> =
            JsonSerializer<DocumentReference> { src, type, context ->
                if (src == null) null else JsonPrimitive(
                    src.path
                )
            }
        // Custom deserializer that accepts a json string for the DocumentReference data type and
        // returns a new DocumentReference that is created using the string reference path.
        val referenceDeserializer: JsonDeserializer<DocumentReference> =
            JsonDeserializer<DocumentReference> { json: JsonElement?, type: Type?, context: JsonDeserializationContext? ->
                if (json == null) null else FirebaseFirestore.getInstance()
                    .document(json.asString)
            }
        // Builds the gson object using our custom DocumentReference serializer/deserializer above.
        gson = GsonBuilder()
            .registerTypeAdapter(DocumentReference::class.java, referenceSerializer)
            .registerTypeAdapter(DocumentReference::class.java, referenceDeserializer).create()
    }

    /**
     * Serialize an object to Json.
     *
     * @param entity Object to serialize.
     * @param clazz Type of the entity to serialize.
     */
    fun <T> serialize(entity: T, clazz: Type): String? {
        return gson.toJson(entity, clazz)
    }

    /**
     * Deserialize a json representation of an object.
     *
     * @param string Entity json string to deserialize.
     * @param clazz Type of the entity to deserialize.
     */
    fun <T> deserialize(string: String?, clazz: Type): T {
        return gson.fromJson(string, clazz)
    }
}