package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.Image
import edu.iuhuniversity.alothoforworkerapp.R
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.airbnb.lottie.compose.*
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography

@Composable
fun EmptyDataSection(
    modifier: Modifier = Modifier,
    title: String,
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_empty_results),
            contentDescription = null,
        )
        Text(
            text = "Không có $title mới",
            style = AloThoTypography.h5.copy(fontWeight = FontWeight.SemiBold)
        )
    }
}

@Preview
@Composable
fun EmptySearchResults(
    modifier: Modifier = Modifier,
    title: String = "Không tìm thấy kết quả"
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.empty_search))
        val progress by animateLottieCompositionAsState(
            composition,
            iterations = LottieConstants.IterateForever,
        )

        LottieAnimation(
            composition,
            progress
        )

        if (progress > 0) {
            Text(
                text = title,
                style = AloThoTypography.h5.copy(fontWeight = FontWeight.SemiBold)
            )
        }
    }
}

@Composable
fun LoadingDataSection(
    modifier: Modifier = Modifier,
    title: String = "Đang load dữ liệu...",
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val composition by rememberLottieComposition(LottieCompositionSpec.RawRes(R.raw.loading_data))
        val progress by animateLottieCompositionAsState(
            composition,
            iterations = LottieConstants.IterateForever,
        )

        LottieAnimation(
            composition,
            progress
        )

        if (progress > 0) {
            Text(
                text = title,
                style = AloThoTypography.h5.copy(fontWeight = FontWeight.SemiBold)
            )
        }
    }
}
