package edu.iuhuniversity.alothoforworkerapp.core.data.cache.service

import androidx.room.*
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedservice.CachedService

@Dao
interface ServiceDao {
    @Query("SELECT * FROM tbl_service WHERE serviceTypeId = :serviceTypeId")
    fun getServicesByServiceType(serviceTypeId: String) : MutableList<CachedService>

    @Query("SELECT * FROM tbl_service WHERE id = :serviceId")
    fun getServiceById(serviceId: String) : CachedService?
}