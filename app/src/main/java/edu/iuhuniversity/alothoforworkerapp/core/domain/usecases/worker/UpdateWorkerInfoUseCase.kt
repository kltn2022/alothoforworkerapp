package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.worker

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository

class UpdateWorkerInfoUseCase(private val workerRepository: WorkerRepository) {
    suspend fun execute(uid: String, data: MutableMap<String, Any>) =
        workerRepository.updateWorkerInfo(uid, data)
}