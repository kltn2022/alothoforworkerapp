package edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedservice

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.RoomTypeConverters
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils

@Entity(tableName = "TBL_SERVICE")
@TypeConverters(
    RoomTypeConverters.FeedbackConverter::class, RoomTypeConverters.AddressConverter::class,
    RoomTypeConverters.StringListConverter::class
)
data class CachedService(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    @ColumnInfo
    val name: String,
    @ColumnInfo
    val unit: String,
    @ColumnInfo
    val price: Double,
    @ColumnInfo
    val warrantyPeriod: Int,
    @ColumnInfo
    val serviceTypeId: String
) {
    companion object {
        fun fromDomain(domainModel: Service): CachedService {
            return CachedService(
                id = domainModel.id,
                name = domainModel.name,
                unit = domainModel.unit,
                price = domainModel.price ?: 0.0,
                warrantyPeriod = domainModel.warrantyPeriod,
                serviceTypeId = domainModel.serviceType.id
            )
        }
    }

    fun toDomain(): Service {
        return Service(
            this.id,
            this.name,
            this.unit,
            this.price,
            this.warrantyPeriod,
            CommonUtils.getDocumentReference(
                CollectionConst.COLLECTION_SERVICE_TYPE,
                this.serviceTypeId
            )
        )
    }
}
