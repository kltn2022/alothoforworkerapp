package edu.iuhuniversity.alothoforworkerapp.core.utils

import java.lang.StringBuilder

const val ALOTHO_EMAIL_SUFFIX = "@alothoapp.vn"
const val PHONE_NUMBER_REGEX = "(09|01[2|6|8|9])+([0-9]{8})"


fun isValidatePhoneNumber(phoneNumber: String): Boolean {
    return if (phoneNumber.length !in 9..10) {
        false
    } else {
        val standardPhoneNumber = StringBuilder(phoneNumber)

        if (phoneNumber.length == 9 && !phoneNumber.startsWith("0")) {
            standardPhoneNumber.insert(0, "0")
        }
        Regex(PHONE_NUMBER_REGEX).matches(standardPhoneNumber)
    }
}

fun String.toAloThoPhone(): String {
    // +84968958053
    // 0968958053
    // 968958053
    val formattedPhone = StringBuilder(this)
    if (this.length == 10 && this.startsWith("0")) {
        formattedPhone.deleteCharAt(0)
    }
    if (!this.startsWith("+84")) {
        formattedPhone.insert(0, "+84")
    }
    return formattedPhone.toString()
}

fun String.revertToAloThoPhone(): String {
    return this.removeSuffix(ALOTHO_EMAIL_SUFFIX)
}


