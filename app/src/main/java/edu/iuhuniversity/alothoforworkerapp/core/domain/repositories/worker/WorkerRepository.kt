package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker

interface WorkerRepository {
    suspend fun getWorker(uid: String): Resource<Worker?>
    suspend fun getWorker(documentReference: DocumentReference): Resource<Worker?>
    suspend fun getWorkerByPhoneNumber(phoneNumber: String): Resource<Worker?>
    suspend fun getCurrentUser(): Resource<Worker?>?
    suspend fun updateWorkerInfo(uid: String, data: MutableMap<String, Any>): Resource<Boolean>
    suspend fun insertWorker(firebaseUser: FirebaseUser): Resource<Worker?>
}