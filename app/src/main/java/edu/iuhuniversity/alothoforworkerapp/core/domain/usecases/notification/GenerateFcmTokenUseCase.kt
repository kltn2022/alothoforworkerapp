package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository

class GenerateFcmTokenUseCase(private val tokenRepository: TokenRepository) {
    suspend fun execute() = tokenRepository.generateRegistrationToken()
}