package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasourceimpl

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User.Companion.toUser
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker.Companion.toWorker
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.toResponse
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasource.WorkerRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.utils.toAloThoPhone

class WorkerRemoteDataSourceImpl(
    db: FirebaseFirestore
) : WorkerRemoteDataSource {
    private val workerCollection = db.collection(CollectionConst.COLLECTION_WORKER);
    private val dataSource: FirestoreDataSource<Worker> = FirestoreDataSourceImpl(workerCollection)

    override suspend fun getWorker(uid: String): Resource<Worker?> {
        return dataSource.get(uid) {
            it.toWorker()
        }
    }

    override suspend fun insertWorker(firebaseUser: FirebaseUser): Resource<Worker?> {
        return dataSource.insert(Worker.convertFrom(firebaseUser)!!) { it.toWorker() }
    }

    override suspend fun updateWorkerInfo(
        uid: String,
        data: MutableMap<String, Any>
    ): Resource<Boolean> {
        return dataSource.update("id" to uid, data)
    }

    override suspend fun getWorker(documentReference: DocumentReference): Resource<Worker?> {
        return documentReference.get()
            .toResponse {
                it.toWorker()
            }
    }

    override suspend fun getWorkerByPhoneNumber(phoneNumber: String): Resource<Worker?> {
        return Resource.Success(dataSource
            .filterWith(
                workerCollection
                    .whereEqualTo("phoneNumber", phoneNumber.toAloThoPhone()),
            ) { it.toWorker() }
            .data
            ?.first())
    }


}