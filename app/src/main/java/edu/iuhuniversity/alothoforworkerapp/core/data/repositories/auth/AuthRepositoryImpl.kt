package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.auth

import android.app.Activity
import androidx.annotation.NonNull
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository
import edu.iuhuniversity.alothoforworkerapp.core.utils.toAloThoPhone
import kotlinx.coroutines.tasks.await
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AuthRepositoryImpl(
    private val firebaseAuth: FirebaseAuth,
    private val workerRepository: WorkerRepository,
    private val workerPreferencesRepository: WorkerPreferencesRepository
) : AuthRepository {


    override suspend fun sendOtp(
        activity: Activity,
        phoneNumber: String,
        resendingToken: PhoneAuthProvider.ForceResendingToken?,
        callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    ) {
        val options = PhoneAuthOptions.newBuilder(firebaseAuth)
            .setPhoneNumber(phoneNumber.toAloThoPhone())
            .setActivity(activity)
            .setTimeout(50L, TimeUnit.SECONDS)
            .setCallbacks(callback)
            .requireSmsValidation(false)

        if (resendingToken != null) {
            options.setForceResendingToken(resendingToken)
        }

        PhoneAuthProvider.verifyPhoneNumber(options.build())

    }

    private suspend fun getAndSaveAuthToken(tokenTask: Task<GetTokenResult>) {
        tokenTask
            .await()
            .let {
                it.token?.let { token ->
                    Timber.i("AuthToken", token)
                    workerPreferencesRepository.saveAuthToken(token)
                }
            }
    }

    private fun settingsDefaultPhoneAuth() {
        val phoneNumber = "+84968958033"
        val smsCode = "190599"
        firebaseAuth.firebaseAuthSettings
            .setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode)
    }

    override suspend fun signInWithPhoneAuthCredential(
        @NonNull verificationId: String,
        @NonNull otp: String
    ): Resource<Worker?> {
        try {
            settingsDefaultPhoneAuth()

            Timber.d("verificationId: ", verificationId, "otp: ", otp)
            val phoneAuthCredential = PhoneAuthProvider.getCredential(verificationId, otp)

            firebaseAuth.signInWithCredential(phoneAuthCredential)
                .let { task ->
                    task.await()
                        .let { authResult ->
                            if (task.isSuccessful) {
                                val currentUser = authResult.user
                                    ?: return Resource.Error("Vui lòng đăng nhập lại để tiếp tục!")

                                // save current user info
                                workerPreferencesRepository.saveCurrentWorkerInfo(currentUser)
                                // save auth token
                                getAndSaveAuthToken(currentUser.getIdToken(true))

                                // check if user if exist
                                val storedWorker =
                                    workerRepository.getWorkerByPhoneNumber(phoneNumber = currentUser.phoneNumber!!)
                                return if (storedWorker.data != null) storedWorker
                                else
                                    workerRepository.insertWorker(currentUser) //authResult.user

                            }

                            return task.exception?.let {
                                if (task.exception is FirebaseAuthInvalidCredentialsException) {
                                    Resource.Error("OTP không hợp lệ. Vui lòng thử lại!")
                                } else {
                                    it.message?.let { it1 -> Resource.Error(it1) }
                                }
                            }!!
                        }
                }
        } catch (e: Exception) {
            Timber.tag("AuthRepositoryImpl").w(e, e.message.toString())
            return Resource.Error(e.message ?: "Có lỗi nào đó xảy ra. Vui lòng thử lại!")
        }


    }

    override fun isSingedIn(): Boolean {
        if (firebaseAuth.currentUser != null) {
            return true
        }
        return false;
    }

    override fun logout() {
        firebaseAuth.signOut()
    }
}