package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message

import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasource.MessageRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Conservation
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.ConservationInfo
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Message
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

class MessageRepositoryImpl(
    private val messageRemoteDataSource: MessageRemoteDataSource,
    private val workerPreferencesRepository: WorkerPreferencesRepository,
    private val userRepository: UserRepository
) : MessageRepository {

    override suspend fun sendMessage(
        conservationId: String,
        content: String,
        messageType: MessageType
    ): Resource<Message?> {
        val from = workerPreferencesRepository.currentUserUid.first()
        val message = Message.createNewMessage(
            conservationId,
            content,
            from!!,
            messageType
        )
        return messageRemoteDataSource.sendMessage(message)
    }

    override suspend fun listenForChatMessages(conservationId: String): Flow<List<Message?>> {
        return messageRemoteDataSource.listenForChatMessages(conservationId)
    }

    override suspend fun getConservationInfo(conservationId: String): Resource<Conservation?> {
        return messageRemoteDataSource.getConservationInfo(conservationId)
    }

    override suspend fun listenForConservations(): Flow<List<ConservationInfo?>> {
        val worker = workerPreferencesRepository.workerPreference.first()

        return messageRemoteDataSource.getConservations().map {
            it.map { conservation ->
                val user = userRepository.getUser(conservation?.customerId!!).data
                ConservationInfo(
                    id = conservation.id,
                    customer = user!!,
                    consultant = worker!!
                )
            }
        }
    }
}