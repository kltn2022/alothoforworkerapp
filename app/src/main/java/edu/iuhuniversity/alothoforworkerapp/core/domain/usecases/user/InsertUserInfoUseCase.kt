package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user

import com.google.firebase.auth.FirebaseUser
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository

class InsertUserInfoUseCase(private val userRepository: UserRepository) {
    suspend fun execute(firebaseUser: FirebaseUser) = userRepository.insertUser(firebaseUser)
}

