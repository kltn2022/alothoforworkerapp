package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging

import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.tasks.await
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmCreateNotificationGroup
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmGroupMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmNotificationGroupResponse
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse
import retrofit2.Response

class FirebaseSendMessageServiceImpl(
    private val iFcmService: IFcmService
) : FirebaseSendMessageService {
    override suspend fun sendMessage(payload: FcmMessagePayload<String>): Response<FcmResponse> {
        return iFcmService.sendMessage(payload)
    }

    override suspend fun <T> sendGroupMessage(payload: FcmGroupMessagePayload<T>): Response<FcmResponse> {
       return iFcmService.sendGroupMessage(payload)
    }

    override suspend fun createDeviceGroup(payload: FcmCreateNotificationGroup): Response<FcmNotificationGroupResponse> {
        return iFcmService.createDeviceGroup(payload)
    }

    override suspend fun retrievingNotificationKey(notificationKeyName: String): Response<FcmNotificationGroupResponse> {
        return iFcmService.retrievingNotificationKey(notificationKeyName)
    }

    override suspend fun subscribeToTopic(topicName: String): Resource<Boolean> {
        FirebaseMessaging
            .getInstance()
            .subscribeToTopic(topicName)
            .let { task ->
                task
                    .await()
                    .let {
                        if (!task.isSuccessful) {
                            return task.exception?.message?.let { it1 -> Resource.Error(it1) }!!
                        }
                        return Resource.Success(true)
                    }
            }
    }
}