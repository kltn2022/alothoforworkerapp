package edu.iuhuniversity.alothoforworkerapp.core.data.model.order

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class OrderDetail(
    val unitPrice: Double,
    val serviceId: String, // serviceId: j3Sbvzt1RN4T7CeihMMl
    val subtotal: Double,
    val quantity: Int,
) : Parcelable, Serializable