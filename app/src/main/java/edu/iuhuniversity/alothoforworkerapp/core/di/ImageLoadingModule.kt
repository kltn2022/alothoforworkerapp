package edu.iuhuniversity.alothoforworkerapp.core.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetAuthTokenUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ImageLoadingModule {

    @Singleton
    @Provides
    fun provideImageLoadingUtil(
        getAuthTokenUseCase: GetAuthTokenUseCase
    ): ImageLoadingUtil {
        return ImageLoadingUtil(getAuthTokenUseCase)
    }
}
