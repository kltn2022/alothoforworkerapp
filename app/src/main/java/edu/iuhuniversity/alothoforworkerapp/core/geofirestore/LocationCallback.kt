package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation

interface LocationCallback {
    /**
     * This method is called with the current location of the key. location will be null if there is no location
     * stored in GeoFire for the key.
     * @param key The key whose location we are getting
     * @param location The location of the key
     */
    fun onLocationResult(key: String?, location: GeoLocation?)

    /**
     * Called if the callback could not be added due to failure on the server or security rules.
     * @param exception The exception that occurred
     */
    fun onCancelled(exception: Exception?)
}

