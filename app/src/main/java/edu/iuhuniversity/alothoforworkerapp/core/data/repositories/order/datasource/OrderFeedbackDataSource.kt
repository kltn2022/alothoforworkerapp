package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus

sealed interface OrderFeedbackDataSource {
    interface Remote {
        suspend fun createOrUpdateFeedback(orderId: String, rating: Float, review: String): Resource<Boolean>
        suspend fun updateWorkerRating(orderId: String, oldRating: Float, newRating: Float): Resource<Boolean>
        suspend fun addWorkerToFavoriteList(workerId: String): Resource<Boolean>
    }
}