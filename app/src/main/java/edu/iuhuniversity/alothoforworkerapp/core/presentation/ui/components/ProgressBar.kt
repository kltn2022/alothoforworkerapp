package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLightVariantColor

@Composable
fun MyLinearProgressIndicator(
    modifier: Modifier = Modifier,
    progress: Float,
) {
    LinearProgressIndicator(
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier),
        color = PrimaryColor,
        trackColor = PrimaryLightVariantColor,
        progress = progress
    )
}