package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository

class GetRegistrationTokenUseCase(private val tokenRepository: TokenRepository) {
    suspend fun execute(uid: String) = tokenRepository.getRegistrationToken(uid)
}