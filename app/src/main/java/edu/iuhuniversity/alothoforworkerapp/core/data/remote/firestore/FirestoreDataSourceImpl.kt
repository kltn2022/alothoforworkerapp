package edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore

import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import edu.iuhuniversity.alothoforworkerapp.core.data.model.PAGINATE_DEFAULT_OFFSET
import edu.iuhuniversity.alothoforworkerapp.core.data.model.PagedResult
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import kotlinx.coroutines.tasks.await

class FirestoreDataSourceImpl<T>(
    private val collectionRef: CollectionReference
): FirestoreDataSource<T> {

    override var totalItems: Long? = 0
        get() = field
        set(value) {
            field = value ?: 0
        }

    override suspend fun get(
        documentId: String,
        transformObject: (DocumentSnapshot) -> T
    ): Resource<T?> {
        return collectionRef
            .document(documentId)
            .get()
            .toResponse(transformObject)
    }

    override suspend fun filterWith(
        query: Query,
        transformObject: (DocumentSnapshot) -> T
    ): Resource<MutableList<T>> {
        return query
            .get()
            .toResponses(transformObject)
    }

    suspend fun getAll(
        page: Long,
        size: Long? = PAGINATE_DEFAULT_OFFSET,
        initialQuery: Query? = null,
        orderByField: String? = "id",
        transformObject: (DocumentSnapshot) -> T
    ): Resource<PagedResult<T>> {
        if (page == 0L) {
            return Resource.Error("Page must be greater than 0")
        } else {
            if (totalItems?.compareTo(0L)!! > 0) {
                totalItems = collectionRef.get().await().size().toLong()
            }

            val totalPages = (totalItems ?: 0) / (size ?: 0)

            val offset = page.minus(1).times(size ?: 0)

            val firstQuery = initialQuery
                ?.orderBy(orderByField.orEmpty())
                ?.limit(offset)

            firstQuery
                ?.get()
                ?.let { task ->
                    task.await()
                        .let { documentSnapshots ->
                            if (!task.isSuccessful) {
                                return task.exception?.message?.let { Resource.Error(it) }!!
                            }

                            val lastVisible =
                                documentSnapshots?.documents?.get(documentSnapshots.size() - 1)

                            initialQuery
                                .orderBy(orderByField.orEmpty())
                                .startAfter(lastVisible)
                                .limit(size ?: 0)
                                .get()
                                .let { task ->
                                    task
                                        .await()
                                        .let { querySnapshot ->
                                            if (!task.isSuccessful) {
                                                return task.exception?.message?.let {
                                                    Resource.Error(
                                                        it
                                                    )
                                                }!!
                                            }
                                            return querySnapshot
                                                ?.mapNotNull {
                                                    transformObject(it)
                                                }
                                                ?.toList<T>()
                                                .let {
                                                    Resource.Success(
                                                        PagedResult(
                                                            currentPage = page,
                                                            perPage = size!!,
                                                            totalPages = totalPages,
                                                            totalResults = totalItems!!,
                                                            results = it
                                                        )
                                                    )
                                                }
                                        }
                                }
                        }
                }
        }
        return Resource.Error("Something wrong")
    }

    override suspend fun getAll(
        transformObject: (DocumentSnapshot) -> T
    ): Resource<MutableList<T>> {
        return collectionRef
            .get()
            .toResponses(transformObject)
    }

    override suspend fun insert(
        data: T,
        transformObject: (DocumentSnapshot) -> T?
    ): Resource<T?> {
        if (data == null)
            return Resource.Error("Data is Null. This method or property cannot be called on Null values")

        collectionRef
            .add(data)
            .let { task ->
                task.await()
                    .let { docRef ->
                        if (!task.isSuccessful) {
                            return task.exception?.message?.let { Resource.Error(it) }!!
                        }

                        return docRef
                            .get()
                            .toResponse(transformObject)
                    }
            }
    }

    override suspend fun update(
        documentId: String,
        data: MutableMap<String, Any>
    ): Resource<Boolean> {
        collectionRef
            .document(documentId)
            .update(data)
            .let { task ->
                task.await()
                    .let {
                        return if (task.isSuccessful) Resource.Success(true)
                        else task.exception?.message?.let { error -> Resource.Error(error) }!!
                    }
            }
    }

    override suspend fun delete(documentId: String): Resource<Boolean> {
        collectionRef
            .document(documentId)
            .delete()
            .let { task ->
                task.await()
                    .let {
                        return if (task.isSuccessful) Resource.Success(true)
                        else task.exception?.message?.let { error -> Resource.Error(error) }!!
                    }
            }
    }

    override suspend fun set(documentId: String, data: T): Resource<Boolean> {
        collectionRef
            .document(documentId)
            .set(data!!)
            .let { task ->
                task.await()
                    .let {
                        return if (task.isSuccessful) Resource.Success(true)
                        else task.exception?.message?.let { error -> Resource.Error(error) }!!
                    }
            }
    }

    override suspend fun update(
        keyValue: Pair<String, String>,
        data: MutableMap<String, Any>
    ): Resource<Boolean> {
        collectionRef
            .whereEqualTo(keyValue.first, keyValue.second)
            .get()
            .await()
            .first()
            .reference
            .update(data)
            .let { task ->
                task.await()
                    .let {
                        return if (task.isSuccessful) Resource.Success(true)
                        else task.exception?.message?.let { error -> Resource.Error(error) }!!
                    }
            }
    }
}
