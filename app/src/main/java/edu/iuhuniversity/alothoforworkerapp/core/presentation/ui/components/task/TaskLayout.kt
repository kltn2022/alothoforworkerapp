package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.task

import androidx.compose.foundation.BorderStroke
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.FilledTonalButton
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import edu.iuhuniversity.alothoforworkerapp.task.model.UiOrder
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.utils.CurrencyUtils

@Composable
fun TaskItem(order: UiOrder, onTaskItemClicked: () -> Unit) {
    Card(
        shape = RoundedCornerShape(24.dp),
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .clip(RoundedCornerShape(24.dp))
            .clickable {
                onTaskItemClicked()
            },
        border = BorderStroke(1.dp, PrimaryColor),
    ) {
        Column(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(12.dp),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(12.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_tho_thong_hut_cong),
                    contentDescription = null,
                    modifier = Modifier
                        .size(40.dp)
                        .clip(CircleShape)
                        .border(2.dp, PrimaryColor, shape = CircleShape)
                        .padding(5.dp)
                )
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.Start,
                ) {
                    Text(
                        text = "Mã đơn hàng: ${order.id}",
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Bold,
                        )
                    )
                    Text(
                        text = "Dịch vụ: ${order.services?.first()?.name}",
                        style = AloThoTypography.subtitle2.copy(
                            fontWeight = FontWeight.Light
                        )
                    )
                }
            }
            Divider()
            Column(
                verticalArrangement = Arrangement.spacedBy(space = 5.dp, alignment = Alignment.Top),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = "Tình trạng: ",
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Bold,
                        )
                    )
                    Text(
                        text = order.status?.title.orEmpty(),
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Light
                        )
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()

                ) {
                    Text(
                        text = "Tổng tiền dự kiến",
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Bold,
                        )
                    )
                    Text(
                        text = CurrencyUtils.formatVnCurrency(order.estimatedTotal.toString()),
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Light
                        )
                    )
                }
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Text(
                        text = "Thợ làm việc: ",
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Bold,
                        )
                    )
                    Text(
                        text = order.worker?.displayName.orEmpty(),
                        style = AloThoTypography.body2.copy(
                            fontWeight = FontWeight.Light
                        )
                    )
                }
                FilledTonalButton(
                    onClick = {

                    },
                    colors = ButtonDefaults.filledTonalButtonColors(
                        containerColor = PrimaryVariantColor,
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Text("Xem đơn hàng")
                }
            }
        }
    }
}

//@Preview
//@Composable
//fun TaskList() {
//    LazyList(
//        items = DemoDataProvider.defaultOrders,
//        spaceBetweenItems = 10.dp
//    ) {
//        TaskItem(order = it)
//    }
//}