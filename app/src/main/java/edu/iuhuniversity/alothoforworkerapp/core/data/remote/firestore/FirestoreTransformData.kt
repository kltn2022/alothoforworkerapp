package edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.tasks.await
import timber.log.Timber

suspend fun <T> Task<DocumentSnapshot>.toResponse(transformObject: (DocumentSnapshot) -> T): Resource<T?> {
    this.await()
        .let { documentSnapshot ->
            if (!this.isSuccessful) {
                return this.exception?.message?.let { Resource.Error(it) }!!
            } else {
                if (!documentSnapshot.exists()) {
                    return Resource.Success(null)
                }
                return Resource.Success(transformObject(documentSnapshot))
            }

        }
}

suspend fun <T> Task<QuerySnapshot>.toResponses(transformObject: (DocumentSnapshot) -> T): Resource<MutableList<T>> {
    this.await()
        .let { querySnapshot ->
            if (!this.isSuccessful) {
                return this.exception?.message?.let { Resource.Error(it) }!!
            }
            return querySnapshot
                .mapNotNull {
                    transformObject(it)
                }
                .toMutableList<T>()
                .let {
                    Resource.Success(it)
                }
        }
}

@ExperimentalCoroutinesApi
fun Query.getQuerySnapshotFlow(): Flow<QuerySnapshot?> {
    return callbackFlow {
        val listenerRegistration =
            addSnapshotListener { value, error ->
                if (error != null) {
                    Timber.d(error)
                    cancel(
                        message = "Listened failed",
                        cause = error
                    )
                    return@addSnapshotListener
                }
                trySend(value)
            }
        awaitClose {
            Timber.d("Cancelling the listener on collection path")
            listenerRegistration.remove()
        }
    }
}

@ExperimentalCoroutinesApi
fun DocumentReference.getDocRefSnapshotFlow(): Flow<DocumentSnapshot?> {
    return callbackFlow {
        val listenerRegistration =
            addSnapshotListener { value, error ->
                if (error != null) {
                    Timber.d(error)
                    cancel(
                        message = "Listened failed",
                        cause = error
                    )
                    return@addSnapshotListener
                }
                trySend(value)
            }
        awaitClose {
            Timber.d("Cancelling the listener on document path")
            listenerRegistration.remove()
        }
    }
}

@ExperimentalCoroutinesApi
fun <T> Query.getDataFlow(mapper: (QuerySnapshot?) -> T): Flow<T> {
    return getQuerySnapshotFlow()
        .map {
            return@map mapper(it)
        }
}

@ExperimentalCoroutinesApi
fun <T> DocumentReference.getDataFlow(mapper: (DocumentSnapshot?) -> T): Flow<T> {
    return getDocRefSnapshotFlow()
        .map {
            return@map mapper(it)
        }
}