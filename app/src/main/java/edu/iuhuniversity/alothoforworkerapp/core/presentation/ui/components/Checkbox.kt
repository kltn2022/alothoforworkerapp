package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.selection.selectable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor

@Composable
fun CustomTriStateCheckbox(
    categoryName: String,
    checked: List<Boolean>,
    vararg checkboxes: @Composable () -> Unit,
    onParentStateChange: ((Boolean) -> Unit),
    modifier: Modifier = Modifier
) {
    val numberOfCheckedState = remember { mutableStateOf(checked.count { it }) }
    val parentState = remember(numberOfCheckedState) {
        when (numberOfCheckedState.value) {
            checked.size -> ToggleableState.On
            0 -> ToggleableState.Off
            else -> ToggleableState.Indeterminate
        }
    }

    Column(
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        ParentCheckBox(
            item = categoryName,
            onParentClickListener = {
                onParentStateChange(parentState != ToggleableState.On)
            },
            parentState = parentState,
            modifier = modifier
        )
        checkboxes.forEach { it() }
    }
}


@Composable
fun SingleCheckbox(
    modifier: Modifier = Modifier,
    item: String,
    initialChecked: Boolean = false,
    colors: CheckboxColors? = null,
    onCheckedChangeListener: ((Boolean) -> Unit),
) {
    val checkedState = remember { mutableStateOf(initialChecked) }

    Row(
        Modifier
            .then(modifier)
            .fillMaxWidth()
            .wrapContentSize(align = Alignment.CenterStart)
            .selectable(
                selected = (checkedState.value == !checkedState.value),
                onClick = {
                    onCheckedChangeListener(checkedState.value)
                },
                role = Role.Checkbox
            )
            .padding(vertical = 5.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Checkbox(
            checked = checkedState.value,
            colors = colors
                ?: CheckboxDefaults.colors(
                    checkedColor = PrimaryColor,
                    uncheckedColor = PrimaryVariantColor
                ),
            onCheckedChange = onCheckedChangeListener
        )
        Text(
            text = item,
            style = AloThoTypography.body1,
            modifier = Modifier
                .padding(start = 16.dp)
        )
    }
}

@Composable
fun ParentCheckBox(
    modifier: Modifier = Modifier,
    item: String,
    colors: CheckboxColors? = null,
    onParentClickListener: (() -> Unit),
    parentState: ToggleableState
) {
    Row(
        Modifier
            .then(modifier)
            .fillMaxWidth()
            .wrapContentSize(align = Alignment.CenterStart)
            .selectable(
                selected = parentState == ToggleableState.On || parentState == ToggleableState.Indeterminate,
                onClick = onParentClickListener,
                role = Role.Checkbox
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        TriStateCheckbox(
            state = parentState,
            onClick = onParentClickListener,
            colors = colors
                ?: CheckboxDefaults.colors(
                    checkedColor = PrimaryColor,
                    uncheckedColor = PrimaryVariantColor
                ),
        )
        Text(
            text = item,
            style = AloThoTypography.body1,
            modifier = Modifier
                .padding(start = 16.dp)
        )
    }
}