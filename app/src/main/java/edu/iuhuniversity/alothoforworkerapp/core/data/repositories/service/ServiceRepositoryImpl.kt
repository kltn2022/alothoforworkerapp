package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service.ServiceRepository
import timber.log.Timber

class ServiceRepositoryImpl(
    private val serviceCacheDataSource: ServiceDataSource.Cache,
    private val serviceRemoteDataSource: ServiceDataSource.Remote,

    ) : ServiceRepository {
    override suspend fun getServicesByServiceType(serviceTypeId: String): Resource<MutableList<Service>> {
        lateinit var services: List<Service>
        try {
            services = serviceCacheDataSource.getServicesByTypeFromDb(serviceTypeId)
        } catch (e: Exception) {
            Timber.i(e.message!!)
            return Resource.Error(e.message!!)
        }

        return if (services.isNotEmpty()) {
            Resource.Success(services)
        } else {
            serviceRemoteDataSource.getServicesByType(serviceTypeId)
        }

    }

    override suspend fun getService(serviceId: String): Resource<Service?> {
        var service: Service?
        try {
            service = serviceCacheDataSource.getServiceById(serviceId)
        } catch (e: Exception) {
            Timber.i(e.message!!)
            return Resource.Error(e.message!!)
        }

        return if (service != null) {
            Resource.Success(service)
        } else {
            serviceRemoteDataSource.getServiceById(serviceId)
        }

    }
}