package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.service

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service.ServiceRepository

class GetServicesByTypeUseCase(private val serviceRepository: ServiceRepository) {
    suspend fun execute(serviceTypeId: String) = serviceRepository.getServicesByServiceType(serviceTypeId)
}