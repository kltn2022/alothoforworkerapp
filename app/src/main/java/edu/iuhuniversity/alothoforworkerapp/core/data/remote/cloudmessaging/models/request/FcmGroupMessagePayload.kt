package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request

import com.google.gson.annotations.SerializedName
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmData
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification

/**
 * Reference: https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload
 * -support
 */
data class FcmGroupMessagePayload<T>(
    @SerializedName("registration_ids")
    val to: List<String>,
    @SerializedName("data")
    val data: FcmData<T>,
    @SerializedName("notification")
    val notification: FcmNotification? = null
)