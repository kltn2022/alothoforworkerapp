package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.Color
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.utils.parse
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapColorSystem.GeneralColorEnum

enum class BootstrapColorSystem {
    ;
    enum class GeneralColorEnum {
        COLOR_0,
        COLOR_5,
        COLOR_10,
        COLOR_20,
        COLOR_30,
        COLOR_40,
        COLOR_50,
        COLOR_60,
        COLOR_70,
        COLOR_80,
        COLOR_90,
        COLOR_100
    }

    enum class DefaultColorEnum(val hexColor: String) {
        COLOR_0("#f7f7fa"),
        COLOR_5("#ededf0"),
        COLOR_10("#e1e1e3"),
        COLOR_20("#d2d2d6"),
        COLOR_30("#b4b4bb"),
        COLOR_40("#9696a0"),
        COLOR_50("#787885"),
        COLOR_60("#5a5b6a"),
        COLOR_70("#4a4b57"),
        COLOR_80("#3a3a44"),
        COLOR_90("#292a31"),
        COLOR_100("#19191d")
    }

    enum class PrimaryColorEnum(val hexColor: String) {
        COLOR_0("#f5f8ff"),
        COLOR_5("#ebf2ff"),
        COLOR_10("#d8e6ff"),
        COLOR_20("#c4daff"),
        COLOR_30("#9dc2ff"),
        COLOR_40("#76a9ff"),
        COLOR_50("#4f91ff"),
        COLOR_60("#2979ff"),
        COLOR_70("#2264d1"),
        COLOR_80("#1b4ea3"),
        COLOR_90("#133774"),
        COLOR_100("#0c2146")
    }

    enum class WarningColorEnum(val hexColor: String) {
        COLOR_0("#fff8eb"),
        COLOR_5("#ffefd1"),
        COLOR_10("#ffe5b3"),
        COLOR_20("#ffd98f"),
        COLOR_30("#f5ce84"),
        COLOR_40("#ebbf67"),
        COLOR_50("#e5ae40"),
        COLOR_60("#d6981b"),
        COLOR_70("#b88217"),
        COLOR_80("#8f6512"),
        COLOR_90("#66480d"),
        COLOR_100("#463209")
    }

    enum class DangerColorEnum(val hexColor: String) {
        COLOR_0("#fef2f1"),
        COLOR_5("#fee8e7"),
        COLOR_10("#fddcda"),
        COLOR_20("#fccbc8"),
        COLOR_30("#faa9a3"),
        COLOR_40("#f8877f"),
        COLOR_50("#f6655a"),
        COLOR_60("#f44336"),
        COLOR_70("#c8372d"),
        COLOR_80("#9c2b23"),
        COLOR_90("#6f1f19"),
        COLOR_100("#43130f")
    }

    enum class SuccessColorEnum(val hexColor: String) {
        COLOR_0("#f5faf5"),
        COLOR_5("#ecf7ed"),
        COLOR_10("#dceddd"),
        COLOR_20("#cbe5cc"),
        COLOR_30("#a9d3ab"),
        COLOR_40("#87c289"),
        COLOR_50("#65b168"),
        COLOR_60("#43a047"),
        COLOR_70("#37833b"),
        COLOR_80("#2b662e"),
        COLOR_90("#1f4921"),
        COLOR_100("#132c14")
    }

}

data class DialogAttribute(
    val textColor: Color,
    val backgroundColor: Color,
    val btnPositiveTextColor: Color,
    val btnPositiveBg: Color,
    val btnNegativeTextColor: Color,
    val btnNegativeBg: Color,
    val iconColor: Color
)


enum class BootstrapType {
    FILLED, SMOOTH, RAISED
}


enum class BootstrapColor {
    DEFAULT {
        override fun getColorByVal(value: GeneralColorEnum): Color
            = Color.parse(BootstrapColorSystem.DefaultColorEnum.valueOf(value.name).hexColor)
    },
    PRIMARY {
        override fun getColorByVal(value: GeneralColorEnum): Color
                = Color.parse(BootstrapColorSystem.PrimaryColorEnum.valueOf(value.name).hexColor)
    },
    WARNING {
        override fun getColorByVal(value: GeneralColorEnum): Color
                = Color.parse(BootstrapColorSystem.WarningColorEnum.valueOf(value.name).hexColor)
    },
    DANGER {
        override fun getColorByVal(value: GeneralColorEnum): Color
                = Color.parse(BootstrapColorSystem.DangerColorEnum.valueOf(value.name).hexColor)
    },
    SUCCESS {
        override fun getColorByVal(value: GeneralColorEnum): Color
                = Color.parse(BootstrapColorSystem.SuccessColorEnum.valueOf(value.name).hexColor)
    };

    abstract fun getColorByVal(value: GeneralColorEnum): Color
}

enum class MessageType(
    var color: BootstrapColor = BootstrapColor.DANGER,
    @field:DrawableRes var resIconId: Int
) {
    INFO(
        BootstrapColor.PRIMARY,
        R.drawable.ic_info_circle_solid
    ),
    SUCCESS(
        BootstrapColor.SUCCESS,
        R.drawable.ic_check_circle_solid
    ),
    WARNING(
        BootstrapColor.WARNING,
        R.drawable.ic_warning
    ),
    ERROR(
        BootstrapColor.DANGER,
        R.drawable.ic_error_solid
    );
}