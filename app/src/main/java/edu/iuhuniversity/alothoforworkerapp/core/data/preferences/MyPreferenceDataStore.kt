package edu.iuhuniversity.alothoforworkerapp.core.data.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import timber.log.Timber
import java.io.IOException

/**
 * [MyPreferenceDataStore] Sample
 * + You can extend or call this class to use these following function
 */
class MyPreferenceDataStore(
    private val dataStorePrefs: DataStore<Preferences>
) {

    /**
     * Get Flow of data by a key name
     *
     * @param key name of preference key
     * @return The Flow of Data
     *
     * @see #getData(mapTransfrom: (Preferences) -> T) for a custom get data function
     *
     */
    fun getData(key: String): Flow<Any?> {
        return dataStorePrefs.data
            .catch { exception ->
                // dataStore.data throws an IO Exception when an error is encountered when reading data
                if (exception is IOException) {
                    Timber.d(exception, "Error reading user preferences")
                    emit(emptyPreferences())
                } else {
                    Timber.d(exception, exception.message)
                    throw exception
                }
            }.map { preferences ->
                // Get our show completed value
                preferences[getPreferencesKey(key, Any::class.java)]
            }
            .distinctUntilChanged()
    }

    /**
     * Get custom Flow of data
     *
     * @param mapTransfrom a custom function to get data
     * @return The Flow of Data
     *
     * @see #getData(String) for getting single data
     *
     */
    fun <T> getData(mapTransfrom: (Preferences) -> T): Flow<T> {
        return dataStorePrefs.data
            .catch { exception ->
                // dataStore.data throws an IO Exception when an error is encountered when reading data
                if (exception is IOException) {
                    Timber.d(exception, "Error reading user preferences")
                    emit(emptyPreferences())
                } else {
                    Timber.d(exception, exception.message)
                    throw exception
                }
            }.map { preferences ->
                mapTransfrom(preferences)
            }
            .distinctUntilChanged()
    }

    /**
     * Write data to preference by a pair of key-value
     *
     * @param key key name
     * @param value value of key
     *
     * @see #putData(MutableMap) for putting multiple data
     * @see #putData(String, Any, moreFieldsAndValues) for putting multiple data
     *
     */
    suspend fun putData(key: String, value: Any) {
        dataStorePrefs.edit { preferences ->
            preferences.plusAssign(getPreferencesKeyPair(key, value))
        }
    }

    /**
     * Write data to preference by a map of pair of key-value
     *
     * @param data A Data Map
     *
     * @see #putData(String, Any) for putting single data
     * @see #putData(String, Any, moreFieldsAndValues) for putting multiple data
     *
     */
    suspend fun putData(data: MutableMap<String, Any>) {
        dataStorePrefs.edit { preferences ->
            data.forEach { (key, value) ->
                preferences.plusAssign(getPreferencesKeyPair(key, value))
            }
        }
    }

    /**
     * Write data to preference by a multiple fields of pair of key-value
     *
     * @param key The first key name
     * @param value The first key value
     * @param moreFieldsAndValues More fields and values (optional)
     *
     * @see #putData(String, Any) for putting single data
     * @see #putData(MutableMap) for putting multiple data
     *
     */
    suspend fun <T> putData(key: String, value: Any, vararg moreFieldsAndValues: Any) {
        dataStorePrefs.edit { preferences ->
            preferences.plusAssign(getPreferencesKeyPair(key, value))

            if (!moreFieldsAndValues.isNullOrEmpty()) {
                val fieldsAndValuesIterator = moreFieldsAndValues.iterator()
                while (fieldsAndValuesIterator.hasNext()) {
                    preferences.plusAssign(
                        getPreferencesKeyPair(
                            fieldsAndValuesIterator.next() as String,
                            fieldsAndValuesIterator.next()
                        )
                    )
                }
            }
        }
    }

    /**
     * Remove Data by one or more key name
     *
     * @param keys one or more key name
     *
     */
    suspend fun removeData(vararg keys: String) {
        dataStorePrefs.edit { preferences ->
            if (!keys.isNullOrEmpty()) {
                keys.forEach { key ->
                    preferences.remove(getPreferencesKey(key))
                }
            }
        }
    }

    /**
     * Removes all preferences
     */
    suspend fun removeAll() {
        dataStorePrefs.edit { preferences ->
            preferences.clear()
        }
    }

    /**
     * Get preferencesKeyPair by key and value
     *
     * @param key name of key
     * @param value value of key
     *
     * @return Preferences.Pair<*>
     *
     */
    @Suppress("UNCHECKED_CAST")
    fun getPreferencesKeyPair(key: String, value: Any? = ""): Preferences.Pair<*> {
        return when (value) {
            is String -> {
                stringPreferencesKey(key) to value
            }
            is Int -> {
                intPreferencesKey(key) to value
            }
            is Double -> {
                doublePreferencesKey(key) to value
            }
            is Long -> {
                longPreferencesKey(key) to value
            }
            is Float -> {
                floatPreferencesKey(key) to value
            }
            is Boolean -> {
                booleanPreferencesKey(key) to value
            }
            else -> {
                stringSetPreferencesKey(key) to (value as Set<String>)
            }
        }
    }

    /**
     * Get preferencesKey by key and value
     *
     * @param key name of key
     * @param value value of key
     *
     * @return Preferences.Key
     *
     */
    @Suppress("UNCHECKED_CAST")
    fun getPreferencesKey(key: String, value: Any? = ""): Preferences.Key<Any> {
        return when (value) {
            is String -> {
                stringPreferencesKey(key) as Preferences.Key<Any>
            }
            is Int -> {
                intPreferencesKey(key) as Preferences.Key<Any>
            }
            is Double -> {
                doublePreferencesKey(key) as Preferences.Key<Any>
            }
            is Long -> {
                longPreferencesKey(key) as Preferences.Key<Any>
            }
            is Float -> {
                floatPreferencesKey(key) as Preferences.Key<Any>
            }
            is Boolean -> {
                booleanPreferencesKey(key) as Preferences.Key<Any>
            }
            else -> {
                stringSetPreferencesKey(key) as Preferences.Key<Any>
            }
        }
    }
}