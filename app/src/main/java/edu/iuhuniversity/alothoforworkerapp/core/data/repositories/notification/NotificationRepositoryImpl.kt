package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification

import edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification.FcmNotificationDao
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository

class NotificationRepositoryImpl(
    private val notificationDao: FcmNotificationDao
) : NotificationRepository {
    override suspend fun getSavedNotification(): Resource<MutableList<FcmNotification>> {
        return Resource.Success(notificationDao.getSavedNotification())
    }

    override suspend fun saveNotification(notification: FcmNotification): Resource<Boolean> {
        notificationDao.saveNotification(notification)
        return Resource.Success(true)
    }

    override suspend fun deleteNotification(notification: FcmNotification): Resource<Boolean> {
       notificationDao.deleteNotification(notification)
        return Resource.Success(true)
    }
}