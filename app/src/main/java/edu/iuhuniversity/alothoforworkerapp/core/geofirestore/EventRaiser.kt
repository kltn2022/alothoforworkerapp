package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

interface EventRaiser {
    fun raiseEvent(r: Runnable?)
}