package edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedorder

import androidx.room.*
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.RoomTypeConverters
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.*
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import java.util.*

@Entity(tableName = "TBL_ORDER")
@TypeConverters(
    RoomTypeConverters.OrderDetailsConverter::class, RoomTypeConverters.DateConverter::class,
    RoomTypeConverters.FeedbackConverter::class, RoomTypeConverters.AddressConverter::class,
    RoomTypeConverters.StringListConverter::class
)
data class CachedOrder(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    @ColumnInfo
    val type: String,
    @ColumnInfo
    val createdDate: Date?,
    @ColumnInfo
    val workingDate: Date?,
    @ColumnInfo
    val finishedDate: Date?,
    @ColumnInfo
    val note: String,
    @ColumnInfo
    val movingFee: Double? = 0.0,
    @ColumnInfo
    val estimatedFee: Double? = 0.0,
    @ColumnInfo
    val overtimeFee: Double? = 0.0,
    @ColumnInfo
    val total: Double? = 0.0,
    @ColumnInfo
    val customerId: String,
    @ColumnInfo
    val workerId: String,
    @ColumnInfo
    val status: String?,
    @ColumnInfo
    val workingAddress: Address?,
    @ColumnInfo
    val images: List<String>? = null,
    @ColumnInfo
    val feedback: Feedback? = null,
//    @ColumnInfo
//    val orderDetails: List<OrderDetail>,
) {
    companion object {
        fun fromDomain(domainModel: Order): CachedOrder {
            return CachedOrder(
                id = domainModel.id,
                type = domainModel.type.toString(),
                createdDate = domainModel.createdDate,
                workingDate = domainModel.workingDate,
                finishedDate = domainModel.finishedDate,
                note = domainModel.note,
                movingFee = domainModel.movingFee,
                estimatedFee = domainModel.estimatedFee,
                overtimeFee = domainModel.overtimeFee,
                total = domainModel.total,
                customerId = domainModel.customer.id,
                workerId = domainModel.worker?.id.orEmpty(),
                status = domainModel.status.toString(),
                workingAddress = domainModel.workingAddress,
                images = domainModel.images,
                feedback = domainModel.feedback,
//                orderDetails = domainModel.orderDetails
            )
        }

    }


    fun toDomain(): Order {
        return Order(
            id = id,
            type = OrderType.valueOf(type),
            createdDate = createdDate,
            workingDate = workingDate,
            finishedDate = finishedDate,
            note = note,
            movingFee = movingFee,
            estimatedFee = estimatedFee,
            overtimeFee = overtimeFee,
            total = total,
            customer = CommonUtils.getCurrentUserRef()!!,
            worker = CommonUtils.getDocumentReference(CollectionConst.COLLECTION_WORKER, workerId),
            status = OrderStatus.valueOf(status.orEmpty()),
            workingAddress = workingAddress,
            images = images,
            feedback = feedback,
            orderDetails = listOf()
        )
    }
}



