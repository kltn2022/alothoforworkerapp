package edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage

import android.annotation.SuppressLint
import android.net.Uri
import com.google.android.gms.tasks.Task
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StreamDownloadTask
import com.google.firebase.storage.network.NetworkRequest
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.utils.PREFIX_CLOUD_STORAGE_URI
import kotlinx.coroutines.tasks.await
import java.io.File
import java.net.URLEncoder
import kotlin.text.Charsets.UTF_8

class FirebaseFileDownloader(
    private val firebaseStorage: FirebaseStorage
) : StorageDataSource.Download {
    override suspend fun downloadAsBytes(input: String): Resource<ByteArray> {
        return input
            .getReference(firebaseStorage)
            .getBytes(Long.MAX_VALUE)
            .toResponse()
    }

    override suspend fun downloadAsStream(input: String): Resource<StreamDownloadTask.TaskSnapshot> {
        return input
            .getReference(firebaseStorage)
            .stream
            .toResponse()
    }

    override suspend fun downloadAsFile(input: String, file: File): Resource<File> {
        input
            .getReference(firebaseStorage)
            .getFile(file)
            .let { task ->
                (task as Task<*>).toResponse()
                task
                    .await()
                    .let {
                        return when {
                            !task.isSuccessful -> {
                                task.exception?.message?.let { Resource.Error(it) }!!
                            }
                            task.isPaused -> {
                                Resource.Error("Can't download right now. Please try again!")
                            }
                            task.isCanceled -> {
                                Resource.Error("Download process is cancelled. Please try again")
                            }
                            else -> Resource.Success(file)
                        }
                    }
            }
    }

    override suspend fun getDownloadUrl(input: String): Resource<Uri> {
        return input
            .getReference(firebaseStorage)
            .downloadUrl
            .toResponse<Uri>()
    }
}

fun String.getFileName(): String {
    return this.split("/").last()
}

fun String.getFolderPath(fileName: String? = ""): String {
    return if (fileName.isNullOrEmpty()) {
        this.replace(this.split("/").last(), "")
    } else {
        this.replace(fileName, "")
    }
}

@SuppressLint("RestrictedApi")
fun String.getReference(firebaseStorage: FirebaseStorage): StorageReference {
    return when {
        this.startsWith(PREFIX_CLOUD_STORAGE_URI) -> {
            firebaseStorage.getReferenceFromUrl(this)
        }
        this.startsWith(NetworkRequest.PROD_BASE_URL.toString()) -> {
            val fileName = this.split("/").last()
            firebaseStorage.getReferenceFromUrl(
                this.getFolderPath(fileName).plus(
                    URLEncoder.encode(
                        fileName,
                        UTF_8.name()
                    )
                )
            )
        }
        else -> {
            firebaseStorage.reference.child(this)
        }
    }
}

fun String.createReference(firebaseStorage: FirebaseStorage): StorageReference {
    return firebaseStorage.reference.child(this)
}

suspend fun <T> Task<T>.toResponse(): Resource<T> {
    this
        .await()
        .let { resp ->
            if (!this.isSuccessful) {
                return this.exception?.message?.let { Resource.Error(it) }!!
            } else if (this.isCanceled) {
                return Resource.Error("Download process is cancelled. Please try again")
            }

            return Resource.Success(resp)
        }
}