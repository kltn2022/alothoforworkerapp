package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth

import android.app.Activity
import androidx.annotation.NonNull
import com.google.firebase.auth.PhoneAuthProvider
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker

interface AuthRepository {
    suspend fun sendOtp(
        activity: Activity,
        phoneNumber: String,
        resendingToken: PhoneAuthProvider.ForceResendingToken?,
        callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    )

    suspend fun signInWithPhoneAuthCredential(
        @NonNull verificationId: String,
        @NonNull otp: String
    ): Resource<Worker?>

    fun logout()

    fun isSingedIn(): Boolean


}