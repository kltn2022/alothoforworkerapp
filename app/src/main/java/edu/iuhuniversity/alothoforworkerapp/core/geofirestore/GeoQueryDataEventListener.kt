package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.firebase.firestore.DocumentSnapshot
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation


interface GeoQueryDataEventListener {
    /**
     * Called if a documentSnapshot entered the search area of the GeoQuery. This method is called for every documentSnapshot currently in the
     * search area at the time of adding the listener.
     *
     * This method is once per documentSnapshot, and is only called again if onDataExited was called in the meantime.
     *
     * @param documentSnapshot The associated documentSnapshot that entered the search area
     * @param location The location for this documentSnapshot as a GeoLocation object
     */
    fun onDataEntered(documentSnapshot: DocumentSnapshot?, location: GeoLocation?)

    /**
     * Called if a documentSnapshot exited the search area of the GeoQuery. This is method is only called if onDataEntered was called
     * for the documentSnapshot.
     *
     * @param documentSnapshot The associated documentSnapshot that exited the search area
     */
    fun onDataExited(documentSnapshot: DocumentSnapshot?)

    /**
     * Called if a documentSnapshot moved within the search area.
     *
     * This method can be called multiple times.
     *
     * @param documentSnapshot The associated documentSnapshot that moved within the search area
     * @param location The location for this documentSnapshot as a GeoLocation object
     */
    fun onDataMoved(documentSnapshot: DocumentSnapshot?, location: GeoLocation?)

    /**
     * Called if a documentSnapshot changed within the search area.
     *
     * An onDataMoved() is always followed by onDataChanged() but it is be possible to see
     * onDataChanged() without an preceding onDataMoved().
     *
     * This method can be called multiple times for a single location change, due to the way
     * the Firestore Database handles floating point numbers.
     *
     * Note: this method is not related to ValueEventListener#onDataChange(DocumentSnapshot).
     *
     * @param documentSnapshot The associated documentSnapshot that moved within the search area
     * @param location The location for this documentSnapshot as a GeoLocation object
     */
    fun onDataChanged(documentSnapshot: DocumentSnapshot?, location: GeoLocation?)

    /**
     * Called once all initial GeoFire data has been loaded and the relevant events have been fired for this query.
     * Every time the query criteria is updated, this observer will be called after the updated query has fired the
     * appropriate documentSnapshot entered or documentSnapshot exited events.
     */
    fun onGeoQueryReady()

    /**
     * Called in case an error occurred while retrieving locations for a query, e.g. violating security rules.
     * @param error The error that occurred while retrieving the query
     */
    fun onGeoQueryError(error: Exception?)
}