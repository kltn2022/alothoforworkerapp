package edu.iuhuniversity.alothoforworkerapp.core.data.remote.api

import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * [RetrofitClientInstance] Sample
 *
 * Including:
 * + Standard code ✅
 * + OkHttp Interceptor ✅
 * + Gson Builder ✅
 * + Setup connect/read/write timeout,... ✅
 * + Secure BASE_URL ✅
 * + Add default header, default parameter to every request ✅
 */

class RetrofitClientInstance {
    companion object {
        private const val RETROFIT_CONNECT_TIMEOUT = 30L;
        private const val RETROFIT_READ_TIMEOUT = 20L;
        private const val RETROFIT_WRITE_TIMEOUT = 25L;
        private const val HEADER_AUTHORIZATION = "Authorization";
        private const val BASE_URL = "https://jsonplaceholder.typicode.com/" // just example

        // OkHttp Interceptor
        private fun buildClient(token: String? = ""): OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            })
            .addInterceptor(buildAuthorizationInterceptor(token))
            .connectTimeout(RETROFIT_CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(RETROFIT_READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(RETROFIT_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .build()

        private fun buildAuthorizationInterceptor(token: String? = "") = Interceptor { chain ->
            val originalRequest = chain.request()
            if (!token.isNullOrBlank()) chain.proceed(originalRequest)
            else {
                val newRequest = originalRequest.newBuilder()
                    .addHeader(HEADER_AUTHORIZATION, token!!)
                    .build()
                chain.proceed(newRequest)
            }
        }

        fun getInstance(baseUrl: String? = BASE_URL): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl!!)
                .client(buildClient())
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }

        fun getInstance(baseUrl: String? = BASE_URL, token: String): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl!!)
                .client(buildClient(token))
                .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
                .build()
        }
    }
}