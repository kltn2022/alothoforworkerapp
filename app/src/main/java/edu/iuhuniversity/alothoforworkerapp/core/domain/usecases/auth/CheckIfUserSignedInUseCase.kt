package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository

class CheckIfUserSignedInUseCase(private val authRepository: AuthRepository) {
    fun execute() = authRepository.isSingedIn()
}