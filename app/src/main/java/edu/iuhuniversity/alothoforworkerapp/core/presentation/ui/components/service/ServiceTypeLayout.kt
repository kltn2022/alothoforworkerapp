package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.service

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import edu.iuhuniversity.alothoforworkerapp.R
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.ServiceType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyGridFor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel.DemoDataProvider.defaultServiceTypeList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography

@Composable
fun ServiceTypeItem(
    title: String,
    content: @Composable (() -> Unit),
    onItemClicked: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clip(RoundedCornerShape(10.dp))
            .clickable { onItemClicked() }
            .padding(10.dp)
    ) {
        Box(
            modifier = Modifier
                .border(1.dp, colorResource(id = R.color.colorStepWait), RoundedCornerShape(5.dp))
                .clip(RoundedCornerShape(10.dp))
                .wrapContentSize(Alignment.Center)
        ) {
            content()
        }

        Text(
            text = title,
            modifier = Modifier
                .padding(top = 5.dp)
                .heightIn(min = 45.dp),
            style = AloThoTypography.body1,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Center,
        )
    }

}

val list = arrayListOf("ABC", "DBD", "FFD", "FSDFS", "gsds")

@Composable
fun ServiceTypeList(
    modifier: Modifier = Modifier,
    serviceTypeList: List<ServiceType> = defaultServiceTypeList,
    onItemClicked: (ServiceType) -> Unit
) {
    Column(
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Center,
        modifier = Modifier
            .background(Color.White)
            .padding(15.dp)
            .then(modifier)
    ) {
        Text(
            text = "Dịch vụ",
            modifier = Modifier.padding(bottom = 10.dp),
            style = AloThoTypography.h6,
            fontWeight = FontWeight.SemiBold
        )
        LazyGridFor(
            items = serviceTypeList,
            rowSize = 2,
        ) {
            ServiceTypeItem(
                title = it.name,
                content = {
                    Image(
                        painter = painterResource(id = it.icon.toInt()),
                        contentDescription = it.name,
                        modifier = Modifier
                            .size(56.dp)
                            .padding(10.dp)

                    )
                },
                onItemClicked = { onItemClicked(it) }
            )
        }
    }
}