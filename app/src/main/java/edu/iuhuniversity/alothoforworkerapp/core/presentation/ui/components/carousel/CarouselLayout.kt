package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.util.lerp
import com.google.accompanist.pager.*
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import kotlin.math.absoluteValue

@OptIn(ExperimentalPagerApi::class)
@Composable
fun CarouselLayout() {
    val items = DemoDataProvider.slider

    val pagerState = rememberPagerState()

    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        HorizontalPager(
            count = items.size,
            contentPadding = PaddingValues(30.dp),
            modifier = Modifier
                .size(350.dp)
                .padding(20.dp),
            state = pagerState
        ) { page ->
            val item = items[this.currentPage]

            SliderItem(
                modifier = Modifier
                    .graphicsLayer {
                        val pageOffset = calculateCurrentOffsetForPage(page).absoluteValue

                        lerp(
                            start = 0.85f,
                            stop = 1f,
                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                        ).also {
                            scaleX = scaleX
                            scaleY = scaleY
                        }

                        alpha = lerp(
                            start = 0.8f,
                            stop = 1f,
                            fraction = 1f - pageOffset.coerceIn(0f, 1f)
                        )
                    },
                item
            )
        }

        HorizontalPagerIndicator(
            pagerState = pagerState,
            activeColor = PrimaryColor,
            //inactiveColor = PrimaryColor.copy(alpha = 0.2f),
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = 80.dp)
        )
    }

//    Box(
//        modifier = Modifier
//            .size(350.dp)
//            .padding(40.dp)
//    ) {
//
//        Pager(state = pagerState, modifier = Modifier.fillMaxSize()) {
//            val item = items[page]
//            selectedPage.value = pagerState.currentPage
//            SliderItem(item)
//        }
//        Row(
//            modifier = Modifier
//                .align(Alignment.BottomCenter)
//                .padding(bottom = 40.dp)
//        ) {
//            items.forEachIndexed { index, _ ->
//                CarouselDot(
//                    selected = index == selectedPage.value,
//                    PrimaryColor,
//                    Icons.Filled.Lens
//                )
//            }
//        }
//    }
}


@Composable
fun CarouselDot(selected: Boolean, color: Color, icon: ImageVector) {
    Icon(
        imageVector = icon,
        modifier = Modifier
            .padding(4.dp)
            .size(12.dp),
        contentDescription = null,
        tint = if (selected) color else Color.Gray
    )
}

@Composable
fun SliderItem(
    modifier: Modifier = Modifier,
    item: CarouselSliderItem
) {
    Image(
        painter = painterResource(id = item.imageId),
        contentScale = ContentScale.Crop,
        contentDescription = null,
        modifier = Modifier
            .clip(RoundedCornerShape(10.dp))
            .fillMaxSize()
            .then(modifier)
    )
}

@Preview
@Composable
fun PreviewCarouselLayout() {
    CarouselLayout()
}
