package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

data class GeoLocation(
    val latitude: Double,
    val longitude: Double,
) {
   companion object {
       /**
        * Checks if these coordinates are valid geo coordinates.
        * @param latitude The latitude must be in the range [-90, 90]
        * @param longitude The longitude must be in the range [-180, 180]
        * @return True if these are valid geo coordinates
        */
       fun coordinatesValid(latitude: Double, longitude: Double): Boolean {
           return latitude >= -90 && latitude <= 90 && longitude >= -180 && longitude <= 180
       }
   }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val (latitude1, longitude1) = o as GeoLocation
        if (java.lang.Double.compare(latitude1, latitude) != 0) return false
        return java.lang.Double.compare(longitude1, longitude) == 0
    }

    override fun hashCode(): Int {
        var result: Int
        var temp: Long = java.lang.Double.doubleToLongBits(latitude)
        result = (temp xor (temp ushr 32)).toInt()
        temp = java.lang.Double.doubleToLongBits(longitude)
        result = 31 * result + (temp xor (temp ushr 32)).toInt()
        return result
    }

    override fun toString(): String {
        return "GeoLocation($latitude, $longitude)"
    }
}
