package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

interface GeoFirestoreCallback {
    fun onKeyEnteredResponse(key: String, location: GeoLocation)
    fun onKeyExitedResponse(key: String)
}