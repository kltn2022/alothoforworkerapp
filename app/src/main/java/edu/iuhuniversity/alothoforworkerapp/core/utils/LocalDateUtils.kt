package edu.iuhuniversity.alothoforworkerapp.core.utils

import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

const val VIETNAM_ZONE_ID = "Asia/Ho_Chi_Minh"

object LocalDateUtils {
    fun convertToLocalDateViaInstant(dateToConvert: Date): LocalDate? {
        return dateToConvert.toInstant()
            .atZone(ZoneId.of(VIETNAM_ZONE_ID))
            .toLocalDate()
    }

    fun convertToLocalDateViaMillisecond(dateToConvert: Date): LocalDate? {
        return Instant.ofEpochMilli(dateToConvert.time)
            .atZone(ZoneId.of(VIETNAM_ZONE_ID))
            .toLocalDate()
    }
}