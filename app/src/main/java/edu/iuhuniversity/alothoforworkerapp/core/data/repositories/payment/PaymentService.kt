package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.payment

import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST

sealed interface PaymentService {
    interface Momo {
        @POST("v2/gateway/api/create")
        suspend fun processMomoPayment(
            @Body
            payload: ProcessingPayment.Request,
            @Header("Content-Type")
            contentType: String? = "application/json",
        ): Response<ProcessingPayment.Response>
    }

    interface Vnpay {

    }
}