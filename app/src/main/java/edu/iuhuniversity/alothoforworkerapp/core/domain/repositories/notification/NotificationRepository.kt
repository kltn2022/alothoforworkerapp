package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification

interface NotificationRepository {
    suspend fun getSavedNotification() : Resource<MutableList<FcmNotification>>
    suspend fun saveNotification(notification: FcmNotification): Resource<Boolean>
    suspend fun deleteNotification(notification: FcmNotification): Resource<Boolean>
}