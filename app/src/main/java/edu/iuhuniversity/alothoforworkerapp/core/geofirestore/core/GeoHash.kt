package edu.iuhuniversity.alothoforworkerapp.core.geofirestore.core

import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils.isValidBase32String
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils.valueToBase32Char


class GeoHash {
    val geoHashString: String

    constructor(location: GeoLocation) : this(
        location.latitude,
        location.longitude,
        DEFAULT_PRECISION
    ) {
    }

    @JvmOverloads
    constructor(latitude: Double, longitude: Double, precision: Int = DEFAULT_PRECISION) {
        require(precision >= 1) { "Precision of GeoHash must be larger than zero!" }
        require(precision <= MAX_PRECISION) { "Precision of a GeoHash must be less than " + (MAX_PRECISION + 1) + "!" }
        require(
            GeoLocation.coordinatesValid(
                latitude,
                longitude
            )
        ) { String.format("Not valid location coordinates: [%f, %f]", latitude, longitude) }
        val longitudeRange = doubleArrayOf(-180.0, 180.0)
        val latitudeRange = doubleArrayOf(-90.0, 90.0)
        val buffer = CharArray(precision)
        for (i in 0 until precision) {
            var hashValue = 0
            for (j in 0 until Base32Utils.BITS_PER_BASE32_CHAR) {
                val even = (i * Base32Utils.BITS_PER_BASE32_CHAR + j) % 2 == 0
                val `val` = if (even) longitude else latitude
                val range = if (even) longitudeRange else latitudeRange
                val mid = (range[0] + range[1]) / 2
                if (`val` > mid) {
                    hashValue = (hashValue shl 1) + 1
                    range[0] = mid
                } else {
                    hashValue = hashValue shl 1
                    range[1] = mid
                }
            }
            buffer[i] = valueToBase32Char(hashValue)
        }
        geoHashString = String(buffer)
    }

    constructor(hash: String) {
        require(!(hash.length == 0 || !isValidBase32String(hash))) { "Not a valid geoHash: $hash" }
        geoHashString = hash
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val other = o as GeoHash
        return geoHashString == other.geoHashString
    }

    override fun toString(): String {
        return "GeoHash{" +
                "geoHash='" + geoHashString + '\'' +
                '}'
    }

    override fun hashCode(): Int {
        return geoHashString.hashCode()
    }


    @JvmName("getGeoHashString1")
    fun getGeoHashString(): String {
        return this.geoHashString
    }

    companion object {
        // The default precision of a geohash
        private const val DEFAULT_PRECISION = 10

        // The maximal precision of a geohash
        const val MAX_PRECISION = 22

        // The maximal number of bits precision for a geohash
        val MAX_PRECISION_BITS = MAX_PRECISION * Base32Utils.BITS_PER_BASE32_CHAR
    }
}

