package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.AUTH_TOKEN
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.GUEST_PASSWORD_INPUT
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.GUEST_PHONE_INPUT
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_BALANCE_MONEY
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_DISPLAY_NAME
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_EMAIL
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_IS_ACTIVE
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_ONLINE
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_PHONE
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_PHOTO_URL
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_PROMO_CODE
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_RATE
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_SERVICE_LIST
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl.PreferencesKey.USER_UID
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.io.IOException

class WorkerPreferencesRepositoryImpl(
    private val userPreferencesStore: DataStore<Preferences>,
) : WorkerPreferencesRepository {

    private val TAG: String = WorkerPreferencesRepositoryImpl::class.simpleName!!

    /*
      Get the user uid flow
   */
    override val currentUserUid: Flow<String?>
        get() = userPreferencesStore.data
            .catch { exception ->
                if (exception is IOException) {
                    Timber.e(exception, "Error reading user uid preference")
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }
            .map { preferences ->
                val uid = preferences[USER_UID]
                uid
            }
            .distinctUntilChanged()

    override val workerPreference: Flow<Worker?>
        get() = userPreferencesStore.data
            .catch { exception ->
                // dataStore.data throws an IO Exception when an error is encountered when reading data
                if (exception is IOException) {
                    Timber.e(exception, "Error reading user preferences")
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map { preferences ->
                val uid = preferences[USER_UID]
                val displayName = preferences[USER_DISPLAY_NAME]
                val phone = preferences[USER_PHONE]
                val email = preferences[USER_EMAIL]
                val photoUrl = preferences[USER_PHOTO_URL]
                val promoCode = preferences[USER_PROMO_CODE]
                val rate = preferences[USER_RATE]
                val online = preferences[USER_ONLINE]
                val isActive = preferences[USER_IS_ACTIVE]
                val balanceMoney = preferences[USER_BALANCE_MONEY]
                val serviceType = preferences[USER_SERVICE_LIST]
                    ?.map {
                        CommonUtils.getDocumentReference(
                            CollectionConst.COLLECTION_SERVICE_TYPE,
                            it
                        )
                    }

                Worker(
                    uid!!,
                    displayName.orEmpty(),
                    phone!!,
                    email.orEmpty(),
                    photoUrl,
                    promoCode,
                    rate,
                    online,
                    isActive,
                    balanceMoney.takeIf { it != null } ?: 0.0,
                    serviceType
                )
            }
            .distinctUntilChanged()

    /*
        Get the auth token flow
     */
    override val authToken: Flow<String?>
        get() = userPreferencesStore.data
            .catch { exception ->
                // dataStore.data throws an IO Exception when an error is encountered when reading data
                if (exception is IOException) {
                    Timber.e(exception, "Error reading user preferences")
                    emit(emptyPreferences())
                } else {
                    throw exception
                }
            }.map { preferences ->
                preferences[AUTH_TOKEN]
            }

    override suspend fun saveAuthToken(token: String) {
        userPreferencesStore.edit { preferences ->
            preferences[AUTH_TOKEN] = token
        }
    }

    override suspend fun saveCurrentWorkerInfo(firebaseUser: FirebaseUser) {
        userPreferencesStore.edit { preferences ->
            preferences[USER_UID] = firebaseUser.uid
            preferences[USER_DISPLAY_NAME] = firebaseUser.displayName.toString()
            preferences[USER_PHONE] = firebaseUser.phoneNumber.toString()
            preferences[USER_EMAIL] = firebaseUser.email.toString()
            preferences[USER_PHOTO_URL] = firebaseUser.photoUrl.toString()
        }
    }

    override suspend fun saveRememberedLoginSession(phone: String, password: String) {
        userPreferencesStore.edit { preferences ->
            preferences[GUEST_PHONE_INPUT] = phone
            preferences[GUEST_PASSWORD_INPUT] = password
        }
    }

    override suspend fun updateCurrentWorkerInfo(request: UserProfileChangeRequest) {
        userPreferencesStore.edit { preferences ->
            preferences[USER_DISPLAY_NAME] = request.displayName.toString()
            preferences[USER_PHOTO_URL] = request.photoUri.toString()
        }
    }

    override suspend fun logout() {
        userPreferencesStore.edit { preferences ->
            preferences.remove(AUTH_TOKEN)
            preferences.remove(USER_UID)
            preferences.remove(USER_DISPLAY_NAME)
            preferences.remove(USER_PHONE)
            preferences.remove(USER_EMAIL)
            preferences.remove(USER_PHOTO_URL)
        }
    }

    private object PreferencesKey {
        val AUTH_TOKEN = stringPreferencesKey("auth_token")
        val USER_UID = stringPreferencesKey("user_uid")
        val USER_DISPLAY_NAME = stringPreferencesKey("user_display_name")
        val USER_PHONE = stringPreferencesKey("user_phone")
        val USER_EMAIL = stringPreferencesKey("user_email")
        val USER_PROMO_CODE = stringPreferencesKey("user_promote_code")
        val USER_PHOTO_URL = stringPreferencesKey("user_photo_url")
        val USER_RATE = floatPreferencesKey("user_rate")
        val USER_ONLINE = booleanPreferencesKey("user_photo_url")
        val USER_SERVICE_LIST = stringSetPreferencesKey("user_service_list")
        val USER_IS_ACTIVE = booleanPreferencesKey("user_is_active")
        val USER_BALANCE_MONEY = doublePreferencesKey("user_balance_money")


        val GUEST_PHONE_INPUT = stringPreferencesKey("guest_phone_input")
        val GUEST_PASSWORD_INPUT = stringPreferencesKey("guest_password_input")
    }
}