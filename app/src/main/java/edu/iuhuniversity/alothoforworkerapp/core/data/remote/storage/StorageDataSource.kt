package edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage

import android.net.Uri
import com.google.firebase.storage.StreamDownloadTask
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import java.io.File

sealed interface StorageDataSource {
    interface Download {
        suspend fun downloadAsBytes(input: String): Resource<ByteArray>
        suspend fun downloadAsStream(input: String): Resource<StreamDownloadTask.TaskSnapshot>
        suspend fun downloadAsFile(input: String, file: File): Resource<File>
        suspend fun getDownloadUrl(input: String): Resource<Uri>
    }

    interface Upload {
        suspend fun uploadFiles(
            folderPath: String,
            requestFiles: List<Uri>,
            fileUploaderCallback: FileUploaderCallback
        )
    }
}