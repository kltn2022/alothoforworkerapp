package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order

import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository

class UpdateOrderUseCase(private val orderRepository: OrderRepository) {
    suspend fun execute(order: Order) = orderRepository.updateOrder(order)
}