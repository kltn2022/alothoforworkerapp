package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging

import edu.iuhuniversity.alothoforworkerapp.BuildConfig
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmCreateNotificationGroup
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmGroupMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmUpdateNotificationGroup
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmNotificationGroupResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Reference: https://firebase.google.com/docs/reference/fcm/rest/v1/projects.messages/send
 */
const val FIREBASE_CLOUD_MESSAGE_ENDPOINT = "https://fcm.googleapis.com/fcm/"

/**
 * API Interface in order to send a message to specific target (a registration token, topic or
 * condition)
 */
interface IFcmService {
    /**
     * [TODO: Please implement me!]
     * To use this method, please take the following steps:
     * + Go to [gradle.properties], add: FCM_SERVER_KEY="INSERT-YOUR-KEY-HERE"
     * + Go to [build.gradle] (app level), add in defaultConfig section: buildConfigField('String',
     * 'FCM_SERVER_KEY',FCM_SERVER_KEY)
     * + Rebuild your app, then call: BuildConfig.FCM_SERVER_KEY for using it
     * + Finally, edit the line in below method to become like this: apiKey: String? =
     * "key=$BuildConfig
     * .FCM_SERVER_KEY",
     */
    @POST("send")
    suspend fun sendMessage(
        @Body
        payload: FcmMessagePayload<String>,
        @Header("Content-Type")
        contentType: String? = "application/json",
        @Header("Authorization")
        apiKey: String? = "key=${BuildConfig.FCM_SERVER_KEY}"
    ): Response<FcmResponse>

    @POST("send")
    suspend fun <T> sendGroupMessage(
        @Body
        payload: FcmGroupMessagePayload<T>,
        @Header("Content-Type")
        contentType: String? = "application/json",
        @Header("Authorization")
        apiKey: String? = "key=${BuildConfig.FCM_SERVER_KEY}",
    ): Response<FcmResponse>

    @POST("notification")
    suspend fun createDeviceGroup(
        @Body
        payload: FcmCreateNotificationGroup,
        @Header("Content-Type")
        contentType: String? = "application/json",
        @Header("Authorization")
        apiKey: String? = "key=${BuildConfig.FCM_SERVER_KEY}",
        @Header("project_id")
        senderId: String? = "do-something-here"
    ): Response<FcmNotificationGroupResponse>

    @POST("notification")
    suspend fun updateDevicesFromGroup(
        @Body
        payload: FcmUpdateNotificationGroup,
        @Header("Content-Type")
        contentType: String? = "application/json",
        @Header("Authorization")
        apiKey: String? = "key=${BuildConfig.FCM_SERVER_KEY}",
        @Header("project_id")
        senderId: String? = "do-something-here"
    ): Response<FcmNotificationGroupResponse>

    suspend fun retrievingNotificationKey(
        @Query("notification_key_name")
        notificationKeyName: String,
        @Header("Content-Type")
        contentType: String? = "application/json",
        @Header("Authorization")
        apiKey: String? = "key=${BuildConfig.FCM_SERVER_KEY}e",
        @Header("project_id")
        senderId: String? = "do-something-here"
    ): Response<FcmNotificationGroupResponse>
}