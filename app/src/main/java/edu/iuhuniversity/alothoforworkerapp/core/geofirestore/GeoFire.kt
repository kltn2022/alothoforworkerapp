package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.*
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.core.GeoHash
import java.util.logging.Logger


/**
 * A GeoFire instance is used to store geo location data in Firebase.
 */
class GeoFire {

    /**
     * A listener that can be used to be notified about a successful write or an error on writing.
     */
    interface CompletionListener {
        /**
         * Called once a location was successfully saved on the server or an error occurred. On success, the parameter
         * error will be null; in case of an error, the error will be passed to this method.
         *
         * @param key   The key whose location was saved
         * @param exception The exception or null if no exception occurred
         */
        fun onComplete(key: String?, exception: Exception?)
    }

    /**
     * A small wrapper class to forward any events to the LocationEventListener.
     */
    private class LocationValueEventListener internal constructor(
        private val key: String,
        private val callback: LocationCallback
    ) :
        OnCompleteListener<DocumentSnapshot?> {
        override fun onComplete(task: Task<DocumentSnapshot?>) {
            if (task.isSuccessful) {
                if (task.result == null) {
                    callback.onLocationResult(key, null)
                } else {
                    val location = getLocationValue(task.result!!)
                    if (location != null) {
                        callback.onLocationResult(key, location)
                    } else {
                        val message =
                            "GeoFire data has invalid format: " + task.result.toString()
                        callback.onCancelled(Exception(message))
                    }
                }
            } else {
                callback.onCancelled(task.exception)
            }
        }
    }

    /**
     * @return The Firebase reference this GeoFire instance uses
     */
    val collectionReference: CollectionReference
    private var query: Query? = null
    private val eventRaiser: EventRaiser

    /**
     * Creates a new GeoFire instance at the given Firebase reference.
     *
     * @param collectionReference The Firebase reference this GeoFire instance uses
     */
    constructor(collectionReference: CollectionReference) {
        this.collectionReference = collectionReference
        var eventRaiser: EventRaiser
        try {
            eventRaiser = AndroidEventRaiser()
        } catch (e: Throwable) {
            // We're not on Android, use the ThreadEventRaiser
            eventRaiser = ThreadEventRaiser()
        }
        this.eventRaiser = eventRaiser
    }

    /**
     * Creates a new GeoFire instance at the given Firebase reference.
     *
     * @param collectionReference The Firebase reference this GeoFire instance uses
     * @param query The query to add filters to the collection
     */
    constructor(collectionReference: CollectionReference, query: Query?) {
        this.collectionReference = collectionReference
        this.query = query
        var eventRaiser: EventRaiser
        try {
            eventRaiser = AndroidEventRaiser()
        } catch (e: Throwable) {
            // We're not on Android, use the ThreadEventRaiser
            eventRaiser = ThreadEventRaiser()
        }
        this.eventRaiser = eventRaiser
    }

    /**
     * @return The query to add filters to the collection
     */
    fun getQuery(): Query? {
        return query
    }

    /**
     * @return Set the query to add filters to the collection
     * @param query This will be added to the collection
     */
    fun setQuery(query: Query?) {
        this.query = query
    }

    /**
     * Sets the location for a given key.
     *
     * @param key      The key to save the location for
     * @param location The location of this document
     */
    fun setLocation(key: String?, location: GeoLocation) {
        this.setLocation(key, location, null)
    }

    /**
     * Sets the location for a given key.
     *
     * @param key                The key to save the location for
     * @param location           The location of this key
     * @param completionListener A listener that is called once the location was successfully saved on the server or an
     * error occurred
     */
    fun setLocation(key: String?, location: GeoLocation, completionListener: CompletionListener?) {
        val docRef = collectionReference.document(
            key!!
        )
        val geoHash = GeoHash(location)
        val updates: MutableMap<String, Any> = HashMap()
        updates["g"] = geoHash.getGeoHashString()
        updates["l"] = GeoPoint(location.latitude, location.longitude)
        if (completionListener != null) {
            docRef.set(updates, SetOptions.merge()).addOnCompleteListener { task: Task<Void?> ->
                completionListener.onComplete(
                    key,
                    task.exception
                )
            }
        } else {
            docRef.update(updates)
        }
    }
    /**
     * Removes the location for a key from this GeoFire.
     *
     * @param key                The key to remove from this GeoFire
     * @param completionListener A completion listener that is called once the location is successfully removed
     * from the server or an error occurred
     */
    /**
     * Removes the location for a key from this GeoFire.
     *
     * @param key The key to remove from this GeoFire
     */
    @JvmOverloads
    fun removeLocation(key: String?, completionListener: CompletionListener? = null) {
        val docRef = collectionReference.document(
            key!!
        )
        val updates: MutableMap<String, Any> = HashMap()
        updates["g"] = FieldValue.delete()
        updates["l"] = FieldValue.delete()
        if (completionListener != null) {
            docRef.update(updates).addOnCompleteListener { task: Task<Void?> ->
                completionListener.onComplete(
                    key,
                    task.exception
                )
            }
        } else {
            docRef.update(updates)
        }
    }

    /**
     * Gets the current location for a key and calls the callback with the current value.
     *
     * @param key      The key whose location to get
     * @param callback The callback that is called once the location is retrieved
     */
    fun getLocation(key: String, callback: LocationCallback) {
        val keyRef = collectionReference.document(key)
        val valueListener = LocationValueEventListener(key, callback)
        keyRef.get().addOnCompleteListener(valueListener)
    }

    /**
     * Returns a new Query object centered at the given location and with the given radius.
     *
     * @param center The center of the query
     * @param radius The radius of the query, in kilometers
     * @return The new GeoQuery object
     */
    fun queryAtLocation(center: GeoLocation?, radius: Double): GeoQuery {
        return GeoQuery(this, center!!, radius)
    }

    fun raiseEvent(r: Runnable?) {
        eventRaiser.raiseEvent(r)
    }

    companion object {
        var LOGGER: Logger = Logger.getLogger("GeoFire")
        fun getLocationValue(documentSnapshot: DocumentSnapshot): GeoLocation? {
            return try {
                val data = documentSnapshot.data
                val location = data!!["l"] as GeoPoint?
                val latitudeObj: Number = location!!.latitude
                val longitudeObj: Number = location.longitude
                val latitude = latitudeObj.toDouble()
                val longitude = longitudeObj.toDouble()
                if (GeoLocation.coordinatesValid(latitude, longitude)) {
                    GeoLocation(latitude, longitude)
                } else {
                    null
                }
            } catch (e: NullPointerException) {
                null
            } catch (e: ClassCastException) {
                null
            }
        }
    }
}