package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message


import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Conservation
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.ConservationInfo
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.Message
import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.MessageType
import kotlinx.coroutines.flow.Flow

interface MessageRepository {
    suspend fun sendMessage(
        conservationId: String,
        content: String,
        messageType: MessageType
    ): Resource<Message?>

    suspend fun listenForChatMessages(conservationId: String): Flow<List<Message?>>

    suspend fun getConservationInfo(conservationId: String): Resource<Conservation?>

    suspend fun listenForConservations(): Flow<List<ConservationInfo?>>
}