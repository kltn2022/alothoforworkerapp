package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.service

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.gradientBackground
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.OrientationList

@Composable
fun ServiceItem(item: Service, selectedItem: (String) -> Unit) {
    Box(
        modifier = Modifier
            .height(120.dp)
            .clip(
                shape = RoundedCornerShape(
                    corner = CornerSize(20.dp)
                )
            )
            .background(
                color = colorResource(R.color.colorBackground)
            )
            .clickable { selectedItem("Item selected") }
    ) {
        Image(
            painter = painterResource(id = R.drawable.edit_profile_background),
            contentDescription = "thumbnail",
            modifier = Modifier
                .fillMaxSize()
                .clip(RoundedCornerShape(10.dp)),
            contentScale = ContentScale.Crop
        )
        Surface(
            modifier = Modifier
                .clip(
                    RoundedCornerShape(
                        corner = CornerSize(10.dp)
                    )
                )
                .gradientBackground(
                    colors = listOf(
                        Color.Transparent,
                        Color.Transparent,
                        Color.Black.copy(alpha = 1f)
                    ),
                    angle = 270f
                )
                .fillMaxSize(),
            color = Color.Transparent
        ) {}
        Image(
            painter = painterResource(id = R.drawable.ic_logo_ab),
            contentDescription = "logo",
            alignment = Alignment.TopEnd,
            modifier = Modifier
                .padding(
                    horizontal = 10.dp,
                    vertical = 10.dp
                )
                .fillMaxWidth()
                .size(20.dp)
        )
        Column(
            verticalArrangement = Arrangement.Bottom,
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .padding(
                    horizontal = 10.dp,
                    vertical = 15.dp
                )
        ) {
            Text(
                text = item.name,
                color = Color.White,
                fontSize = 20.sp,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
            )
        }
    }
}

@Composable
fun ServiceList(
    modifier: Modifier = Modifier,
    services: List<Service>,
    onServiceClicked: (String) -> Unit,
) {
    LazyList(
        items = services,
        orientation = OrientationList.VERTICAL,
        itemContent = {
            ServiceItem(
                Service(
                    id = it.id,
                    name = it.name,
                    it.unit,
                    it.price,
                    it.warrantyPeriod,
                    it.serviceType
                ),
                selectedItem = {
                    onServiceClicked(it)
                })
        },
        spaceBetweenItems = 10.dp,
        modifier = modifier
    )
}