package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order


import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import kotlinx.coroutines.flow.Flow

interface OrderRepository {
    suspend fun insertOrder(order: Order): Resource<Order?>
    suspend fun getOrders(): Resource<List<Order>>
    suspend fun getOrdersFromCache(): Resource<List<Order>>
    suspend fun getOrderFromCache(orderId: String): Resource<Order?>
    suspend fun updateOrder(order: Order): Resource<Boolean>
    suspend fun updateOrderStatus(orderId: String, status: OrderStatus): Resource<Boolean>
    suspend fun updateOrders(): List<Order>
    suspend fun getOrder(id: String): Resource<Order?>
    suspend fun listenRealtimeOrders(): Flow<List<Order?>>
    suspend fun listenRealtimeOrder(orderId: String): Flow<Order?>
}