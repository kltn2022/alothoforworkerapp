package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.os.Parcel
import com.google.firebase.firestore.DocumentReference
import kotlinx.parcelize.Parceler

object CustomTypeParcelable {
    object DocumentReferenceParceler : Parceler<DocumentReference> {
        override fun create(parcel: Parcel): DocumentReference {
            return CommonUtils.getDocumentReference(parcel.readString().orEmpty())
        }

        override fun DocumentReference.write(parcel: Parcel, flags: Int) {
            parcel.writeString(this.path)
        }
    }

    object DocumentReferenceNullableParceler : Parceler<DocumentReference?> {
        override fun create(parcel: Parcel): DocumentReference? {
            val path = parcel.readString()
            if (path.isNullOrEmpty()) {
                return null
            }
            return CommonUtils.getDocumentReference(path)
        }

        override fun DocumentReference?.write(parcel: Parcel, flags: Int) {
            if (this != null) {
                parcel.writeString(this.path)
            } else {
                parcel.writeString(null)
            }
        }
    }

    object DocumentReferenceListParceler : Parceler<List<DocumentReference>> {
        override fun create(parcel: Parcel): List<DocumentReference> {
            val list = parcel.createStringArrayList()
            return list?.map {
                CommonUtils.getDocumentReference(it)
            } ?: emptyList()
        }

        override fun List<DocumentReference>.write(parcel: Parcel, flags: Int) {
            val list = this.map { it.path }
            parcel.writeStringList(list)
        }
    }

    object DocumentReferenceNullableListParceler : Parceler<List<DocumentReference>?> {
        override fun create(parcel: Parcel): List<DocumentReference>? {
            val list = parcel.createStringArrayList()
            return list?.map {
                CommonUtils.getDocumentReference(it)
            }
        }

        override fun List<DocumentReference>?.write(parcel: Parcel, flags: Int) {
            val list = this?.map { it.path }
            parcel.writeStringList(list)
        }
    }

}