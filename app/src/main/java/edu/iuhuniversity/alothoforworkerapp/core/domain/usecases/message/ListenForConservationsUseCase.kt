package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository

class ListenForConservationsUseCase(private val messageRepository: MessageRepository) {
    suspend fun execute() = messageRepository.listenForConservations()
}