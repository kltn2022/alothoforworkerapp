package edu.iuhuniversity.alothoapp.core.data.model.epayment.vnpay

data class ReturnedPaymentResult(
    val vnp_TmnCode: String? = null,
    val vnp_Amount: Long = 0,
    val vnp_BankCode: String? = null,
    val vnp_BankTranNo: String? = null,
    val vnp_CardType: String,
    val vnp_PayDate: String, //yyyyMMddHHmmss
    val vnp_OrderInfo: String? = null,
    val vnp_TransactionNo: String,
    val vnp_ResponseCode: String,
    val vnp_TransactionStatus: String,

    )
