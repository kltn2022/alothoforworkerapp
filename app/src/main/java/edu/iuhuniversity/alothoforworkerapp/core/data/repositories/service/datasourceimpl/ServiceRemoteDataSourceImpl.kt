package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasourceimpl

import com.google.firebase.firestore.FirebaseFirestore
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service.Companion.toService
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils

class ServiceRemoteDataSourceImpl(
    private val db: FirebaseFirestore
) : ServiceDataSource.Remote {
    private val serviceCollection = db.collection(CollectionConst.COLLECTION_SERVICE);
    private val dataSource: FirestoreDataSource<Service> =
        FirestoreDataSourceImpl(serviceCollection)

    override suspend fun getServicesByType(serviceTypeId: String): Resource<MutableList<Service>> {
        return dataSource.filterWith(
            serviceCollection.whereEqualTo(
                "serviceType",
                CommonUtils.getDocumentReference(
                    CollectionConst.COLLECTION_SERVICE_TYPE,
                    serviceTypeId
                )
            )
        ) {
            it.toService()
        }
    }

    override suspend fun getServiceById(serviceId: String): Resource<Service?> {
        return dataSource.get(serviceId) {
            it.toService()
        }
    }

}