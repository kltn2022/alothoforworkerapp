package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasource

import com.google.firebase.auth.FirebaseUser
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User

interface UserRemoteDataSource {
    suspend fun getUser(uid: String) : Resource<User?>
    suspend fun getUserByPhoneNumber(phoneNumber: String) : Resource<User?>
    suspend fun insertUser(firebaseUser: FirebaseUser): Resource<User?>
    suspend fun updateUserInfo(uid: String, data: MutableMap<String, Any>): Resource<Boolean>
}