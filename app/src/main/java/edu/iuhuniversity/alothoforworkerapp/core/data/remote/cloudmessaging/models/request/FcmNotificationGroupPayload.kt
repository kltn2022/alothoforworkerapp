package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request

import com.google.gson.annotations.SerializedName

/**
 * Refer: https://firebase.google.com/docs/cloud-messaging/android/device-group#managing-device
 * -groups-on-the-app-server
 */

enum class NotificationOperation(value: String) {
    // CREATE("create"), // create a new device group
    ADD("add"), // add devices from an existing group
    DELETE("remove") // remove devices from an existing group
}

data class FcmUpdateNotificationGroup(
    @SerializedName("operation")
    val operation: NotificationOperation,
    @SerializedName("notification_key_name")
    val notificationKeyName: String,
    @SerializedName("notification_key")
    val notificationKey: String,
    @SerializedName("registration_ids")
    val registrationIds: List<String>
)

data class FcmCreateNotificationGroup(
    @SerializedName("operation")
    val operation: String = "create",
    @SerializedName("notification_key_name")
    val notificationKeyName: String,
    @SerializedName("registration_ids")
    val registrationIds: List<String>
)