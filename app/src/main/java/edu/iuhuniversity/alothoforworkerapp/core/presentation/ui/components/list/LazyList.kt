package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

enum class OrientationList {
    VERTICAL,
    HORIZONTAL
}

@Composable
fun <T> LazyList(
    modifier: Modifier = Modifier,
    orientation: OrientationList = OrientationList.VERTICAL,
    items: List<T>,
    spaceBetweenItems: Dp = 5.dp,
    lazyListState : LazyListState = rememberLazyListState(),
    itemContent: @Composable() (LazyItemScope.(T) -> Unit),
) {
    if (orientation == OrientationList.VERTICAL) {
        LazyColumn(
            modifier = Modifier
                .fillMaxWidth()
                .then(modifier),
            verticalArrangement = Arrangement.spacedBy(spaceBetweenItems),
            horizontalAlignment = Alignment.CenterHorizontally,
            state = lazyListState
        ) {

            items(items = items) { item ->
                itemContent(item)
            }
        }
    } else {
        LazyRow(
            modifier = Modifier
                .fillMaxWidth()
                .then(modifier),
            horizontalArrangement = Arrangement.spacedBy(spaceBetweenItems),
            verticalAlignment = Alignment.CenterVertically
        ) {
            items(items = items) {
                itemContent(it)
            }
        }
    }

}
