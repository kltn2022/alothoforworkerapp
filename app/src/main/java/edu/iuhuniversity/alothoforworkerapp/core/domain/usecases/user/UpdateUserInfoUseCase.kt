package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository

class UpdateUserInfoUseCase(private val workerRepository: WorkerRepository) {
    suspend fun execute(uid: String, data: MutableMap<String, Any>) =
        workerRepository.updateWorkerInfo(uid, data)
}