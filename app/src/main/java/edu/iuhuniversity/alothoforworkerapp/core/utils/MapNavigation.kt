package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import com.mapbox.android.core.location.LocationEngineProvider
import com.mapbox.android.core.location.LocationEngineRequest
import com.mapbox.geojson.Point
import com.mapbox.navigation.base.options.NavigationOptions
import com.mapbox.navigation.base.route.RouteRefreshOptions
import com.mapbox.navigation.core.MapboxNavigation
import com.mapbox.navigation.core.MapboxNavigationProvider
import com.mapbox.navigation.core.trip.session.LocationMatcherResult
import com.mapbox.navigation.core.trip.session.LocationObserver
import edu.iuhuniversity.alothoforworkerapp.R
import timber.log.Timber
import java.util.concurrent.TimeUnit

private const val DESIRED_INTERVAL_BETWEEN_LOCATION_UPDATES = 30L
private const val DEFAULT_MAX_WAIT_TIME = 1L

private val alothoLocationEngineRequest = LocationEngineRequest.Builder(
    TimeUnit.MINUTES.toMillis(
        DESIRED_INTERVAL_BETWEEN_LOCATION_UPDATES
    )
)
    .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
    .setMaxWaitTime(
        TimeUnit.MINUTES.toMillis(
            DEFAULT_MAX_WAIT_TIME
        )
    )
    .build()

fun buildMapboxNavigation(
    context: Context
): MapboxNavigation {
    return if (!MapboxNavigationProvider.isCreated()) {
        MapboxNavigationProvider.create(
            NavigationOptions.Builder(context)
                .accessToken(context.getString(R.string.mapbox_access_token))
                .locationEngine(LocationEngineProvider.getBestLocationEngine(context))
                .locationEngineRequest(alothoLocationEngineRequest)
                .routeRefreshOptions(
                    RouteRefreshOptions.Builder()
                        .intervalMillis(
                            TimeUnit.MINUTES.toMillis(
                                DESIRED_INTERVAL_BETWEEN_LOCATION_UPDATES
                            )
                        )
                        .build()
                )
                .build()
        )
    } else {
        MapboxNavigationProvider.retrieve()
    }
}


fun trackCurrentUserLocation(
    context: Context,
    updateCurrentLocationToServer: (Point) -> Unit
) {
    val mapboxNavigation: MapboxNavigation = buildMapboxNavigation(context)
    mapboxNavigation.apply {
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startTripSession()
        } else {
            Timber.tag("NoPermission").d("Ko được cấp quyền")
        }

        registerLocationObserver(object : LocationObserver {
            override fun onNewLocationMatcherResult(locationMatcherResult: LocationMatcherResult) {
                val enhancedLocation = locationMatcherResult.enhancedLocation

                val point =
                    Point.fromLngLat(enhancedLocation.longitude, enhancedLocation.latitude)

                updateCurrentLocationToServer(point)
            }

            override fun onNewRawLocation(rawLocation: Location) {}

        })
    }
}