package edu.iuhuniversity.alothoforworkerapp.core.data.cache.order

import androidx.room.*
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedorder.CachedOrder

@Dao
interface OrderDao {
    @Query("SELECT * FROM tbl_order")
    fun getOrders() : MutableList<CachedOrder>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrders(orders: List<CachedOrder>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrder(order: CachedOrder)

    @Update
    fun updateOrder(order: CachedOrder)

    @Delete
    fun deleteOrder(order: CachedOrder)

    @Query("DELETE FROM tbl_order")
    fun deleteAllOrders()

    @Query("SELECT * FROM tbl_order WHERE id = :orderId")
    fun getOrderById(orderId: String): CachedOrder?
}