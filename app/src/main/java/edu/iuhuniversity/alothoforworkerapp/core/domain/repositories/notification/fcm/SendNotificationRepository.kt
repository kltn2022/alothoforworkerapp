package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse

interface SendNotificationRepository {
    suspend fun sendNotificationToCustomer(token: String?, order: Order): Resource<FcmResponse>
}