package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.annotation.SuppressLint
import android.net.Uri
import android.text.TextUtils
import com.google.android.gms.common.internal.Preconditions
import com.google.firebase.storage.network.NetworkRequest
import timber.log.Timber
import java.io.UnsupportedEncodingException
import java.lang.IllegalArgumentException
import java.lang.StringBuilder

const val PREFIX_CLOUD_STORAGE_URI = "gs://"
const val STORAGE_FOLDER_PATH_DEFAULT = "default"

/**
 * Normalizes a Firebase Storage uri into its "gs://" format and strips any trailing slash.
 *
 * @param s the url to normalize
 * @return a gs Uri parsed from the given string.
 */
@SuppressLint("RestrictedApi")
@Throws(UnsupportedEncodingException::class)
fun normalize(s: String?): Uri? {
    if (TextUtils.isEmpty(s)) {
        return null
    }
    val invalidUrlMessage =
        ("Firebase Storage URLs must point to an object in your Storage Bucket. Please "
                + "obtain a URL using the Firebase Console or getDownloadUrl().")
    val baseUrl = NetworkRequest.PROD_BASE_URL
    val trimmedInput = s!!.lowercase()
    val bucket: String
    var encodedPath: String
    if (trimmedInput.startsWith("gs://")) {
        val fullUri = Slashes.preserveSlashEncode(Slashes.normalizeSlashes(s.substring(5)))
        return Uri.parse("gs://$fullUri")
    } else {
        val uri = Uri.parse(s)
        val scheme = uri.scheme
        if (scheme != null
            && (scheme.equals("http", ignoreCase = true) || scheme.equals(
                "https",
                ignoreCase = true
            ))
        ) {
            val lowerAuthority = uri.authority?.lowercase()
            val indexOfAuth = lowerAuthority?.indexOf(baseUrl?.authority!!)
            encodedPath = Slashes.slashize(uri?.encodedPath!!)
            if (indexOfAuth == 0 && encodedPath.startsWith("/")) {
                val firstBSlash = encodedPath.indexOf("/b/", 0) // /v0/b/bucket.storage
                // .firebase.com/o/child/image.png
                val endBSlash = encodedPath.indexOf("/", firstBSlash + 3)
                val firstOSlash = encodedPath.indexOf("/o/", 0)
                if (firstBSlash != -1 && endBSlash != -1) {
                    bucket = encodedPath.substring(firstBSlash + 3, endBSlash)
                    encodedPath = if (firstOSlash != -1) {
                        encodedPath.substring(firstOSlash + 3)
                    } else {
                        ""
                    }
                } else {
                    Timber.w(invalidUrlMessage)
                    throw IllegalArgumentException(invalidUrlMessage)
                }
            } else if (indexOfAuth!! > 1) {
                bucket = uri.authority!!.substring(0, indexOfAuth - 1)
            } else {
                Timber.w(invalidUrlMessage)
                throw IllegalArgumentException(invalidUrlMessage)
            }
        } else {
            Timber.w("FirebaseStorage is unable to support the scheme:$scheme")
            throw IllegalArgumentException("Uri scheme")
        }
    }
    Preconditions.checkNotEmpty(bucket, "No bucket specified")
    return Uri.Builder().scheme("gs").authority(bucket).encodedPath(encodedPath).build()
}

/**
 * Utility methods for Firebase Storage.
 *
 * @hide
 */
object Slashes {
    /**
     * URL encodes a string, but leaves slashes unmolested. This is used for encoding gs uri paths to
     * objects -- where the individual path segments need to be escaped, but the slashes do not.
     *
     * @param s The String to convert
     * @return A partially URL encoded string where slashes are preserved.
     */
    fun preserveSlashEncode(s: String?): String {
        return if (TextUtils.isEmpty(s)) {
            ""
        } else slashize(Uri.encode(s))
    }

    /**
     * Restores slashes within an encoded string.
     *
     * @param s The String to change
     * @return A modified string that replaces escaped slashes with unescaped slashes.
     */
    fun slashize(s: String): String {
        Preconditions.checkNotNull(s)
        return s.replace("%2F", "/")
    }

    fun normalizeSlashes(uriSegment: String): String {
        if (TextUtils.isEmpty(uriSegment)) {
            return ""
        }
        if (uriSegment.startsWith("/") || uriSegment.endsWith("/") || uriSegment.contains("//")) {
            val result = StringBuilder()
            for (stringSegment in uriSegment.split("/").dropLastWhile { it.isEmpty() }
                .toTypedArray()) {
                if (!TextUtils.isEmpty(stringSegment)) {
                    if (result.isNotEmpty()) {
                        result.append("/").append(stringSegment)
                    } else {
                        result.append(stringSegment)
                    }
                }
            }
            return result.toString()
        }
        return uriSegment
    }
}