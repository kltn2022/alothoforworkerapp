package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction.TransactionRepository

class GetTransactionsUseCase(private val transactionRepository: TransactionRepository) {
    suspend fun execute() = transactionRepository.getTransactions()
}