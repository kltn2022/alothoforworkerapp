package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service

sealed interface ServiceDataSource {
    interface Cache {
        suspend fun getServicesByTypeFromDb(serviceTypeId: String): MutableList<Service>
        suspend fun getServiceById(serviceId: String): Service?
    }

    interface Remote {
        suspend fun getServicesByType(serviceTypeId: String): Resource<MutableList<Service>>
        suspend fun getServiceById(serviceId: String): Resource<Service?>
    }
}