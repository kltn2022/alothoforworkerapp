package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.payment

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.payment.PaymentRepository

class PaymentRepositoryImpl(
    private val paymentService: PaymentService.Momo
) : PaymentRepository {

    override suspend fun createMomoPayment(payload: ProcessingPayment.Request): Resource<ProcessingPayment.Response> {
        val response = paymentService.processMomoPayment(payload)
        return if (response.isSuccessful) {
            Resource.Success(response.body()!!)
        } else {
            Resource.Error(response.message())
        }
    }

}