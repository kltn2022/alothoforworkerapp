package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasourceimpl

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst.COLLECTION_USER
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.FirestoreDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User.Companion.toUser
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasource.UserRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.utils.toAloThoPhone

class UserRemoteDataSourceImpl(
    db: FirebaseFirestore,
) : UserRemoteDataSource {

    private val userCollection = db.collection(COLLECTION_USER);
    private var dataSource: FirestoreDataSource<User> = FirestoreDataSourceImpl(userCollection)

    override suspend fun getUser(uid: String): Resource<User?> {
        return dataSource.get(uid) { it.toUser() }
    }

    override suspend fun getUserByPhoneNumber(phoneNumber: String): Resource<User?> {
        return Resource.Success(dataSource
            .filterWith(
                userCollection
                    .whereEqualTo("phoneNumber", phoneNumber.toAloThoPhone()),
            ) { it.toUser() }
            .data
            ?.first())
    }

    override suspend fun insertUser(firebaseUser: FirebaseUser): Resource<User?> {
        return dataSource.insert(User.convertFrom(firebaseUser)!!) { it.toUser() }
    }

    override suspend fun updateUserInfo(
        uid: String,
        data: MutableMap<String, Any>
    ): Resource<Boolean> {
        return dataSource.update("id" to uid, data)
    }
}