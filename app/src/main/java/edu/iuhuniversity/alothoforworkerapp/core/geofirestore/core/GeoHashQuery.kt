package edu.iuhuniversity.alothoforworkerapp.core.geofirestore.core

import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils.base32CharToValue
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Base32Utils.valueToBase32Char
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.GeoUtils
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.Constants as Constants1


class GeoHashQuery(val startValue: String, val endValue: String) {
    class Utils private constructor() {
        companion object {
            fun bitsLatitude(resolution: Double): Double {
                return Math.min(
                    (Math.log(Constants1.EARTH_MERIDIONAL_CIRCUMFERENCE / 2 / resolution) / Math.log(
                        2.0
                    )),
                    GeoHash.MAX_PRECISION_BITS.toDouble()
                )
            }

            fun bitsLongitude(resolution: Double, latitude: Double): Double {
                val degrees = GeoUtils.distanceToLongitudeDegrees(resolution, latitude)
                return if (Math.abs(degrees) > 0) Math.max(
                    1.0,
                    Math.log(360 / degrees) / Math.log(2.0)
                ) else 1.0
            }

            fun bitsForBoundingBox(location: GeoLocation, size: Double): Int {
                val latitudeDegreesDelta = GeoUtils.distanceToLatitudeDegrees(size)
                val latitudeNorth = Math.min(90.0, location.latitude + latitudeDegreesDelta)
                val latitudeSouth = Math.max(-90.0, location.latitude - latitudeDegreesDelta)
                val bitsLatitude = Math.floor(bitsLatitude(size)).toInt() * 2
                val bitsLongitudeNorth =
                    Math.floor(bitsLongitude(size, latitudeNorth)).toInt() * 2 - 1
                val bitsLongitudeSouth =
                    Math.floor(bitsLongitude(size, latitudeSouth)).toInt() * 2 - 1
                return Math.min(bitsLatitude, Math.min(bitsLongitudeNorth, bitsLongitudeSouth))
            }
        }

        init {
            throw AssertionError("No instances.")
        }
    }

    private fun isPrefix(other: GeoHashQuery): Boolean {
        return other.endValue.compareTo(startValue) >= 0 &&
                other.startValue.compareTo(startValue) < 0 &&
                other.endValue.compareTo(endValue) < 0
    }

    private fun isSuperQuery(other: GeoHashQuery): Boolean {
        val startCompare = other.startValue.compareTo(startValue)
        return startCompare <= 0 && other.endValue.compareTo(endValue) >= 0
    }

    fun canJoinWith(other: GeoHashQuery): Boolean {
        return isPrefix(other) || other.isPrefix(this) || isSuperQuery(other) || other.isSuperQuery(
            this
        )
    }

    fun joinWith(other: GeoHashQuery): GeoHashQuery {
        return if (other.isPrefix(this)) {
            GeoHashQuery(startValue, other.endValue)
        } else if (isPrefix(other)) {
            GeoHashQuery(other.startValue, endValue)
        } else if (isSuperQuery(other)) {
            other
        } else if (other.isSuperQuery(this)) {
            this
        } else {
            throw IllegalArgumentException("Can't join these 2 queries: $this, $other")
        }
    }

    fun containsGeoHash(hash: GeoHash): Boolean {
        val hashStr = hash.geoHashString
        return startValue.compareTo(hashStr) <= 0 && endValue.compareTo(hashStr) > 0
    }

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val that = o as GeoHashQuery
        if (endValue != that.endValue) return false
        return if (startValue != that.startValue) false else true
    }

    override fun hashCode(): Int {
        var result = startValue.hashCode()
        result = 31 * result + endValue.hashCode()
        return result
    }

    override fun toString(): String {
        return "GeoHashQuery{" +
                "startValue='" + startValue + '\'' +
                ", endValue='" + endValue + '\'' +
                '}'
    }

    companion object {
        fun queryForGeoHash(geohash: GeoHash, bits: Int): GeoHashQuery {
            var hash = geohash.geoHashString
            val precision = Math.ceil(bits.toDouble() / Base32Utils.BITS_PER_BASE32_CHAR).toInt()
            if (hash.length < precision) {
                return GeoHashQuery(hash, "$hash~")
            }
            hash = hash.substring(0, precision)
            val base = hash.substring(0, hash.length - 1)
            val lastValue = base32CharToValue(hash[hash.length - 1])
            val significantBits = bits - base.length * Base32Utils.BITS_PER_BASE32_CHAR
            val unusedBits = Base32Utils.BITS_PER_BASE32_CHAR - significantBits
            // delete unused bits
            val startValue = lastValue shr unusedBits shl unusedBits
            val endValue = startValue + (1 shl unusedBits)
            val startHash = base + valueToBase32Char(startValue)
            val endHash: String
            endHash = if (endValue > 31) {
                "$base~"
            } else {
                base + valueToBase32Char(endValue)
            }
            return GeoHashQuery(startHash, endHash)
        }

        fun queriesAtLocation(location: GeoLocation, radius: Double): Set<GeoHashQuery> {
            val queryBits = Math.max(1, Utils.bitsForBoundingBox(location, radius))
            val geoHashPrecision =
                Math.ceil((queryBits.toFloat() / Base32Utils.BITS_PER_BASE32_CHAR).toDouble())
                    .toInt()
            val latitude = location.latitude
            val longitude = location.longitude
            val latitudeDegrees: Double = radius / Constants1.METERS_PER_DEGREE_LATITUDE
            val latitudeNorth = Math.min(90.0, latitude + latitudeDegrees)
            val latitudeSouth = Math.max(-90.0, latitude - latitudeDegrees)
            val longitudeDeltaNorth = GeoUtils.distanceToLongitudeDegrees(radius, latitudeNorth)
            val longitudeDeltaSouth = GeoUtils.distanceToLongitudeDegrees(radius, latitudeSouth)
            val longitudeDelta = Math.max(longitudeDeltaNorth, longitudeDeltaSouth)
            val queries: MutableSet<GeoHashQuery> = HashSet()
            val geoHash = GeoHash(latitude, longitude, geoHashPrecision)
            val geoHashW = GeoHash(
                latitude,
                GeoUtils.wrapLongitude(longitude - longitudeDelta),
                geoHashPrecision
            )
            val geoHashE = GeoHash(
                latitude,
                GeoUtils.wrapLongitude(longitude + longitudeDelta),
                geoHashPrecision
            )
            val geoHashN = GeoHash(latitudeNorth, longitude, geoHashPrecision)
            val geoHashNW = GeoHash(
                latitudeNorth,
                GeoUtils.wrapLongitude(longitude - longitudeDelta),
                geoHashPrecision
            )
            val geoHashNE = GeoHash(
                latitudeNorth,
                GeoUtils.wrapLongitude(longitude + longitudeDelta),
                geoHashPrecision
            )
            val geoHashS = GeoHash(latitudeSouth, longitude, geoHashPrecision)
            val geoHashSW = GeoHash(
                latitudeSouth,
                GeoUtils.wrapLongitude(longitude - longitudeDelta),
                geoHashPrecision
            )
            val geoHashSE = GeoHash(
                latitudeSouth,
                GeoUtils.wrapLongitude(longitude + longitudeDelta),
                geoHashPrecision
            )
            queries.add(queryForGeoHash(geoHash, queryBits))
            queries.add(queryForGeoHash(geoHashE, queryBits))
            queries.add(queryForGeoHash(geoHashW, queryBits))
            queries.add(queryForGeoHash(geoHashN, queryBits))
            queries.add(queryForGeoHash(geoHashNE, queryBits))
            queries.add(queryForGeoHash(geoHashNW, queryBits))
            queries.add(queryForGeoHash(geoHashS, queryBits))
            queries.add(queryForGeoHash(geoHashSE, queryBits))
            queries.add(queryForGeoHash(geoHashSW, queryBits))

            // Join queries
            var didJoin: Boolean
            do {
                var query1: GeoHashQuery? = null
                var query2: GeoHashQuery? = null
                for (query in queries) {
                    for (other in queries) {
                        if (query !== other && query.canJoinWith(other)) {
                            query1 = query
                            query2 = other
                            break
                        }
                    }
                }
                didJoin = if (query1 != null && query2 != null) {
                    queries.remove(query1)
                    queries.remove(query2)
                    queries.add(query1.joinWith(query2))
                    true
                } else {
                    false
                }
            } while (didJoin)
            return queries
        }
    }
}
