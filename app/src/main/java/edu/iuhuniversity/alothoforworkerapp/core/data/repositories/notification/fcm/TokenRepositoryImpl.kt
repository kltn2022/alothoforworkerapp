package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.fcm

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.TokenDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.RegistrationToken
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository

class TokenRepositoryImpl(
    private val tokenDataSource: TokenDataSource
): TokenRepository {
    override suspend fun generateRegistrationToken(): Resource<Boolean> {
        return tokenDataSource.generateRegistrationToken()
    }

    override suspend fun sendRegistrationToServer(registrationToken: RegistrationToken): Resource<Boolean> {
        return tokenDataSource.sendRegistrationToServer(registrationToken)
    }
    override suspend fun getRegistrationToken(uid: String): Resource<RegistrationToken?> {
        return tokenDataSource.getRegistrationToken(uid)
    }
}