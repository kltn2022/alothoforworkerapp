package edu.iuhuniversity.alothoforworkerapp.core.data.model

const val PAGINATE_DEFAULT_OFFSET = 10L

data class PagedResult<T>(
    val currentPage: Long,
    val perPage: Long,
    val totalPages: Long,
    val totalResults: Long,
    val results: List<T>?
)