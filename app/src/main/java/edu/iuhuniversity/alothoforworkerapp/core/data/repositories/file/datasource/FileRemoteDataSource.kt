package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasource

import android.net.Uri
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback

interface FileRemoteDataSource {
    suspend fun getDownloadUrl(input: String): Resource<Uri>
    suspend fun uploadFiles(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    )
}