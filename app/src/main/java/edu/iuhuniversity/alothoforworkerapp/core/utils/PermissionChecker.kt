package edu.iuhuniversity.alothoforworkerapp.core.utils

import androidx.compose.runtime.Composable
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberMultiplePermissionsState
import edu.iuhuniversity.alothoforworkerapp.core.constant.PermissionConst
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun LocationPermissionChecker(
    onLocationPermissionsGranted: () -> Unit
) {
    val locationPermissionsState = rememberMultiplePermissionsState(
        PermissionConst.LOCATION_PERMISSIONS
    )

    if (locationPermissionsState.allPermissionsGranted) {
        onLocationPermissionsGranted()
    } else {
        val allPermissionsRevoked =
            locationPermissionsState.permissions.size ==
                    locationPermissionsState.revokedPermissions.size

        val textToShow = if (!allPermissionsRevoked) {
            // If not all the permissions are revoked, it's because the user accepted the COARSE
            // location permission, but not the FINE one.
            "Yay! Thanks for letting me access your approximate location. " +
                    "But you know what would be great? If you allow me to know where you " +
                    "exactly are. Thank you!"
        } else if (locationPermissionsState.shouldShowRationale) {
            // Both location permissions have been denied
            "Getting your exact location is important for this app. " +
                    "Please grant us fine location. Thank you :D"
        } else {
            // First time the user sees this feature or the user doesn't want to be asked again
            "This feature requires location permission"
        }

        val buttonText = if (!allPermissionsRevoked) {
            "Allow precise location"
        } else {
            "Request permissions"
        }

        DialogMessage(
            title = "Thông báo cấp quyền truy cập",
            messageType = MessageType.WARNING,
            colorType = BootstrapType.SMOOTH,
            text = textToShow,
            onDismissRequest = { /*TODO*/ },
            positiveButton = ActionButton(buttonText) {
                locationPermissionsState.launchMultiplePermissionRequest()
            }
        )
    }
}

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun PermissionChecker(
    permissions: List<String>,
    onPermissionsGranted: @Composable () -> Unit
) {
    val permissionsState = rememberMultiplePermissionsState(permissions)

    if (permissionsState.allPermissionsGranted) {
        onPermissionsGranted()
    } else {
        val allPermissionsRevoked =
            permissionsState.permissions.size ==
                    permissionsState.revokedPermissions.size

        val textToShow = if (!allPermissionsRevoked) {
            "Xin hãy cấp đủ quyền để chúng tôi có thể cung cấp dịch vụ với trải nghiệm tốt nhất. Cảm ơn!"
        } else if (permissionsState.shouldShowRationale) {
            "Xin hãy cấp đủ quyền để chúng tôi có thể cung cấp dịch vụ với trải nghiệm tốt nhất. Cảm ơn!"
        } else {
            "Chức năng này cần cấp một số quyền"
        }

        val buttonText = if (!allPermissionsRevoked) {
            "Cấp quyền còn thiếu"
        } else {
            "Cấp tất cả"
        }

        DialogMessage(
            title = "Thông báo cấp quyền truy cập",
            messageType = MessageType.WARNING,
            colorType = BootstrapType.SMOOTH,
            text = textToShow,
            onDismissRequest = { },
            positiveButton = ActionButton(buttonText) {
                permissionsState.launchMultiplePermissionRequest()
            }
        )
    }
}