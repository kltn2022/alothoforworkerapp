package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.Column
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.AccessibilityManager
import androidx.compose.ui.platform.LocalAccessibilityManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import kotlinx.coroutines.delay
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.SnackbarAttribute.Companion.toSnackbarAttribute
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapColorSystem
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.BootstrapType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import timber.log.Timber

@Composable
fun SnackbarMessage(
    modifier: Modifier = Modifier,
    message: String,
    duration: SnackbarDuration = SnackbarDuration.Short,
    actionLabel: String? = null,
    onActionClickListener: () -> Unit,
    actionOnNewLine: Boolean = false,
    snackbarType: MessageType = MessageType.WARNING,
    type: BootstrapType = BootstrapType.FILLED,
    onDismiss: (Boolean) -> Unit
) {
    val (snackbarVisibleState, setSnackBarState) = remember { mutableStateOf(true) }
    val accessibilityManager = LocalAccessibilityManager.current
    val snackbarAttribute = type.toSnackbarAttribute(snackbarType.color)

    val actionComposable: (@Composable () -> Unit)? = if (actionLabel != null) {
        @Composable {
            TextButton(
                colors = ButtonDefaults.textButtonColors(contentColor = snackbarAttribute.textColor),
                onClick = {
                    setSnackBarState(false)
                    onDismiss(snackbarVisibleState)
                    onActionClickListener()
                },
                content = {
                    Text(
                        actionLabel,
                        style = AloThoTypography.subtitle1,
                        fontWeight = FontWeight.SemiBold
                    )
                }
            )
        }
    } else {
        null
    }
    if (snackbarVisibleState) {
        Snackbar(
            backgroundColor = snackbarAttribute.backgroundColor,
            contentColor = snackbarAttribute.textColor,
            action = actionComposable,
            actionOnNewLine = actionOnNewLine,
            modifier = modifier
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    painter = painterResource(id = snackbarType.resIconId),
                    contentDescription = "",
                    tint = snackbarAttribute.iconTintColor
                )
                Text(
                    text = message,
                    style = AloThoTypography.body1,
                    color = snackbarAttribute.textColor,
                    modifier = Modifier
                        .padding(start = 10.dp)
                )
            }
        }
    }
    LaunchedEffect(snackbarVisibleState) {
        if (snackbarVisibleState) {
            delay(
                duration.toMillis(
                    hasAction = actionLabel != null,
                    accessibilityManager = accessibilityManager
                )
            )
            setSnackBarState(false)
            onDismiss(snackbarVisibleState)
        }
    }

}

fun SnackbarDuration.toMillis(
    hasAction: Boolean,
    accessibilityManager: AccessibilityManager?
): Long {
    val original = when (this) {
        SnackbarDuration.Indefinite -> Long.MAX_VALUE
        SnackbarDuration.Long -> 10000L
        SnackbarDuration.Short -> 4000L
    }
    if (accessibilityManager == null) {
        return original
    }
    return accessibilityManager.calculateRecommendedTimeoutMillis(
        original,
        containsIcons = true,
        containsText = true,
        containsControls = hasAction
    )
}


data class SnackbarAttribute(
    val backgroundColor: Color,
    val textColor: Color,
    val iconTintColor: Color,
) {
    companion object {
        fun BootstrapType.toSnackbarAttribute(
            colorType: BootstrapColor
        ) : SnackbarAttribute {
            return when (this) {
                BootstrapType.FILLED -> SnackbarAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_50),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30),
                )
                BootstrapType.SMOOTH -> SnackbarAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_5),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_100),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30)
                )
                BootstrapType.RAISED -> SnackbarAttribute(
                    backgroundColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_0),
                    textColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_100),
                    iconTintColor = colorType.getColorByVal(BootstrapColorSystem.GeneralColorEnum.COLOR_30)
                )
            }
        }
    }
}


@Preview
@Composable
fun Preview2() {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(10.dp),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        val (snackbarVisibleState, setSnackBarState) = remember { mutableStateOf(false) }

        Button(onClick = { setSnackBarState(!snackbarVisibleState) }) {
            Text("Show Snackbar")
        }
        if (snackbarVisibleState) {
            SnackbarMessage(
                message = "Đây là snackbar của tui nhe chời, hàng VN chất lượng caooooo!",
                actionLabel = "Đã hiểu",
                onActionClickListener = { Timber.i("Đã click action") },
                snackbarType = MessageType.SUCCESS,
                onDismiss = {
                    // setSnackBarState(it)
                }
            )
        }
    }
}