package edu.iuhuniversity.alothoforworkerapp.core.utils

import androidx.compose.ui.graphics.painter.Painter
import cafe.adriel.voyager.navigator.tab.TabOptions

data class NavRailOptions(
    val index: UShort,
    val title: String,
    val icon: Painter? = null
)

fun TabOptions.toNavRailOptions() : NavRailOptions {
    return NavRailOptions(
        this.index,
        this.title,
        this.icon
    )
}

fun NavRailOptions.toTabOptions() : TabOptions {
    return TabOptions(
        this.index,
        this.title,
        this.icon
    )
}