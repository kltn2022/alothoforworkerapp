package edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.vnpay

import edu.iuhuniversity.alothoforworkerapp.core.utils.VNP_DATE_FORMAT
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.EPaymentUtil
import java.util.*

data class CreatePaymentRequest(
    val vnp_Version: String = "2",
    val vnp_Command: String = "pay",
    val vnp_TmnCode: String? = null,
    val vnp_Amount: Long = 0,
    val vnp_BankCode: String? = null,
    val vnp_CreateDate: String = "",
    val vnp_CurrCode: String = "VND",
    val vnp_IpAddr: String = "0.0.0.0",
    val vnp_Locale: String = "vn",
    val vnp_OrderInfo: String? = null,
    val vnp_OrderType: Long = 0,
    val vnp_ReturnUrl: String = "",
    val vnp_TxnRef: String? = null,
    val vnp_SecureHashType: String = "",
    val vnp_SecureHash: String = "SHA256"
) {
    companion object {
        fun createFrom(
            orderId: String,
            amount: Long,
            bankCode: String?
        ): CreatePaymentRequest {
            val request = CreatePaymentRequest(
                vnp_TmnCode = "",
                vnp_Amount = amount * 100,
                vnp_BankCode = bankCode,
                vnp_CreateDate = VNP_DATE_FORMAT.format(Date()),
                vnp_OrderInfo = orderId,
                vnp_OrderType = 260000,
                vnp_TxnRef = orderId,
                vnp_ReturnUrl = "https://thonow.page.link/successful-payment",
            )

            val hash = EPaymentUtil.generateVnPaySecureHash(request)!!
            return request.copy(vnp_SecureHash = hash)
        }
    }
}



