package edu.iuhuniversity.alothoforworkerapp.core.constant

object CollectionConst {
    const val COLLECTION_USER = "users"
    const val COLLECTION_USER_GEOLOCATION = "user_geolocations"
    const val LIB_COUNTRY_CODE = "lib_country_code"
    const val COLLECTION_OFFER = "offers"
    const val COLLECTION_NEWS = "news"
    const val COLLECTION_SERVICE = "services"
    const val COLLECTION_SERVICE_TYPE = "service_type"
    const val COLLECTION_WORKER = "workers"
    const val COLLECTION_WORKER_GEOLOCATION = "worker_geolocations"
    const val COLLECTION_TOKEN = "tokens"
    const val COLLECTION_ORDER = "orders"
    const val COLLECTION_MESSAGE = "messages"
    const val COLLECTION_CONSERVATION = "conservations"
    const val COLLECTION_LAST_MESSAGE = "last_message"
    const val COLLECTION_CUSTOMER_TRANSACTION_HISTORY = "customer_transaction_history"
}
