package edu.iuhuniversity.alothoforworkerapp.core.data.model.message

import com.google.firebase.firestore.DocumentSnapshot
import timber.log.Timber
import java.util.*


data class Message(
    val id: String? = "",
    val content: String? = "",
    val from: String,
    val conservationId: String,
    val timestamp: Date? = Date(),
    val type: MessageType? = MessageType.TEXT
) {
    companion object {

        fun createNewMessage(
            conservationId: String, content: String, from: String, messageType:
            MessageType
        ):
                Message {
            return Message(
                content = content,
                conservationId = conservationId,
                type = messageType,
                from = from
            )
        }

        fun DocumentSnapshot.toMessage(): Message? {
            return try {
                val content = getString("content").orEmpty()
                val from = getString("content").orEmpty()
                val conservationId = getString("conservationId").orEmpty()
                val timestamp = getDate("timestamp")
                val messageType = getString("messageType")?.let { MessageType.valueOf(it) }
                Message(
                    id,
                    content,
                    from,
                    conservationId,
                    timestamp,
                    messageType
                )
            } catch (e: Exception) {
                Timber.d(e.message!!.toString())
                null
            }
        }
    }
}
