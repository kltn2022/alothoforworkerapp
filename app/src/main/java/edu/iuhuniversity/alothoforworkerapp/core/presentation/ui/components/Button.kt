package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import android.os.Build
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Paint
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*

@Composable
fun LianButton_StylePrimary(
    layoutModifier: Modifier = Modifier,
    modifier: Modifier = Modifier,
    label: String,
    onClick: (() -> Unit),
    cornerRadius: Dp? = 100.dp,
    width: Dp = 250.dp,
    verticalPadding: Dp? = 8.dp
) {
    val roundedShape = RoundedCornerShape(cornerRadius!!)
    Button(
        shape = roundedShape,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent
        ),
        contentPadding = PaddingValues(0.dp),
        modifier = Modifier
            .padding(
                horizontal = verticalPadding!!,
                vertical = verticalPadding
            )
            .dropShadow(roundedShape)
            .then(layoutModifier)
    ) {
        Box(
            modifier = modifier
                .width(width)
                .background(
                    brush = Brush.horizontalGradient(
                        colors = listOf(
                            PrimaryVariantColor,
                            PrimaryColor,
                            SecondaryColor
                        )
                    )
                )
                .padding(
                    horizontal = verticalPadding,
                    vertical = verticalPadding
                ),
            contentAlignment = Alignment.Center
        ) {
            Text(
                label,
                color = Color.White,
                style = AloThoTypography.button.copy(fontSize = 20.sp, fontWeight = FontWeight.SemiBold)
            )
        }
    }
}

@Composable
fun LianButton_StyleWhite(
    layoutModifier: Modifier = Modifier,
    modifier: Modifier? = Modifier,
    label: String,
    onClick: (() -> Unit),
    cornerRadius: Dp? = 100.dp,
    width: Dp? = 250.dp,
    verticalPadding: Dp? = 8.dp,
) {
    val roundedShape = RoundedCornerShape(cornerRadius!!)

    Button(
        shape = roundedShape,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            backgroundColor = Color.Transparent
        ),
        contentPadding = PaddingValues(0.dp),
        modifier = Modifier
            .dropShadow(roundedShape)
            .then(layoutModifier)
    ) {
        Box(
            modifier = modifier!!
                .background(Color.White)
                .width(width!!)
                .padding(
                    horizontal = verticalPadding!!,
                    vertical = verticalPadding
                ),
            contentAlignment = Alignment.Center
        ) {
            Text(
                label,
                color = SecondaryColor,
                style = AloThoTypography.button.copy(fontSize = 20.sp),
                modifier = Modifier
            )
        }
    }
}

@Preview
@Composable
fun PreviewButton() {
    LianButton_StyleWhite(
        label = "Đăng nhập",
        onClick = { /*TODO*/ },
    )
}

fun Modifier.dropShadow(
    roundedCornerShape: RoundedCornerShape
) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
    this.drawColoredShadow(
        color = PrimaryColor,
        borderRadius = 10.dp,
        shadowRadius = 10.dp,
        offsetX = 2.dp,
        offsetY = 2.dp
    )
} else {
    this.shadow(
        elevation = 2.dp,
        shape = roundedCornerShape,
        clip = true
    )
}

fun Modifier.drawColoredShadow(
    color: Color,
    alpha: Float = 0.2f,
    borderRadius: Dp = 0.dp,
    shadowRadius: Dp = 20.dp,
    offsetY: Dp = 0.dp,
    offsetX: Dp = 0.dp
) = this.drawBehind {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val transparentColor =
            android.graphics.Color.toArgb(color.copy(alpha = 0.0f).value.toLong())
        val shadowColor = android.graphics.Color.toArgb(color.copy(alpha = alpha).value.toLong())
        this.drawIntoCanvas {
            val paint = Paint()
            val frameworkPaint = paint.asFrameworkPaint()
            frameworkPaint.color = transparentColor
            frameworkPaint.setShadowLayer(
                shadowRadius.toPx(),
                offsetX.toPx(),
                offsetY.toPx(),
                shadowColor
            )
            it.drawRoundRect(
                0f,
                0f,
                this.size.width,
                this.size.height,
                borderRadius.toPx(),
                borderRadius.toPx(),
                paint
            )
        }
    }
}