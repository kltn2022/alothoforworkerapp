package edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet

import android.annotation.SuppressLint
import android.content.Intent
import com.google.firebase.messaging.Constants.MessageTypes.MESSAGE
import edu.iuhuniversity.alothoforworkerapp.BuildConfig
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo.ProcessingPayment
import edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.vnpay.CreatePaymentRequest
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.AMOUNT
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.DATA
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.DESCRIPTION
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.EXTRA
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.FEE
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.MERCHANT_CODE
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.MERCHANT_NAME
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.MERCHANT_NAME_LABEL
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.MOMO_PARTNER_CODE
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.ORDER_ID
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.ORDER_LABEL
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.PARTNER_CODE
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.PHONE_NUMBER_V1
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.REQUEST_ID
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.STATUS
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters.USERNAME_V1
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.VnPayParameters.DOMAIN_VNPAY_SANDBOX
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.VnPayParameters.VNPAY_PAYMENT
import timber.log.Timber
import java.util.*
import java.util.Map
import java.util.stream.Collectors
import kotlin.collections.HashMap
import kotlin.collections.LinkedHashMap
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set


object MoMoParameters {
    const val CUSTOMER_NUMBER = "customerNumber"
    const val PARTNER_REF_ID = "partnerRefId"
    const val PARTNER_TRANS_ID = "partnerTransId"
    const val USERNAME = "userName"
    const val USERNAME_V1 = "username"
    const val PARTNER_NAME = "partnerName"
    const val STORE_ID = "storeId"
    const val STORE_NAME = "storeName"

    var PARTNER_CODE = "partnerCode"
    var ACCESS_KEY = "accessKey"
    var REQUEST_ID = "requestId"
    var AMOUNT = "amount"
    var ORDER_ID = "orderId"
    var ORDER_INFO = "orderInfo"
    var ORDER_LABEL = "orderLabel"
    var RETURN_URL = "returnUrl"
    var NOTIFY_URL = "notifyUrl"
    var REQUEST_TYPE = "requestType"
    var EXTRA_DATA = "extraData"
    var EXTRA = "extra"
    var BANK_CODE = "bankCode"
    var TRANS_ID = "transId"
    var PAY_TRANS_ID = "transid"
    var MESSAGE = "message"
    var LOCAL_MESSAGE = "localMessage"
    var DESCRIPTION = "description"
    var PAY_URL = "payUrl"
    var DEEP_LINK = "deeplink"
    var QR_CODE = "qrCode"
    var ERROR_CODE = "errorCode"
    var STATUS = "status"
    var PAY_TYPE = "payType"
    var TRANS_TYPE = "transType"
    var ORDER_TYPE = "orderType"
    var MOMO_TRANS_ID = "momoTransId"
    var PAYMENT_CODE = "paymentCode"
    var DATE = "responseTime"

    var VERSION = 2.0
    var APP_PAY_TYPE = 3

    // MERCHANT
    const val MERCHANT_NAME = "merchantname"
    const val MERCHANT_CODE = "merchantcode"
    const val MERCHANT_NAME_LABEL = "merchantnamelabel"
    const val FEE = "fee"

    const val DATA = "data"
    const val PHONE_NUMBER_V1 = "phonenumber"
    const val APP_DATA = "appData"
    const val RESPONSE_TIME = "responseTime"

    const val MOMO_PARTNER_NAME = "alothoforworkerapp"
    const val MOMO_PARTNER_CODE = "MOMOWVIA20220509"

}

object VnPayParameters {
    const val DOMAIN_VNPAY_SANDBOX = "http://sandbox.vnpayment.vn/"
    const val VNPAY_PAYMENT = "paymentv2/vpcpay.html"
}

object EPaymentUtil {
    private const val M_MERCHANT_NAME = "Công ty TNHH AloTho"

    /*  E-Wallet : MOMO
		https://developers.momo.vn/#/docs/app_in_app?id=x%e1%bb%ad-l%c3%bd-thanh-to%c3%a1n
	 */
    fun getMomoData(orderId: String, amount: Long): MutableMap<String, Any> {
        val eventValue: MutableMap<String, Any> = HashMap()
        //client Required
        eventValue[MERCHANT_NAME] = M_MERCHANT_NAME
        eventValue[MERCHANT_CODE] = MOMO_PARTNER_CODE
        eventValue[AMOUNT] = amount
        eventValue[ORDER_ID] = orderId
        eventValue[ORDER_LABEL] = "Mã giao dịch"
        eventValue[USERNAME_V1] = CommonUtils.currentUserUid!!

        //client Optional - bill info
        eventValue[MERCHANT_NAME_LABEL] = "Dịch vụ"
        eventValue[FEE] = 0
        eventValue[DESCRIPTION] = "Thanh toán bằng thẻ"

        //client extra data
        eventValue[REQUEST_ID] =
            MOMO_PARTNER_CODE + "merchant_billId_" + Calendar.getInstance().getTimeInMillis()
        eventValue[PARTNER_CODE] = MOMO_PARTNER_CODE
        eventValue[EXTRA] = ""
        return eventValue
    }

    @Throws(Exception::class)
    fun generateVnPaySecureHash(request: CreatePaymentRequest?): String? {
        val requestRawData: String = getQueryStringHash(request!!).orEmpty()
        Timber.d("rawData", requestRawData)
        //return Encoder.Sha256(BuildConfig.VNP_HASHSECRET.toString() + requestRawData)
        return Encoder.Sha256("" + requestRawData)
    }

    @Throws(Exception::class)
    fun generateMomoSignature(request: ProcessingPayment.Request?): String {
        val requestRawData: String = getQueryStringHash(request!!).orEmpty()
        Timber.d("rawData", requestRawData)
        return Encoder.signHmacSHA256(requestRawData, BuildConfig.MOMO_ACCESS_KEY)
    }


    @SuppressLint("NewApi")
    fun getQueryStringHash(o: Any): String? {
        val queryStringBuilder = java.lang.StringBuilder()
        val map: MutableMap<String, Any?> = HashMap()
        for (field in o.javaClass.declaredFields) {
            field.isAccessible = true
            map[field.name] = field[o]
        }

        // remove key-value pair if it's null
        map.entries.removeIf { (_, value): kotlin.collections.Map.Entry<String, Any?> -> value == null }
        val sortedMap = map.entries.stream()
            .sorted { (key): kotlin.collections.Map.Entry<String, Any?>, (key1): kotlin.collections.Map.Entry<String, Any?> ->
                key.substring(
                    4
                ).compareTo(key1.substring(4))
            }
            .collect(
                Collectors.toMap<kotlin.collections.Map.Entry<String, Any?>, String, Any, java.util.LinkedHashMap<String, Any>>(
                    { (key, value) -> Map.entry(key, value!!).key },
                    { (key, value) -> Map.entry(key, value!!).value },
                    { oldValue: Any, _: Any? -> oldValue },
                    { LinkedHashMap() })
            )

        sortedMap.forEach { (key, value) ->
            queryStringBuilder.append(key)
            queryStringBuilder.append("=")
            queryStringBuilder.append(value)
            queryStringBuilder.append("&")
        }

        val queryString = queryStringBuilder.toString()
        Timber.tag("QueryString").d(queryString.substring(0, queryString.length - 1))
        return queryString.substring(0, queryString.length - 1)
    }

    fun getPaymentUrl(request: CreatePaymentRequest?): String {
        return DOMAIN_VNPAY_SANDBOX + VNPAY_PAYMENT + "?" + getQueryStringHash(request!!)
    }

//    @Throws(
//        NoSuchAlgorithmException::class,
//        InvalidKeyException::class,
//        UnsupportedEncodingException::class
//    )
//    fun generateSignRequest(request: TransactionConfirmation.Request): String? {
//        val requestRawData: String = StringBuilder()
//            .append(PARTNER_CODE).append("=").append(request.partnerCode).append("&")
//            .append(PARTNER_REF_ID).append("=").append(request.partnerRefId).append("&")
//            .append(REQUEST_TYPE).append("=").append(request.requestType).append("&")
//            .append(REQUEST_ID).append("=").append(request.requestId).append("&")
//            .append(MOMO_TRANS_ID).append("=").append(request.momoTransId)
//            .toString()
//        return Encoder.signHmacSHA256(requestRawData, BuildConfig.MOMO_ACCESS_KEY)
//    }

//    @Throws(java.lang.Exception::class)
//    fun generateRSA(eventValue: kotlin.collections.Map<String?, Any?>): String? {
//        val rawData: MutableMap<String, Any?> = HashMap()
//        rawData[PARTNER_REF_ID] = eventValue[ORDER_ID]
//        rawData[PARTNER_CODE] = eventValue[PARTNER_CODE]
//        rawData[AMOUNT] = eventValue[AMOUNT]
//        rawData[PARTNER_NAME] = eventValue[PHONE_NUMBER_V1]
//        rawData[PARTNER_TRANS_ID] = eventValue[ORDER_ID]
//        val gson = Gson()
//        val jsonStr = gson.toJson(rawData)
//        Timber.d("jsonStr", jsonStr)
//        val testByte: ByteArray = jsonStr.toByteArray(StandardCharsets.UTF_8)
//        return Encoder.encryptRSA(testByte, BuildConfig.MOMO_PUBLIC_KEY)
//    }
}

data class MomoResponse(
    val status: Int = 0,
    val message: String? = null,
    val data: String? = null,
    val phoneNumber: String? = null
) {
    companion object {
        fun getResponse(data: Intent): MomoResponse {
            return MomoResponse(
                data.getIntExtra(STATUS, -1),
                data.getStringExtra(DATA),
                data.getStringExtra(PHONE_NUMBER_V1),
                data.getStringExtra(MESSAGE)
            )
        }
    }
}