package edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasoureimpl

import android.net.Uri
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.StorageDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasource.FileRemoteDataSource

class FileRemoteDataSourceImpl(
    private val uploadDataSource: StorageDataSource.Upload,
    private val downloadDataSource: StorageDataSource.Download
) : FileRemoteDataSource {

    override suspend fun getDownloadUrl(input: String): Resource<Uri> {
        return downloadDataSource.getDownloadUrl(input)
    }

    override suspend fun uploadFiles(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    ) {
        uploadDataSource.uploadFiles(folderPath, requestFiles, fileUploaderCallback)
    }


}