package edu.iuhuniversity.alothoforworkerapp.core.data.model.order

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Feedback(
    val rating: Double = 0.0,
    val review: String = ""
) : Parcelable, Serializable

