package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository

class ListenForChatMessageUseCase(private val messageRepository: MessageRepository) {
    suspend fun execute(conservationId: String) = messageRepository.listenForChatMessages(conservationId)
}