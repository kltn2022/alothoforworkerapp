package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.navigation

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import cafe.adriel.voyager.navigator.tab.Tab
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLightVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor

@Composable
fun RowScope.BottomNavigationItem(tab: Tab, onClick: (() -> Unit)? = null) {
    val bottomNavigator = LocalTabNavigator.current

    NavigationBarItem(
        selected = bottomNavigator.current == tab,
        onClick = {
            bottomNavigator.current = tab
            if (onClick != null) {
                onClick()
            }
        },
        icon = { Icon(painter = tab.options.icon!!, contentDescription = tab.options.title) },
        label = {
            Text(text = tab.options.title)
        },
        colors = NavigationBarItemDefaults.colors(
            indicatorColor = PrimaryLightVariantColor
        )
    )
}

@Preview
@Composable
fun AloThoWorkerLogo(
    modifier: Modifier = Modifier
) {
    Image(
        painter = painterResource(id = R.drawable.ic_logo_ab),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        colorFilter = ColorFilter.tint(PrimaryColor, blendMode = BlendMode.Color),
        modifier = Modifier
            .border(1.dp, PrimaryVariantColor, CircleShape)
            .clip(CircleShape)
            .then(modifier)
    )
}