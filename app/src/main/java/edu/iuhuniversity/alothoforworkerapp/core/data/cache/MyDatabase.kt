package edu.iuhuniversity.alothoforworkerapp.core.data.cache

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedorder.CachedOrder
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.model.cachedservice.CachedService
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification.FcmNotificationDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.order.OrderDao
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.service.ServiceDao
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification

/**
 * [MyDatabase] Sample
 * + Defines a database and specifies data tables that will be used.
 * + Version is incremented as new tables/columns are added/removed/changed.
 * + You can optionally use this class for one-time setup, such as pre-populating a database.
 */
@Database(
    entities = [CachedOrder::class, FcmNotification::class, CachedService::class],
    exportSchema = false,
    version = 1
)
abstract class MyDatabase : RoomDatabase() {
    // Declare dao class here
    abstract fun notificationDao(): FcmNotificationDao
    abstract fun orderDao(): OrderDao
    abstract fun serviceDao(): ServiceDao

    companion object {
        @Volatile
        private var INSTANCE: MyDatabase? = null
        private const val DATABASE_NAME = "db.alothoapp"

        fun getDatabase(context: Context): MyDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context,
                    MyDatabase::class.java,
                    DATABASE_NAME
                )
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance

                instance
            }
        }
    }
}