package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification

import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.FcmNotification
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository

class SaveNotificationUseCase(private val notificationRepository: NotificationRepository) {
    suspend fun execute(notification: FcmNotification) = notificationRepository.saveNotification(notification)
}