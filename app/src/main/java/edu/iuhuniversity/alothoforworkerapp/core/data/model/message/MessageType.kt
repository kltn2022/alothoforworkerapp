package edu.iuhuniversity.alothoforworkerapp.core.data.model.message

enum class MessageType {
    TEXT,
    IMAGE,
    FILE,
    VIDEO,
    STICKER
}