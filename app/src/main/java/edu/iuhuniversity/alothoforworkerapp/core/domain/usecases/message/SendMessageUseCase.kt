package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message

import edu.iuhuniversity.alothoforworkerapp.core.data.model.message.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository

class SendMessageUseCase(private val messageRepository: MessageRepository) {
    suspend fun execute(
        conservationId: String,
        content: String,
        messageType: MessageType
    ) = messageRepository.sendMessage(conservationId, content, messageType)
}