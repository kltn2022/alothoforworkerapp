package edu.iuhuniversity.alothoforworkerapp.core.data.model.transaction

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
enum class PaymentMethod(val title: String) : Parcelable, Serializable {
    CASH("Tiền mặt"),
    CREDIT_CARD ("Thẻ thanh toán");
}