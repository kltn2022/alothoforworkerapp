package edu.iuhuniversity.alothoforworkerapp.core.data.model.order

import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.utils.CustomTypeParcelable
import edu.iuhuniversity.alothoforworkerapp.core.utils.Serializer
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import timber.log.Timber
import java.io.Serializable
import java.util.*

@Parcelize
data class Order(
    val id: String,
    val type: OrderType,
    val createdDate: Date?,
    val workingDate: Date?,
    val finishedDate: Date?,
    val note: String,
    val movingFee: Double? = 0.0,
    val estimatedFee: Double? = 0.0,
    val overtimeFee: Double? = 0.0,
    val total: Double? = 0.0,
    @Transient val customer: @WriteWith<CustomTypeParcelable.DocumentReferenceParceler> DocumentReference,
    @Transient val worker: @WriteWith<CustomTypeParcelable.DocumentReferenceNullableParceler> DocumentReference?,
    val orderDetails: List<OrderDetail>,
    val status: OrderStatus?,
    val workingAddress: Address?,
    val feedback: Feedback? = null,
    val images: List<String>? = null
) : Parcelable, Serializable {
    companion object {
        fun DocumentSnapshot.toOrder(): Order? {
            return try {
                val orderDetailType = object : TypeToken<List<OrderDetail>>() {}.type
                val orderDetailList = Serializer().deserialize(
                    get("orderDetails").toString(),
                    orderDetailType
                ) as List<OrderDetail>

                val orderId = this.reference.id
                val type = OrderType.valueOf(getString("type").orEmpty())
                val createdDate = getDate("createdDate")
                val workingDate = getDate("workingDate")
                val finishedDate = getDate("finishedDate")
                val note = getString("note").orEmpty()
                val movingFee = getDouble("movingFee")
                val estimatedFee = getDouble("estimatedFee")
                val overtimeFee = getDouble("overtimeFee")
                val total = getDouble("total")
                val customer = getDocumentReference("customer")
                val worker = getDocumentReference("worker")
                val orderStatus = OrderStatus.valueOf(getString("status").orEmpty())
                val workingAddress = get("workingAddress", Address::class.java)
                val feedback = get("feedback", Feedback::class.java)
                val images = get("images", ArrayList::class.java) as List<String>?

                Order(
                    orderId,
                    type,
                    createdDate,
                    workingDate,
                    finishedDate,
                    note,
                    movingFee,
                    estimatedFee,
                    overtimeFee,
                    total,
                    customer!!,
                    worker,
                    orderDetailList,
                    orderStatus,
                    workingAddress,
                    feedback,
                    images
                )
            } catch (e: Exception) {
                Timber.d(e.message!!.toString())
                null
            }
        }
    }
}
