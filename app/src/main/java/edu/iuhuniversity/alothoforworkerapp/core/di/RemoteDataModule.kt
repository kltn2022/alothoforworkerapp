package edu.iuhuniversity.alothoforworkerapp.core.di

import android.app.Application
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.storage.FirebaseStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.TokenDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.TokenDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FirebaseFileDownloader
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FirebaseFileUploader
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.StorageDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasource.FileRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasoureimpl.FileRemoteDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasource.MessageRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasourceimpl.MessageRemoteDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasourceimpl.OrderRemoteDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasourceimpl.ServiceRemoteDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasource.UserRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasourceimpl.UserRemoteDataSourceImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasource.WorkerRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasourceimpl.WorkerRemoteDataSourceImpl
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteDataModule {

    @Singleton
    @Provides
    fun provideFirebaseFileUploaderDataSource(
        application: Application,
        firebaseStorage: FirebaseStorage
    ): StorageDataSource.Upload {
        return FirebaseFileUploader(application, firebaseStorage)
    }

    @Singleton
    @Provides
    fun provideFirebaseFileDownloaderDataSource(
        firebaseStorage: FirebaseStorage
    ): StorageDataSource.Download {
        return FirebaseFileDownloader(firebaseStorage)
    }

    @Singleton
    @Provides
    fun provideFileRemoteDataSource(
        uploadDataSource: StorageDataSource.Upload,
        downloadDataSource: StorageDataSource.Download
    ): FileRemoteDataSource {
        return FileRemoteDataSourceImpl(uploadDataSource, downloadDataSource)
    }

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(
        firebaseFirestore: FirebaseFirestore
    ): UserRemoteDataSource {
        return UserRemoteDataSourceImpl(firebaseFirestore)
    }

    @Singleton
    @Provides
    fun provideOrderRemoteDataSource(
        firebaseFirestore: FirebaseFirestore
    ): OrderDataSource.Remote = OrderRemoteDataSourceImpl(firebaseFirestore)

    @Singleton
    @Provides
    fun provideMessageRemoteDataSource(
        firebaseFirestore: FirebaseFirestore
    ): MessageRemoteDataSource = MessageRemoteDataSourceImpl(firebaseFirestore)

    @Singleton
    @Provides
    fun provideWorkerRemoteDataSource(
        firebaseFirestore: FirebaseFirestore
    ): WorkerRemoteDataSource = WorkerRemoteDataSourceImpl(firebaseFirestore)

    @Singleton
    @Provides
    fun provideServiceRemoteDataSource(
        firebaseFirestore: FirebaseFirestore
    ): ServiceDataSource.Remote = ServiceRemoteDataSourceImpl(firebaseFirestore)

    @Singleton
    @Provides
    fun provideTokenDataSource(
        firebaseFirestore: FirebaseFirestore,
        firebaseMessaging: FirebaseMessaging,
        firebaseAuth: FirebaseAuth
    ): TokenDataSource =
        TokenDataSourceImpl(firebaseFirestore, firebaseMessaging, firebaseAuth)
}
