package edu.iuhuniversity.alothoforworkerapp.core.data.model.order

import android.os.Parcelable
import android.text.TextUtils
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
enum class OrderType(val title: String) : Parcelable, Serializable {
    QUICK_CALL ("Gọi nhanh"),
    MAKE_AN_APPOINTMENT ("Đặt lịch");

    companion object {
        fun getOrderTypeByTitle(title: String?): OrderType? {
            if (!TextUtils.isEmpty(title)) {
                for (orderType in OrderType.values()) {
                    if (orderType.title == title) {
                        return orderType
                    }
                }
            }
            return null
        }
    }
}