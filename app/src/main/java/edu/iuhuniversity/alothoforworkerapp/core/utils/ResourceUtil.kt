package edu.iuhuniversity.alothoforworkerapp.core.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.compose.ui.graphics.Color
import androidx.core.content.res.ResourcesCompat
import edu.iuhuniversity.alothoforworkerapp.R
import timber.log.Timber

fun getDrawableByName(name: String, context: Context): Drawable? {
    val drawableResId = getDrawableIdByName(name, context)
    return ResourcesCompat.getDrawable(context.resources, drawableResId, context.theme)
}

fun getDrawableIdByName(name: String? = "", context: Context): Int {
    val drawableResId = context.resources.getIdentifier(name, "drawable", context.packageName)
    if (drawableResId == 0) {
        Timber.d("Can't find drawable with name: $name")
        return R.mipmap.ic_launcher_round
    }
    return drawableResId
}

fun Color.Companion.parse(colorString: String): Color =
    Color(color = android.graphics.Color.parseColor(colorString))