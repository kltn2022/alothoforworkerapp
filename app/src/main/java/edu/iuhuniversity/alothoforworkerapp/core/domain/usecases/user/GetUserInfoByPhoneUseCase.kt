package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user


import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository

class GetUserInfoByPhoneUseCase(private val userRepository: UserRepository) {
    suspend fun execute(phoneNumber: String): Resource<User?> {
        return userRepository.getUserByPhoneNumber(phoneNumber)
    }
}