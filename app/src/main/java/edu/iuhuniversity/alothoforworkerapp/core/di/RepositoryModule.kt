package edu.iuhuniversity.alothoforworkerapp.core.di

import android.app.Application
import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import edu.iuhuniversity.alothoforworkerapp.core.data.cache.notification.FcmNotificationDao
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.IFcmService
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.TokenDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.auth.AuthRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.FileRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.file.datasource.FileRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.MessageRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.message.datasource.MessageRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.NotificationRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.fcm.SendNotificationRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.notification.fcm.TokenRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.OrderRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.order.datasource.OrderDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.payment.PaymentRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.payment.PaymentService
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.ServiceRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.service.datasource.ServiceDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.transaction.TransactionRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.WorkerPreferencesRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.UserRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.user.datasource.UserRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.WorkerRepositoryImpl
import edu.iuhuniversity.alothoforworkerapp.core.data.repositories.worker.datasource.WorkerRemoteDataSource
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file.FileRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.message.MessageRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.NotificationRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.SendNotificationRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.notification.fcm.TokenRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.payment.PaymentRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service.ServiceRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.transaction.TransactionRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.UserRepository
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.worker.WorkerRepository
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {
    @Singleton
    @Provides
    fun provideFileRepository(
        fileRemoteDataSource: FileRemoteDataSource
    ): FileRepository {
        return FileRepositoryImpl(fileRemoteDataSource)
    }

    @Singleton
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore("user_preferences")

    @Singleton
    @Provides
    fun provideUserPreferenceRepository(
        app: Application
    ): WorkerPreferencesRepository = WorkerPreferencesRepositoryImpl(app.dataStore)

    @Singleton
    @Provides
    fun provideUserRepository(
        userRemoteDataSource: UserRemoteDataSource,
    ): UserRepository = UserRepositoryImpl(userRemoteDataSource)

    @Singleton
    @Provides
    fun provideOrderRepository(
        orderCacheDataSource: OrderDataSource.Cache,
        orderRemoteDataSource: OrderDataSource.Remote,
        //orderMapper: UiOrderMapper
    ): OrderRepository =
        OrderRepositoryImpl(orderCacheDataSource, orderRemoteDataSource)

    @Singleton
    @Provides
    fun provideNotificationRepository(
        notificationDao: FcmNotificationDao
    ): NotificationRepository = NotificationRepositoryImpl(notificationDao)

    @Singleton
    @Provides
    fun provideMessageRepository(
        messageRemoteDataSource: MessageRemoteDataSource,
        workerPreferencesRepository: WorkerPreferencesRepository,
        userRepository: UserRepository
    ): MessageRepository =
        MessageRepositoryImpl(messageRemoteDataSource, workerPreferencesRepository, userRepository)

    @Singleton
    @Provides
    fun provideWorkerRepository(
        workerPreferencesRepository: WorkerPreferencesRepository,
        workerRemoteDataSource: WorkerRemoteDataSource,
        firebaseAuth: FirebaseAuth
    ): WorkerRepository =
        WorkerRepositoryImpl(workerPreferencesRepository, workerRemoteDataSource, firebaseAuth)

    @Singleton
    @Provides
    fun provideAuthRepository(
        firebaseAuth: FirebaseAuth,
        workerRepository: WorkerRepository,
        workerPreferencesRepository: WorkerPreferencesRepository
    ): AuthRepository =
        AuthRepositoryImpl(firebaseAuth, workerRepository, workerPreferencesRepository)

    @Singleton
    @Provides
    fun provideServiceRepository(
        cache: ServiceDataSource.Cache,
        remote: ServiceDataSource.Remote
    ): ServiceRepository = ServiceRepositoryImpl(cache, remote)

    @Singleton
    @Provides
    fun provideTokenRepository(tokenDataSource: TokenDataSource): TokenRepository =
        TokenRepositoryImpl(tokenDataSource)

    @Singleton
    @Provides
    fun provideSendNotificationRepository(
        iFcmService: IFcmService
    ): SendNotificationRepository = SendNotificationRepositoryImpl(iFcmService)

    @Singleton
    @Provides
    fun providePaymentRepository(paymentService: PaymentService.Momo): PaymentRepository =
        PaymentRepositoryImpl(paymentService)

    @Singleton
    @Provides
    fun provideTransactionRepository(db: FirebaseFirestore): TransactionRepository =
        TransactionRepositoryImpl(db)
}
