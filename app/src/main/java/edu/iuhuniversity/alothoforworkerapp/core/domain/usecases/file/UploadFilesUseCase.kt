package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.file

import android.net.Uri
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.storage.FileUploaderCallback
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.file.FileRepository

class UploadFilesUseCase(private val fileRepository: FileRepository) {
    suspend fun execute(
        folderPath: String,
        requestFiles: List<Uri>,
        fileUploaderCallback: FileUploaderCallback
    ) {
        return fileRepository.uploadFiles(folderPath, requestFiles, fileUploaderCallback)
    }
}