package edu.iuhuniversity.alothoforworkerapp.core.constant

object CallWorkerConst {
    const val RECEIVED_CALL_WORKER_REQUEST_CODE = "received_call_worker_request"
    const val IS_FROM_RECEIVED_CALL_WORKER_REQUEST_CODE = "isFromReceiveCallWorkerRequest"
    const val CALL_WORKER_FEE = 10000.0
}

enum class NotificationType {
    SEND_CALL_WORKER_REQUEST,
    RECEIVE_CALL_WORKER_NOTIFICATION,
    NEW_MESSAGE_NOTIFICATION,
    NEW_AUDIO_CALL_NOTIFICATION;
}