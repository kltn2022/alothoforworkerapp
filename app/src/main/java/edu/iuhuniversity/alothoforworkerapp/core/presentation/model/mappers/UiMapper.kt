package edu.iuhuniversity.alothoforworkerapp.core.presentation.model.mappers

interface UiMapper<E, V> {
    suspend fun mapToView(input: E): V
    suspend fun mapToView(input: E, output: (V) -> Unit)
    suspend fun mapToView(input: List<E?>, output: (List<V>) -> Unit)
}