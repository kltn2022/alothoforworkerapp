package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val PrimaryColor = Color(0xFF0075ff)
val PrimaryVariantColor = Color(0xFF1a83ff)
val SecondaryColor = Color(0xFF005ecc)
val PrimaryLightVariantColor = Color(0xFF9ac8ff)
val PrimaryLighterVariantColor = Color(0xFFe7f2ff)

val Lian_Color_Foreground = android.graphics.Color.parseColor("#FF9823")
val Lian_Color_Background = android.graphics.Color.parseColor("#FFAF23")

val Lian_Color_Orange_Vivid = android.graphics.Color.parseColor("#FF711B")

val Lian_Color_Text = Color(0xFFA1AAB1)
val Lian_Color_Text_Ab = Color(0xFF62727D)
val Lian_Color_LightGray = Color(0xFFF6F6F6)
val Lian_Color_LighterGray = Color(0xFFE8E8E8)
val Lian_Color_DarkGray = Color(0xFFa5a5a5)
val Lian_Color_DarkerGray = Color(0xFF7a7a7c)
val Lian_Color_Error = Color(0xFFB00020)
val Lian_Color_Settings_Header = Color(0xFFADADAD)
val Lian_Color_Order_Detail = Color(0xFFF0F4F5)