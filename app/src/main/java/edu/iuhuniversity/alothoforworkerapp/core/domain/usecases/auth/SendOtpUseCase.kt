package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth

import android.app.Activity
import com.google.firebase.auth.PhoneAuthProvider
import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.auth.AuthRepository

class SendOtpUseCase(private val authRepository: AuthRepository) {
    suspend fun execute(
        activity: Activity,
        phoneNumber: String,
        resendingToken: PhoneAuthProvider.ForceResendingToken?,
        callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    ) = authRepository.sendOtp(activity, phoneNumber, resendingToken, callback)
}