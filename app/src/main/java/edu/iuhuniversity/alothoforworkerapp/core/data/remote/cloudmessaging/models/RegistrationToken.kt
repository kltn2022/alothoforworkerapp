package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models

import com.google.firebase.firestore.DocumentSnapshot

data class RegistrationToken(
    val uid: String? = null,
    val token: String,
) {
    companion object {
        fun DocumentSnapshot.toToken(): RegistrationToken {
            return RegistrationToken(
                uid = id,
                token = getString("token")!!
            )
        }
    }
}
