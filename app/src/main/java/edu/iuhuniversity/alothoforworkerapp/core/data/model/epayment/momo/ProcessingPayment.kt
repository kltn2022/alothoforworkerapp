package edu.iuhuniversity.alothoforworkerapp.core.data.model.epayment.momo

import androidx.annotation.Size
import edu.iuhuniversity.alothoforworkerapp.BuildConfig
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.EPaymentUtil
import edu.iuhuniversity.alothoforworkerapp.core.utils.ewallet.MoMoParameters
import java.util.*

sealed class ProcessingPayment {
    data class Request(
        val partnerCode: String,
        val partnerName: String? = null,
        val storeId: String? = null,
        val requestId: String,
        @Size(min = 1000, max = 50000000)
        val amount: Long,
        val orderId: String,
        val orderInfo: String,
        val orderGroupId: String? = null,
        val redirectUrl: String,
        val ipnUrl: String,
        val requestType: String = "captureWallet",
        val extraData: String,
        val autoCapture: Boolean? = true,
        val lang: String? = "vi",
        var signature: String,
        val accessKey: String? = BuildConfig.MOMO_ACCESS_KEY
    ) : ProcessingPayment() {
        companion object {
            fun createFrom(
                order: Order
            ): Request {
                val request = Request(
                    partnerCode = MoMoParameters.MOMO_PARTNER_CODE,
                    requestId = "${order.id}_${Date().time}",
                    amount = order.total?.toLong() ?: 1000,
                    orderId = order.id,
                    orderInfo = "",
                    redirectUrl = "https://thonow.page.link/successful-payment",
                    ipnUrl = "",
                    extraData = "",
                    signature = ""
                )
                val signature = EPaymentUtil.generateMomoSignature(request)
                request.signature = signature
                return request
            }
        }
    }

    data class Response(
        val partnerCode: String,
        val requestId: String,
        val orderId: String,
        val amount: Long,
        val responseTime: Long,
        val message: String,
        val resultCode: Int,
        val payUrl: String,
        val deeplink: String? = null,
        val qrCodeUrl: String? = null,
        val deeplinkMiniApp: String? = null,
    ): ProcessingPayment()

    data class Result(
        val partnerCode: String,
        val orderId: String,
        val requestId: String,
        val amount: Long,
        val orderInfo: String,
        val orderType: String,
        val transId: Long,
        val resultCode: Int,
        val message: String,
        val payType: String,
        val responseTime: Long,
        val extraData: String,
        val signature: String,
    )
}
