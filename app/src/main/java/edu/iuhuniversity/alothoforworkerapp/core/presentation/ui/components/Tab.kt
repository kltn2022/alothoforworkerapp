package edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.UiComposable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.navigator.tab.LocalTabNavigator
import cafe.adriel.voyager.navigator.tab.Tab
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.SecondaryColor
import timber.log.Timber

@Composable
fun AloThoTab(
    tabItems: @Composable () -> Unit
) {
    val selectedTabIndex = LocalTabNavigator.current.current.options.index.toInt()

    val indicator = @Composable { tabPositions: List<TabPosition> ->
        Timber.d(tabPositions.toString())
        FancyAnimatedIndicator(tabPositions = tabPositions, selectedTabIndex = selectedTabIndex)
    }

    TabNavigation(indicator = indicator, selectedTabIndex = selectedTabIndex, content = tabItems)
}

@Composable
fun TabNavigation(
    modifier: Modifier = Modifier,
    backgroundColor: Color = androidx.compose.material.MaterialTheme.colors.primarySurface,
    selectedTabIndex: Int,
    contentColor: Color = contentColorFor(backgroundColor),
    indicator: @Composable (List<TabPosition>) -> Unit,
    content: @Composable @UiComposable () -> Unit,
) {

    Surface(
        color = backgroundColor,
        contentColor = contentColor,
        modifier = modifier
    ) {
        TabRow(
            tabs = content,
            indicator = indicator,
            selectedTabIndex = selectedTabIndex,
            backgroundColor = Color.White
        )
    }

}


@Composable
fun TabItem(tab: Tab) {
    val tabNavigator = LocalTabNavigator.current

    FancyTab(
        title = tab.options.title,
        icon = tab.options.icon!!,
        selected = tabNavigator.current == tab,
        onClick = { tabNavigator.current = tab }
    )
}


@Composable
fun FancyAnimatedIndicator(tabPositions: List<TabPosition>, selectedTabIndex: Int) {
    // val currentTab = LocalTabNavigator.current.current

    val colors = listOf(SecondaryColor, SecondaryColor)
    val transition = updateTransition(selectedTabIndex, label = "")
    val indicatorStart by transition.animateDp(
        transitionSpec = {
            // Handle directionality here, if we are moving to the right, we
            // want the right side of the indicator to move faster, if we are
            // moving to the left, we want the left side to move faster.
            if (initialState < targetState) {
                spring(dampingRatio = 1f, stiffness = 50f)
            } else {
                spring(dampingRatio = 1f, stiffness = 1000f)
            }
        }, label = ""
    ) {
        tabPositions[it].left
    }

    val indicatorEnd by transition.animateDp(
        transitionSpec = {
            // Handle directionality here, if we are moving to the right, we
            // want the right side of the indicator to move faster, if we are
            // moving to the left, we want the left side to move faster.
            if (initialState < targetState) {
                spring(dampingRatio = 1f, stiffness = 1000f)
            } else {
                spring(dampingRatio = 1f, stiffness = 50f)
            }
        }, label = ""
    ) {
        tabPositions[it].right
    }

    val indicatorColor by transition.animateColor(label = "") {
        colors[it % colors.size]
    }

    FancyIndicator(
        indicatorColor,
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(align = Alignment.BottomStart)
            .offset(x = indicatorStart)
            .width(indicatorEnd - indicatorStart)
    )
}

@Composable
fun FancyIndicator(color: Color, modifier: Modifier) {
    Box(
        modifier
            .height(2.dp)
            .background(color)
            .then(modifier)
    )
}

@Composable
fun FancyTab(
    title: String,
    icon: Painter,
    selected: Boolean,
    onClick: () -> Unit
) {
    Tab(selected, onClick) {
        Column(
            Modifier
                .padding(10.dp)
                .height(50.dp)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.SpaceBetween
        ) {
            Icon(
                painter = icon,
                contentDescription = null,
                modifier = Modifier
                    .size(24.dp)
                    .align(Alignment.CenterHorizontally),
                tint = SecondaryColor,
            )
            Text(
                text = title,
                style = MaterialTheme.typography.bodyLarge,
                fontWeight = if (selected) FontWeight.SemiBold else FontWeight.Normal,
                color = SecondaryColor,
                modifier = Modifier.align(Alignment.CenterHorizontally)
            )
        }
    }
}