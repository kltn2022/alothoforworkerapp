package edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging

import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmCreateNotificationGroup
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmGroupMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.request.FcmMessagePayload
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmNotificationGroupResponse
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.cloudmessaging.models.response.FcmResponse
import retrofit2.Response

/**
 * https://firebase.google.com/docs/cloud-messaging/android/send-multiple
 */
interface FirebaseSendMessageService {
    // Type 3: Send downstream message to a specific device(s)
    suspend fun sendMessage(payload: FcmMessagePayload<String>) : Response<FcmResponse>

    // Type 2: Sending downstream messages to device groups (https://firebase.google
    // .com/docs/cloud-messaging/android/device-group#sending_downstream_messages_to_device_groups)
    suspend fun <T> sendGroupMessage(payload: FcmGroupMessagePayload<T>) : Response<FcmResponse>

    // Type 2: Device group messaging (Manage Managing device groups on the app server
    // https://firebase.google.com/docs/cloud-messaging/android/device-group#managing-device
    // -groups-on-the-app-server)
    suspend fun createDeviceGroup(payload: FcmCreateNotificationGroup) : Response<FcmNotificationGroupResponse>

    suspend fun retrievingNotificationKey(notificationKeyName: String) : Response<FcmNotificationGroupResponse>

    // Type 3: Topic messaging (https://firebase.google
    // .com/docs/cloud-messaging/android/topic-messaging#subscribe_the_client_app_to_a_topic)
    suspend fun subscribeToTopic(topicName: String): Resource<Boolean>
}