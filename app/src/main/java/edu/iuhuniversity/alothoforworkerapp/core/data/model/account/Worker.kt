package edu.iuhuniversity.alothoforworkerapp.core.data.model.account

import android.os.Parcelable
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.CustomTypeParcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.io.Serializable

@Parcelize
data class Worker(
    val id: String,
    val displayName: String,
    val email: String,
    val phoneNumber: String,
    val photoUrl: String?,
    val promoCode: String?,
    val rate: Float? = 0f,
    val online: Boolean? = false,
    val isActive: Boolean? = false,
    val balanceMoney: Double = 0.0,
    val serviceTypeList: @WriteWith<CustomTypeParcelable.DocumentReferenceNullableListParceler> List<DocumentReference>? = null
) : Parcelable, Serializable {
    companion object {
        fun convertFrom(firebaseUser: FirebaseUser?): Worker? {
            if (firebaseUser != null) {

                return Worker(
                    id = firebaseUser.uid,
                    displayName = firebaseUser.displayName.orEmpty(),
                    email = firebaseUser.email.orEmpty(),
                    phoneNumber = firebaseUser.phoneNumber.orEmpty(),
                    photoUrl = firebaseUser.photoUrl.takeIf { it != null }.toString(),
                    promoCode = "",
                    rate = 0f,
                    online = true,
                )
            }
            return null
        }

        fun DocumentSnapshot.toWorker(): Worker {
            return Worker(
                id = getString("id").orEmpty(),
                displayName = getString("displayName").orEmpty(),
                email = getString("email").orEmpty(),
                phoneNumber = getString("phoneNumber").orEmpty(),
                photoUrl = getString("photoUrl").orEmpty(),
                promoCode = getString("promoCode").orEmpty(),
                rate = getDouble("rate")?.toFloat(),
                online = getBoolean("online"),
                isActive = getBoolean("isActive"),
                balanceMoney = getDouble("balanceMoney").takeIf { it != null } ?: 0.0,
                serviceTypeList = listOf(
                    CommonUtils.getDocumentReference(
                        CollectionConst.COLLECTION_SERVICE_TYPE,
                        "1"
                    )
                )
            )
        }
    }
}
