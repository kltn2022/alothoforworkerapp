package edu.iuhuniversity.alothoforworkerapp.core.data.model.service

import android.os.Parcelable
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.DocumentSnapshot
import edu.iuhuniversity.alothoforworkerapp.core.utils.CustomTypeParcelable
import kotlinx.parcelize.Parcelize
import kotlinx.parcelize.WriteWith
import java.io.Serializable

@Parcelize
data class Service(
    val id: String,
    val name: String,
    val unit: String,
    val price: Double? = 0.0,
    val warrantyPeriod: Int,
    @Transient val serviceType: @WriteWith<CustomTypeParcelable.DocumentReferenceParceler> DocumentReference
) : Parcelable, Serializable {
    companion object {
        fun DocumentSnapshot.toService(): Service {
            return Service(
                id = getString("id") ?: this.id,
                name = getString("name").orEmpty(),
                unit = getString("unit").orEmpty(),
                price = getDouble("price") ?: 0.0,
                warrantyPeriod = getLong("warrantyPeriod")?.toInt() ?: 2,
                serviceType = getDocumentReference("serviceType")!!
            )
        }
    }
}
