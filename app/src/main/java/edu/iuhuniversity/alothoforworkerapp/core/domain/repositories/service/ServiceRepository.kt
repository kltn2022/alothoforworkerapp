package edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.service

import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service

interface ServiceRepository {
    suspend fun getServicesByServiceType(serviceTypeId: String): Resource<MutableList<Service>>
    suspend fun getService(serviceId: String): Resource<Service?>
}