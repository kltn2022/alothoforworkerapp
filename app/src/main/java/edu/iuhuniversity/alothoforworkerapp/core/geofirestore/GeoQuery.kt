package edu.iuhuniversity.alothoforworkerapp.core.geofirestore

import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.firestore.*
import com.google.firebase.firestore.DocumentChange.Type.*
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.core.GeoHash
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.core.GeoHashQuery
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.util.GeoUtils
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.*

/**
 * A GeoQuery object can be used for geo queries in a given circle. The GeoQuery class is thread safe.
 */
class GeoQuery internal constructor(
    private val geoFire: GeoFire,
    private var center: GeoLocation,
    radius: Double
) {
    private var removeListerRegistration: ListenerRegistration? = null

    private class LocationInfo(
        val location: GeoLocation,
        val inGeoQuery: Boolean,
        documentSnapshot: DocumentSnapshot
    ) {
        val geoHash: GeoHash
        val documentSnapshot: DocumentSnapshot

        init {
            geoHash = GeoHash(location)
            this.documentSnapshot = documentSnapshot
        }
    }

    private val queryListener =
        EventListener<QuerySnapshot> { queryDocumentSnapshots, e ->
            for (dc in queryDocumentSnapshots!!.documentChanges) {
                when (dc.type) {
                    ADDED -> childAdded(dc.document)
                    MODIFIED -> childChanged(dc.document)
                    REMOVED -> childRemoved(dc.document)
                }
            }
        }
    private val eventListeners: MutableSet<GeoQueryDataEventListener> = HashSet()
    private val firebaseQueries: MutableMap<GeoHashQuery, ListenerRegistration> = HashMap()
    private val outstandingQueries: MutableSet<GeoHashQuery> = HashSet()
    private val locationInfos: MutableMap<String, LocationInfo> = HashMap()
    private var radius: Double
    private var queries: Set<GeoHashQuery>? = null
    private fun locationIsInQuery(location: GeoLocation): Boolean {
        return GeoUtils.distance(location, center) <= radius
    }

    private fun updateLocationInfo(documentSnapshot: DocumentSnapshot, location: GeoLocation) {
        val key = documentSnapshot.id
        val oldInfo = locationInfos[key]
        val isNew = oldInfo == null
        val changedLocation = oldInfo != null && !oldInfo.location.equals(location)
        val wasInQuery = oldInfo != null && oldInfo.inGeoQuery
        val isInQuery = locationIsInQuery(location)
        if ((isNew || !wasInQuery) && isInQuery) {
            for (listener in eventListeners) {
                geoFire.raiseEvent {
                    listener.onDataEntered(
                        documentSnapshot,
                        location
                    )
                }
            }
        } else if (!isNew && isInQuery) {
            for (listener in eventListeners) {
                geoFire.raiseEvent {
                    if (changedLocation) {
                        listener.onDataMoved(documentSnapshot, location)
                    }
                    listener.onDataChanged(documentSnapshot, location)
                }
            }
        } else if (wasInQuery && !isInQuery) {
            for (listener in eventListeners) {
                geoFire.raiseEvent { listener.onDataExited(documentSnapshot) }
            }
        }
        val newInfo = LocationInfo(
            location,
            locationIsInQuery(location), documentSnapshot
        )
        locationInfos[key] = newInfo
    }

    private fun geoHashQueriesContainGeoHash(geoHash: GeoHash): Boolean {
        if (queries == null) {
            return false
        }
        for (query in queries!!) {
            if (query.containsGeoHash(geoHash)) {
                return true
            }
        }
        return false
    }

    private fun reset() {
        for ((_, value) in firebaseQueries) {
            value.remove()
        }
        outstandingQueries.clear()
        firebaseQueries.clear()
        queries = null
        locationInfos.clear()
    }

    private fun hasListeners(): Boolean {
        return !eventListeners.isEmpty()
    }

    private fun canFireReady(): Boolean {
        return outstandingQueries.isEmpty()
    }

    private fun checkAndFireReady() {
        if (canFireReady()) {
            for (listener in eventListeners) {
                geoFire.raiseEvent(listener::onGeoQueryReady)
            }
        }
    }

    private fun addValueToReadyListener(firebase: Query, query: GeoHashQuery) {
        firebase.get().addOnCompleteListener { task ->
            if (task.isCanceled) {
                synchronized(this@GeoQuery) {
                    for (listener in eventListeners) {
                        geoFire.raiseEvent {
                            listener.onGeoQueryError(
                                task.exception
                            )
                        }
                    }
                }
            } else {
                synchronized(this@GeoQuery) {
                    outstandingQueries.remove(query)
                    checkAndFireReady()
                }
            }
        }
    }

    private fun setupQueries() {
        val oldQueries: Set<GeoHashQuery> = if (queries == null) HashSet() else queries!!
        val newQueries: Set<GeoHashQuery> = GeoHashQuery.queriesAtLocation(
            center, radius
        )
        queries = newQueries
        for (query in oldQueries) {
            if (!newQueries.contains(query)) {
                firebaseQueries[query]!!.remove()
                firebaseQueries.remove(query)
                outstandingQueries.remove(query)
            }
        }
        for (query in newQueries) {
            if (!oldQueries.contains(query)) {
                outstandingQueries.add(query)
                val collectionReference = geoFire.collectionReference
                val filterQuery = geoFire.getQuery()
                val firebaseQuery =
                    (filterQuery ?: collectionReference).orderBy("g").startAt(query.startValue)
                        .endAt(query.endValue)
                val registration = firebaseQuery.addSnapshotListener { queryDocumentSnapshots, e ->
                    try {
                        if (e != null) {
                            throw e
                        }
                        for (dc in queryDocumentSnapshots!!.documentChanges) {
                            when (dc.type) {
                                ADDED -> childAdded(dc.document)
                                MODIFIED -> childChanged(dc.document)
                                REMOVED -> childRemoved(dc.document)
                            }
                        }
                    } catch (exception: Exception) {
                        exception.printStackTrace()
                    }
                }
                addValueToReadyListener(firebaseQuery, query)
                firebaseQueries[query] = registration
            }
        }

        for ((_, oldLocationInfo) in locationInfos) {
            updateLocationInfo(oldLocationInfo.documentSnapshot, oldLocationInfo.location)
        }
        // remove locations that are not part of the geo query anymore
        val it: MutableIterator<Map.Entry<String, LocationInfo>> = locationInfos.entries.iterator()
        while (it.hasNext()) {
            val (_, value) = it.next()
            if (!geoHashQueriesContainGeoHash(value.geoHash)) {
                it.remove()
            }
        }
        checkAndFireReady()
    }

    private fun setupQueriesForSingleValueEvent(listener: GeoQueryValueEventListener) {
        val newQueries: Set<GeoHashQuery> = GeoHashQuery.queriesAtLocation(
            center,
            radius
        )
        val taskCompletionSourceList: MutableList<TaskCompletionSource<QuerySnapshot>> = ArrayList()
        val result: MutableList<DocumentChange> = ArrayList()
        for (query in newQueries) {
            val collectionReference = geoFire.collectionReference
            val filterQuery = geoFire.getQuery()
            val firebaseQuery = (filterQuery ?: collectionReference)
                .orderBy("g").startAt(query.startValue).endAt(query.endValue)
            val completionSource = TaskCompletionSource<QuerySnapshot>()
            firebaseQuery.get()
                .addOnCompleteListener { task ->
                    val querySnapshot = task.result
                    for (change in querySnapshot.documentChanges) {
                        val destination =
                            change.document.data["l"] as GeoPoint?
                        if (locationIsInQuery(
                                GeoLocation(
                                    destination!!.latitude,
                                    destination.longitude
                                )
                            )
                        ) {
                            result.add(change)
                        }
                    }
                    completionSource.setResult(querySnapshot)
                }
            taskCompletionSourceList.add(completionSource)
        }
        val tasks: MutableList<Task<QuerySnapshot>> = ArrayList()
        for (task in taskCompletionSourceList) {
            tasks.add(task.task)
        }
        Tasks.whenAll(tasks).addOnCompleteListener { task: Task<Void?>? ->
            listener.onDocumentChange(
                result
            )
        }
    }

    private fun childAdded(documentSnapshot: DocumentSnapshot) {
        val location = GeoFire.getLocationValue(documentSnapshot)
        if (location != null) {
            updateLocationInfo(
                documentSnapshot,
                location
            )
        } else {
            // throw an error in future?
        }
    }

    private fun childChanged(documentSnapshot: DocumentSnapshot) {
        val location = GeoFire.getLocationValue(documentSnapshot)
        if (location != null) {
            updateLocationInfo(documentSnapshot, location)
        } else {
            // throw an error in future?
        }
    }

    private fun childRemoved(documentSnapshot: DocumentSnapshot) {
        val key = documentSnapshot.id
        val info = locationInfos[key]
        if (info != null) {
            removeListerRegistration = geoFire.collectionReference.document(key)
                .addSnapshotListener { documentSnapshot1: DocumentSnapshot?, _: FirebaseFirestoreException? ->
                    removeListerRegistration!!.remove()
                    synchronized(this@GeoQuery) {
                        val info1 = locationInfos[key]
                        locationInfos.remove(key)
                        for (listener in eventListeners) {
                            geoFire.raiseEvent {
                                listener.onDataExited(
                                    info1!!.documentSnapshot
                                )
                            }
                        }
                    }
                }
        }
    }

    /**
     * Adds a new GeoQueryEventListener to this GeoQuery.
     *
     * @throws IllegalArgumentException If this listener was already added
     *
     * @param listener The listener to add
     */
    @Synchronized
    fun addGeoQueryEventListener(listener: GeoQueryEventListener?) {
        listener?.let { EventListenerBridge(it) }?.let { addGeoQueryDataEventListener(it) }
    }

    /**
     * Adds a single listener to this GeoQuery. The callback will only be fired once. There is no need
     * to remove this listener after usage
     *
     * @param listener The listener to add
     */
    @Synchronized
    fun addGeoQueryDataForSingleValueEvent(listener: GeoQueryDataValueEventListener) {
        val newQueries: Set<GeoHashQuery> = GeoHashQuery.queriesAtLocation(
            center,
            radius
        )
        val taskCompletionSourceList: MutableList<TaskCompletionSource<QuerySnapshot>> = ArrayList()
        val result: MutableList<GeoQueryDocumentChange> = ArrayList()
        for (query in newQueries) {
            val collectionReference = geoFire.collectionReference
            val filterQuery = geoFire.getQuery()
            val firebaseQuery = (filterQuery ?: collectionReference)
                .orderBy("g").startAt(query.startValue).endAt(query.endValue)
            val completionSource = TaskCompletionSource<QuerySnapshot>()
            firebaseQuery.get()
                .addOnCompleteListener { task ->
                    val querySnapshot = task.result
                    for (dc in querySnapshot.documentChanges) {
                        val destination = dc.document.data["l"] as GeoPoint?
                        if (locationIsInQuery(
                                GeoLocation(
                                    destination!!.latitude,
                                    destination.longitude
                                )
                            )
                        ) {
                            result.add(GeoQueryDocumentChange(dc.document, destination))
                        }
                    }
                    completionSource.setResult(querySnapshot)
                }
            taskCompletionSourceList.add(completionSource)
        }
        val tasks: MutableList<Task<QuerySnapshot>> = ArrayList()
        for (task in taskCompletionSourceList) {
            tasks.add(task.task)
        }
        Tasks.whenAll(tasks).addOnCompleteListener { task: Task<Void?>? ->
            listener.onDocumentChange(
                result
            )
        }
    }

    /**
     * Adds a new GeoQueryEventListener to this GeoQuery.
     *
     * @throws IllegalArgumentException If this listener was already added
     *
     * @param listener The listener to add
     */
    @Synchronized
    fun addGeoQueryDataEventListener(listener: GeoQueryDataEventListener) {
        require(!eventListeners.contains(listener)) { "Added the same listener twice to a GeoQuery!" }
        eventListeners.add(listener)
        if (queries == null) {
            setupQueries()
        } else {
            for ((_, info) in locationInfos) {
                if (info.inGeoQuery) {
                    geoFire.raiseEvent {
                        listener.onDataEntered(
                            info.documentSnapshot,
                            info.location
                        )
                    }
                }
            }
            if (canFireReady()) {
                geoFire.raiseEvent(listener::onGeoQueryReady)
            }
        }
    }

    /**
     * Adds a single listener to this GeoQuery. The callback will only be fired once. There is no need
     * to remove this listener after usage
     *
     * @param listener The listener to add
     */
    @Synchronized
    fun addGeoQueryForSingleValueEvent(listener: GeoQueryValueEventListener) {
        setupQueriesForSingleValueEvent(listener)
    }

    /**
     * Removes an event listener.
     *
     * @param listener The listener to remove
     * @throws IllegalArgumentException If the listener was removed already or never added
     */
    @Synchronized
    fun removeGeoQueryEventListener(listener: GeoQueryEventListener?) {
        removeGeoQueryEventListener(EventListenerBridge(listener!!))
    }

    /**
     * Removes an event listener.
     *
     * @throws IllegalArgumentException If the listener was removed already or never added
     *
     * @param listener The listener to remove
     */
    @Synchronized
    fun removeGeoQueryEventListener(listener: GeoQueryDataEventListener) {
        require(eventListeners.contains(listener)) { "Trying to remove listener that was removed or not added!" }
        eventListeners.remove(listener)
        if (!hasListeners()) {
            reset()
        }
    }

    /**
     * Removes all event listeners from this GeoQuery.
     */
    @Synchronized
    fun removeAllListeners() {
        eventListeners.clear()
        reset()
    }

    /**
     * Returns the current center of this query.
     * @return The current center
     */
    @Synchronized
    fun getCenter(): GeoLocation {
        return center
    }

    /**
     * Sets the new center of this query and triggers new events if necessary.
     * @param center The new center
     */
    @Synchronized
    fun setCenter(center: GeoLocation) {
        this.center = center
        if (hasListeners()) {
            setupQueries()
        }
    }

    /**
     * Returns the radius of the query, in kilometers.
     * @return The radius of this query, in kilometers
     */
    @Synchronized
    fun getRadius(): Double {
        // convert from meters
        return radius / 1000
    }

    /**
     * Sets the radius of this query, in kilometers, and triggers new events if necessary.
     * @param radius The new radius value of this query in kilometers
     */
    @Synchronized
    fun setRadius(radius: Double) {
        // convert to meters
        this.radius = radius * 1000
        if (hasListeners()) {
            setupQueries()
        }
    }

    /**
     * Sets the center and radius (in kilometers) of this query, and triggers new events if necessary.
     * @param center The new center
     * @param radius The new radius value of this query in kilometers
     */
    @Synchronized
    fun setLocation(center: GeoLocation, radius: Double) {
        this.center = center
        // convert radius to meters
        this.radius = radius * 1000
        if (hasListeners()) {
            setupQueries()
        }
    }

    companion object {
        private const val KILOMETER_TO_METER = 1000
    }

    /**
     * Creates a new GeoQuery object centered at the given location and with the given radius.
     * @param geoFire The GeoFire object this GeoQuery uses
     * @param center The center of this query
     * @param radius The radius of the query, in kilometers. The maximum radius that is
     * supported is about 8587km. If a radius bigger than this is passed we'll cap it.
     */
    init {
        // convert from kilometers to meters
        this.radius = radius * KILOMETER_TO_METER // Convert from kilometers to meters.
    }
}
