package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.user.WorkerPreferencesRepository

class GetAuthTokenUseCase(private val workerPreferencesRepository: WorkerPreferencesRepository) {
    fun execute() = workerPreferencesRepository.authToken
}