package edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order

import edu.iuhuniversity.alothoforworkerapp.core.domain.repositories.order.OrderRepository

class GetOrderByIdUseCase(private val orderRepository: OrderRepository) {
    suspend fun execute(id: String) = orderRepository.getOrder(id)
}