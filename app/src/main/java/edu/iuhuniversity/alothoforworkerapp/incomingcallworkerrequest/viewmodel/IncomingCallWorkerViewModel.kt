package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.viewmodel

import android.app.Application
import android.os.Build
import android.os.CountDownTimer
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.navigator.Navigator
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.GetRegistrationTokenUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.SendNotificationToCustomerUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.GetOrderByIdUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.UiCallWorkerRequest
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.mappers.UiCallWorkerRequestMapper
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit

class IncomingCallWorkerViewModel @AssistedInject constructor(
    @Assisted private val app: Application,
    @Assisted private val getOrderByIdUseCase: GetOrderByIdUseCase,
    @Assisted private val updateOrderUseCase: UpdateOrderUseCase,
    @Assisted private val getRegistrationTokenUseCase: GetRegistrationTokenUseCase,
    @Assisted private val sendNotificationToCustomer: SendNotificationToCustomerUseCase,
    @Assisted private val dispatchersProvider: DispatchersProvider,
    @Assisted private val uiCallWorkerRequestMapper: UiCallWorkerRequestMapper,
) : StateScreenModel<IncomingCallWorkerViewModel.State>(State.Loading) {

    private lateinit var currentOrder: MutableState<Order>

    private var countDownTimer: CountDownTimer? = null
    private val RECEIVE_REQUEST_TOTAL_TIME = TimeUnit.MINUTES.toMillis(1)

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            getOrderByIdUseCase: GetOrderByIdUseCase,
            updateOrderUseCase: UpdateOrderUseCase,
            getRegistrationTokenUseCase: GetRegistrationTokenUseCase,
            sendNotificationToCustomer: SendNotificationToCustomerUseCase,
            dispatchersProvider: DispatchersProvider,
            uiCallWorkerRequestMapper: UiCallWorkerRequestMapper,
        ): IncomingCallWorkerViewModel
    }

    sealed class State {
        object Loading : State()
        data class Loaded(val uiCallWorkerRequest: UiCallWorkerRequest) : State()
        object AcceptedOrder : State()
        object RejectedOrder : State()
        data class Error(val error: String) : State()
        data class CountingDownTime(val timeRemaining: Long) : State()
    }

    fun handleEvent(event: IncomingCallWorkerEvent) {
        when (event) {
            is IncomingCallWorkerEvent.Loading -> loadOrderInformation(event.orderId)
            is IncomingCallWorkerEvent.AcceptOrder -> {
                doAcceptOrder(currentOrder.value.id)
            }
            is IncomingCallWorkerEvent.RejectOrder -> doRejectedOrder()
            is IncomingCallWorkerEvent.StartCountDown -> startCountdown()
        }
    }

    private fun loadOrderInformation(orderId: String) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {

                        when (val result = getOrderByIdUseCase.execute(orderId)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                if (!this@IncomingCallWorkerViewModel::currentOrder.isInitialized) {
                                    currentOrder = mutableStateOf(result.data!!)
                                } else {
                                    currentOrder.value = result.data!!
                                }

                                uiCallWorkerRequestMapper.mapToView(result.data) {
                                    mutableState.value = State.Loaded(it)
                                }
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun doAcceptOrder(orderId: String) {
        // kiểm tra xem order đó đã có người accept chưa
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = getOrderByIdUseCase.execute(orderId)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                val order = result.data
                                if (order != null) {
                                    //check xem người dùng có huỷ đơn tức thời không
                                    if (order.status != null && order.status == OrderStatus.CANCELED) {
                                        mutableState.value =
                                            State.Error("Rất tiếc! Người dùng đã huỷ yêu cầu này.")
                                    } else {
                                        if (order.worker == null) { // chưa có thợ nhận
                                            acceptOrder(
                                                order.copy(
                                                    worker = CommonUtils.getCurrentUserRef()!!,
                                                    status = order.status?.nextStatus()
                                                )
                                            )
                                        } else {
                                            mutableState.value =
                                                State.Error("Rất tiếc! Đã có thợ nhận công việc này. Hãy nhanh tay hơn nhé")
                                        }
                                    }
                                } else {
                                    mutableState.value =
                                        State.Error("Dường như có lỗi xảy ra. Xin thử lại!")
                                }
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun acceptOrder(order: Order) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = updateOrderUseCase.execute(order)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                sendNotificationToCustomer(order)
                                mutableState.value = State.AcceptedOrder
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun doRejectedOrder() {
        mutableState.value = State.RejectedOrder
    }

    private fun sendNotificationToCustomer(order: Order) {
        val errorMessage = "Có lỗi xảy ra!"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = getRegistrationTokenUseCase.execute(order.customer.id)) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> {
                                // send notification to user
                                when (val resp =
                                    sendNotificationToCustomer.execute(result.data?.token, order)) {
                                    is Resource.Loading -> mutableState.value = State.Loading
                                    is Resource.Error -> mutableState.value =
                                        State.Error(result.message.orEmpty())
                                    is Resource.Success -> {
                                        if (resp.data?.success!! != 0) {
                                            // sendNotificationToCustomer(order)
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }

    private fun startCountdown() {
        countDownTimer = object : CountDownTimer(RECEIVE_REQUEST_TOTAL_TIME, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mutableState.value = State.CountingDownTime(TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished))
            }

            override fun onFinish() {
                mutableState.value = State.CountingDownTime(0L)
                countDownTimer?.cancel()
            }
        }.start()
    }

    fun back() {
        countDownTimer?.cancel()
    }
}

sealed class IncomingCallWorkerEvent {
    data class Loading(val orderId: String) : IncomingCallWorkerEvent()
    object AcceptOrder : IncomingCallWorkerEvent()
    object RejectOrder : IncomingCallWorkerEvent()
    object StartCountDown : IncomingCallWorkerEvent()
}