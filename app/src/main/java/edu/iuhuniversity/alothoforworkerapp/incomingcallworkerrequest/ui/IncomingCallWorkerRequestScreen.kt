package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.ui

import android.app.Application
import android.os.CountDownTimer
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ReceiptLong
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.gradientBackground
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.CurrencyUtils
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.core.utils.toDateFormat
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.viewmodel.IncomingCallWorkerEvent
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.viewmodel.IncomingCallWorkerViewModel
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.UiCallWorkerRequest
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.mappers.UiCallWorkerRequestMapper

data class IncomingCallWorkerRequestScreen(
    private val orderId: String
) : AndroidScreen() {

    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow
        val currentContext = LocalContext.current
        val incomingCallWorkerRequestActivity =
            (currentContext.getComponentActivity() as IncomingCallWorkerRequestActivity)
        val getOrderByIdUseCase =
            incomingCallWorkerRequestActivity.getOrderByIdUseCase
        val updateOrdersUseCase =
            incomingCallWorkerRequestActivity.updateOrderUseCase
        val dispatchersProvider =
            incomingCallWorkerRequestActivity.dispatchersProvider
        val orderMapper =
            incomingCallWorkerRequestActivity.orderMapper
        val getRegistrationTokenUseCase =
            incomingCallWorkerRequestActivity.getRegistrationTokenUseCase
        val sendNotificationToCustomerUseCase =
            incomingCallWorkerRequestActivity.sendNotificationToCustomer
        val orderCallWorkerRequestMapper: UiCallWorkerRequestMapper =
            incomingCallWorkerRequestActivity.orderCallWorkerRequestMapper
        val imageLoadingUtil = incomingCallWorkerRequestActivity.imageLoadingUtil

        val viewModel =
            getScreenModel<IncomingCallWorkerViewModel, IncomingCallWorkerViewModel.Factory> {
                it.create(
                    currentContext.applicationContext as Application,
                    getOrderByIdUseCase,
                    updateOrdersUseCase,
                    getRegistrationTokenUseCase,
                    sendNotificationToCustomerUseCase,
                    dispatchersProvider,
                    orderCallWorkerRequestMapper
                )
            }
        viewModel.handleEvent(IncomingCallWorkerEvent.Loading(orderId))

        IncomingCallWorkerRequestUi(
            viewModel = viewModel,
            incomingCallWorkerRequestActivity = incomingCallWorkerRequestActivity,
            imageLoadingUtil
        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    private fun IncomingCallWorkerRequestUi(
        viewModel: IncomingCallWorkerViewModel,
        incomingCallWorkerRequestActivity: IncomingCallWorkerRequestActivity,
        imageLoadingUtil: ImageLoadingUtil
    ) {
        val (dialogVisibleState, setDialogState) = remember { mutableStateOf(false) }
        val (errorDialogVisibleState, setErrorDialogState) = remember { mutableStateOf("") }
        val state by viewModel.state.collectAsState()

        val uiCallWorkerRequest = remember { mutableStateOf(UiCallWorkerRequest()) }
        val (isUiCallWorkerInitialized, setUiCallWorkerInitialized) = remember {
            mutableStateOf(
                false
            )
        }
        var timeRemaining = 0L

        when (val result = state) {
            is IncomingCallWorkerViewModel.State.Loading -> {
            }
            is IncomingCallWorkerViewModel.State.Loaded -> {
                if (!isUiCallWorkerInitialized) setUiCallWorkerInitialized(true)
                uiCallWorkerRequest.value = result.uiCallWorkerRequest
                viewModel.handleEvent(IncomingCallWorkerEvent.StartCountDown)
            }
            is IncomingCallWorkerViewModel.State.RejectedOrder -> {
                viewModel.back()
                incomingCallWorkerRequestActivity.finish()
            }
            is IncomingCallWorkerViewModel.State.AcceptedOrder -> {
                setDialogState(true)
            }
            is IncomingCallWorkerViewModel.State.Error -> {
                setErrorDialogState(result.error)
            }
            is IncomingCallWorkerViewModel.State.CountingDownTime -> {
                timeRemaining = result.timeRemaining
                if (result.timeRemaining == 0L) { // Hết thời gian tối đa để nhận đơn
                    incomingCallWorkerRequestActivity.finishAfterTransition()
                }
            }
        }

        if (dialogVisibleState) {
            DialogMessage(
                title = "Thông báo",
                text = "Nhận việc thành công. Hãy đến [Quản lý việc] để bắt đầu làm việc ngay bây giờ.",
                onDismissRequest = {
                    setDialogState(false)
                    viewModel.back()
                    incomingCallWorkerRequestActivity.finish()
                },
                positiveButton = ActionButton("Đã hiểu") {
                    setDialogState(false)
                    viewModel.back()
                    incomingCallWorkerRequestActivity.finish()
                }
            )
        }

        if (errorDialogVisibleState.isNotEmpty()) {
            DialogMessage(
                title = "Thông báo",
                text = errorDialogVisibleState,
                onDismissRequest = { setErrorDialogState("") },
                positiveButton = ActionButton("Đã hiểu") {
                    setErrorDialogState("")
                },
                messageType = MessageType.ERROR
            )
        }

        if (isUiCallWorkerInitialized) {
            Scaffold(
                topBar = {
                    SmallTopAppBar(
                        title = {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center,
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_alarm_clock),
                                    contentDescription = null,
                                    tint = Color.White
                                )
                                Text(
                                    text = "Thời gian để xác nhận: $timeRemaining",
                                    style = AloThoTypography.h6,
                                    modifier = Modifier.padding(start = 5.dp),
                                    color = Color.White
                                )
                            }
                        },
                        actions = {

                        },
                        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                            containerColor = Color.Transparent,
                        ),
                        modifier = Modifier
                            .statusBarsPadding()
                            .gradientBackground(
                                arrayListOf(
                                    SecondaryColor,
                                    PrimaryColor
                                ), 270f
                            )
                            .zIndex(4f)
                    )
                }
            ) {
                ConstraintLayout(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .fillMaxSize()
                        .padding(it)
                ) {
                    val (newRequestImg, serviceType, serviceNames, address, quantity, workingDate, estimatedTotal, divider, customer, btnAcceptOrder, btnRejectOrder) = createRefs()

                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .size(100.dp)
                            .clip(CircleShape)
                            .border(5.dp, Color.White, shape = CircleShape)
                            .clickable { }
                            .constrainAs(newRequestImg) {
                                top.linkTo(parent.top)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                    ) {
                        Icon(
                            Icons.Rounded.ReceiptLong,
                            contentDescription = null,
                            modifier = Modifier.size(64.dp)
                        )
                    }

                    Text(
                        text = uiCallWorkerRequest.value.serviceType.orEmpty(),
                        style = AloThoTypography.h6,
                        modifier = Modifier.constrainAs(serviceType) {
                            top.linkTo(newRequestImg.bottom, 10.dp)
                            start.linkTo(newRequestImg.start)
                            end.linkTo(newRequestImg.end)
                        }
                    )

                    Text(
                        text = "Địa chỉ: ${uiCallWorkerRequest.value.address?.fullAddress}",
                        style = AloThoTypography.h6,
                        modifier = Modifier.constrainAs(address) {
                            top.linkTo(serviceType.bottom, 15.dp)
                            start.linkTo(serviceType.start)
                            end.linkTo(serviceType.end)
                        }
                    )

                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .constrainAs(quantity) {
                                top.linkTo(address.bottom, 10.dp)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .padding(horizontal = 10.dp)
                    ) {
                        Text(
                            "Số lượng: ",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            "${uiCallWorkerRequest.value.orderDetails.size} công việc",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Normal
                        )
                    }

                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .constrainAs(workingDate) {
                                top.linkTo(quantity.bottom)
                                start.linkTo(quantity.start)
                                end.linkTo(quantity.end)
                            }
                    ) {
                        Text(
                            "Ngày bắt đầu: ",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            uiCallWorkerRequest.value.workingDate?.toDateFormat().orEmpty(),
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Normal
                        )
                    }

                    Row(
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .constrainAs(estimatedTotal) {
                                top.linkTo(workingDate.bottom)
                                start.linkTo(workingDate.start)
                                end.linkTo(workingDate.end)
                            }
                    ) {
                        Text(
                            "Số tiền tạm tính: ",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Bold
                        )
                        Text(
                            CurrencyUtils.formatVnCurrency(uiCallWorkerRequest.value.total.toString()),
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.Normal
                        )
                    }

                    Divider(modifier = Modifier
                        .constrainAs(divider) {
                            top.linkTo(estimatedTotal.bottom, 20.dp)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .padding(horizontal = 20.dp)
                    )

                    Column(
                        modifier = Modifier
                            .constrainAs(customer) {
                                top.linkTo(divider.bottom, 20.dp)
                                start.linkTo(divider.start)
                                end.linkTo(divider.end)
                            }
                            .padding(horizontal = 20.dp),
                        horizontalAlignment = Alignment.CenterHorizontally,
                        verticalArrangement = Arrangement.spacedBy(5.dp)
                    ) {
                        val orderCustomer = uiCallWorkerRequest.value.user

                        Text(
                            "Khách hàng: ${orderCustomer?.displayName.orEmpty()}",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.SemiBold,
                            color = Lian_Color_Text_Ab
                        )
                        val photoUrl = orderCustomer?.photoUrl

                        if (photoUrl.isNullOrEmpty()) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_user_circle_solid),
                                contentDescription = null,
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .size(100.dp)
                                    .clip(CircleShape)
                                    .border(5.dp, PrimaryColor, shape = CircleShape)
                                    .align(Alignment.CenterHorizontally)
                            )
                        } else {
                            imageLoadingUtil.ComposeImage(
                                data = photoUrl,
                                modifier = Modifier
                                    .size(100.dp)
                                    .clip(CircleShape)
                                    .border(5.dp, PrimaryColor, shape = CircleShape)
                                    .align(Alignment.CenterHorizontally)
                            )
                        }

                    }

                    FilledTonalButton(
                        onClick = {
                            viewModel.handleEvent(IncomingCallWorkerEvent.AcceptOrder)
                        },
                        colors = ButtonDefaults.filledTonalButtonColors(
                            containerColor = PrimaryVariantColor,
                        ),
                        modifier = Modifier
                            .constrainAs(btnAcceptOrder) {
                                top.linkTo(customer.bottom, 10.dp)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                    ) {
                        Text(
                            "Chấp nhận đơn hàng",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 18.sp,
                            color = Color.White
                        )
                    }

                    OutlinedButton(
                        onClick = {
                            viewModel.handleEvent(IncomingCallWorkerEvent.RejectOrder)
                        },
                        colors = ButtonDefaults.filledTonalButtonColors(
                            containerColor = Color.Red
                        ),
                        modifier = Modifier
                            .constrainAs(btnRejectOrder) {
                                top.linkTo(btnAcceptOrder.bottom, 10.dp)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                    ) {
                        Text(
                            "Từ chối đơn hàng",
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 18.sp,
                            color = Color.White
                        )
                    }
                }
            }
        }
    }
}