package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.mappers

import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User.Companion.toUser
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.Service.Companion.toService
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.ServiceType.Companion.toServiceType
import edu.iuhuniversity.alothoforworkerapp.core.data.remote.firestore.toResponse
import edu.iuhuniversity.alothoforworkerapp.core.presentation.model.mappers.UiMapper
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.UiCallWorkerRequest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

class UiCallWorkerRequestMapper @Inject constructor() : UiMapper<Order, UiCallWorkerRequest> {
    override suspend fun mapToView(
        input: Order,
        output: (UiCallWorkerRequest) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            val getServiceAsync = async {
                // todo
                val service = CommonUtils.getDocumentReference(CollectionConst.COLLECTION_SERVICE,"j3Sbvzt1RN4T7CeihMMl")
                service.get().toResponse { it.toService() }
            }
            val serviceList = listOf(getServiceAsync.await().data).takeIf { true } ?: listOf()

            val getServiceTypeAsync = async {
                serviceList.first()?.serviceType?.get()?.toResponse { it.toServiceType() }
            }
            val serviceType = getServiceTypeAsync.await()?.data

            val customerAsync = async {
                input.customer.get().toResponse { it.toUser() }
            }
            val customer = customerAsync.await().data

            val request = UiCallWorkerRequest(
                id = input.id,
                status = input.status!!,
                total = input.estimatedFee ?: 100000.0,
                estimatedTotal = input.estimatedFee ?: 100000.0,
                services = serviceList.map { it?.name.orEmpty() },
                address = input.workingAddress,
                user = customer,
                serviceType = serviceType?.name ?: "Dịch vụ mở khoá",
                workingDate = input.workingDate,
                orderDetails = input.orderDetails,
                images = input.images
            )

            output(request)
        }
    }

    override suspend fun mapToView(input: Order): UiCallWorkerRequest {
        TODO("Not yet implemented")
    }

    override suspend fun mapToView(
        input: List<Order?>,
        output: (List<UiCallWorkerRequest>) -> Unit
    ) {
        TODO("Not yet implemented")
    }
}