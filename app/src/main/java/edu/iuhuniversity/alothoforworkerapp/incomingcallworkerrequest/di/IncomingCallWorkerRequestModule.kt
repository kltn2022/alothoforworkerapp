package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.viewmodel.IncomingCallWorkerViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class IncomingCallWorkerRequestModule {
    @Binds
    @IntoMap
    @ScreenModelFactoryKey(IncomingCallWorkerViewModel.Factory::class)
    abstract fun bindIncomingCallWorkerRequestViewModelFactory(incomingCallWorkerViewModelFactory: IncomingCallWorkerViewModel.Factory): ScreenModelFactory
}