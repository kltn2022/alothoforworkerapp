package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model

import android.os.Parcelable
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Address
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderDetail
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.OrderStatus
import kotlinx.parcelize.Parcelize
import java.util.*

@Parcelize
data class UiCallWorkerRequest(
    val id: String? = null,
    val total: Double? = 0.0,
    val estimatedTotal: Double? = 0.0,
    val serviceType: String? = null,
    val services: List<String> = emptyList(),
    val address: Address? = null,
    val status: OrderStatus? = null,
    val worker: Worker? = null,
    val user: User? = null,
    val workingDate: Date? = null,
    val images: List<String>? = null,
    val orderDetails: List<OrderDetail> = emptyList(),
) : Parcelable