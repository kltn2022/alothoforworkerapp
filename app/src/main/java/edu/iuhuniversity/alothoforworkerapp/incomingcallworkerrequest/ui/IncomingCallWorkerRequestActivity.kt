package edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.ui

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.core.view.WindowCompat
import androidx.navigation.ActivityNavigator
import cafe.adriel.voyager.navigator.Navigator
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.order.Order
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.GetRegistrationTokenUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.SendNotificationToCustomerUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.GetOrderByIdUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.UpdateOrderUseCase
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.mappers.UiCallWorkerRequestMapper
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.ui.IncomingCallWorkerRequestScreen
import edu.iuhuniversity.alothoforworkerapp.task.model.mappers.UiOrderMapper
import javax.inject.Inject

@AndroidEntryPoint
class IncomingCallWorkerRequestActivity : AppCompatActivity() {

    @Inject
    lateinit var getOrderByIdUseCase: GetOrderByIdUseCase

    @Inject
    lateinit var updateOrderUseCase: UpdateOrderUseCase

    @Inject
    lateinit var dispatchersProvider: DispatchersProvider

    @Inject
    lateinit var orderMapper: UiOrderMapper

    @Inject
    lateinit var getRegistrationTokenUseCase: GetRegistrationTokenUseCase

    @Inject
    lateinit var sendNotificationToCustomer: SendNotificationToCustomerUseCase

    @Inject
    lateinit var orderCallWorkerRequestMapper: UiCallWorkerRequestMapper

    @Inject
    lateinit var imageLoadingUtil: ImageLoadingUtil


    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_AloThoForWorkerApp)

        setContent {
            CompositionLocalProvider(
                LocalOverScrollConfiguration provides null,
            ) {
                WindowCompat.setDecorFitsSystemWindows(window, false)

                val systemUiController = rememberSystemUiController()

                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = PrimaryColor,
                    )
                }

                if (intent?.hasExtra("orderId") == true) {
                    val orderId = intent.getStringExtra("orderId")

                    Navigator(screen = IncomingCallWorkerRequestScreen(orderId!!))

                } else {
                    DialogMessage(
                        title = "Thông báo lỗi",
                        text = "Có lỗi không mong muốn xảy ra. Vui lòng quay lại!",
                        onDismissRequest = { finish() },
                        positiveButton = ActionButton("Quay lại") {
                            finish()
                        }
                    )
                }
            }
        }
    }

    override fun finish() {
        super.finish()
        ActivityNavigator.applyPopAnimationsToPendingTransition(this)
    }
}