package edu.iuhuniversity.alothoforworkerapp.signin.viewmodels

import android.app.Activity
import android.app.Application
import android.os.Build
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SendOtpUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import edu.iuhuniversity.alothoforworkerapp.core.utils.isValidatePhoneNumber
import edu.iuhuniversity.alothoforworkerapp.signin.SignInEvent
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.utils.toAloThoPhone
import kotlinx.coroutines.launch
import timber.log.Timber

class SignInViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val sendOtpUseCase: SendOtpUseCase
) : StateScreenModel<SignInViewModel.State>(State.Loading) {

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(app: Application, sendOtpUseCase: SendOtpUseCase): SignInViewModel
    }

    private var resendToken = MutableLiveData<PhoneAuthProvider.ForceResendingToken>()
    private var storedVertificationId = mutableStateOf("")

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        object ReadyForSendOtp : State()
        data class SentOtp(val vertificationId: String?, val otp: String?) : State()
    }

    fun handleEvents(event: SignInEvent) {
        when (event) {
            is SignInEvent.PrepareForSignIn -> prepareForSignIn()
            is SignInEvent.CheckPhoneNumber -> checkPhoneNumber(event.phoneNumber)
            is SignInEvent.SendOtpToPhoneNumber -> onSignInClicked(
                event.activity,
                event.phoneNumber
            )
        }
    }

    private fun onSignInClicked(activity: Activity, phoneNumber: String) = coroutineScope.launch {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isNetworkIsAvailable(app)) {
                sendOtpUseCase.execute(
                    activity,
                    phoneNumber.toAloThoPhone(),
                    resendToken.value,
                    object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                            val smsCode = phoneAuthCredential.smsCode!!

                            if (smsCode.isNotEmpty() && smsCode.length == 6) {
                                mutableState.value = State.SentOtp(
                                    storedVertificationId.value,
                                    smsCode
                                )
                            }
                        }

                        override fun onVerificationFailed(ex: FirebaseException) {
                            Timber.w("onVerificationFailed", ex)

                            if (ex is FirebaseAuthInvalidCredentialsException) {
                                mutableState.value = State.Error("Có lỗi xảy ra. Vui lòng thử lại!")
                            } else if (ex is FirebaseTooManyRequestsException) {
                                mutableState.value =
                                    State.Error("Không thể gửi mã OTP ngay bây giờ. Chúng tôi sẽ khắc phục lỗi sớm")
                            }
                        }

                        override fun onCodeSent(
                            verificationId: String,
                            token: PhoneAuthProvider.ForceResendingToken
                        ) {
                            storedVertificationId.value = verificationId
                            resendToken.value = token

                            Timber.d("SignInByPhoneAuth: onCodeSent:$verificationId")

                            mutableState.value = State.SentOtp(storedVertificationId.value, null)
                        }
                    })
                // mutableState.value = State.SentOtp(storedVertificationId.value, null)
            } else {
                mutableState.value = State.Error("Không kết nối được Internet!")
            }
        } else {
            mutableState.value = State.Error("Không kết nối được Internet!")
        }
    }

    private fun checkPhoneNumber(phoneNumber: String) {
        if (phoneNumber.isEmpty()) {
            mutableState.value = State.Error("Vui lòng nhập số điện thoại")
        } else {
            if (!isValidatePhoneNumber(phoneNumber))
                mutableState.value = State.Error("Số điện thoại không hợp lệ")
            else mutableState.value = State.ReadyForSendOtp
        }
    }

    private fun prepareForSignIn() {
        mutableState.value = State.Loading
    }

}