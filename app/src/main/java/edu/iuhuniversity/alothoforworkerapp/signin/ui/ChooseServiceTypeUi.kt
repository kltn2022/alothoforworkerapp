package edu.iuhuniversity.alothoforworkerapp.signin.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.ChipDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FilterChip
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Done
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cafe.adriel.voyager.androidx.AndroidScreen
import edu.iuhuniversity.alothoforworkerapp.core.data.model.service.ServiceType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel.DemoDataProvider
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyGridFor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.service.ServiceTypeItem
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*

object ChooseServiceTypeUi : AndroidScreen() {
    @OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
    @Composable
    override fun Content() {
        Column(
            verticalArrangement = Arrangement.spacedBy(10.dp),
            horizontalAlignment = Alignment.Start,
            modifier = Modifier.padding(10.dp)
        ) {
            var selectedServiceTypes = remember {
                mutableListOf<ServiceType>()
            }
            Text(text = "Chọn loại dịch vụ mà bạn muốn đăng ký?", style = AloThoTypography.h6)

            LazyVerticalGrid(
                columns = GridCells.Fixed(2),
                contentPadding = PaddingValues(horizontal = 10.dp, vertical = 5.dp),
            ) {
                items(selectedServiceTypes.toList()) { serviceType ->
                    FilterChip(
                        selected = selectedServiceTypes.contains(serviceType),
                        onClick = { selectedServiceTypes.remove(serviceType) },
                        colors = ChipDefaults.filterChipColors(
                            backgroundColor = PrimaryLighterVariantColor,
                            contentColor = PrimaryColor,
                            selectedBackgroundColor = PrimaryLightVariantColor,
                        ),
                        selectedIcon = {
                            Icon(
                                Icons.Rounded.Done,
                                contentDescription = null,
                                tint = PrimaryColor
                            )
                        },
                        modifier = Modifier.padding(5.dp),
                        border = BorderStroke(1.dp, PrimaryColor)
                    ) {
                        androidx.compose.material3.Text(
                            text = serviceType.name,
                            style = AloThoTypography.subtitle2.copy(fontWeight = FontWeight.SemiBold),
                            color = PrimaryColor
                        )
                    }
                }
            }
            LazyGridFor(
                items = DemoDataProvider.defaultServiceTypeList,
                rowSize = 2,
            ) {
                ServiceTypeItem(
                    title = it.name,
                    content = {
                        Image(
                            painter = painterResource(id = it.icon.toInt()),
                            contentDescription = it.name,
                            modifier = Modifier
                                .size(56.dp)
                                .padding(10.dp)

                        )
                    },
                    onItemClicked = { }
                )
            }

            FilledTonalButton(
                onClick = {

                },
                colors = ButtonDefaults.filledTonalButtonColors(
                    containerColor = PrimaryVariantColor,
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(20.dp)
            ) {
                Text("Tiếp tục")
            }
        }
    }

}
