package edu.iuhuniversity.alothoforworkerapp.signin.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.signin.viewmodels.EnterOtpViewModel
import edu.iuhuniversity.alothoforworkerapp.signin.viewmodels.SignInViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class SignInModule {
    @Binds
    @IntoMap
    @ScreenModelFactoryKey(SignInViewModel.Factory::class)
    abstract fun bindSignInViewModelFactory(signInViewModelFactory: SignInViewModel.Factory): ScreenModelFactory

    @Binds
    @IntoMap
    @ScreenModelFactoryKey(EnterOtpViewModel.Factory::class)
    abstract fun bindEnterOtpViewModelFactory(enterOtpViewModelFactory: EnterOtpViewModel.Factory): ScreenModelFactory


}