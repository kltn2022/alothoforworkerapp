
package edu.iuhuniversity.alothoforworkerapp.signin.ui

import android.app.Activity
import android.app.Application
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.core.screen.ScreenKey
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.CustomTextField
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.LianButton_StyleWhite
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.navigation.AloThoWorkerLogo
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.signin.ui.EnterOtpScreen
import edu.iuhuniversity.alothoforworkerapp.signin.SignInEvent
import edu.iuhuniversity.alothoforworkerapp.signin.viewmodels.SignInViewModel
import timber.log.Timber

class SignInScreen(
    // private val application: Application,
    // private val sendOtpUseCase: SendOtpUseCase
) : AndroidScreen() {

    override val key: ScreenKey = uniqueScreenKey

    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow

        val currentContext = LocalContext.current
        val sendOtpUseCase = (currentContext.getComponentActivity() as MainActivity).sendOtpUseCase
        val viewModel = getScreenModel<SignInViewModel, SignInViewModel.Factory> {
            it.create(currentContext.applicationContext as Application, sendOtpUseCase)
        }

        LifecycleEffect(
            onStarted = {
                viewModel.handleEvents(SignInEvent.PrepareForSignIn)
            },
            onDisposed = {
                viewModel.handleEvents(SignInEvent.PrepareForSignIn)
            }
        )

        SignInUi(navigator, viewModel)
    }

    @Composable
    fun SignInUi(navigator: Navigator, screenModel: SignInViewModel) {
        val state by screenModel.state.collectAsState()
        val activity = LocalContext.current.getComponentActivity()

        ConstraintLayout(
            modifier = Modifier
                .fillMaxSize()
                .background(PrimaryVariantColor)
        ) {
            val (bg, logo, phoneNumberInput, btnLogin, txtDieuKhoan) = createRefs()
            var text by rememberSaveable { mutableStateOf("") }
            var error by rememberSaveable { mutableStateOf("") }

            when (val result = state) {
                is SignInViewModel.State.Loading -> {
                    // Loading Effect
                }

                is SignInViewModel.State.SentOtp ->  {
                    Timber.d("Sent OTP: ${result.vertificationId} : ${result.otp}")
                    navigator.push(EnterOtpScreen(text, result.vertificationId.orEmpty(), result.otp.orEmpty()))
                }
                is SignInViewModel.State.Error -> {
                    error = result.error
                }
                is SignInViewModel.State.ReadyForSendOtp -> {
                    screenModel.handleEvents(SignInEvent.SendOtpToPhoneNumber(activity as Activity, text))
                }
            }

            Image(
                painter = painterResource(R.drawable.ic_logo_screen_login),
                contentDescription = "ic_logo_screen_login",
                colorFilter = ColorFilter.tint(PrimaryColor, BlendMode.Color),
                modifier = Modifier.constrainAs(bg) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
            )

            AloThoWorkerLogo(
                modifier = Modifier
                    .size(150.dp)
                    .constrainAs(logo) {
                        top.linkTo(parent.top, 80.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )

            CustomTextField(
                value = text,
                placeholder = "Nhập số điện thoại của bạn",
                onValueChange = {
                    text = it
                },
                error = error,
                keyboardActions = KeyboardActions {
                    screenModel.handleEvents(SignInEvent.CheckPhoneNumber(text))
                },
                modifier = Modifier
                    .constrainAs(phoneNumberInput) {
                        top.linkTo(logo.bottom, 30.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(horizontal = 30.dp),
                primaryColor = Color.White,
                textColor = Color.White,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Phone,
                    imeAction = ImeAction.Done
                )
            )

            LianButton_StyleWhite(
                label = "Tiếp tục",
                onClick = {
                    Timber.d("SDT: $text")
                    screenModel.handleEvents(SignInEvent.CheckPhoneNumber(text))
                },
                layoutModifier = Modifier
                    .constrainAs(btnLogin) {
                        top.linkTo(phoneNumberInput.bottom, 30.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    },
                verticalPadding = 15.dp
            )

            Text(
                text = stringResource(id = R.string.txt_title_dieu_khoan_register),
                style = AloThoTypography.body1.copy(
                    lineHeight = 20.sp,
                    color = Color.White,
                ),
                modifier = Modifier
                    .constrainAs(txtDieuKhoan) {
                        bottom.linkTo(parent.bottom, 100.dp)
                        start.linkTo(phoneNumberInput.start)
                        end.linkTo(phoneNumberInput.end)
                    }
                    .padding(horizontal = 20.dp),
                textAlign = TextAlign.Center
            )
        }
    }
}

