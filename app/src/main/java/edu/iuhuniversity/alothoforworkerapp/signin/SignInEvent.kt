package edu.iuhuniversity.alothoforworkerapp.signin

import android.app.Activity
import cafe.adriel.voyager.navigator.Navigator

sealed class SignInEvent {
    object PrepareForSignIn : SignInEvent()
    data class PhoneInput(val input: String) : SignInEvent()
    data class CheckPhoneNumber(val phoneNumber: String) : SignInEvent()
    data class SendOtpToPhoneNumber(
        val activity: Activity,
        val phoneNumber: String
    ) : SignInEvent()
}

sealed class EnterOtpEvent {
    data class VerifyVerificationCode(val verificationId: String, val otp: String): EnterOtpEvent()
    data class CheckOtp(val otp: String?) : EnterOtpEvent()
    data class ResendOtp(val activity: Activity, val phoneNumber: String, val verificationId: String): EnterOtpEvent()
    object StartCountDown: EnterOtpEvent()
    data class BackSignInForm(val navigator: Navigator): EnterOtpEvent()
}
