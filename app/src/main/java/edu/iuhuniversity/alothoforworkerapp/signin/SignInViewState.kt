package edu.iuhuniversity.alothoforworkerapp.signin

import com.google.firebase.auth.FirebaseUser
import edu.iuhuniversity.alothoforworkerapp.core.presentation.Event

data class SignInViewState(
    val loggedUser: FirebaseUser? = null,
    val failure: Event<Throwable>? = null
)
