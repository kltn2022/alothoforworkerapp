package edu.iuhuniversity.alothoforworkerapp.signin.viewmodels

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SendOtpUseCase

//class SignInViewModelFactory(
//     private val app: Application,
//    private val sendOtpUseCase: SendOtpUseCase
//) : ViewModelProvider.Factory {
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        return SignInViewModel(
//            app,
//            sendOtpUseCase
//        ) as T
//    }
//
//}
//