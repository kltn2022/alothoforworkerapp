package edu.iuhuniversity.alothoforworkerapp.signin.ui

import android.app.Activity
import android.app.Application
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clipToBounds
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.core.screen.ScreenKey
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.CustomTextField
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.LianButton_StylePrimary
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.signin.EnterOtpEvent
import edu.iuhuniversity.alothoforworkerapp.signin.viewmodels.EnterOtpViewModel
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import java.io.Serializable
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.SecondaryColor
import edu.iuhuniversity.alothoforworkerapp.home.ui.HomeUi

data class EnterOtpScreen(
    val phoneNumber: String,
    val verificationId: String,
    val otp: String?,
) : AndroidScreen(), Serializable {

    override val key: ScreenKey = uniqueScreenKey

    @Composable
    override fun Content() {
        val currentContext = LocalContext.current
        val signInWithPhoneAuthUseCase =
            (currentContext.getComponentActivity() as MainActivity).signInWithPhoneAuthUseCase
        val sendOtpUseCase = (currentContext.getComponentActivity() as MainActivity).sendOtpUseCase


        val viewModel = getScreenModel<EnterOtpViewModel, EnterOtpViewModel.Factory> {
            it.create(
                currentContext.applicationContext as Application,
                signInWithPhoneAuthUseCase,
                sendOtpUseCase
            )
        }

        EnterOtpUi(viewModel, verificationId, otp, phoneNumber)
    }

}


@Composable
fun EnterOtpUi(
    viewModel: EnterOtpViewModel,
    verificationId: String,
    otp: String?,
    phoneNumber: String,
) {
    val navigator = LocalNavigator.currentOrThrow
    val state by viewModel.state.collectAsState()
    viewModel.handleEvents(EnterOtpEvent.StartCountDown)

    ConstraintLayout(
        modifier = Modifier
            .background(Color.White)
            .fillMaxSize()
    ) {
        val (icon, icon_pass, txtPhoneNumber, otpTextInput, btnResendOtp, btnContinue, btnBack) = createRefs()
        // lateinit var countDownTimer: CountDownTimer

        Image(
            painter = painterResource(R.drawable.ic_khien),
            contentDescription = "ic_khien",
            colorFilter = ColorFilter.tint(PrimaryColor, BlendMode.Overlay),
            modifier = Modifier.constrainAs(icon) {
                top.linkTo(parent.top, 80.dp)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
                .clipToBounds()
        )

        Image(
            painter = painterResource(R.drawable.ic_pass),
            contentDescription = "ic_pass",
            colorFilter = ColorFilter.tint(PrimaryColor, BlendMode.Color),
            modifier = Modifier.constrainAs(icon_pass) {
                top.linkTo(icon.top)
                start.linkTo(icon.start)
                end.linkTo(icon.end)
                bottom.linkTo(icon.bottom)
            }
        )

        Text(
            text = "Nhập mã xác nhận đã gửi đến số $phoneNumber của bạn",
            style = AloThoTypography.h6.copy(
                color = colorResource(id = R.color.colorText),
            ),
            modifier = Modifier
                .padding(horizontal = 40.dp)
                .constrainAs(txtPhoneNumber) {
                    top.linkTo(icon.bottom, 50.dp)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        var text by rememberSaveable { mutableStateOf("") }
        var error by rememberSaveable { mutableStateOf("") }
        var timeRemaining = 0L
        var (label, setLabel) = rememberSaveable { mutableStateOf("Gửi lại mã xác thực (timeRemaining)") }

        var message by remember { mutableStateOf("") }
        val activity = LocalContext.current.getComponentActivity()

        if (!otp.isNullOrEmpty()) {
            text = otp
        }

        when (val result = state) {
            is EnterOtpViewModel.State.Loading -> {

            }
            is EnterOtpViewModel.State.SignedIn -> {
                navigator.replaceAll(HomeUi)
            }
            is EnterOtpViewModel.State.Error -> {
                error = result.error
            }
            is EnterOtpViewModel.State.ReadyForSignIn -> {
                viewModel.handleEvents(EnterOtpEvent.VerifyVerificationCode(text, verificationId))
            }
            is EnterOtpViewModel.State.ResentOtp -> {
                // Thông báo đã gửi OTP
                message = "Đã gửi lại OTP. Vui lòng kiểm tra tin nhắn của bạn."
                (activity as MainActivity).showSnackBar(message, MessageType.SUCCESS)
            }
            is EnterOtpViewModel.State.CountingDownTime -> {
                timeRemaining = result.timeRemaining
            }
        }

        CustomTextField(
            value = text,
            placeholder = "OTP",
            onValueChange = {
                text = it
                if (it.length == 6) {
                    viewModel.handleEvents(EnterOtpEvent.VerifyVerificationCode(verificationId, it))
                }
            },
            error = error,
            modifier = Modifier
                .constrainAs(otpTextInput) {
                    top.linkTo(txtPhoneNumber.bottom, 30.dp)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(horizontal = 40.dp),
            // primaryColor = colorResource(id = R.color.colorIndicator),
            textColor = SecondaryColor,
            contentAlignment = Alignment.Center
        )

        Box(
            modifier = Modifier
                .padding(horizontal = 40.dp)
                .constrainAs(btnResendOtp) {
                    top.linkTo(otpTextInput.bottom, 30.dp)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
            contentAlignment = Alignment.Center
        ) {

            if (timeRemaining == 0L) {
                Text(
                    text = "Gửi lại mã xác thực",
                    style = AloThoTypography.body1.copy(
                        color = PrimaryColor,
                    ),
                    modifier = Modifier
                        .clickable {
                            viewModel.handleEvents(
                                EnterOtpEvent.ResendOtp(
                                    activity as Activity,
                                    phoneNumber,
                                    verificationId
                                )
                            )
                        }
                )
            } else {
                Text(
                    text = "Gửi lại mã xác thực (${timeRemaining}s)",
                    style = AloThoTypography.body1.copy(
                        color = colorResource(id = R.color.colorText),
                    ),
                    modifier = Modifier
                )
            }
        }

        LianButton_StylePrimary(
            layoutModifier = Modifier.constrainAs(btnContinue) {
                top.linkTo(btnResendOtp.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
                bottom.linkTo(parent.bottom)
            },
            label = "Tiếp tục",
            onClick = {
                viewModel.handleEvents(EnterOtpEvent.CheckOtp(text))
            },
            verticalPadding = 15.dp,
            width = 250.dp
        )

        Text(
            text = "Đổi số diện thoại",
            style = AloThoTypography.body1.copy(
                color = colorResource(id = R.color.colorText),
            ),
            modifier = Modifier
                .constrainAs(btnBack) {
                    top.linkTo(btnContinue.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom, 30.dp)
                }
                .clickable {
                    viewModel.handleEvents(EnterOtpEvent.BackSignInForm(navigator))
                },
            textDecoration = TextDecoration.Underline
        )

    }
}
