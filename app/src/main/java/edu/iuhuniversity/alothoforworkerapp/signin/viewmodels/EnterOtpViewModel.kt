package edu.iuhuniversity.alothoforworkerapp.signin.viewmodels

import android.app.Activity
import android.app.Application
import android.os.Build
import android.os.CountDownTimer
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.navigator.Navigator
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.User
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SendOtpUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SignInWithPhoneAuthUseCase
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import edu.iuhuniversity.alothoforworkerapp.core.utils.toAloThoPhone
import edu.iuhuniversity.alothoforworkerapp.signin.EnterOtpEvent
import kotlinx.coroutines.launch
import timber.log.Timber

class EnterOtpViewModel @AssistedInject constructor(
    @Assisted val app: Application,
    @Assisted val signInWithPhoneAuthUseCase: SignInWithPhoneAuthUseCase,
    @Assisted val sendOtpUseCase: SendOtpUseCase
) : StateScreenModel<EnterOtpViewModel.State>(State.Loading) {

    private var resendToken = MutableLiveData<PhoneAuthProvider.ForceResendingToken>()
    private var storedVertificationId = mutableStateOf("")

    private var countDownTimer: CountDownTimer? = null

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            signInWithPhoneAuthUseCase: SignInWithPhoneAuthUseCase,
            sendOtpUseCase: SendOtpUseCase
        ): EnterOtpViewModel
    }

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        data class ReadyForSignIn(val otp: String) : State()
        data class Verify(val verificationId: String?, val otp: String?) : State()
        data class SignedIn(val currentUser: Worker) : State()
        data class ResentOtp(val verificationId: String?, val otp: String?) : State()
        data class CountingDownTime(val timeRemaining: Long) : State()
    }

    fun handleEvents(event: EnterOtpEvent) {
        when (event) {
            is EnterOtpEvent.VerifyVerificationCode -> {
                verifyVerificationCode(event.verificationId, event.otp)
            }
            is EnterOtpEvent.CheckOtp -> {
                checkOtp(event.otp)
            }
            is EnterOtpEvent.StartCountDown -> {
                startCountdown()
            }
            is EnterOtpEvent.ResendOtp -> {
                resendOtp(event.activity, event.phoneNumber)
            }
            is EnterOtpEvent.BackSignInForm -> {
                back(event.navigator)
            }
        }
    }

    private fun checkOtp(otp: String?) {
        if (otp.isNullOrEmpty()) {
            mutableState.value = State.Error("Vui lòng nhập OTP")
        } else {
            if (otp.length != 6) {
                mutableState.value = State.Error("Vui lòng nhập OTP hợp lệ")
            }
        }
        mutableState.value = State.ReadyForSignIn(otp!!)
    }

    private fun verifyVerificationCode(verificationId: String, otp: String) =
        coroutineScope.launch {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    Timber.d(verificationId, " x:x ", otp)

                    when (val result = signInWithPhoneAuthUseCase.execute(verificationId, otp)) {
                        is Resource.Error -> mutableState.value =
                            State.Error(result.message.toString())
                        is Resource.Loading -> mutableState.value = State.Loading
                        else -> mutableState.value = State.SignedIn(result.data!!)
                    }
                } else {
                    mutableState.value =
                        State.Error("Không kết nối được Internet. Vui lòng thử lại")
                }
            }
        }

    private fun resendOtp(activity: Activity, phoneNumber: String) = coroutineScope.launch {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isNetworkIsAvailable(app)) {
                sendOtpUseCase.execute(
                    activity,
                    phoneNumber.toAloThoPhone(),
                    resendToken.value,
                    object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                            val smsCode = phoneAuthCredential.smsCode!!

                            if (smsCode.isNotEmpty() && smsCode.length == 6) {
                                mutableState.value = State.ResentOtp(
                                    verificationId = null,
                                    smsCode
                                )
                            }
                        }

                        override fun onVerificationFailed(ex: FirebaseException) {
                            Timber.w("onVerificationFailed", ex)

                            if (ex is FirebaseAuthInvalidCredentialsException) {
                                mutableState.value = State.Error("Có lỗi xảy ra. Vui lòng thử lại!")
                            } else if (ex is FirebaseTooManyRequestsException) {
                                mutableState.value =
                                    State.Error("Không thể gửi mã OTP ngay bây giờ. Chúng tôi sẽ khắc phục lỗi sớm")
                            }
                        }

                        override fun onCodeSent(
                            verificationId: String,
                            token: PhoneAuthProvider.ForceResendingToken
                        ) {
                            storedVertificationId.value = verificationId
                            resendToken.value = token

                            Timber.d("SignInByPhoneAuth: onCodeSent:$verificationId")

                            mutableState.value = State.ResentOtp(storedVertificationId.value, null)
                        }
                    })
                // mutableState.value = State.SentOtp(storedVertificationId.value, null)
            } else {
                mutableState.value = State.Error("Không kết nối được Internet!")
            }
        } else {
            mutableState.value = State.Error("Không kết nối được Internet!")
        }
    }

    private fun startCountdown() {
        countDownTimer = object : CountDownTimer(10000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mutableState.value = State.CountingDownTime(millisUntilFinished / 1000 ?: 0L)
            }

            override fun onFinish() {
                mutableState.value = State.CountingDownTime(0L)
                countDownTimer?.cancel()
            }
        }.start()
    }

    private fun back(navigator: Navigator) {
        countDownTimer?.cancel()
        navigator.pop()
    }
}