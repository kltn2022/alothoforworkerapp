package edu.iuhuniversity.alothoforworkerapp

import android.animation.ObjectAnimator
import android.graphics.Path
import android.os.Bundle
import android.view.View
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideInVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.animation.doOnEnd
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.splashscreen.SplashScreenViewProvider
import androidx.core.view.WindowCompat
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.CurrentScreen
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.NavigatorContent
import cafe.adriel.voyager.navigator.OnBackPressed
import cafe.adriel.voyager.navigator.tab.Tab
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.CheckIfUserSignedInUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.LogoutUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SendOtpUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.auth.SignInWithPhoneAuthUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.file.UploadFilesUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.message.ListenForConservationsUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.notification.*
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.order.*
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.payment.CreateMomoPaymentUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.service.GetServicesByTypeUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.transaction.InsertTransactionUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetAuthTokenUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetCurrentUserUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.UpdateUserInfoUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.worker.UpdateWorkerInfoUseCase
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.SnackbarMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.navigation.AloThoWorkerLogo
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.ImageLoadingUtil
import edu.iuhuniversity.alothoforworkerapp.home.ui.HomeUi
import edu.iuhuniversity.alothoforworkerapp.incomingcallworkerrequest.model.mappers.UiCallWorkerRequestMapper
import edu.iuhuniversity.alothoforworkerapp.profile.viewmodels.ProfileViewModel
import edu.iuhuniversity.alothoforworkerapp.signin.ui.SignInScreen
import edu.iuhuniversity.alothoforworkerapp.task.model.mappers.UiOrderMapper
import edu.iuhuniversity.alothoforworkerapp.task.viewmodels.TaskViewModel
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    companion object {
        const val splashFadeDurationMillis = 300
    }

    @Inject
    lateinit var sendOtpUseCase: SendOtpUseCase

    @Inject
    lateinit var signInWithPhoneAuthUseCase: SignInWithPhoneAuthUseCase

    @Inject
    lateinit var getOrdersUseCase: GetOrdersUseCase

    @Inject
    lateinit var updateOrderUseCase: UpdateOrderUseCase

    @Inject
    lateinit var dispatchersProvider: DispatchersProvider

    @Inject
    lateinit var checkIfUserSignedInUseCase: CheckIfUserSignedInUseCase

    @Inject
    lateinit var getAuthTokenUseCase: GetAuthTokenUseCase

    @Inject
    lateinit var imageLoadingUtil: ImageLoadingUtil

//    @Inject
//    lateinit var getWorkerUseCase: GetWorkerUseCase

    @Inject
    lateinit var getOrderByIdUseCase: GetOrderByIdUseCase

    @Inject
    lateinit var orderMapper: UiOrderMapper

    @Inject
    lateinit var orderCallWorkerRequestMapper: UiCallWorkerRequestMapper

    @Inject
    lateinit var getSaveNotificationUseCase: GetNotificationUseCase

    @Inject
    lateinit var listenForConservationsUseCase: ListenForConservationsUseCase

    @Inject
    lateinit var getServicesByTypeUseCase: GetServicesByTypeUseCase

    @Inject
    lateinit var updateUserInfoUseCase: UpdateUserInfoUseCase

    @Inject
    lateinit var getRegistrationTokenUseCase: GetRegistrationTokenUseCase

    @Inject
    lateinit var sendNotificationToCustomer: SendNotificationToCustomerUseCase

    @Inject
    lateinit var logoutUseCase: LogoutUseCase

    @Inject
    lateinit var currentUserUseCase: GetCurrentUserUseCase

    @Inject
    lateinit var uploadFilesUseCase: UploadFilesUseCase

    @Inject
    lateinit var sendRegistrationTokenUseCase: SendRegistrationTokenToServerUseCase

    @Inject
    lateinit var insertTransactionUseCase: InsertTransactionUseCase

    @Inject
    lateinit var updateOrderStatusUseCase: UpdateOrderStatusUseCase

    @Inject
    lateinit var createMomoPaymentUseCase: CreateMomoPaymentUseCase

    @Inject
    lateinit var updateWorkerInfoUseCase: UpdateWorkerInfoUseCase

    @Inject
    lateinit var listenRealtimeOrdersUseCase: ListenRealtimeOrdersUseCase

    @Inject
    lateinit var generateFcmTokenUseCase: GenerateFcmTokenUseCase

    @Inject
    lateinit var listenRealtimeOrderUseCase: ListenRealtimeOrderUseCase

    private var snackBarMessage = mutableStateOf("")
    private var snackBarType = mutableStateOf(MessageType.ERROR)

    private var selectedTab = mutableStateOf<Tab>(HomeUi.HomeTab)

    lateinit var taskViewModel : TaskViewModel
    lateinit var profileViewModel: ProfileViewModel

    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val splashWasDisplayed = savedInstanceState != null
        if (!splashWasDisplayed) {
            val splashScreen = installSplashScreen()

            splashScreen.setOnExitAnimationListener { splashScreenViewProvider ->
                showSplashExitAnimator(splashScreenViewProvider) { StartSplashScreen() }
            }
        } else {
            setTheme(R.style.Theme_AloThoForWorkerApp_MySplashScreen)
            setContent {
                CompositionLocalProvider(
                    LocalOverScrollConfiguration provides null,
                ) {
                    WindowCompat.setDecorFitsSystemWindows(window, false)

                    StartSplashScreen()
                }
            }
        }
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun StartSplashScreen() {
        var visible = remember { mutableStateOf(false) }

        Box(
            modifier = Modifier
                .background(PrimaryVariantColor)
                .fillMaxSize(),
        ) {
            AnimatedVisibility(
                visible = visible.value,
                enter = slideInVertically(
                    initialOffsetY = { -it },
                    animationSpec = tween(
                        durationMillis = splashFadeDurationMillis,
                        easing = LinearEasing
                    ),
                )
            ) {
                AloThoWorkerLogo(
                    modifier = Modifier.size(120.dp),
                )
            }

            Scaffold(
                snackbarHost = {
                    if (snackBarMessage.value.isNotEmpty()) {
                        SnackbarMessage(
                            message = snackBarMessage.value,
                            actionLabel = "Đã hiểu",
                            onActionClickListener = { Timber.i("Đã click action") },
                            snackbarType = snackBarType.value,
                            onDismiss = {
                                snackBarMessage.value = ""
                            },
                            modifier = Modifier.padding(bottom = 40.dp),
                        )
                    }
                },
                modifier = Modifier
                    .fillMaxSize()
                    .navigationBarsPadding()
            ) {
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = isSystemInDarkTheme()

                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = PrimaryColor,
                    )
                }

                if (checkIfUserSignedInUseCase.execute()) {
                    Navigator(screen = HomeUi,
                        onBackPressed = { true })
                } else {
                    Navigator(SignInScreen(), onBackPressed = { true })
                }
            }
        }
        LaunchedEffect(true) {
            visible.value = true
        }
    }

    fun showSnackBar(
        message: String,
        type: MessageType = MessageType.ERROR
    ) {
        snackBarMessage.value = message
        snackBarType.value = type
    }

    @OptIn(ExperimentalFoundationApi::class)
    private fun showSplashExitAnimator(
        splashScreenViewProvider: SplashScreenViewProvider,
        setContentView: @Composable () -> Unit
    ) {
        val scaleOut = ObjectAnimator.ofFloat(
            splashScreenViewProvider.iconView,
            View.SCALE_X,
            View.SCALE_Y,
            Path().apply {
                moveTo(1.0f, 1.0f)
                lineTo(0f, 0f)
            }
        )
        scaleOut.doOnEnd {
            splashScreenViewProvider.remove()
            setContent {
                CompositionLocalProvider(
                    LocalOverScrollConfiguration provides null,
                ) {
                    WindowCompat.setDecorFitsSystemWindows(window, false)
                    setContentView()
                }
            }
        }
        scaleOut.start()
    }

    fun addNavigator(
        screen: Screen,
        autoDispose: Boolean = true,
        onBackPressed: OnBackPressed = { true },
        content: NavigatorContent = { CurrentScreen() }
    ) {
        setContent {
            Navigator(
                screen = screen,
                autoDispose = autoDispose,
                onBackPressed = onBackPressed,
                content = content
            )
        }
    }
}
