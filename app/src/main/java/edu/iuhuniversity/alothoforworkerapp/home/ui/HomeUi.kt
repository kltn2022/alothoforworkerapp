package edu.iuhuniversity.alothoforworkerapp.home.ui

import android.annotation.SuppressLint
import android.app.Application
import android.os.Parcelable
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material.icons.rounded.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.core.lifecycle.LifecycleEffect
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.hilt.getScreenModel
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.Navigator
import cafe.adriel.voyager.navigator.currentOrThrow
import cafe.adriel.voyager.navigator.tab.*
import com.mapbox.geojson.Point
import edu.iuhuniversity.alothoforworkerapp.MainActivity
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.MessageType
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.navigation.BottomNavigationItem
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.home.model.UiMainNavigationItem
import edu.iuhuniversity.alothoforworkerapp.home.ui.offer.OfferDetailScreen
import edu.iuhuniversity.alothoforworkerapp.home.ui.offer.OfferList
import edu.iuhuniversity.alothoforworkerapp.home.viewmodels.HomeEvent
import edu.iuhuniversity.alothoforworkerapp.home.viewmodels.HomeViewModel
import edu.iuhuniversity.alothoforworkerapp.notification.NotificationUi
import edu.iuhuniversity.alothoforworkerapp.profile.PersonalizeScreen
import edu.iuhuniversity.alothoforworkerapp.task.ui.TaskManagementScreen
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.navigation.AloThoWorkerLogo
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.LocationPermissionChecker
import edu.iuhuniversity.alothoforworkerapp.core.utils.navigate
import edu.iuhuniversity.alothoforworkerapp.core.utils.trackCurrentUserLocation
import edu.iuhuniversity.alothoforworkerapp.home.ui.offer.OfferDetailActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

object HomeUi : Screen {
    @SuppressLint("CoroutineCreationDuringComposition")
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun Content() {

        val mainActivity = LocalContext.current.getActivity() as MainActivity

        val generateFcmTokenUseCase = mainActivity.generateFcmTokenUseCase

        TabNavigator(HomeTab) {
            val bottomNavigator = LocalTabNavigator.current
            val selectedTab = remember { mutableStateOf(bottomNavigator.current) }
            val navigator = LocalNavigator.currentOrThrow

            Scaffold(
                bottomBar = {
                    NavigationBar() {
                        BottomNavigationItem(tab = HomeTab) { selectedTab.value = HomeTab }
                        BottomNavigationItem(tab = TaskManagementTab) {
                            selectedTab.value = TaskManagementTab
                        }
                        BottomNavigationItem(tab = NotificationTab) {
                            selectedTab.value = NotificationTab
                        }
                        BottomNavigationItem(tab = PersonalizeTab) {
                            selectedTab.value = PersonalizeTab
                        }
                    }
                },
                topBar = {
                    if (navigator.lastItem == HomeTab) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(48.dp),
                            contentAlignment = Alignment.CenterStart
                        ) {
                            Image(
                                painter = painterResource(R.drawable.ic_bkg_ab_home),
                                contentScale = ContentScale.FillBounds,
                                contentDescription = null,
                                colorFilter = ColorFilter.tint(PrimaryColor, BlendMode.Color),
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(48.dp)
                            )
                            if (selectedTab.value == HomeTab) {
                                SmallTopAppBar(
                                    title = {},
                                    navigationIcon = {
                                        if (selectedTab.value == HomeTab && !navigator.canPop) {
                                            AloThoWorkerLogo(
                                                modifier = Modifier
                                                    .size(36.dp)
                                            )
                                        } else {
                                            Icon(
                                                painter = rememberVectorPainter(image = Icons.Rounded.ArrowBackIosNew),
                                                contentDescription = null,
                                                tint = Color.White
                                            )
                                        }
                                    },
                                    actions = {

                                    },
                                    colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                                        containerColor = Color.Transparent,
                                    ),
                                    modifier = Modifier
                                        .zIndex(4f)
                                )
                            }
                        }
                    }
                },
                modifier = Modifier.statusBarsPadding()
            ) { innerPadding ->
                Box(
                    contentAlignment = Alignment.TopCenter,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                ) {
                    Surface(
                    ) {
                        CurrentTab()
                    }
                }
            }
        }

        // mở app generate FCM Token
        CoroutineScope(Dispatchers.IO).launch {
            generateFcmTokenUseCase.execute()
        }
    }

    object HomeTab : Tab {
        override val options: TabOptions
            @Composable
            get() {
                val title = stringResource(R.string.tab_home)
                val icon = rememberVectorPainter(Icons.Rounded.Home)

                return remember {
                    TabOptions(
                        index = 0u,
                        title = title,
                        icon = icon
                    )
                }
            }

        @OptIn(ExperimentalMaterial3Api::class)
        @Composable
        override fun Content() {
            Navigator(
                screen = HomeContent(),
                onBackPressed = { currentScreen -> currentScreen != HomeContent() })
        }
    }
}

class HomeContent : Screen {
    @Composable
    override fun Content() {
        ConstraintLayout(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
                .fillMaxSize()
        ) {
            // val (headerImgBg, sliderSection, serviceSection, offerSection) = createRefs()
            val (headerImgBg, headerLine, mainNavigation, serviceSection, offerSection) = createRefs()
            val mainActivity = LocalContext.current.getComponentActivity() as MainActivity
            val currentContext = LocalContext.current

            val currentUserUseCase = (currentContext as MainActivity).currentUserUseCase
            val updateUserInfoUseCase = currentContext.updateUserInfoUseCase
            val dispatchersProvider = currentContext.dispatchersProvider

            val updatedPoint = remember { mutableStateOf(Point.fromLngLat(0.0,0.0))}

            val viewModel = getScreenModel<HomeViewModel, HomeViewModel.Factory> {
                it.create(
                    currentContext.applicationContext as Application,
                    currentUserUseCase,
                    updateUserInfoUseCase,
                    dispatchersProvider
                )
            }

            val state by viewModel.state.collectAsState()
            val currentUser = remember { mutableStateOf<Worker?>(null) }

            val (isCurrentUserLoaded, setCurrentUserLoaded) = remember { mutableStateOf(false) }

            val (isLocationPermissionsGranted, setLocationPermissionsGranted) = remember {
                mutableStateOf(
                    false
                )
            }

            viewModel.handleEvents(HomeEvent.Loading)

            when (val result = state) {
                is HomeViewModel.State.CurrentUserProfile -> {
                    setCurrentUserLoaded(true)
                    currentUser.value = result.currentUser

                    if (result.currentUser.isActive == true) {
                        if (!isLocationPermissionsGranted) {
                            LocationPermissionChecker {
                                setLocationPermissionsGranted(true)
                            }
                        } else {
                            Timber.tag("UpdateLocationWorker").d("Đã vào")
                            trackCurrentUserLocation(mainActivity) {
                                // TODO: s
//                                if (it != updatedPoint.value) {
//
//                                }
                                // val point = Point.fromLngLat(106.682282, 10.790424)
                                //viewModel.updateCurrentLocationToServer(result.currentUser.id, point)
                                //updatedPoint.value = it
                            }
                        }
                    }
                }
                is HomeViewModel.State.Loading -> {

                }
                is HomeViewModel.State.Error -> {
                    mainActivity.showSnackBar(
                        result.error, type = MessageType.ERROR
                    )
                }
                is HomeViewModel.State.UpdatedActiveStatus -> {
                    mainActivity.showSnackBar(
                        "Đã cập nhật trạng thái nhận đơn", type = MessageType.SUCCESS
                    )
                }
            }

            LifecycleEffect(
                onStarted = {
                    viewModel.handleEvents(HomeEvent.Loading)
                },
                onDisposed = {},
            )


            val navigationItems = mutableListOf(
                UiMainNavigationItem(
                    title = "Nạp tiền",
                    icon = Icons.Filled.CreditScore,
                    onClick = {

                    }
                ),
                UiMainNavigationItem(
                    title = "Lịch sử việc",
                    icon = Icons.Filled.History,
                    onClick = {

                    }
                ),
                UiMainNavigationItem(
                    title = "Thống kê doanh thu",
                    icon = Icons.Filled.Insights,
                    onClick = {

                    }
                ),
            )

            val fullNavigationItems = navigationItems.apply {
                this.add(
                    UiMainNavigationItem(
                        title = "Mời bạn bè",
                        icon = Icons.Filled.Share,
                        onClick = {

                        }
                    ))
                this.add(
                    UiMainNavigationItem(
                        title = "Cài đặt",
                        icon = Icons.Filled.ManageAccounts,
                        onClick = {

                        }
                    ),
                )
            }

            Image(
                painter = painterResource(R.drawable.img_hearder_view_home),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                colorFilter = ColorFilter.tint(PrimaryColor, BlendMode.Color),
                modifier = Modifier
                    .height(180.dp)
                    .fillMaxWidth()
                    .constrainAs(headerImgBg) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            )

            if (isCurrentUserLoaded) {
                HeaderLine(
                    currentUser = currentUser.value!!,
                    modifier = Modifier
                        .padding(horizontal = 20.dp, vertical = 10.dp)
                        .height(40.dp)
                        .constrainAs(headerLine) {
                            top.linkTo(parent.top, 30.dp)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        },
                    onActiveStatusChanged = {
                        viewModel.handleEvents(
                            HomeEvent.ActiveOfDeActiveRequest(
                                currentUser.value!!.id,
                                it
                            )
                        )
                    }
                )
            }

            MainNavigation(
                modifier = Modifier
                    .padding(horizontal = 20.dp, vertical = 5.dp)
                    .height(120.dp)
                    .constrainAs(mainNavigation) {
                        top.linkTo(headerLine.bottom)
                        start.linkTo(headerLine.start)
                        end.linkTo(headerLine.end)
                    }
            )

            val navigator = LocalNavigator.current
            FullMainNavigation(
                items = fullNavigationItems,
                modifier = Modifier
                    .height(240.dp)
                    .constrainAs(serviceSection) {
                        top.linkTo(headerImgBg.bottom, 40.dp)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    })

            OfferList(
                modifier = Modifier
                    .height(250.dp)
                    .padding(horizontal = 20.dp)
                    .constrainAs(offerSection) {
                        top.linkTo(serviceSection.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
            ) {
                mainActivity.navigate(OfferDetailActivity::class.java, "offer" to it)
            }
        }
    }

}

object TaskManagementTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = stringResource(R.string.tab_task_management)
            val icon = rememberVectorPainter(Icons.Rounded.Checklist)

            return remember {
                TabOptions(
                    index = 1u,
                    title = title,
                    icon = icon
                )
            }
        }


    @Composable
    override fun Content() {
        Navigator(screens = listOf(TaskManagementScreen()))

    }

}

object NotificationTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = stringResource(R.string.tab_notifications)
            val icon = rememberVectorPainter(Icons.Rounded.Notifications)

            return remember {
                TabOptions(
                    index = 2u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        Navigator(screens = listOf(NotificationUi))
    }
}

object PersonalizeTab : Tab {
    override val options: TabOptions
        @Composable
        get() {
            val title = stringResource(R.string.tab_profile)
            val icon = rememberVectorPainter(Icons.Rounded.AccountCircle)

            return remember {
                TabOptions(
                    index = 3u,
                    title = title,
                    icon = icon
                )
            }
        }

    @Composable
    override fun Content() {
        Navigator(screens = listOf(PersonalizeScreen))
    }
}