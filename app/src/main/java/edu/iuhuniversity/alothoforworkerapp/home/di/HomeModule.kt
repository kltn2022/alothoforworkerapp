package edu.iuhuniversity.alothoforworkerapp.home.di

import cafe.adriel.voyager.hilt.ScreenModelFactory
import cafe.adriel.voyager.hilt.ScreenModelFactoryKey
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.multibindings.IntoMap
import edu.iuhuniversity.alothoforworkerapp.home.viewmodels.HomeViewModel

@Module
@InstallIn(ActivityComponent::class)
abstract class HomeModule {
    @Binds
    @IntoMap
    @ScreenModelFactoryKey(HomeViewModel.Factory::class)
    abstract fun bindHomeViewModelFactory(homeViewModelFactory: HomeViewModel.Factory): ScreenModelFactory
}