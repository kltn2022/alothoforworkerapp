package edu.iuhuniversity.alothoforworkerapp.home.ui.offer

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.ArrowBackIosNew
import androidx.compose.material3.Icon
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.zIndex
import androidx.constraintlayout.compose.ConstraintLayout
import cafe.adriel.voyager.androidx.AndroidScreen
import cafe.adriel.voyager.core.screen.uniqueScreenKey
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Offer
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.gradientBackground
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.Lian_Color_Text
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryLightVariantColor
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.SecondaryColor
import edu.iuhuniversity.alothoforworkerapp.core.utils.CommonUtils.getComponentActivity
import edu.iuhuniversity.alothoforworkerapp.core.utils.HtmlText
import edu.iuhuniversity.alothoforworkerapp.core.utils.toDateFormat

data class OfferDetailScreen(val offer: Offer) : AndroidScreen() {

    override val key = uniqueScreenKey

    @Composable
    override fun Content() {
        OfferDetailUi(offer)
    }
}

@Composable
fun OfferDetailUi(offer: Offer) {
    val parentActivity = LocalContext.current.getComponentActivity() as OfferDetailActivity

    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .statusBarsPadding()
            .background(
                Color.White
            )
    ) {
        item {
            ConstraintLayout {
                val (topAppBar, thumbnail, detailSection) = createRefs()

                SmallTopAppBar(
                    title = {

                    },
                    navigationIcon = {
                        androidx.compose.material3.IconButton(onClick = {
                            parentActivity.finishAfterTransition()
                        }) {
                            Icon(
                                painter = rememberVectorPainter(image = Icons.Rounded.ArrowBackIosNew),
                                contentDescription = null,
                                tint = Color.White,
                            )
                        }
                    },
                    actions = {

                    },
                    colors = TopAppBarDefaults.centerAlignedTopAppBarColors(
                        containerColor = Color.Transparent,
                    ),
                    modifier = Modifier
                        .gradientBackground(
                            arrayListOf(
                                colorResource(id = R.color.black_54),
                                Color.Transparent
                            ), 270f
                        )
                        .zIndex(4f)
                        .constrainAs(topAppBar) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                Image(
                    painter = painterResource(id = R.drawable.news_example),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .constrainAs(thumbnail) {
                            top.linkTo(parent.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                )

                Column(
                    modifier = Modifier
                        .background(
                            Color.White,
                            RoundedCornerShape(20.dp, 20.dp, 0.dp, 0.dp)
                        )
                        .constrainAs(detailSection) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(parent.top, 150.dp)
                        }
                        .fillMaxSize()
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.SpaceBetween,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(20.dp)
                    ) {
                        Text(
                            text = "KHUYẾN MÃI",
                            style = AloThoTypography.h6,
                            fontWeight = FontWeight.SemiBold,
                            color = SecondaryColor
                        )

                        Text(
                            text = offer.code.orEmpty(),
                            style = AloThoTypography.subtitle1,
                            fontWeight = FontWeight.SemiBold,
                            color = SecondaryColor,
                            modifier = Modifier
                                .background(
                                    PrimaryLightVariantColor,
                                    RoundedCornerShape(20.dp)
                                )
                                .clip(RoundedCornerShape(20.dp))
                                .clickable {

                                }
                                .padding(vertical = 5.dp, horizontal = 40.dp)
                        )


                    }

                    Text(
                        text = offer.title,
                        style = AloThoTypography.h5,
                        fontWeight = FontWeight.SemiBold,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp)
                    )

                    Text(
                        text = "Ngày đăng: ${offer.createdAt.toDateFormat()}",
                        style = AloThoTypography.subtitle2,
                        fontWeight = FontWeight.Medium,
                        color = Lian_Color_Text,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp, vertical = 5.dp)
                    )

                    HtmlText(
                        html = offer.content,
                        modifier = Modifier
                            .padding(horizontal = 20.dp, vertical = 10.dp)
                            .fillMaxSize()
                    )
                }
            }
        }
    }
}