package edu.iuhuniversity.alothoforworkerapp.home.ui.offer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.gestures.LocalOverScrollConfiguration
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.SideEffect
import androidx.core.view.WindowCompat
import androidx.navigation.ActivityNavigator
import cafe.adriel.voyager.navigator.Navigator
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Offer
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.PrimaryColor

class OfferDetailActivity : ComponentActivity() {
    @OptIn(ExperimentalFoundationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            CompositionLocalProvider(
                LocalOverScrollConfiguration provides null,
            ) {
                WindowCompat.setDecorFitsSystemWindows(window, false)
                val systemUiController = rememberSystemUiController()
                val useDarkIcons = isSystemInDarkTheme()

                SideEffect {
                    systemUiController.setSystemBarsColor(
                        color = PrimaryColor,
                    )
                }

                if (intent?.hasExtra("offer") == true) {
                    val offer = intent.getParcelableExtra<Offer>("offer") as Offer
                    Navigator(screen = OfferDetailScreen(offer))
                } else {
                    DialogMessage(
                        title = "Thông báo lỗi",
                        text = "Có lỗi không mong muốn xảy ra. Vui lòng quay lại!",
                        onDismissRequest = { finish() },
                        positiveButton = ActionButton("Quay lại") {
                            finishAfterTransition()
                        }
                    )
                }
            }
        }
    }
}