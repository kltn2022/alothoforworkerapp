package edu.iuhuniversity.alothoforworkerapp.home.ui

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.ActionButton
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.DialogMessage
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.*
import edu.iuhuniversity.alothoforworkerapp.core.utils.CurrencyUtils
import edu.iuhuniversity.alothoforworkerapp.home.model.UiMainNavigationItem

@Composable
fun HomeUi() {

}

val navigationItems = listOf<UiMainNavigationItem>(
    UiMainNavigationItem(
        title = "Nạp tiền",
        icon = Icons.Filled.CreditScore,
        onClick = {

        }
    ),
    UiMainNavigationItem(
        title = "Lịch sử việc",
        icon = Icons.Filled.History,
        onClick = {

        }
    ),
    UiMainNavigationItem(
        title = "Thống kê doanh thu",
        icon = Icons.Filled.Insights,
        onClick = {

        }
    ),
)

@Preview
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainNavigation(
    modifier: Modifier = Modifier,
    items: List<UiMainNavigationItem> = navigationItems
) {
    Card(
        shape = RoundedCornerShape(20.dp),
        border = BorderStroke(1.dp, SecondaryColor),
        modifier = Modifier
            .then(modifier),
    ) {
        LazyVerticalGrid(
            columns = GridCells.Fixed(items.size), modifier = Modifier
                .background(Color.White),
            verticalArrangement = Arrangement.Center,
            horizontalArrangement = Arrangement.Center
        ) {
            items(items) { item ->
                MainNavigationItem(
                    title = item.title,
                    content = {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = null,
                            tint = SecondaryColor,
                            modifier = Modifier.size(42.dp)
                        )
                    },
                    onItemClicked = item.onClick
                )
            }
        }
    }
}

@Preview
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun FullMainNavigation(
    modifier: Modifier = Modifier,
    items: List<UiMainNavigationItem> = navigationItems
) {
    // Nạp tiền
    // Lịch sử việc
    // Thống kê doanh thu
    Box(
        modifier = Modifier
            .background(Color.White)
            .then(modifier),

        ) {
        LazyVerticalGrid(columns = GridCells.Fixed(3)) {
            items(items) { item ->
                MainNavigationItem(
                    title = item.title,
                    content = {
                        Icon(
                            imageVector = item.icon,
                            contentDescription = null,
                            tint = SecondaryColor,
                            modifier = Modifier.size(42.dp)
                        )
                    },
                    onItemClicked = item.onClick
                )
            }
        }
    }
}

@Composable
fun MainNavigationItem(
    title: String,
    content: @Composable (() -> Unit),
    onItemClicked: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .clickable(
                indication = null,
                interactionSource = remember { MutableInteractionSource() }
            ) { onItemClicked() }
            .padding(10.dp)
    ) {
        Box(
            modifier = Modifier
                .clip(RoundedCornerShape(10.dp))
                .wrapContentSize(Alignment.Center)
        ) {
            content()
        }

        Text(
            text = title,
            modifier = Modifier
                .padding(top = 5.dp),
            style = AloThoTypography.body1,
            fontWeight = FontWeight.SemiBold,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Center,
            color = SecondaryColor
        )
    }

}

@Preview
@Composable
fun PreviewItem() {
    MainNavigationItem(
        title = "Test thử",
        content = { Icon(Icons.Filled.AccountBalance, contentDescription = null) }) {
    }
}

@Composable
fun HeaderLine(
    modifier: Modifier = Modifier,
    currentUser: Worker,
    onActiveStatusChanged: (Boolean) -> Unit
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .then(modifier)
    ) {
        BalanceMoneyLine(
            amount = currentUser.balanceMoney,
            modifier = Modifier.weight(7f)
        )
        ActiveSwitcher(
            isActive = currentUser.isActive,
            onValueChanged = { onActiveStatusChanged(it) },
            modifier = Modifier.weight(3f)
        )
    }
}

@Preview
@Composable
fun BalanceMoneyLine(
    modifier: Modifier = Modifier,
    amount: Double? = 23000.0
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(10.dp, Alignment.Start),
        modifier = Modifier.then(modifier)
    ) {
        Icon(
            Icons.Filled.MonetizationOn,
            contentDescription = null,
            modifier = Modifier.size(24.dp)
        )
        Text(
            text = CurrencyUtils.formatVnCurrency(amount?.toString() ?: "0"),
            modifier = Modifier,
            style = AloThoTypography.h6,
            maxLines = 2,
            textAlign = TextAlign.Center,
        )

    }
}

@Preview
@Composable
fun ActiveSwitcher(
    modifier: Modifier = Modifier,
    isActive: Boolean? = true,
    onValueChanged: (Boolean) -> Unit = { }
) {
    val icon: (@Composable () -> Unit)? = if (isActive == true) {
        {
            Icon(
                imageVector = Icons.Filled.Check,
                contentDescription = null,
                modifier = Modifier.size(16.dp),
            )
        }
    } else {
        null
    }

    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = "Nhận yêu cầu",
            modifier = Modifier,
            style = AloThoTypography.subtitle1,
            fontWeight = FontWeight.SemiBold,
            textAlign = TextAlign.Center,
        )

        val (activeValue, setActiveValue) = remember { mutableStateOf(isActive) }

        if (!activeValue!!) {
            DialogMessage(
                title = "Tắt trạng thái hoạt động",
                text = "Việc tắt trạng thái hoạt động bạn sẽ không nhận được bất kỳ công việc nào trong thời gian tới.",
                onDismissRequest = { /*TODO*/ },
                positiveButton = ActionButton("Tắt") {
                    onValueChanged(activeValue)
                },
                negativeButton = ActionButton("Huỷ") {
                    setActiveValue(true)
                }
            )
        }
        Switch(
            checked = activeValue,
            onCheckedChange = {
                if (it) {
                    onValueChanged(it)
                } else {
                    setActiveValue(it)
                }
            },
            colors = SwitchDefaults.colors(
                checkedThumbColor = PrimaryColor,
                checkedTrackColor = PrimaryLightVariantColor,
                uncheckedTrackColor = PrimaryLighterVariantColor
            ),
        )
    }
}