package edu.iuhuniversity.alothoforworkerapp.home.viewmodels

import android.app.Application
import android.os.Build
import cafe.adriel.voyager.core.model.StateScreenModel
import cafe.adriel.voyager.core.model.coroutineScope
import cafe.adriel.voyager.hilt.ScreenModelFactory
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.mapbox.geojson.Point
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import edu.iuhuniversity.alothoforworkerapp.core.constant.CollectionConst
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Resource
import edu.iuhuniversity.alothoforworkerapp.core.data.model.account.Worker
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.GetCurrentUserUseCase
import edu.iuhuniversity.alothoforworkerapp.core.domain.usecases.user.UpdateUserInfoUseCase
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoFire
import edu.iuhuniversity.alothoforworkerapp.core.geofirestore.GeoLocation
import edu.iuhuniversity.alothoforworkerapp.core.utils.DispatchersProvider
import edu.iuhuniversity.alothoforworkerapp.core.utils.createExceptionHandler
import edu.iuhuniversity.alothoforworkerapp.core.utils.isNetworkIsAvailable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

data class HomeViewModel @AssistedInject constructor(
    @Assisted private val app: Application,
    @Assisted private val currentUserUseCase: GetCurrentUserUseCase,
    @Assisted private val updateUserInfoUseCase: UpdateUserInfoUseCase,
    @Assisted private val dispatchersProvider: DispatchersProvider,
) : StateScreenModel<HomeViewModel.State>(State.Loading) {

    @AssistedFactory
    interface Factory : ScreenModelFactory {
        fun create(
            app: Application,
            currentUserUseCase: GetCurrentUserUseCase,
            updateUserInfoUseCase: UpdateUserInfoUseCase,
            dispatchersProvider: DispatchersProvider
        ): HomeViewModel
    }

    sealed class State {
        object Loading : State()
        data class Error(val error: String) : State()
        data class CurrentUserProfile(val currentUser: Worker) : State()
        object UpdatedActiveStatus : State()
    }

    fun handleEvents(event: HomeEvent) {
        when (event) {
            is HomeEvent.Loading -> {
                displayProfile()
            }
            is HomeEvent.ActiveOfDeActiveRequest -> {
                doActiveOfDeActiveReceiveRequest(event.workerUid, event.value)
            }
        }
    }

    private fun doActiveOfDeActiveReceiveRequest(uid: String, value: Boolean) {
        val errorMessage = "Có lỗi xảy ra"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }
        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    val dataToUpdate = mutableMapOf<String, Any>()
                    dataToUpdate["isActive"] = value
                    withContext(dispatchersProvider.io()) {
                        when (val result = updateUserInfoUseCase.execute(
                            uid,
                            dataToUpdate
                        )) {
                            is Resource.Loading -> mutableState.value =
                                State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> mutableState.value = State.UpdatedActiveStatus
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }
    }

    private fun displayProfile() {
        val errorMessage = "Có lỗi xảy ra"
        val exceptionHandler = coroutineScope.createExceptionHandler(errorMessage) {
            onFailure(it)
        }

        coroutineScope.launch(exceptionHandler) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isNetworkIsAvailable(app)) {
                    withContext(dispatchersProvider.io()) {
                        when (val result = currentUserUseCase.execute()) {
                            is Resource.Loading -> mutableState.value = State.Loading
                            is Resource.Error -> mutableState.value =
                                State.Error(result.message.orEmpty())
                            else -> mutableState.value = State.CurrentUserProfile(result?.data!!)
                        }
                    }
                } else {
                    mutableState.value =
                        State.Error("Không thể kết nối Internet. Thử lại!")
                }
            }
        }


    }

    private fun onFailure(throwable: Throwable) {
        mutableState.value = State.Error(throwable.message.orEmpty())
    }

    private val workerLocationRef: CollectionReference =
        FirebaseFirestore.getInstance().collection(CollectionConst.COLLECTION_WORKER_GEOLOCATION)
    private val workerGeoFire = GeoFire(workerLocationRef)

    fun updateCurrentLocationToServer(workerUid: String, point: Point) {
        coroutineScope.launch {
            workerGeoFire.setLocation(workerUid,
                GeoLocation(point.latitude(), point.longitude()),
                object : GeoFire.CompletionListener {
                    override fun onComplete(key: String?, exception: Exception?) {
                        if (exception != null) {
                            Timber.tag(
                                "UpdateCurrentLocation"
                            ).d(
                                "There was an error saving the location to Geofire: $exception"
                            );
                        } else {
                            Timber.tag(
                                "UpdateCurrentLocation"
                            ).d(
                                "Location saved on server successfully!"
                            );
                        }
                    }
                })
        }

    }
}

sealed class HomeEvent {
    object Loading : HomeEvent()
    data class ActiveOfDeActiveRequest(val workerUid: String, val value: Boolean) : HomeEvent()
}