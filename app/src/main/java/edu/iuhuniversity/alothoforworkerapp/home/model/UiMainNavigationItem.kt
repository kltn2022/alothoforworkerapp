package edu.iuhuniversity.alothoforworkerapp.home.model

import androidx.compose.ui.graphics.vector.ImageVector

data class UiMainNavigationItem(
    val title: String,
    val icon: ImageVector,
    val onClick: () -> Unit
)