package edu.iuhuniversity.alothoforworkerapp.home.ui.offer

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.gradientBackground
import edu.iuhuniversity.alothoforworkerapp.R
import edu.iuhuniversity.alothoforworkerapp.core.data.model.Offer
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.carousel.DemoDataProvider
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.LazyList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.components.list.OrientationList
import edu.iuhuniversity.alothoforworkerapp.core.presentation.ui.theme.AloThoTypography

@Composable
fun OfferList(
    modifier: Modifier = Modifier,
    selectedItem: (Offer) -> Unit
) {
    Column(
        modifier = Modifier
            .padding(
                vertical = 10.dp
            )
            .then(modifier),
        verticalArrangement = Arrangement.spacedBy(10.dp)
    ) {
        Text(
            text = "Các ưu đãi",
            fontWeight = FontWeight.Bold,
            style = AloThoTypography.h6,
            modifier = Modifier
                .padding(start = 5.dp)
        )
        LazyList(
            items = DemoDataProvider.defaultOfferList,
            orientation = OrientationList.HORIZONTAL,
            itemContent = {
                OfferItem(
                    item = it,
                    selectedItem = selectedItem
                )
            },
            spaceBetweenItems = 10.dp
        )
    }
}

@Composable
fun OfferItem(
    item: Offer,
    selectedItem: (Offer) -> Unit
) {
    Box(
        modifier = Modifier
            .size(
                width = 280.dp,
                height = 150.dp,
            )
            .clip(
                shape = RoundedCornerShape(
                    corner = CornerSize(20.dp)
                )
            )
            .background(
                color = colorResource(R.color.colorBackground)
            )
            .clickable { selectedItem(item) }
    ) {
        Image(
            painter = painterResource(id = R.drawable.offer_example),
            contentDescription = "thumbnail",
            modifier = Modifier
                .fillMaxSize()
                .clip(RoundedCornerShape(20.dp)),
            contentScale = ContentScale.Crop
        )
        Surface(
            modifier = Modifier
                .clip(
                    RoundedCornerShape(
                        corner = CornerSize(20.dp)
                    )
                )
                .gradientBackground(
                    colors = listOf(
                        Color.Transparent,
                        Color.Transparent,
                        Color.Black.copy(alpha = 1f)
                    ),
                    angle = 270f
                )
                .fillMaxSize(),
            color = Color.Transparent
        ) {}
        Image(
            painter = painterResource(id = R.drawable.ic_logo_ab),
            contentDescription = "logo",
            alignment = Alignment.TopEnd,
            modifier = Modifier
                .padding(
                    horizontal = 10.dp,
                    vertical = 10.dp
                )
                .fillMaxWidth()
                .size(20.dp)
        )
        Column(
            verticalArrangement = Arrangement.Bottom,
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth()
                .padding(
                    horizontal = 10.dp,
                    vertical = 15.dp
                )
        ) {
            Text(
                text = item.title,
                color = Color.White,
                fontSize = 16.sp,
                style = MaterialTheme.typography.h6,
                fontWeight = FontWeight.Bold
            )
            if (item.subtitle.isNullOrEmpty().not()) {
                Text(
                    text = item.subtitle.orEmpty(),
                    color = Color.White,
                    fontSize = 11.sp,
                    style = MaterialTheme.typography.body1
                )
            }

        }
    }
}